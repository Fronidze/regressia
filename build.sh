#!/usr/bin/env sh

echo "
89.111.132.36 regression.ru
89.111.132.36 education.io
" >> /etc/hosts

composer install &&
  php init --env=Development --force &&
  php yii asset ./frontend/assets/main.php ./frontend/config/assets.php &&
  php yii migrate --interactive=0 &&
  php yii admin/create 'admin@admin.ru' 'admin' 'Администратор' &&
  chmod -R 775 /app/upload &&
  chown -R www-data:www-data /app/upload
