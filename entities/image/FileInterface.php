<?php

namespace entities\image;

use entities\storage\StorageInterface;

interface FileInterface
{
    public function makeStorage(): StorageInterface;
}