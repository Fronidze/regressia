<?php


namespace entities\image;


use entities\storage\GalleryStorage;
use entities\storage\StorageInterface;

class PersonalFile implements FileInterface
{
    public function makeStorage(): StorageInterface
    {
        return new GalleryStorage('upload', 'images/personal');
    }
}