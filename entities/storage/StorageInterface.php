<?php

namespace entities\storage;

interface StorageInterface
{
    public function getFullPath(): string;
    public function getPath(): string;
}