<?php


namespace entities\storage;


use yii\helpers\FileHelper;

class GalleryStorage implements StorageInterface
{
    /** @var string */
    protected $folder;
    /** @var string */
    protected $relative_path;
    /** @var string */
    protected $path_template = '{folder}/{relative_path}';

    public function __construct($folder = null, $relative_path = null)
    {
        $this->folder = $folder ?? 'upload';
        $this->relative_path = $relative_path ?? '';
    }

    public function getPath(): string
    {
        return \Yii::t('app', "/{$this->path_template}/", [
            'folder' => $this->folder,
            'relative_path' => $this->relative_path,
        ]);
    }

    public function getFullPath(): string
    {
        $path = \Yii::t('app', $this->path_template, [
            'folder' => \Yii::getAlias("@{$this->folder}"),
            'relative_path' => $this->relative_path,
        ]);

        FileHelper::createDirectory($path);
        return $path;
    }

}