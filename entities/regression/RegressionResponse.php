<?php

namespace entities\regression;

use yii\helpers\ArrayHelper;
use yii\httpclient\Response;

class RegressionResponse
{
    /** @var Response  */
    protected $response;
    /** @var null|array */
    public $content;
    /** @var string */
    public $message;
    /** @var bool */
    public $error;

    public function __construct(Response $response = null)
    {
        if ($response === null) {
            $this->content = null;
            $this->message = 'Передан пустой обьект ответа.';
            $this->error = true;
        } else {
            $this->response = $response;
            $this->data();
            $this->prepareResponse();
        }
    }

    public function isSuccess(): bool
    {
        return $this->error === false;
    }

    protected function prepareResponse()
    {
        switch ($this->statusCode()) {
            case 200: $this->success(); break;
            case 400: $this->error(); break;
            default: $this->serverError();
        }
    }

    protected function statusCode()
    {
        return (integer)$this->response->statusCode;
    }

    protected function data(): array
    {
        return $this->response->data;
    }

    protected function success()
    {
        $this->content = ArrayHelper::getValue($this->data(), 'data');
        $this->error = false;
        $this->message = 'Запрос выполнен успешно';
    }

    protected function error()
    {
        $this->error = true;
        $this->message = ArrayHelper::getValue($this->data(), 'data.message');
    }

    protected function serverError()
    {
        $this->error = true;
        $this->message = 'Возникли ошибки обработки запроса на сервере';
    }

    public function modifyContent()
    {
        $this->content = $this->modifyData($this->content);
    }

    protected function modifyData($storage)
    {
        $storage['adj_r_squared'] = $this->roundFloatValue($storage['adj_r_squared']);
        $storage['deviation'] = $this->roundFloatValue($storage['deviation']);
        $storage['r_squared'] = $this->roundFloatValue($storage['r_squared']);

        $storage['f-stat-values'] = $this->roundArrayValue($storage['f-stat-values']);
        $storage['predicted_y'] = $this->roundArrayValue($storage['predicted_y']);
        foreach ($storage['correlation']['table'] as $key => &$data) {
            $data = $this->roundArrayValue($data);
        }

        foreach ($storage['result-table'] as $key => &$data) {
            $data = $this->roundArrayValue($data);
        }

        foreach ($storage['result-table']['estimates'] as $key => &$data) {
            $data = abs($data);
        }

        foreach ($storage['correlation']['correlated_params'] as $key => &$data) {
            $data[2] = $this->roundFloatValue($data[2]);
        }

        return $storage;
    }

    protected function roundArrayValue(array $data, int $precision = 3)
    {
        array_walk($data, function (&$value) use ($precision) {
            $value = round($value, $precision);
        });

        return $data;
    }

    protected function roundFloatValue(float $value, int $precision = 3)
    {
        return round($value, $precision);
    }


}