<?php

namespace frontend\jobs;

use common\models\ExamPreparation;
use services\schedule\MailerService;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class MailJobSchoolchild extends BaseObject implements JobInterface
{
    public $exam_preparation_id;
    public $schoolchild_name;
    public $schoolchild_email;

    public function execute($queue)
    {
        $exam_preparation = ExamPreparation::find()
            ->where(['and', ['=', 'id', $this->exam_preparation_id]])
            ->one();

        MailerService::instance()->courseRegistrationNoticeExamPreparation($exam_preparation, $this->schoolchild_name, $this->schoolchild_email);
    }
}