<?php

namespace frontend\jobs;

use common\models\Schedule;
use frontend\assets\AppAsset;
use services\schedule\MailerService;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class MailJob extends BaseObject implements JobInterface
{
    public $schedule_id;
    public $student_name;
    public $student_email;

    public function execute($queue)
    {
        $schedule = Schedule::find()
            ->with(['technology'])
            ->where(['and', ['=', 'id', $this->schedule_id], ['=', 'status', Schedule::STATUS_ACTIVE]])
            ->one();

        MailerService::instance()->courseRegistrationNotice($schedule, $this->student_name, $this->student_email);
    }
}