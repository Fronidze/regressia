<?php

namespace frontend\controllers;

use common\models\Group;
use Mobile_Detect;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Site controller
 */
class GroupController extends Controller
{
    public $layout = '//education';

    public function actionIndex(string $code)
    {
        $detect = new Mobile_Detect;
        $group = Group::find()
            ->where(['=', 'code', $code])
            ->one();

        Url::remember();
        return $this->render('index', ['group' => $group, 'detect' => $detect]);
    }
}
