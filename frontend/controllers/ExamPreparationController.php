<?php

namespace frontend\controllers;

use common\models\ExamPreparation;
use frontend\forms\CourseRegisterExamPreparation;
use services\exam\RecordSchoolchildService;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class ExamPreparationController extends Controller
{
    public $layout = '//education';

    public function actionIndex(string $code)
    {
        $exam_preparation = ExamPreparation::find()
            ->where([
                'and',
                ['=', 'code', $code],
                ['=', 'publish', ExamPreparation::PUBLISH_OPEN]
            ])
            ->one();
        if ($exam_preparation === null) {
            throw new NotFoundHttpException();
        }

        return $this->render('index', ['exam_preparation' => $exam_preparation]);
    }

    public function actionRegister(int $id)
    {
        $register = new CourseRegisterExamPreparation();
        $register->exam_preparation_id = $id;
        $send = false;

        if ($register->load(Yii::$app->request->post()) && $register->validate()) {
            RecordSchoolchildService::instance()->createFromFrontend($register);
            $send = true;
        }

        return $this->renderAjax('register', [
            'register'         => $register,
            'send'             => $send,
            'exam_preparation' => $register->getExamPreparation(),
        ]);
    }
}
