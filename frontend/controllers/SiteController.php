<?php

namespace frontend\controllers;

use common\models\abstractions\PublishActiveRecord;
use common\models\ExamPreparation;
use common\models\Gallery;
use common\models\Group;
use common\models\Question;
use common\models\Schedule;
use common\models\Teacher;
use frontend\forms\CourseSignUp;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ErrorAction;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => ['class' => ErrorAction::class],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $schedules = Schedule::find()
            ->where(['and', ['=', 'publish', Schedule::PUBLISH_OPEN], ['=', 'status', Schedule::STATUS_ACTIVE]])
            ->orderBy(['sorting' => SORT_ASC])
            ->all();

        $exam_preparations = ExamPreparation::find()
            ->where(['and', ['=', 'publish', Schedule::PUBLISH_OPEN]])
            ->all();

        $groups = Group::find()
            ->where(['=', 'publish', Group::PUBLISH_YES])
            ->orderBy(['sorting' => SORT_ASC])
            ->all();

        $teachers = Teacher::find()
            ->where(['and',
                ['=', 'is_publish', PublishActiveRecord::PUBLISH_YES],
                ['is not', 'image_id', null],
            ])
            ->orderBy(['sorting' => SORT_ASC])
            ->all();

        $questions = Question::find()
            ->where(['and',
                ['=', 'is_publish', PublishActiveRecord::PUBLISH_YES],
            ])
            ->orderBy(['sorting' => SORT_ASC])
            ->all();

        $images = Gallery::find()
            ->where(['and',
                ['=', 'publish', PublishActiveRecord::PUBLISH_YES],
                ['is not', 'image_id', null],
            ])
            ->orderBy(['sorting' => SORT_ASC])
            ->all();

        Url::remember();
        return $this->render('index', [
            'schedules'         => $schedules,
            'exam_preparations' => $exam_preparations,
            'groups'            => $groups,
            'teachers'          => $teachers,
            'sign_up'           => new CourseSignUp(),
            'images'            => $images,
            'questions'         => $questions
        ]);
    }

    public function actionPolicy()
    {
        return $this->render('policy');
    }
}
