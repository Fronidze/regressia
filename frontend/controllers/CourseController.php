<?php
namespace frontend\controllers;

use frontend\forms\CourseSignUp;
use services\course\CourseRequestService;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class CourseController extends Controller
{
    public function actionSignUp()
    {
        $request = \Yii::$app->request;
        if ($request->getIsPjax() === false) {
            throw new BadRequestHttpException();
        }

        $sign_up = new CourseSignUp();
        $service = new CourseRequestService();
        if ($sign_up->load($request->post()) && $sign_up->validate()) {
            $service->create($sign_up);
        }

        return $this->render('success');
    }
}
