<?php

namespace frontend\controllers;

use common\models\Schedule;
use frontend\forms\CourseRegister;
use services\schedule\RecordService;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class EducationController extends Controller
{
    public $layout = '//education';

    public function actionIndex(string $code)
    {
        $schedule = Schedule::find()
            ->with(['technology'])
            ->where([
                'and',
                ['=', 'code', $code],
                ['=', 'status', Schedule::STATUS_ACTIVE]
            ])
            ->one();
        if ($schedule === null) {
            throw new NotFoundHttpException();
        }

        return $this->render('index', ['schedule' => $schedule]);
    }

    public function actionRegister(int $id)
    {
        $register = new CourseRegister();
        $register->schedule_id = $id;
        $send = false;

        if ($register->load(Yii::$app->request->post()) && $register->validate()) {
            RecordService::instance()->createFromFrontend($register);
            $send = true;
        }

        return $this->renderAjax('register', [
            'register' => $register,
            'send' => $send,
            'schedule' => $register->getSchedule(),
        ]);
    }
}
