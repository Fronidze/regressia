<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class YandexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];

    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU&coordorder=latlong',
        'js/yandex_map.js',
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];
}
