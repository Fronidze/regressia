<?php

Yii::setAlias('@webroot', __DIR__ . '/../web');
Yii::setAlias('@web', '/');

return [
    'jsCompressor' => 'java -jar compiler.jar --js {from} --js_output_file {to}',
    'cssCompressor' => 'java -jar yuicompressor.jar --type css {from} -o {to}',
    'deleteSource' => false,

    'bundles' => [
        'frontend\assets\AppAsset',
        'yii\validators\ValidationAsset',
        'yii\widgets\ActiveFormAsset',
        'yii\widgets\PjaxAsset',
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ],

    'targets' => [
        'all' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/build',
            'baseUrl' => '@web/build',
            'js' => 'script.js',
            'css' => 'style.css',
        ],
    ],

    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets',
    ],
];