<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class CdnAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap',
    ];

    public $js = [
        'https://mc.yandex.ru/metrika/tag.js',
    ];
    public $depends = [];
}
