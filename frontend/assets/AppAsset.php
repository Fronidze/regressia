<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/plugins/fancybox/jquery.fancybox.css',
        'css/swiper.min.css',
        'css/plugins/select2/select2.min.css',
        'font-awesome/css/font-awesome.css',
        'css/animate.css',
        'css/site.css'
    ];

    public $js = [
        'js/plugins/fancybox/jquery.fancybox.js',
        'js/plugins/select2/select2.full.min.js',
        'js/swiper.min.js',
        'js/plugins/jquery.bpopup.min.js',
        'js/jquery.animateNumber.js',
        'js/bluebird.min.js',
        'js/metrica.js',
        'js/main.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
