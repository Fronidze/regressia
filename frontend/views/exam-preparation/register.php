<?php

use common\models\ExamPreparation;
use frontend\forms\CourseRegisterExamPreparation;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var $this View
 * @var $register CourseRegisterExamPreparation
 * @var $send boolean
 * @var $exam_preparation ExamPreparation
 */

$icon = Html::tag('i', null, ['class' => ['fa', 'fa-times']]);
echo Html::a($icon, 'javascript: void(0);', ['class' => ['close_modal']]);
?>

<div class="wrapper_form">
    <?php
    Pjax::begin(['id' => 'register_form_pjax', 'enablePushState' => false]);
    echo Html::tag('h2', 'Регистрация на курс «' . $exam_preparation->sub_title . '»');
    if ($send === true) {
        echo $this->render('result', ['exam_preparation' => $exam_preparation]);
    } else {
        echo $this->render('blocks/_form', ['register' => $register]);
    }
    Pjax::end();
    ?>
</div>

