<?php

use backend\models\User;
use common\models\ExamPreparation;
use common\models\Files;
use common\models\TemplateExamPreparation;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $exam_preparation ExamPreparation
 * @var $template TemplateExamPreparation
 */

$this->title = 'Академия разработки MediaSoft: ' . $exam_preparation->title;
$template = $exam_preparation->getTemplateExam()->one();

?>
<span id="arrow_back" class="toggle-arrow">
    <?php echo Html::a('<i class="arrow-svg"></i>', Url::previous()); ?>
</span>

<section class="education_title">
    <div class="container">
        <div class="logo">
            <a href="/">
                <img src="/images/logo_academy_rgb.png" alt="">
            </a>
        </div>
        <div class="description">
            <div class="name_course"><?= $exam_preparation->title; ?></div>
            <div class="price_course">Обучение бесплатное</div>
            <div class="date_begin_course"><?= $exam_preparation->empty_date === ExamPreparation::NOT_KNOW_DATE ? $exam_preparation->status : $exam_preparation->getActiveStatusLabel(); ?></div>
            <?php if ($exam_preparation->isFinished() === false): ?>
                <?php if (ArrayHelper::getValue($exam_preparation, 'empty_date') !== ExamPreparation::NOT_KNOW_DATE): ?>
                    <div class="schedule_course">Занятия проходят <?= $exam_preparation->getWeeklyExamPreparationForPage() ?></div>
                <?php endif; ?>
                <?php if ($exam_preparation->canRegister()): ?>
                    <div class="action_course">
                        <?php
                        echo Html::a('Зарегистрироваться', 'javascript: void(0)', ['data' => ['popup' => 'register', 'request' => Url::to(['exam-preparation/register', 'id' => $exam_preparation->id])]]);
                        ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <div class="address_course"><b>MediaSoft</b> / Ульяновск, ул. К.Маркса,13А, корп.3 <span>(ТЦ Амарант, 3 этаж) /</span>
            </div>
        </div>
    </div>
</section>

<section class="education_about">
    <div class="about_wrapper flex">
        <div class="light_section">
            <div class="wrapper_section">
                <div class="title_section">Для кого</div>
                <div class="content_section_wrapper"><?= $template->left_description ?></div>
            </div>
        </div>
        <div class="dark_section">
            <div class="wrapper_section">
                <div class="title_section">Что дает курс</div>
                <div class="content_section_wrapper"><?= $template->right_description ?></div>
            </div>
        </div>
    </div>
</section>
<section class="education_program">
    <div class="container">
        <div class="title_section">О курсе</div>
        <div class="wrapper_content">
            <?= $template->about_course ?>
        </div>
    </div>
</section>
<section class="education_program">
    <div class="container">
        <div class="title_section">Программа</div>
        <div class="wrapper_content">
            <?= $template->program ?>
        </div>
    </div>
</section>

<section class="education_record">
    <div class="record_wrapper flex">
        <div class="light_section">
            <div class="wrapper_section">
                <div class="title_section">Информация</div>
                <div class="content_section_wrapper">
                    <ul>
                        <li><b>Участие бесплатно</b></li>
                        <li>Требуется предварительная
                            <?php
                            if ($exam_preparation->canRegister()) :
                                echo Html::a('регистрация', 'javascript: void(0)', ['data' => ['popup' => 'register', 'request' => Url::to(['exam-preparation/register', 'id' => $exam_preparation->id])]]);
                            else :
                                echo 'регистрация';
                            endif;
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="dark_section">
            <div class="wrapper_section">
                <div class="title_section">Записаться</div>
                <div class="content_section_wrapper">
                    <div class="record_block_wrapper">
                        <div class="person flex">
                            <?php
                            /** @var User $person */
                            $person = ArrayHelper::getValue($exam_preparation, 'responsible');
                            ?>
                            <div class="image">
                                <a href="<?= $person->social; ?>">
                                    <?php
                                    $path = null;
                                    /** @var Files $file */
                                    if ($file = ArrayHelper::getValue($person, 'imageFile')) {
                                        $path = $file->makeResize(50, 50);
                                    }
                                    ?>
                                    <img src="<?= $path ?>" alt="">
                                </a>
                            </div>
                            <div class="name">
                                <p>
                                    <a href="<?= $person->social ?>"><?= $person->username ?></a>
                                </p>
                                <p><?= $person->position ?> </p>
                            </div>
                        </div>
                        <div class="phone"><a href="tel:+78007750679"> <i class="fa fa-phone"></i>+7 (800) 775-06-79</a>
                        </div>
                        <div class="email"><a href="mailto:study@mediasoft.team"> <i class="fa fa-envelope"></i>study@mediasoft.team</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>