<?php

use common\models\ExamPreparation;
use frontend\forms\CourseRegisterExamPreparation;
use yii\web\View;

/**
 * @var $this View
 * @var $register CourseRegisterExamPreparation
 * @var $exam_preparation ExamPreparation
 */

?>

<div class="result">
    <h2>прошла успешно.</h2>
    <?php if ($exam_preparation->empty_date !== ExamPreparation::NOT_KNOW_DATE): ?>
        <?php if (date("Y-m-d h:i:s") < $exam_preparation->date_start): ?>
            <h2>Ждем вас <?php echo \Yii::$app->formatter->asDate($exam_preparation->date_start, 'long'); ?></h2>
        <?php else : ?>
            <h2>Ждем вас <?php echo $exam_preparation->getWeeklyExamPreparationForPage() . "."; ?></h2>
        <?php endif; ?>
    <?php endif; ?>
    <a href="javascript: void(0);" class="close_modal">Закрыть</a>
</div>
