<?php

use yii\web\View;

/**
 * @var $this View
 */
?>

<meta property="og:site_name" content="Академия разработки MediaSoft">
<meta property="og:title" content="Академия разработки MediaSoft">
<meta property="og:description" content="Помогаем преодолеть пропасть между знаниями и реальной работой">
<meta property="og:image" content="https://academy.mediasoft.team/images/open_graph.png">
<meta property="vk:image" content="https://academy.mediasoft.team/images/open_graph.png">