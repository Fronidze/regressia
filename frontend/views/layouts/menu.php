<?php


use common\models\abstractions\PublishActiveRecord;
use common\models\Question;
use yii\web\View;
use yii\db\Query;


$query = (new Query())
    ->from(Question::tableName())
    ->where(['=', 'is_publish', PublishActiveRecord::PUBLISH_YES]);

/**
 * @var $this View
 */

?>

<span id="menu" class="toggle-button">
     <div class="menu-bar menu-bar-top"></div>
     <div class="menu-bar menu-bar-middle"></div>
     <div class="menu-bar menu-bar-bottom"></div>
</span>

<div id="content_menu" class="menu-wrap">
    <div class="menu-sidebar">
        <nav class="links">
            <ul>
                <li><a href="/#courses">Наши курсы</a></li>
                <li><a href="/#pre-registration">Зарегистрироваться</a></li>
                <li><a href="/#teachers">Преподаватели</a></li>
                <?php if ($query->exists() === true): ?>
                    <li><a href="/#faq">FAQ</a></li>
                <?php endif; ?>
                <li><a href="/#contacts">Контакты</a></li>
            </ul>
        </nav>
        <nav class="social">
            <p class="value_cell social_links">
                <a target="_blank" href="https://vk.com/mediasoft.academy"><i class="fa fa-vk"></i></a>
                <a target="_blank" href="https://www.facebook.com/msoft.team/"><i class="fa fa-facebook"></i></a>
                <a target="_blank" href="https://www.instagram.com/mediasoft.team/"><i class="fa fa-instagram"></i></a>
                <a target="_blank" href="https://t.me/mediasoft_academy"><i class="fa fa-telegram"></i></a>
            </p>
        </nav>
    </div>
</div>
