<?php

use yii\web\View;

/**
 * @var $this View
 */

?>

<footer>
    <div class="wrapper_footer">
        <a href="https://mediasoft.team/" target="_blank">© mediasoft.team | <?= (new DateTime())->format('Y');?></a>
    </div>
</footer>
