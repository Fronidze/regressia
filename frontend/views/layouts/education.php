<?php

/**
 * @var $this View
 * @var $content string
 */

use frontend\assets\CdnAsset;
use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\web\View;

CdnAsset::register($this);
AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $this->render('open_graph'); ?>
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php
$this->beginBody();
echo $content;
echo $this->render('footer');
echo $this->render('popup');
echo $this->render('counters');
$this->endBody();
?>
</body>
</html>
<?php $this->endPage() ?>
