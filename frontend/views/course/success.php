<?php

use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var $this View
 */

?>

<?php Pjax::begin(['id' => 'sign_up']);?>
<div class="message_block">
    <div class="container">
        <div class="message">Спасибо! Данные успешно отправлены.</div>
    </div>
</div>
<?php Pjax::end();?>
