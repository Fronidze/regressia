<?php

use backend\models\User;
use common\models\Files;
use common\models\Group;
use common\models\links\GroupSchedule;
use common\models\Schedule;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $group Group
 */

$this->title = 'Академия разработки MediaSoft:';

?>
<span id="arrow_back" class="toggle-arrow">
    <?php echo Html::a('<i class="arrow-svg"></i>', Url::home()); ?>
</span>

<section class="education_title">
    <div class="container">
        <div class="logo">
            <a href="/"><?= Html::img('/images/logo_academy_rgb.png') ?></a>
        </div>
        <div class="description">
            <div class="name_course"><?= $group->full_title; ?></div>
            <div class="price_course">Обучение бесплатное</div>
            <div class="date_begin_course"><?= $group->generateBeginLabel(); ?></div>
            <div class="address_course"><b>MediaSoft</b> / Ульяновск, ул. К.Маркса,13А, корп.3 (ТЦ Амарант, 3 этаж) /
            </div>
        </div>
    </div>
</section>

<section class="education_about group_block">
    <div class="about_wrapper flex">
        <div class="light_section">
            <div class="wrapper_section">
                <div class="content_section_wrapper">
                    <?= $group->left_side ?>
                </div>
            </div>
        </div>
        <div class="dark_section">
            <div class="wrapper_section">
                <div class="content_section_wrapper">
                    <?= $group->right_side ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="main_page_courses schedule_list">
    <div class="container">
        <div class="section_title">Направления</div>
        <div class="course_wrapper flex">
            <?php
            /** @var GroupSchedule[] $schedules */
            $schedules = ArrayHelper::getValue($group, 'schedules');
            foreach ($schedules as $links) :
                /** @var Schedule $schedule */
                $schedule = ArrayHelper::getValue($links, 'schedule');
                ?>
                <div class="course">
                    <a href="<?php echo Url::to(['education/index', 'code' => $schedule->code]) ?>">
                        <div class="course_info flex flex-column">
                            <p class="course_title"><?= $schedule->getTechnologyTitle() ?></p>
                            <p class="course_status"><?= $schedule->getActiveStatusLabel() ?></p>
                        </div>
                        <div class="course_action flex flex-column">
                            <p class="link">Подробнее о курсе <i class="fa fa-angle-double-right"></i></p>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<section class="schedule">
    <div class="container">
        <div class="section_title">Расписание</div>
        <div class="schedule_table">
            <?php
            if ($detect->isMobile() && !$detect->isTablet() && !empty($group->table_mobile)) {
                echo $group->table_mobile;
            } else {
                echo $group->table;
            }
            ?>
        </div>
    </div>
</section>

<section class="education_record">
    <div class="record_wrapper flex">
        <div class="light_section">
            <div class="wrapper_section">
                <div class="title_section">Информация</div>
                <div class="content_section_wrapper">
                    <ul>
                        <li><b>Участие бесплатно</b></li>
                        <li>Требуется предварительная регистрация</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="dark_section">
            <div class="wrapper_section">
                <div class="title_section">Записаться</div>
                <div class="content_section_wrapper">
                    <div class="record_block_wrapper">
                        <div class="person flex">
                            <?php
                            /** @var User $person */
                            $person = ArrayHelper::getValue($group, 'person');
                            ?>
                            <div class="image">
                                <a href="<?= $person->social; ?>">
                                    <?php
                                    $path = null;
                                    /** @var Files $file */
                                    if ($file = ArrayHelper::getValue($person, 'imageFile')) {
                                        $path = $file->makeResize(50, 50);
                                    }
                                    ?>
                                    <img src="<?= $path ?>" alt="">
                                </a>
                            </div>
                            <div class="name">
                                <p>
                                    <a href="<?= $person->social ?>"><?= $person->username ?></a>
                                </p>
                                <p><?= $person->position ?> </p>
                            </div>
                        </div>
                        <div class="phone"><a href="tel:+78007750679"> <i class="fa fa-phone"></i>+7 (800) 775-06-79</a>
                        </div>
                        <div class="email"><a href="mailto:study@mediasoft.team"> <i class="fa fa-envelope"></i>study@mediasoft.team</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>