<?php

use common\models\Schedule;
use frontend\forms\CourseRegister;
use yii\web\View;

/**
 * @var $this View
 * @var $register CourseRegister
 * @var $schedule Schedule
 */

?>

<div class="result">
    <h2>прошла успешно.</h2>

    <?php
    if (date("Y-m-d h:i:s") < $schedule->date_start):?>
        <h2>Ждем вас <?php echo \Yii::$app->formatter->asDate($schedule->date_start, 'long'); ?></h2>
    <?php else : ?>
        <h2>Ждем вас <?php echo $schedule->getWeeklyScheduleForPage() . "."; ?></h2>
    <?php endif; ?>
    <a href="javascript: void(0);" class="close_modal">Закрыть</a>
</div>
