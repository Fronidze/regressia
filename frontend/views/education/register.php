<?php

use common\models\Schedule;
use frontend\forms\CourseRegister;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var $this View
 * @var $register CourseRegister
 * @var $send boolean
 * @var $schedule Schedule
 */

$icon = Html::tag('i', null, ['class' => ['fa', 'fa-times']]);
echo Html::a($icon, 'javascript: void(0);', ['class' => ['close_modal']]);
?>

<div class="wrapper_form">
    <?php
    Pjax::begin(['id' => 'register_form_pjax', 'enablePushState' => false]);
    echo Html::tag('h2', 'Регистрация на курс «'.$schedule->getTechnologyTitle().'»');
    if ($send === true) {
        echo $this->render('result', ['schedule' => $schedule]);
    } else {
        echo $this->render('blocks/_form', ['register' => $register]);
    }
    Pjax::end();
    ?>
</div>

