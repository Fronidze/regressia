<?php

use frontend\forms\CourseRegister;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $register CourseRegister
 */

$form = ActiveForm::begin([
    'action' => ['education/register', 'id' => $register->schedule_id],
    'options' => ['data' => ['pjax' => true]],
]);

echo $form->field($register, 'email')->label(false)->textInput([
    'autocomplete' => 'off',
    'placeholder' => $register->getAttributeLabel('email') . '*'
]);

echo $form->field($register, 'name')->label(false)->textInput([
    'autocomplete' => 'off',
    'placeholder' => $register->getAttributeLabel('name') . '*'
]);

echo $form->field($register, 'social')->label(false)->textInput([
    'autocomplete' => 'off',
    'placeholder' => $register->getAttributeLabel('social')
]);

echo $form->field($register, 'ability')->label(false)->textarea([
    'rows' => 4,
    'autocomplete' => 'off',
    'placeholder' => $register->getAttributeLabel('ability') . '*'
]);

echo Html::submitButton('Зарегистрироваться');

echo Html::beginTag('p', ['class' => 'input-group policy']);
echo "Нажимая на эту кнопку, вы даете согласие на обработку персональных данных и 
            соглашаетесь с ";
echo Html::a('политикой конфеденциальности.', ['/policy/'], ['class' => 'link']);
echo Html::endTag('p');
ActiveForm::end();
