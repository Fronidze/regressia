<?php

use backend\models\User;
use common\models\Files;
use common\models\Schedule;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $schedule Schedule
 */

$this->title = 'Академия разработки MediaSoft: ' . ArrayHelper::getValue($schedule, 'technology.title');

?>
<span id="arrow_back" class="toggle-arrow">
    <?php echo Html::a('<i class="arrow-svg"></i>', Url::previous()); ?>
</span>

<section class="education_title">
    <div class="container">
        <div class="logo">
            <a href="/">
                <img src="/images/logo_academy_rgb.png" alt="">
            </a>
        </div>
        <div class="description">
            <div class="name_course"><?= $schedule->getTechnologySubTitle(); ?></div>
            <div class="price_course">Обучение бесплатное</div>
            <div class="date_begin_course"><?= $schedule->getActiveStatusLabel(); ?></div>
            <?php if ($schedule->isFinished() === false): ?>
                <div class="schedule_course">Занятия проходят <?= $schedule->getWeeklyScheduleForPage() ?></div>
                <?php if ($schedule->canRegister()): ?>
                    <div class="action_course">
                        <?php
                        echo Html::a('Зарегистрироваться', 'javascript: void(0)', [
                            'data' => ['popup' => 'register', 'request' => Url::to(['education/register', 'id' => $schedule->id])]
                        ]);
                        ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <div class="address_course"><b>MediaSoft</b> / Ульяновск, ул. К.Маркса,13А, корп.3 <span>(ТЦ Амарант, 3 этаж) /</span>
            </div>
        </div>
    </div>
</section>

<section class="education_about">
    <div class="about_wrapper flex">
        <div class="light_section">
            <div class="wrapper_section">
                <div class="title_section">Для кого</div>
                <div class="content_section_wrapper"><?= $schedule->technology->left_description ?></div>
            </div>
        </div>
        <div class="dark_section">
            <div class="wrapper_section">
                <div class="title_section">Что дает курс</div>
                <div class="content_section_wrapper"><?= $schedule->technology->right_description ?></div>
            </div>
        </div>
    </div>
</section>

<section class="education_program">
    <div class="container">
        <div class="title_section">Программа</div>
        <div class="wrapper_content">
            <?= $schedule->program ?>
        </div>
    </div>
</section>

<section class="education_record">
    <div class="record_wrapper flex">
        <div class="light_section">
            <div class="wrapper_section">
                <div class="title_section">Информация</div>
                <div class="content_section_wrapper">
                    <ul>
                        <li><b>Участие бесплатно</b></li>
                        <li>Требуется предварительная
                            <?php
                            if ($schedule->canRegister()) :
                                echo Html::a('регистрация', 'javascript: void(0)', [
                                    'data' => ['popup' => 'register', 'request' => Url::to(['education/register', 'id' => $schedule->id])]
                                ]);
                            else :
                                echo 'регистрация';
                            endif;
                            ?>
                        </li>
                        <li>По окончании курса будут выданы сертификаты.</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="dark_section">
            <div class="wrapper_section">
                <div class="title_section">Записаться</div>
                <div class="content_section_wrapper">
                    <div class="record_block_wrapper">
                        <div class="person flex">
                            <?php
                            /** @var User $person */
                            $person = ArrayHelper::getValue($schedule, 'responsible');
                            ?>
                            <div class="image">
                                <a href="<?= $person->social; ?>">
                                    <?php
                                    $path = null;
                                    /** @var Files $file */
                                    if ($file = ArrayHelper::getValue($person, 'imageFile')) {
                                        $path = $file->makeResize(50, 50);
                                    }
                                    ?>
                                    <img src="<?= $path ?>" alt="">
                                </a>
                            </div>
                            <div class="name">
                                <p>
                                    <a href="<?= $person->social ?>"><?= $person->username ?></a>
                                </p>
                                <p><?= $person->position ?> </p>
                            </div>
                        </div>
                        <div class="phone"><a href="tel:+78007750679"> <i class="fa fa-phone"></i>+7 (800) 775-06-79</a>
                        </div>
                        <div class="email"><a href="mailto:study@mediasoft.team"> <i class="fa fa-envelope"></i>study@mediasoft.team</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>