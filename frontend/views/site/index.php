<?php

/**
 * @var $this View
 * @var $schedules Schedule[]
 * @var $exam_preparations ExamPreparation[]
 * @var $template_exam_preparation TemplateExamPreparation
 * @var $groups Group[]
 * @var $teachers Teacher[]
 * @var $sign_up CourseSignUp
 * @var $images Gallery[]
 * @var $questions Question[]
 */

use common\models\ExamPreparation;
use common\models\Gallery;
use common\models\Group;
use common\models\Question;
use common\models\Schedule;
use common\models\Teacher;
use common\models\TemplateExamPreparation;
use frontend\assets\YandexAsset;
use frontend\forms\CourseSignUp;
use yii\web\View;

YandexAsset::register($this);

$this->title = 'Академия разработки MediaSoft';
$title = "помогаем преодолеть пропасть между <br> знаниями и реальной работой";

echo $this->render('sections/section_top', ['title' => $title]);
echo $this->render('sections/section_description');
echo $this->render('sections/section_courses',
    [
        'schedules'         => $schedules,
        'groups'            => $groups,
        'exam_preparations' => $exam_preparations,
    ]);
echo $this->render('sections/section_signup', ['sign_up' => $sign_up]);
echo $this->render('sections/section_teacher', ['teachers' => $teachers]);
if (count($questions) > 0) {
    echo $this->render('sections/section_question', ['questions' => $questions]);
}
echo $this->render('sections/section_mediasoft');
if (count($images) > 0) {
    echo $this->render('sections/section_gallery', ['images' => $images]);
}
echo $this->render('sections/section_info');
