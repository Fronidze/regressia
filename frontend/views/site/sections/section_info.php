<?php

use yii\web\View;

/**
 * @var $this View
 */

?>

<section class="main_page_info" id="contacts">
    <div class="container">
        <div class="info_wrapper flex">
            <div class="contacts">
                <p class="info_title">Появились вопросы?</p>
                <p class="info_description">Обсудите их лично с кураторами курсов</p>
                <ul>
                    <li>
                        <p class="name_cell">Телефон:</p>
                        <p class="value_cell"><a href="tel:+78007750679">+7 800 775 0679</a></p>
                    </li>
                    <li>
                        <p class="name_cell">E-mail:</p>
                        <p class="value_cell"><a href="mailto:study@mediasoft.team">study@mediasoft.team</a></p>
                    </li>
                    <li>
                        <p class="name_cell">Будьте в курсе новостей:</p>
                        <p class="value_cell social_links">
                            <a target="_blank" href="https://vk.com/mediasoft.academy"><i class="fa fa-vk"></i></a>
                            <a target="_blank" href="https://www.facebook.com/msoft.team/"><i class="fa fa-facebook"></i></a>
                            <a target="_blank" href="https://www.instagram.com/mediasoft.team/"><i class="fa fa-instagram"></i></a>
                            <a target="_blank" href="https://t.me/mediasoft_academy"><i class="fa fa-telegram"></i></a>
                        </p>
                    </li>
                </ul>
                <div class="icon">
                    <i class="fa fa-envelope"></i>
                </div>
            </div>
            <div class="address">
                <p class="info_title">Наш адрес</p>
                <div class="address_value">
                    <p class="info_description">Mы расположены в самом центре Ульяновска</p>
                    <p>
                        <b>Карла Маркса 13А, корп.3, Ульяновск</b><br>
                        <span>В ТЦ Амарант на 3 этаже.<br> Вход на цокольном этаже со стороны парковки.</span>
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-map-marker"></i>
                </div>
            </div>

        </div>
    </div>
    <div id="map" class="yandex_map"></div>
</section>