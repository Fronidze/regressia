<?php

use frontend\forms\CourseSignUp;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var $this View
 * @var $sign_up CourseSignUp
 */

?>

<section class="main_page_sign_up" id="pre-registration">
    <div class="container">
        <h2 class="title">Заполните предварительную заявку на обучение</h2>
        <p class="form_description">Потоки запускаются по мере формирования групп</p>
        <?php
        $icon = Html::tag('span', Html::tag('span', null, ['class' => ['fa', 'fa-times']]),
            ['class' => ['input-group-addon', 'has-error']]);
        $icon .= Html::tag('span', Html::tag('span', null, ['class' => ['fa', 'fa-check']]),
            ['class' => ['input-group-addon', 'has-success']]);

        $template = [
            'options' => ['class' => 'input-group'],
            'template' => "{label}\n{input}$icon\n{hint}\n{error}",
        ];

        Pjax::begin(['id' => 'sign_up', 'enablePushState' => false]);
        $form = ActiveForm::begin([
            'action' => Url::to(['course/sign-up']),
            'options' => ['data' => ['pjax' => '1']]
        ]);
        echo $form->field($sign_up, 'email',
            $template)->label(false)->textInput(['placeholder' => $sign_up->getAttributeLabel('email')]);
        echo $form->field($sign_up, 'name',
            $template)->label(false)->textInput(['placeholder' => $sign_up->getAttributeLabel('name')]);
        echo $form->field($sign_up, 'university',
            $template)->label(false)->textInput(['placeholder' => $sign_up->getAttributeLabel('university')]);
        echo $form->field($sign_up, 'code_technology_or_group', $template)->label(false)->dropDownList(
            ArrayHelper::merge($sign_up->listTechnology(), $sign_up->listGroup(), ['student-ege' => 'ЕГЭ по информатике']), [
            'class' => ['select2'],
            'prompt' => '- Выберите направление -',
        ]);
        echo Html::beginTag('div', ['class' => ['input-group']]);
        echo Html::submitButton('Отправить заявку', ['class' => ['submit_signup']]);
        echo Html::endTag('div');

        echo Html::beginTag('p', ['class' => 'input-group policy']);
        echo Html::encode('Нажимая на эту кнопку, вы даете согласие на обработку персональных данных и 
            соглашаетесь с ');
        echo Html::a('политикой конфеденциальности.', ['/policy/'], ['class' => 'link']);
        echo Html::endTag('p');

        ActiveForm::end();
        Pjax::end();
        ?>
    </div>
</section>