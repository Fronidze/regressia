<?php

use common\models\Files;
use common\models\Gallery;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var $this View
 * @var $images Gallery[]
 */

?>

<section class="main_page_gallery">
    <div class="container">
        <div class="gallery_wrapper flex gallery_desktop">
            <?php foreach ($images as $image):

                /** @var Files $file */
                $file = ArrayHelper::getValue($image, 'imageFile');
                if ($file === null ) {
                    continue;
                }

                ?>
                <div class="image">
                    <a data-fancybox href="<?= $file->makePath(); ?>">
                        <img src="<?= $file->makeResize(340, 230)?>" alt="">
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($images as $image):

                    /** @var Files $file */
                    $file = ArrayHelper::getValue($image, 'imageFile');
                    if ($file === null ) {
                        continue;
                    }

                    ?>
                    <div class="swiper-slide">
                        <div class="image">
                            <a data-fancybox href="<?= $file->makePath() ?>">
                                <img src="<?= $file->makeResize(340, 230) ?>" alt="">
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>