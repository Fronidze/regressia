<?php

use yii\web\View;

/**
 * @var $this View
 */

?>

<section class="main_page_mediasoft">
    <div class="container">
        <div class="statistics">
            <p class="mediasoft_today">Сегодня академия — это:</p>
            <div class="numbers flex">
                <div class="indicator">
                    <p class="value">
                        <span class="change_number" data-begin="1" data-value="6">6</span>
                        лет
                    </p>
                    <p class="line"></p>
                    <p class="description">опыта обучения и наставничества</p>
                </div>
                <div class="indicator">
                    <p class="value">
                        <span class="change_number" data-begin="1000" data-value="3000">3000</span>
                    </p>
                    <p class="line"></p>
                    <p class="description">слушателей прошли курсы</p>
                </div>
                <div class="indicator">
                    <p class="value">
                        <span class="change_number" data-begin="10" data-value="85">85</span>
                        %
                    </p>
                    <p class="line"></p>
                    <p class="description">выпускников выходят на стажировки сразу после завершения курсов</p>
                </div>
            </div>
        </div>
    </div>
</section>