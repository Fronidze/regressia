<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 */

?>

<section class="main_page_top">

    <!--<div class="video_bg">
        <video src="/images/IMG_1702.MOV" autoplay="autoplay" loop="loop" muted="muted"></video>
    </div>-->

    <div class="container flex flex-column">
        <?php
        echo Html::img('/images/logo_academy_rgb.png', [
            'alt' => 'Академия разработки MediaSoft',
            'class' => ['logo_academy'],
        ])
        ?>
        <div class="line_under_logo"></div>
        <div class="tagline">
            <p><?= $title ?></p>
        </div>
    </div>

    <svg class="decorating" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1280 200" preserveAspectRatio="none">
        <path d="M640 195.5L0 0v200h1280V0"></path>
    </svg>

</section>
