<?php

use common\models\Files;
use common\models\Teacher;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var $this View
 * @var $teachers Teacher[]
 */

?>

<section class="main_page_teacher" id="teachers">
    <div class="container">
        <h2 class="title">Наши преподаватели</h2>
        <p class="teacher_description">Практикующие эксперты с многолетним <br> прикладным опытом</p>
        <div class="teacher_wrapper flex teacher_desktop">
            <?php foreach ($teachers as $teacher):
                $path = null;

                /** @var Files $file */
                if ($file = ArrayHelper::getValue($teacher, 'imageFile')) {
                    $path = $file->makeResize(260, 260);
                }

                ?>
                <div class="teacher">
                    <img src="<?= $path ?>" alt="<?= $teacher->name ?>">
                    <div class="info">
                        <p class="name"><?= $teacher->name ?></p>
                        <p class="position"><?= $teacher->position ?></p>
                        <p class="position"><?= $teacher->curator ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($teachers as $teacher):
                    $path = null;

                    /** @var Files $file */
                    if ($file = ArrayHelper::getValue($teacher, 'imageFile')) {
                        $path = $file->makeResize(260, 260);
                    }

                    ?>
                    <div class="swiper-slide">
                        <div class="teacher">
                            <img src="<?= $path ?>" alt="<?= $teacher->name ?>">
                            <div class="info">
                                <p class="name"><?= $teacher->name ?></p>
                                <p class="position"><?= $teacher->position ?></p>
                                <p class="position"><?= $teacher->curator ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="swiper-pagination"></div>
        </div>
</section>