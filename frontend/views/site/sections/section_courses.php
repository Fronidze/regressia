<?php

use common\models\ExamPreparation;
use common\models\Group;
use common\models\Schedule;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $schedules Schedule[]
 * @var $groups Group[]
 * @var $exam_preparations ExamPreparation[]
 */

?>

<section class="main_page_courses" id="courses">
    <div class="container">
        <h2 class="title">Наши курсы</h2>
        <div class="course_wrapper flex">

            <?php foreach ($schedules as $schedule): ?>
                <div class="course">
                    <a href="<?php echo Url::to(['education/index', 'code' => $schedule->code]) ?>">
                        <div class="course_info flex flex-column">
                            <p class="course_title"><?= $schedule->getTechnologyTitle() ?></p>
                            <p class="course_status"><?= $schedule->getActiveStatusLabel() ?></p>
                        </div>
                        <div class="course_action flex flex-column">
                            <p class="link">Подробнее о курсе <i class="fa fa-angle-double-right"></i></p>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>

            <?php foreach ($groups as $group): ?>
                <div class="course">
                    <a href="<?php echo Url::to(['group/index', 'code' => $group->code]) ?>">
                        <div class="course_info flex flex-column">
                            <p class="course_title"><?= $group->title ?></p>
                            <p class="course_status"><?= $group->generateBeginLabel(); ?></p>
                        </div>
                        <div class="course_action flex flex-column">
                            <p class="link">Подробнее о курсе <i class="fa fa-angle-double-right"></i></p>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>

            <?php if (count($exam_preparations) > 0): ?>
                <?php foreach ($exam_preparations as $exam_preparation): ?>
                    <div class="course">
                        <a href="<?php echo Url::to(['exam-preparation/index', 'code' => $exam_preparation->code]) ?>">
                            <div class="course_info flex flex-column">
                                <p class="course_title"><?= $exam_preparation->sub_title ?></p>
                                <p class="course_status"><?= $exam_preparation->empty_date === ExamPreparation::NOT_KNOW_DATE ? $exam_preparation->sub_status : $exam_preparation->getActiveStatusLabel(); ?></p>
                            </div>
                            <div class="course_action flex flex-column">
                                <p class="link">Подробнее о курсе <i class="fa fa-angle-double-right"></i></p>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="course-ege">
                    <a href="javascript: void(0);">
                        <div class="course_info flex flex-column">
                            <p class="course_title">ЕГЭ по информатике</p>
                            <p class="course_status">Курс в проработке</p>
                        </div>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>