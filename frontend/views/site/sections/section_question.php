<?php

use common\models\Question;
use yii\web\View;

/**
 * @var $this View
 * @var $questions Question[]
 */

?>

<section class="main_page_questions" id="faq">
    <div class="container">
        <h2 class="title">FAQ: все, что нужно знать о наших курсах</h2>
        <div class="question_wrapper flex">
            <?php foreach ($questions as $question): ?>
                <div class="question">
                    <div class="collapsible">
                        <p>
                            <?= $question->question ?>
                        </p>
                        <span aria-hidden="true" class="fa fa-plus"></span>
                    </div>
                    <div class="content">
                        <?= $question->answer ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <p class="other-questions">Остались вопросы? Смело пишите на почту Академии
            <a href="mailto:study@mediasoft.team">study@mediasoft.team</a></p>
    </div>
</section>