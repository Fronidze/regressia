<?php

use frontend\assets\ArkanoidAsset;
use yii\helpers\Html;
use yii\web\HttpException;

ArkanoidAsset::register($this);

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = "Извините, произошла ошибка. " . $name;

echo $this->render('sections/section_top');

$httpCode = $exception instanceof HttpException ? (int)$exception->statusCode : 500;
?>

<section class="content">
    <div class="error-page error-page--<?= $httpCode ?>">
        <div class="error-page__content">
            <div class="container">
                <div class="error-page__title">Упс! Ошибка <?= $httpCode ?></div>
                <div class="error-page__description"><?= nl2br(Html::encode($message)) ?></div>
                <div class="error-page__back">
                    <a href="<?= Yii::$app->homeUrl ?>" class="error-back-link">
                        <span class='back-icon'>←</span> Вернуться на главную страницу
                    </a>
                </div>
            </div>
            <div class="game">
                <canvas id="gameCanvas" class="game__canvas"></canvas>
                <div class="game__info">Move with the left and right arrow keys or A and D keys</div>
            </div>
        </div>
    </div>
</section>
