class Metrica
{

    constructor() {
        this.code = 55966816;
        this.method = 'reachGoal';
    }

    reach(target) {

        if (is_prod !== true) {
            return null;
        }

        try {
            ym(this.code, this.method, target);
            console.log('Срабатываение события');
        } catch (e) {
            console.warn('Ошибка отправки данных в аналитику');
        }
    }

}