let map = null;

function init() {
    map = new ymaps.Map("map", {
        center: [54.319769, 48.395945],
        zoom: 16
    });

    let placemark = new ymaps.Placemark([54.319769, 48.395945], {
        name: 'Считаем',
        hintContent: 'Академия разработки Mediasoft'
    }, {
        iconLayout: 'default#image',
        iconImageHref: 'images/ballon.png',
        iconImageSize: [100, 100],
        iconImageOffset: [-50, -100]
    });
    map.geoObjects.add(placemark);
    map.behaviors.disable('scrollZoom');
}

ymaps.ready(init);