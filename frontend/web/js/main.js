let run_number_animate = false;
let mediasoft_coordinate = null;
let mediasoft_block = null;
let is_mobile = false;
let is_prod = location.host === 'academy.mediasoft.team';

function load() {
    is_mobile = screen.width <= 480;

    mediasoft_block = document.querySelector('.main_page_mediasoft');
    if (mediasoft_block !== null) {
        mediasoft_coordinate = getCoords(mediasoft_block);
    }

    selectWidget();
    initMenu();

    try {
        $(['data-fancybox']).fancybox();
    } catch (e) {
        console.warn(e.message);
    }

    $('[data-popup]').on('click', function (event) {
        event.preventDefault();

        let type = event.target.dataset['popup'];
        let metrica = new Metrica();
        metrica.reach('angular_register');
        $("#" + type).bPopup({
            closeClass: 'close_modal',
            content: 'ajax',
            loadUrl: event.target.dataset['request'],
            
            onClose: function() {
                $('body').removeClass('no-scroll');
            }
        });

        $('body').addClass('no-scroll');
    });

    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var content = this.nextElementSibling;

            if (this.children[1].classList.contains('fa-plus')) {
                this.children[1].classList.remove('fa-plus');
                this.children[1].classList.add('fa-minus');
            } else {
                this.children[1].classList.remove('fa-minus');
                this.children[1].classList.add('fa-plus');
            }

            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }
}

function initMenu() {
    let element = document.getElementById('menu');
    let menu = document.getElementById('content_menu');

    if (element == null && menu == null) {
        return false;
    }

    element.addEventListener('click', function (event) {
        element.classList.toggle('button-open');
        menu.classList.toggle('menu-show');
    });

    let $page = $('html, body');
    $('a[href*="#"]').click(function () {
        $page.animate({
            scrollTop: $($.attr(this, 'href').replace('/', '')).offset().top
        }, 400, function () {
            closeMenu();
        });
        return false;
    });

}

function closeMenu() {
    let element = document.getElementById('menu');
    let menu = document.getElementById('content_menu');

    if (element == null && menu == null) {
        return false;
    }

    element.classList.remove('button-open');
    menu.classList.remove('menu-show');
}

function scroll(event) {

    if (is_mobile === false) {
        closeMenu();
    }

    statisticNumberAnimate();
}

function getCoords(element) {
    let position = element.getBoundingClientRect();
    return {
        top: position.top + pageYOffset,
        left: position.left + pageXOffset
    };
}

function statisticNumberAnimate() {
    try {
        let scroll = window.pageYOffset || document.documentElement.scrollTop;
        if (mediasoft_coordinate !== null && mediasoft_block !== null) {
            let top = mediasoft_coordinate.top - document.documentElement.clientHeight + mediasoft_block.offsetHeight;

            if (scroll < (mediasoft_coordinate.top - document.documentElement.clientHeight) && run_number_animate === true) {
                run_number_animate = false;
            }

            if (scroll > top && run_number_animate === false) {
                animateNumbers();
            }

        }
    } catch (e) {
        console.warn('Ошибка анимации: ' + e.message);
    }
}

function animateNumbers() {
    let elements = document.querySelectorAll('.change_number');
    elements.forEach(function (element) {
        let dataset = element.dataset;
        let begin = dataset['begin'];
        let value = dataset['value'];
        $(element).prop('number', begin).animateNumber({number: value}, 1000);
    });
    run_number_animate = true;
}

function selectWidget() {
    let select2_config = {
        width: '100%',
        minimumResultsForSearch: -1,
        // theme: 'signup'
    };
    let select = $('select.select2');
    if (select !== undefined || select !== null) {
        select.select2(select2_config);
    }
}

window.onload = load;
window.onscroll = scroll;

var config_teacher = {
    loop: true,
    slidesPerView: 2,
    pagination: {
        el: '.swiper-pagination',
    },
    breakpoints: {
        1024: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        600: {
            slidesPerView: 1,
            spaceBetween: 10
        }
    }
};

var swiper_teacher = new Swiper('.main_page_teacher .swiper-container', config_teacher);

var config_gallery = {
    loop: true,
    slidesPerView: 'auto',
    spaceBetween: 15,
    centeredSlides: true,
    slideNextClass: 'swiper-slide-next gallery_swiper_next',
    slidePrevClass: 'swiper-slide-prev gallery_swiper_prev',
    pagination: {
        el: '.swiper-pagination',
    },
    breakpoints: {
        600: {
            slidesPerView: 1,
            spaceBetween: 20
        }
    }
};

var swiper_gallery = new Swiper('.main_page_gallery .swiper-container', config_gallery);
