<?php

namespace frontend\forms;

use common\models\ExamPreparation;
use common\models\RecordSchoolchild;
use common\models\Schoolchild;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;

class CourseRegisterExamPreparation extends Model
{
    public $email;
    public $name;
    public $social;
    public $exam_preparation_id;

    /** @var ExamPreparation|null */
    protected $exam_preparation = null;


    public function rules()
    {
        return [
            [['email', 'name', 'exam_preparation_id'], 'required'],
            [['email'], 'email'],
            [['name', 'social'], 'string'],
            [['exam_preparation_id'], 'integer'],
            [
                ['email'],
                function ($field) {
                    $exists = (new Query())
                        ->from(['rs' => RecordSchoolchild::tableName()])
                        ->innerJoin(['exam_preparation' => ExamPreparation::tableName()], ['exam_preparation.id' => new Expression('rs.exam_preparation_id')])
                        ->innerJoin(['schoolchild' => Schoolchild::tableName()], ['schoolchild.id' => new Expression('rs.schoolchild_id')])
                        ->where(['and', ['=', 'schoolchild.email', $this->email], ['=', 'rs.exam_preparation_id', $this->exam_preparation_id]])
                        ->exists();

                    if ($exists === true) {
                        $this->addError($field, 'Пользователь с данным email уже зарегистрирован на этот курс.');
                    }
                }
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email'               => 'E-mail',
            'name'                => 'Ф.И.О.',
            'social'              => 'Ссылка на профиль в соц. сетях',
            'exam_preparation_id' => 'Курс',
        ];
    }

    /**
     * @return ExamPreparation|null
     */
    public function getExamPreparation()
    {
        if ($this->exam_preparation === null) {
            return ExamPreparation::find()
                ->where(['=', 'id', $this->exam_preparation_id])
                ->one();
        }

        return $this->exam_preparation;
    }
}