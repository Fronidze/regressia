<?php

namespace frontend\forms;

use common\models\Record;
use common\models\Schedule;
use common\models\Students;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;

class CourseRegister extends Model
{
    public $email;
    public $name;
    public $social;
    public $ability;
    public $schedule_id;

    /** @var Schedule|null */
    protected $schedule = null;


    public function rules()
    {
        return [
            [['email', 'name', 'schedule_id', 'ability'], 'required'],
            [['email'], 'email'],
            [['name', 'social', 'ability'], 'string'],
            [['schedule_id'], 'integer'],
            [
                ['email'],
                function ($field) {
                    $exists = (new Query())
                        ->from(['record' => Record::tableName()])
                        ->innerJoin(['schedule' => Schedule::tableName()], ['schedule.id' => new Expression('record.schedule_id')])
                        ->innerJoin(['student' => Students::tableName()], ['student.id' => new Expression('record.student_id')])
                        ->where(['and', ['=', 'student.email', $this->email], ['=', 'record.schedule_id', $this->schedule_id]])
                        ->exists();

                    if ($exists === true) {
                        $this->addError($field, 'Пользователь с данным email уже зарегистрирован на этот курс.');
                    }
            }
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'name' => 'Ф.И.О.',
            'social' => 'Ссылка на профиль в соц. сетях',
            'ability' => 'Текущие знания',
            'schedule_id' => 'Курс',
        ];
    }

    /**
     * @return Schedule|null
     */
    public function getSchedule()
    {
        if ($this->schedule === null) {
            return Schedule::find()
                ->with(['technology'])
                ->where(['=', 'id', $this->schedule_id])
                ->one();
        }

        return $this->schedule;
    }
}