<?php

namespace frontend\forms;

use common\models\Group;
use common\models\Technology;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class CourseSignUp extends Model
{
    public $email;
    public $name;
    public $university;
    public $code_technology_or_group;

    public function rules()
    {
        return [
            [['email', 'name', 'code_technology_or_group'], 'required'],
            [['email'], 'email'],
            [['name', 'university'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'name' => 'Ф.И.О.',
            'university' => 'ВУЗ',
            'code_technology_or_group' => 'Направление',
        ];
    }

    public function listTechnology()
    {
        $query = Technology::find()
            ->where(['not in', 'id', [12, 13]]);

        return ArrayHelper::map($query->all(), 'short_title', 'title');
    }

    public function listGroup()
    {
        $query = (new Query())
            ->select(['short_title' => 'code', 'title'])
            ->from(Group::tableName())
            ->where(['=', 'publish', Group::PUBLISH_YES]);

        return ArrayHelper::map($query->all(), 'short_title', 'title');
    }
}