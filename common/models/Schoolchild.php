<?php

namespace common\models;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "schoolchild".
 *
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $social
 * @property string $create_at
 * @property string $update_at

 */
class Schoolchild extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'schoolchild';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email', 'name', 'social'], 'string', 'max' => 255],
            [['create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'     => 'ID',
            'email'  => 'E-Mail',
            'name'   => 'ФИО',
            'social' => 'Ссылка на профиль в соц. сетях'
        ];
    }

    public function getRecordSchoolchild()
    {
        return $this->hasMany(RecordSchoolchild::class, ['schoolchild_id' => 'id']);
    }
}
