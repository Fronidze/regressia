<?php

namespace common\models\abstractions;

use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class StatusActiveRecord
 * @package common\models\abstractions
 */
abstract class PublishActiveRecord extends ActiveRecord
{
    const PUBLISH_YES = 1;
    const PUBLISH_NO = 0;

    public function listPublish()
    {
        return [
            static::PUBLISH_YES => 'Публиковать',
            static::PUBLISH_NO => 'Не публиковать',
        ];
    }

    public function isPublish()
    {
        $is_publish = ArrayHelper::getValue($this, 'is_publish');
        if ($is_publish === null) {
            throw new NotSupportedException("Поле `is_publish` не определено в модели: " . get_called_class());
        }

        return $is_publish === static::PUBLISH_YES;
    }
}
