<?php

namespace common\models;

use backend\models\User;
use common\models\abstractions\PublishActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "teacher".
 *
 * @property int $id
 * @property string $name
 * @property string $position
 * @property int $image_id
 * @property string $create_at
 * @property string $update_at
 * @property int $update_by
 * @property string $is_publish
 * @property int $sorting
 * @property string $curator
 *
 * @property User $editor
 * @property User $imageFile
 */
class Teacher extends PublishActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%teacher}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'position'], 'required'],
            [['create_at', 'update_at'], 'safe'],
            [['update_by', 'sorting', 'is_publish', 'image_id'], 'integer'],
            [['name', 'position', 'curator'], 'string', 'max' => 255],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Преподаватель',
            'position' => 'Должность',
            'image_id' => 'Изображение',
            'create_at' => 'Дата создания',
            'update_at' => 'Дата обновления',
            'update_by' => 'Редактировал',
            'is_publish' => 'Публикация на главной странице',
            'sorting' => 'Сортировка',
            'curator' => 'Куратор курса',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getEditor()
    {
        return $this->hasOne(User::class, ['id' => 'update_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getImageFile()
    {
        return $this->hasOne(Files::class, ['id' => 'image_id']);
    }
}
