<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "template_exam_preparation".
 *
 * @property int $id
 * @property string $title
 * @property string $left_description
 * @property string $right_description
 * @property string $about_course
 * @property string $program
 * @property string $create_at
 * @property string $update_at
 */
class TemplateExamPreparation extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'template_exam_preparation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['left_description', 'right_description', 'about_course', 'program'], 'string'],
            [['create_at', 'update_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'Индетификатор',
            'title'             => 'Название',
            'left_description'  => 'Для кого',
            'right_description' => 'Что дает курс',
            'about_course'      => 'О курсе',
            'program'           => 'Программа',
            'create_at'         => 'Create At',
            'update_at'         => 'Update At',
        ];
    }
}
