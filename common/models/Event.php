<?php

namespace common\models;

use backend\models\User;
use common\models\links\ParticipantsEvent;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "event".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $user_id
 * @property string $place
 * @property string $date_event
 * @property string $date_reminder
 * // * @property array $users
 * @property string $date_reminder_clock
 * @property string $date_event_clock
 * @property string $date_create
 * @property string $date_update
 */
class Event extends ActiveRecord
{
//    public $users = [];
    public $date_reminder_clock = '';
    public $date_event_clock = '';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['user_id', 'title'], 'required'],
            [['user_id'], 'integer'],
            [['date_event', 'date_reminder', 'date_reminder_clock', 'date_event_clock'], 'safe'],
            [['title', 'place'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название мероприятия',
            'description' => 'Описание мероприятия',
            'user_id' => 'Создатель мероприятия',
            'place' => 'Место проведения',
            'date_event' => 'Дата проведения',
            'date_reminder' => 'Дата напоминания',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата обновления',
        ];
    }

    public function listPersonal()
    {
        $query = (new Query())
            ->select(['id', 'username'])
            ->from(User::tableName())
            ->where(['!=', 'id', Yii::$app->user->id]);

        return ArrayHelper::map($query->all(), 'id', 'username');
    }

    public function getUsers()
    {
        return $this->hasMany(ParticipantsEvent::class, ['event_id' => 'id'])
            ->innerJoin(['user' => User::tableName()], ['user.id' => new Expression('participants_event.user_id')]);
    }
}
