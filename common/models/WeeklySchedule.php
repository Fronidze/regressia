<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "weekly_schedule".
 *
 * @property int $id
 * @property int $schedule_id
 * @property string $monday
 * @property string $tuesday
 * @property string $wednesday
 * @property string $thursday
 * @property string $friday
 * @property string $saturday
 * @property string $sunday
 */
class WeeklySchedule extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%weekly_schedule}}';
    }

    public function rules()
    {
        return [
            [['schedule_id'], 'required'],
            [['schedule_id'], 'integer'],
            [['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'schedule_id' => 'Рассписание',
            'monday' => 'Понидельник',
            'tuesday' => 'Вторник',
            'wednesday' => 'Среда',
            'thursday' => 'Четверг',
            'friday' => 'Пятница',
            'saturday' => 'Суббота',
            'sunday' => 'Воскресенье',
        ];
    }

    public function prepositionalLabels()
    {
        return [
            'monday' => 'Понедельникам',
            'tuesday' => 'Вторникам',
            'wednesday' => 'Средам',
            'thursday' => 'Четвергам',
            'friday' => 'Пятницам',
            'saturday' => 'Субботам',
            'sunday' => 'Воскресеньям',
        ];
    }

    public function numberWeekDay()
    {
        return [
            'monday' => 1,
            'tuesday' => 2,
            'wednesday' => 3,
            'thursday' => 4,
            'friday' => 5,
            'saturday' => 6,
            'sunday' => 7,
        ];
    }

    public function selectedWeekDay()
    {
        $attributes = $this->getAttributes();
        unset($attributes['id']);
        unset($attributes['schedule_id']);

        $result = [];
        $numberWeek = $this->numberWeekDay();
        foreach ($attributes as $field => $value) {
            if (empty($value) === false) {
                $result[$numberWeek[$field]] = $value;
            }
        }
        
        return $result;
    }

    public function getFullWeekSchedule()
    {
        $attributes = $this->getAttributes();
        $prepositional = $this->prepositionalLabels();
        $result = [];
        foreach ($attributes as $code => $attribute) {
            if ($code === 'id' || $code === 'schedule_id') {
                continue;
            }

            if ($attribute === null || $attribute === '') {
                continue;
            }

            $result[] = [
                'label_week' => ArrayHelper::getValue($prepositional, $code, $this->getAttributeLabel($code)),
                'time' => $attribute
            ];
        }

        return $result;
    }
}
