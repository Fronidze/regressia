<?php

namespace common\models;

use common\models\abstractions\PublishActiveRecord;
use yii\db\ActiveQuery;
use Yii;

/**
 * This is the model class for table "{{%gallery}}".
 *
 * @property int $id
 * @property int $image_id
 * @property int $publish
 * @property int $sorting
 *
 * @property Files $imageFile
 */
class Gallery extends PublishActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%gallery}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_id'], 'required'],
            [['image_id', 'publish', 'sorting'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_id' => 'Изображения',
            'publish' => 'Публикация',
            'sorting' => 'Сортировка',
        ];
    }

    /**
     * @return bool
     */
    public function isPublish()
    {
        return $this->publish === static::PUBLISH_YES;
    }

    /**
     * @return ActiveQuery
     */
    public function getImageFile()
    {
        return $this->hasOne(Files::class, ['id' => 'image_id']);
    }

}
