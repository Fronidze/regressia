<?php

namespace common\models;

use Imagine\Image\Box;
use Imagine\Imagick\Imagine;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "{{%files}}".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int $size
 * @property string $relative_path
 * @property string $full_path
 */
class Files extends ActiveRecord
{
    /** @var string */
    protected $file_full_path;

    public function beforeDelete()
    {
        if (parent::beforeDelete() === false) {
            return false;
        }

        $this->file_full_path = $this->full_path;
        return true;
    }

    public function afterDelete()
    {
        parent::afterDelete();

        if ($this->file_full_path !== null) {
            unlink($this->file_full_path);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%files}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'full_path'], 'required'],
            [['size'], 'integer'],
            [['name', 'type', 'full_path', 'relative_path'], 'string', 'max' => 255],
            [['full_path'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Индетификатор',
            'name' => 'Название',
            'type' => 'Тип',
            'size' => 'Размер',
            'full_path' => 'Полный путь',
            'relative_path' => 'Относительный путь',
        ];
    }

    public function makePath()
    {
        return \Yii::t('app', '{path}{filename}', [
            'path' => $this->relative_path,
            'filename' => $this->name
        ]);
    }

    public function makeResize(int $width, int $height)
    {
        $path = \Yii::t('app', '{folder}/resize/{identifier}/{size}/', [
            'folder' => \Yii::getAlias('@upload'),
            'identifier' => $this->id,
            'size' => $width . 'x' . $height,
        ]);
        
        FileHelper::createDirectory($path);
        if (file_exists($path . $this->name) === true) {
            return \Yii::t('app', '/upload/resize/{identifier}/{size}/{filename}', [
                'identifier' => $this->id,
                'size' => $width . 'x' . $height,
                'filename' => $this->name,
            ]);
        }

        $box = new Box($width, $height);
        (new Imagine())
            ->open($this->full_path)
            ->resize($box)
            ->save($path . $this->name);

        return \Yii::t('app', '/upload/resize/{identifier}/{size}/{filename}', [
            'identifier' => $this->id,
            'size' => $width . 'x' . $height,
            'filename' => $this->name,
        ]);
    }
}
