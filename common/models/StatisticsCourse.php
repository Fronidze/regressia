<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%statistics_course}}".
 *
 * @property int $id
 * @property int $schedule_id
 * @property int $records
 * @property int $total_completed
 * @property int $listened
 * @property int $completed
 * @property int $accepted
 *
 * @property Schedule $schedule
 */
class StatisticsCourse extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%statistics_course}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['schedule_id', 'records', 'total_completed', 'listened', 'completed', 'accepted'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'schedule_id' => 'Schedule ID',
            'records' => 'Онлайн заявки',
            'total_completed' => 'Окончили',
            'listened' => 'Прослушали',
            'completed' => 'Выполнили',
            'accepted' => 'Вышли на стажировку',
        ];
    }

    public function getSchedule()
    {
        return $this->hasOne(Schedule::class, ['id' => 'schedule_id']);
    }
}
