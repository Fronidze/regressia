<?php

namespace common\models;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "weekly_exam_preparation".
 *
 * @property int $id
 * @property int $exam_preparation_id
 * @property string $monday
 * @property string $tuesday
 * @property string $wednesday
 * @property string $thursday
 * @property string $friday
 * @property string $saturday
 * @property string $sunday
 */
class WeeklyExamPreparation extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'weekly_exam_preparation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['exam_preparation_id'], 'required'],
            [['exam_preparation_id'], 'integer'],
            [['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'                  => 'ID',
            'exam_preparation_id' => 'Рассписание',
            'monday'              => 'Понидельник',
            'tuesday'             => 'Вторник',
            'wednesday'           => 'Среда',
            'thursday'            => 'Четверг',
            'friday'              => 'Пятница',
            'saturday'            => 'Суббота',
            'sunday'              => 'Воскресенье',
        ];
    }

    public function prepositionalLabels()
    {
        return [
            'monday'    => 'Понедельникам',
            'tuesday'   => 'Вторникам',
            'wednesday' => 'Средам',
            'thursday'  => 'Четвергам',
            'friday'    => 'Пятницам',
            'saturday'  => 'Субботам',
            'sunday'    => 'Воскресеньям',
        ];
    }

    public function numberWeekDay()
    {
        return [
            'monday'    => 1,
            'tuesday'   => 2,
            'wednesday' => 3,
            'thursday'  => 4,
            'friday'    => 5,
            'saturday'  => 6,
            'sunday'    => 7,
        ];
    }

    public function selectedWeekDay()
    {
        $attributes = $this->getAttributes();
        unset($attributes['id']);
        unset($attributes['exam_preparation_id']);

        $result = [];
        $numberWeek = $this->numberWeekDay();
        foreach ($attributes as $field => $value) {
            if (empty($value) === false) {
                $result[$numberWeek[$field]] = $value;
            }
        }

        return $result;
    }

    public function getFullWeekExamPreparation()
    {
        $attributes = $this->getAttributes();
        $prepositional = $this->prepositionalLabels();
        $result = [];
        foreach ($attributes as $code => $attribute) {
            if ($code === 'id' || $code === 'exam_preparation_id') {
                continue;
            }

            if ($attribute === null || $attribute === '') {
                continue;
            }

            $result[] = [
                'label_week' => ArrayHelper::getValue($prepositional, $code, $this->getAttributeLabel($code)),
                'time'       => $attribute
            ];
        }

        return $result;
    }
}
