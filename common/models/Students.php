<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%students}}".
 *
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $social
 * @property string $abilities
 *
 * @property Record[] $records
 * @property Record[] $listened
 * @property Record[] $completed
 */
class Students extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%students}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
            [['email', 'name', 'social'], 'string', 'max' => 255],
            [['abilities'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-Mail',
            'name' => 'ФИО',
            'social' => 'Ссылка на профиль в соц. сетях',
            'abilities' => 'Текущие знания',
        ];
    }

    public function getRecords()
    {
        return $this->hasMany(Record::class, ['student_id' => 'id']);
    }

    public function getListened()
    {
        return $this->hasMany(Record::class, ['student_id' => 'id'])
            ->andOnCondition(['=', 'listened', Record::RESULT_POSITIVE]);
    }

    public function getCompleted()
    {
        return $this->hasMany(Record::class, ['student_id' => 'id'])
            ->andOnCondition(['=', 'completed', Record::RESULT_POSITIVE]);
    }

}
