<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%record}}".
 *
 * @property int $id
 * @property int $student_id
 * @property int $schedule_id
 * @property string $date_create
 * @property string $rating
 * @property int $listened
 * @property int $completed
 * @property int $accepted
 *
 * @property Schedule $schedule
 * @property Students $student
 */
class Record extends ActiveRecord
{

    const RESULT_POSITIVE = 1;
    const RESULT_NEGATIVE = 0;

    public static function tableName()
    {
        return '{{%record}}';
    }

    public function rules()
    {
        return [
            [['date_create'], 'filter', 'filter' => function ($value) {
                if (empty($value) === false) {
                    return (new \DateTime($value))->format('Y-m-d') . ' ' . (new \DateTime('now', new \DateTimeZone('Europe/Samara')))->format('H:i:s');
                }
                return null;
            }],
            [['listened', 'completed', 'accepted'], 'default', 'value' => static::RESULT_NEGATIVE],
            [['date_create'], 'default', 'value' => (new \DateTime('now', new \DateTimeZone('Europe/Samara')))->format('Y-m-d H:i:s')],
            [['student_id', 'schedule_id'], 'required'],
            [['student_id', 'schedule_id', 'listened', 'completed', 'accepted', 'rating'], 'integer'],
            [['date_create'], 'safe'],
            [
                ['student_id', 'schedule_id'],
                'unique',
                'targetAttribute' => ['student_id', 'schedule_id'],
                'message' => 'Данный стужент уже записан на данное направление'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_id' => 'Студент',
            'schedule_id' => 'Курс',
            'date_create' => 'Дата записи',
            'listened' => 'Прослушал',
            'completed' => 'Выполнил итоговое задание',
            'accepted' => 'Принят на стажировку',
        ];
    }

    public function getSchedule()
    {
        return $this->hasOne(Schedule::class, ['id' => 'schedule_id']);
    }

    public function getStudent()
    {
        return $this->hasOne(Students::class, ['id' => 'student_id']);
    }

    public function listTechnology()
    {
        $query = (new Query())
            ->select(['id', 'title'])
            ->from(Technology::tableName())
            ->orderBy(['title' => SORT_ASC]);

        return ArrayHelper::map($query->all(), 'id', 'title');
    }

    public function listSchedule()
    {
        $query = (new Query())
            ->select(['s.id', 's.date_start', 't.title'])
            ->from(['s' => Schedule::tableName()])
            ->innerJoin(['t' => Technology::tableName()], ['s.technology_id' => new Expression('t.id')])
            ->orderBy(['s.sorting' => SORT_ASC]);

        $records = $query->all();
        $result = [];
        foreach ($records as $record) {
            $result[$record['id']] = \Yii::t('app', '{title} ({date_start})', [
                'title' => $record['title'],
                'date_start' => \Yii::$app->formatter->asDate($record['date_start'], 'long'),
            ]);
        }

        return $result;
    }

    public function listStudents()
    {
        $query = (new Query())
            ->select(['id', 'name', 'email'])
            ->from(Students::tableName())
            ->orderBy(['email' => SORT_ASC]);

        $records = $query->all();
        $result = [];
        foreach ($records as $record) {
            $result[$record['id']] = \Yii::t('app', '{name} ({email})', [
                'name' => $record['name'],
                'email' => $record['email'],
            ]);
        }

        return $result;
    }

}
