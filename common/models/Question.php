<?php

namespace common\models;

use common\models\abstractions\PublishActiveRecord;

/**
 * This is the model class for table "questions".
 *
 * @property int $id
 * @property string $question
 * @property string $answer
 * @property int $is_publish
 * @property int $sorting
 * @property string $create_at
 * @property string $update_at
 */
class Question extends PublishActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%questions}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question', 'answer'], 'required'],
            [['is_publish', 'sorting'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['question', 'answer'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'is_publish' => 'Публикация на главной странице',
            'sorting' => 'Сортировка',
            'create_at' => 'Дата создания',
            'update_at' => 'Дата обновления'
        ];
    }
}
