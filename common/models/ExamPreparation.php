<?php

namespace common\models;

use backend\models\User;
use services\exam\ExamPreparationService;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "exam_preparation".
 *
 * @property int $id
 * @property string $title
 * @property string $sub_title
 * @property string $date_start
 * @property string $date_completion
 * @property string $publish
 * @property string $empty_date
 * @property string $register
 * @property string $status
 * @property string $sub_status
 * @property string $code
 * @property int $lecturer
 * @property int $responsible_id
 * @property int $template_exam_id
 * @property string $create_at
 * @property string $update_at
 *
 * @property WeeklyExamPreparation $weekly
 */
class ExamPreparation extends ActiveRecord
{
    const PUBLISH_OPEN = 'yes';
    const PUBLISH_CLOSE = 'no';

    const REGISTER_OPEN = 'yes';
    const REGISTER_CLOSE = 'no';

    const KNOW_DATE = 'yes';
    const NOT_KNOW_DATE = 'no';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exam_preparation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'code', 'responsible_id'], 'required'],
            [['status', 'sub_status'], 'required', 'when' => function ($model) {
                return $model->empty_date === static::NOT_KNOW_DATE;
            }],
            [
                ['date_start'], 'required', 'when' => function ($model) {
                return $model->empty_date === static::KNOW_DATE;
            }, 'whenClient' => "function (attribute, value) {
                    return $('#empty_date').val() === 'yes';
                }"
            ],
            [['date_start', 'date_completion', 'create_at', 'update_at'], 'safe'],
            [['responsible_id', 'template_exam_id'], 'integer'],
            [['title', 'sub_title', 'publish', 'code', 'lecturer', 'status', 'sub_status', 'register', 'empty_date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'Индетификатор',
            'title'            => 'Название на странице курса',
            'sub_title'        => 'Короткое название',
            'date_start'       => 'Дата старта',
            'date_completion'  => 'Дата окончания',
            'publish'          => 'Публикация на главной странице',
            'empty_date'       => 'Известна дата?',
            'status'           => 'Статус курса для страницы курса',
            'sub_status'       => 'Статус курса для главной страницы',
            'register'         => 'Открыта ли регистрация на курс',
            'code'             => 'Символьный код',
            'lecturer'         => 'Куратор',
            'responsible_id'   => 'Координатор',
            'template_exam_id' => 'Шаблон',
            'create_at'        => 'Создано',
            'update_at'        => 'Обновлено',
        ];
    }

    public function isPublish()
    {
        return $this->publish === static::PUBLISH_OPEN;
    }

    public function getWeeklyExamPreparation()
    {
        /** @var WeeklyExamPreparation $weekly */
        $weekly = ArrayHelper::getValue($this, 'weeklyExam');
        if ($weekly === null) {
            return [];
        }

        return $weekly->getFullWeekExamPreparation();
    }

    public function getExamPreparationDateRange()
    {
        return \Yii::t('app', 'с {from} по {to}', [
            'from' => \Yii::$app->formatter->asDate($this->date_start),
            'to'   => \Yii::$app->formatter->asDate($this->date_completion),
        ]);
    }

    public function getWeeklyExamPreparationForPage()
    {
        $records = $this->getWeeklyExamPreparation();
        if (count($records) === 1) {
            return \Yii::t('app', 'по {day} в {time}', [
                'day'  => mb_strtolower($records[0]['label_week']),
                'time' => $records[0]['time'],
            ]);
        }

        if (count($records) === 2) {
            return \Yii::t('app', 'по {day} и {end_day} в {time}', [
                'day'     => mb_strtolower($records[0]['label_week']),
                'end_day' => mb_strtolower($records[1]['label_week']),
                'time'    => $records[0]['time'],
            ]);
        }

        $last = array_pop($records);
        $days = [];
        foreach ($records as $record) {
            $days[] = mb_strtolower($record['label_week']);
        }

        return \Yii::t('app', 'по {all_day} и {end_day} в {time}', [
            'all_day' => implode(', ', $days),
            'end_day' => mb_strtolower($last['label_week']),
            'time'    => $last['time'],
        ]);
    }

    public function getResponsible()
    {
        return $this->hasOne(User::class, ['id' => 'responsible_id']);
    }

    public function getTemplateExam()
    {
        return $this->hasOne(TemplateExamPreparation::class, ['id' => 'template_exam_id']);
    }

    public function getWeeklyExam()
    {
        return $this->hasOne(WeeklyExamPreparation::class, ['exam_preparation_id' => 'id']);
    }

    public function getActiveStatusLabel()
    {
        return ExamPreparationService::instance()->statusLabelForDates([$this->id]);
    }

    public function isFinished()
    {
        if ($this->date_completion === null) {
            return false;
        }
        $now = new \DateTime();
        $end = new \DateTime($this->date_completion);
        return $now > $end;
    }

    public function canRegister()
    {
        $start = (new \DateTime($this->date_start))->add(new \DateInterval('P7D'));
        $now = new \DateTime();

        if ($this->empty_date !== static::NOT_KNOW_DATE) {
            return $now < $start;
        }
        return $this->register === ExamPreparation::REGISTER_OPEN;
    }
}
