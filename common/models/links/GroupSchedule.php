<?php

namespace common\models\links;

use common\models\Schedule;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%group_schedule}}".
 *
 * @property int $id
 * @property int $schedule_id
 * @property int $group_id
 *
 * @property Schedule $schedule
 */
class GroupSchedule extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%group_schedule}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['schedule_id', 'group_id'], 'required'],
            [['schedule_id', 'group_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'schedule_id' => 'Schedule ID',
            'group_id' => 'Group ID',
        ];
    }

    public function getSchedule()
    {
        return $this->hasOne(Schedule::class, ['id' => 'schedule_id']);
    }
}
