<?php

namespace common\models\links;

use backend\models\User;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%participants_event}}".
 *
 * @property int $id
 * @property int $event_id
 * @property int $user_id
 *
 * @property User $user
 */
class ParticipantsEvent extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%participants_event}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'user_id'], 'required'],
            [['event_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'user_id' => 'User ID',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
