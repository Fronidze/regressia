<?php

namespace common\models;

use backend\models\User;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;

/**
 * This is the model class for table "technology".
 *
 * @property int $id
 * @property string $title
 * @property string $left_description
 * @property string $right_description
 * @property string $program
 * @property string $create_at
 * @property string $update_at
 * @property int $update_by
 * @property string $sub_title
 * @property string $short_title
 *
 * @property User $editor
 */
class Technology extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%technology}}';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['left_description', 'right_description', 'program', 'sub_title', 'short_title'], 'string'],
            [['create_at', 'update_at'], 'safe'],
            [['update_by'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Индетификатор',
            'title' => 'Название технологии',
            'sub_title' => 'Название на странице курса',
            'short_title' => 'Короткое название',
            'left_description' => 'Для кого',
            'right_description' => 'Что дает курс',
            'program' => 'Программа',
            'create_at' => 'Дата создания',
            'update_at' => 'Дата обновления',
            'update_by' => 'Редактировал',
        ];
    }

    public function getEditor()
    {
        return $this->hasOne(User::class, ['id' => 'update_by']);
    }

    public function getCourseRequest($date = null)
    {
        $query = (new Query())
            ->from(CourseRequest::tableName())
            ->where(['=', 'code_technology_or_group', $this->short_title]);

        if ($date !== null) {
            $query->andWhere([
                'and',
                ['=', new Expression('YEAR(date_create)'), new Expression('YEAR(:date)', ['date' => $date])],
                ['=', new Expression('MONTH(date_create)'), new Expression('MONTH(:date)', ['date' => $date])],
            ]);
        }

        return $query->count();
    }
}
