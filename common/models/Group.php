<?php

namespace common\models;

use backend\models\User;
use common\models\links\GroupSchedule;
use services\schedule\ScheduleService;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%group}}".
 *
 * @property int $id
 * @property string $code
 * @property string $title
 * @property string $full_title
 * @property string $date_start
 * @property string $left_side
 * @property string $right_side
 * @property string $table
 * @property string $table_mobile
 * @property int $responsible
 * @property int $sorting
 * @property int $publish
 *
 * @property GroupSchedule $schedules
 * @property User $person
 */
class Group extends ActiveRecord
{

    const PUBLISH_NOT = 0;
    const PUBLISH_YES = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%group}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['date_start'],
                'filter',
                'filter' => function ($value) {
                    return (new \DateTime($value))->format('Y-m-d');
                }
            ],
            [['title', 'date_start', 'responsible'], 'required'],
            [['date_start'], 'safe'],
            [['left_side', 'right_side', 'table','table_mobile', 'code'], 'string'],
            [['responsible', 'sorting', 'publish'], 'integer'],
            [['title', 'full_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Симольный код',
            'title' => 'Название',
            'full_title' => 'Полное название',
            'date_start' => 'Дата старта',
            'left_side' => 'Левая часть',
            'right_side' => 'Правая часть',
            'table' => 'Расписание для десктопа/планшета',
            'table_mobile' => 'Расписание для телефонов',
            'responsible' => 'Куратор группы',
            'publish' => 'Публиковать',
            'sorting' => 'Сортировка',
        ];
    }

    public function isPublish()
    {
        return $this->publish === static::PUBLISH_YES;
    }

    public function getDateStart()
    {
        return \Yii::$app->formatter->asDate($this->date_start, 'long');
    }

    public function generateBeginLabel()
    {
        $sub_query = (new Query())
            ->select('schedule_id')
            ->from(GroupSchedule::tableName())
            ->where(['=', 'group_id', $this->id]);

        return ScheduleService::instance()->statusLabelForDates($sub_query->column());
    }

    public function getSchedules()
    {
        return $this->hasMany(GroupSchedule::class, ['group_id' => 'id'])
            ->innerJoin(['schedule' => Schedule::tableName()], ['schedule.id' => new Expression('schedule_id')])
            ->orderBy(['schedule.sorting' => SORT_ASC]);
    }

    public function getPerson()
    {
        return $this->hasOne(User::class, ['id' => 'responsible']);
    }
}
