<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "record_schoolchild".
 *
 * @property int $id
 * @property int $schoolchild_id
 * @property int $exam_preparation_id
 * @property string $date_create
 */
class RecordSchoolchild extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'record_schoolchild';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['schoolchild_id', 'exam_preparation_id'], 'required'],
            [['schoolchild_id', 'exam_preparation_id'], 'integer'],
            [['date_create'], 'filter', 'filter' => function ($value) {
                if (empty($value) === false) {
                    return (new \DateTime($value))->format('Y-m-d') . ' ' . (new \DateTime('now', new \DateTimeZone('Europe/Samara')))->format('H:i:s');
                }
                return null;
            }],
            [['date_create'], 'default', 'value' => (new \DateTime('now', new \DateTimeZone('Europe/Samara')))->format('Y-m-d H:i:s')],
            [['date_create'], 'safe'],
            [
                ['schoolchild_id', 'exam_preparation_id'],
                'unique',
                'targetAttribute' => ['schoolchild_id', 'exam_preparation_id'],
                'message'         => 'Данный школьник уже записан на данное направление'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                  => 'ID',
            'schoolchild_id'      => 'Школьник',
            'exam_preparation_id' => 'Курс ЕГЭ',
            'date_create'         => 'Дата записи',
        ];
    }

    public function getExamPreparation()
    {
        return $this->hasOne(ExamPreparation::class, ['id' => 'exam_preparation_id']);
    }

    public function getSchoolchild()
    {
        return $this->hasOne(Schoolchild::class, ['id' => 'schoolchild_id']);
    }

    public function listExamPreparation()
    {
        $query = (new Query())
            ->select(['id', 'sub_title'])
            ->from(ExamPreparation::tableName())
            ->orderBy(['sub_title' => SORT_ASC]);

        return ArrayHelper::map($query->all(), 'id', 'sub_title');
    }


    public function listSchoolchild()
    {
        $query = (new Query())
            ->select(['id', 'name', 'email'])
            ->from(Schoolchild::tableName())
            ->orderBy(['email' => SORT_ASC]);

        $records = $query->all();
        $result = [];
        foreach ($records as $record) {
            $result[$record['id']] = \Yii::t('app', '{name} ({email})', [
                'name'  => $record['name'],
                'email' => $record['email'],
            ]);
        }

        return $result;
    }
}
