<?php

namespace common\models;

use backend\models\User;
use services\schedule\ScheduleService;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%schedule}}".
 *
 * @property int $id
 * @property int $technology_id
 * @property int $teacher_id
 * @property string $program
 * @property string $date_start
 * @property string $date_completion
 * @property string $date_last_lesson
 * @property int $quantity_lesson
 * @property int $quantity_vacancies
 * @property int $quantity_organizations
 * @property string $date_create
 * @property string $date_update
 * @property int $update_by
 * @property string $status
 * @property string $code
 * @property string $publish
 * @property string $rework
 * @property int $sorting
 * @property int $responsible_id
 *
 * @property Technology $technology
 * @property Teacher $teacher
 * @property User $editor
 * @property WeeklySchedule $weekly
 * @property User $responsible
 */
class Schedule extends ActiveRecord
{

    const STATUS_DRAFT = 'draft';
    const STATUS_ACTIVE = 'active';
    const STATUS_ARCHIVE = 'archive';

    const PUBLISH_OPEN = 'yes';
    const PUBLISH_CLOSE = 'no';

    const REWORK_OPEN = 'yes';
    const REWORK_CLOSE = 'no';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%schedule}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'default', 'value' => static::STATUS_DRAFT],
            [['technology_id', 'teacher_id', 'quantity_lesson', 'update_by', 'sorting', 'responsible_id', 'quantity_vacancies', 'quantity_organizations'], 'integer'],
            [['program', 'code', 'publish'], 'string'],
            [['date_start', 'date_completion', 'date_last_lesson', 'date_create', 'date_update'], 'safe'],
            [['status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Символьный код',
            'technology_id' => 'Направления курсов',
            'teacher_id' => 'Куратор',
            'program' => 'Программа',
            'date_start' => 'Дата старта',
            'date_completion' => 'Дата окончания',
            'date_last_lesson' => 'Дата последнего занатия',
            'quantity_lesson' => 'Кол-во занятий',
            'quantity_vacancies' => 'Кол-во вакансий',
            'quantity_organizations' => 'Кол-во организаций',
            'publish' => 'Публикация на главной странице',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'update_by' => 'Update By',
            'status' => 'Статус курса',
            'sorting' => 'Сортировка',
            'responsible_id' => 'Координатор',
        ];
    }

    public function statusLabels()
    {
        return [
            static::STATUS_DRAFT => 'Черновик',
            static::STATUS_ACTIVE => 'Активный',
            static::STATUS_ARCHIVE => 'Архив',
        ];
    }

    public function getStatusLabel(string $status)
    {
        $labels = $this->statusLabels();
        return $labels[$status] ?? $status;
    }

    public function isDraft()
    {
        return $this->status === static::STATUS_DRAFT;
    }

    public function isActive()
    {
        return $this->status === static::STATUS_ACTIVE;
    }

    public function isArchive()
    {
        return $this->status === static::STATUS_ARCHIVE;
    }

    public function isPublish()
    {
        return $this->publish === static::PUBLISH_OPEN;
    }

    public function isFinished()
    {
        if ($this->date_completion === null) {
            return false;
        }

        $now = new \DateTime();
        $end = new \DateTime($this->date_completion);

        return $this->isActive() && ($now > $end);
    }

    public function canRegister()
    {
        if ($this->isActive() === false) {
            return false;
        }

        $start = (new \DateTime($this->date_start))->add(new \DateInterval('P7D'));
        $now = new \DateTime();
        
        return $now < $start;
    }

    public function getActiveStatusLabel()
    {
        if ($this->isActive() === false) {
            return false;
        }

        return ScheduleService::instance()->statusLabelForDates([$this->id]);
    }

    public function getScheduleTitle()
    {
        return \Yii::t('app', '{title} ({date})', [
            'title' => $this->getTechnologyTitle(),
            'date' => \Yii::$app->formatter->asDate($this->date_start, 'long'),
        ]);
    }

    public function getTechnologyTitle()
    {
        return ArrayHelper::getValue($this, 'technology.title');
    }

    public function getTechnologySubTitle()
    {
        return ArrayHelper::getValue($this, 'technology.sub_title');
    }

    public function getTeacherName()
    {
        return ArrayHelper::getValue($this, 'teacher.name');
    }

    public function getWeeklySchedule()
    {
        /** @var WeeklySchedule $weekly */
        $weekly = ArrayHelper::getValue($this, 'weekly');
        if ($weekly === null) {
            return [];
        }

        return $weekly->getFullWeekSchedule();
    }

    public function getWeeklyScheduleForPage()
    {
        $records = $this->getWeeklySchedule();
        if (count($records) === 1) {
            return \Yii::t('app', 'по {day} в {time}', [
                'day' => mb_strtolower($records[0]['label_week']),
                'time' => $records[0]['time'],
            ]);
        }

        if (count($records) === 2) {
            return \Yii::t('app', 'по {day} и {end_day} в {time}', [
                'day' => mb_strtolower($records[0]['label_week']),
                'end_day' => mb_strtolower($records[1]['label_week']),
                'time' => $records[0]['time'],
            ]);
        }

        $last = array_pop($records);
        $days = [];
        foreach ($records as $record) {
            $days[] = mb_strtolower($record['label_week']);
        }

        return \Yii::t('app', 'по {all_day} и {end_day} в {time}', [
            'all_day' => implode(', ', $days),
            'end_day' => mb_strtolower($last['label_week']),
            'time' => $last['time'],
        ]);

    }

    public function getScheduleDateRange()
    {
        return \Yii::t('app', 'с {from} по {to}', [
            'from' => \Yii::$app->formatter->asDate($this->date_start),
            'to' => \Yii::$app->formatter->asDate($this->date_completion),
        ]);
    }

    public function getTechnology()
    {
        return $this->hasOne(Technology::class, ['id' => 'technology_id']);
    }

    public function getTeacher()
    {
        return $this->hasOne(Teacher::class, ['id' => 'teacher_id']);
    }

    public function getEditor()
    {
        return $this->hasOne(User::class, ['id' => 'update_by']);
    }

    public function getWeekly()
    {
        return $this->hasOne(WeeklySchedule::class, ['schedule_id' => 'id']);
    }

    public function getResponsible()
    {
        return $this->hasOne(User::class, ['id' => 'responsible_id']);
    }

    public function getTotalRecordCount()
    {
        $query = (new Query())
            ->from(Record::tableName())
            ->where(['=', 'schedule_id', $this->id]);

        return $query->count();
    }

    public function getTotalCompleted()
    {
        $query = (new Query())
            ->from(Record::tableName())
            ->where([
                'and',
                ['=', 'schedule_id', $this->id],
                ['or', ['=', 'listened', 1], ['=', 'completed', 1]]
            ]);


        return $query->count();
    }

    public function getListened()
    {
        $query = (new Query())
            ->from(Record::tableName())
            ->where(['and', ['=', 'schedule_id', $this->id], ['=', 'listened', 1]]);

        return $query->count();
    }

    public function getCompleted()
    {
        $query = (new Query())
            ->from(Record::tableName())
            ->where(['and', ['=', 'schedule_id', $this->id], ['=', 'completed', 1]]);

        return $query->count();
    }

    public function getAccepted()
    {
        $query = (new Query())
            ->from(Record::tableName())
            ->where(['and', ['=', 'schedule_id', $this->id], ['=', 'accepted', 1]]);

        return $query->count();
    }
}
