<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "course_request".
 *
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $university
 * @property string $code_technology_or_group
 * @property string $date_create
 *
 * @property Technology[] $technologyRecord
 */
class CourseRequest extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%course_request}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email', 'name', 'university', 'date_create', 'code_technology_or_group'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-Mail',
            'name' => 'Ф.И.О.',
            'university' => 'ВУЗ',
            'code_technology_or_group' => 'Направление',
            'date_create' => 'Дата создания',
        ];
    }

    public function getTechnologyRecord()
    {
        return $this->hasOne(Technology::class, ['short_title' => 'code_technology_or_group']);
    }

    public function getGroupRecord()
    {
        return $this->hasOne(Group::class, ['code' => 'code_technology_or_group']);
    }
}
