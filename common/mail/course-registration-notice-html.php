<?php

/**
 * @var $name string
 * @var $schedule_title string
 * @var $schedule_date_start string
 * @var $academy_logo string
 * @var $message Message
 */

use yii\swiftmailer\Message; ?>
<table width="524" align="center" valign="top" border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;">
    <tbody>

    <tr>
        <td height="45" style="height: 45px;line-height: 45px;"></td>
    </tr>

    <tr width="100%" height="106" style="background-color:#000000;">
        <td width="80%" align="center" style="padding: 16px 0;">
            <img width="450" height="31" src="<?= $message->embed($academy_logo) ?>" style="border:0; outline:none; text-decoration:none; display:block;" alt="Академия разработки MediaSoft" title="Академия разработки MediaSoft">
        </td>
    </tr>

    <tr>
        <td width="100%" align="center"
            style="width: 100%;font-size: 20px;line-height: 27px;letter-spacing:0.3px;font-family: 'Arial';text-align: left;white-space:pre-line;padding: 0 20px;">
            Здравствуйте, <?= $name ?>! Подтверждена ваша регистрация на курс <?= $schedule_title ?>.
        </td>
    </tr>
    <tr>
        <td width="100%" align="center"
            style="width: 100%;font-size: 20px;line-height: 27px;letter-spacing:0.3px;font-family: 'Arial';text-align: left;white-space:pre-line;padding: 0 20px;">
            Ждем вас <?= $schedule_date_start ?> в Академии разработки MediaSoft <a href="https://www.google.com/maps/place/Mediasoft/@54.3198291,48.3937825,17z/data=!3m1!4b1!4m5!3m4!1s0x415d375cebcae22f:0x4d651807d5566ee2!8m2!3d54.3198291!4d48.3959711" target="_blank"><span style="color: #0bcf6b;"> (ТЦ Амарант, третий этаж, вход со стороны парковки)</span></a>.
        </td>
    </tr>
    <tr>
        <td width="100%" align="center"
            style="width: 100%;font-size: 20px;line-height: 27px;letter-spacing:0.3px;font-family: 'Arial';text-align: left;white-space:pre-line;padding: 0 20px;">
            Остались вопросы? Пишите в ответном письме.
        </td>
    </tr>
    <tr>
        <td width="100%" align="center"
            style="width: 100%;font-size: 20px;line-height: 27px;letter-spacing:0.3px;font-family: 'Arial';text-align: left;white-space:pre-line;padding: 0 20px;">
            Подписывайтесь на нас <a href="https://vk.com/mediasoft.academy" target="_blank"><span
                        style="color: #0bcf6b;">Вконтакте</span></a> и в <a href="https://t.me/mediasoft_academy"
                                                                            target="_blank"><span
                        style="color: #0bcf6b;">Телеграме</span></a>, чтобы узнавать о новостях первыми.
        </td>
    </tr>
    </tbody>
</table>

