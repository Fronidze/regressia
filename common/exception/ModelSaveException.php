<?php

namespace common\exception;

use Throwable;
use yii\base\Model;

class ModelSaveException extends \Exception
{
    public $model = null;

    public function __construct(Model $model, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->model = $model;
        if ($message === '') {
            $message = \Yii::t('app', 'Ошибка сохранение или обнволения модели: {model}', ['model' => get_class($model)]);
        }

        parent::__construct($message, $code, $previous);
    }
}