<?php
return [
    'adminEmail' => getenv('MAILER_FROM_LOGIN'),
    'supportEmail' => getenv('MAILER_FROM_LOGIN'),
    'senderEmail' => getenv('MAILER_FROM_LOGIN'),
    'senderPassword' => getenv('MAILER_FROM_PASS'),
    'senderName' => 'Академия разработки MediaSoft',

    'user.passwordResetTokenExpire' => 3600,

];
