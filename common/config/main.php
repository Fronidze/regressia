<?php

use yii\mutex\MysqlMutex;
use yii\queue\db\Queue;
use yii\swiftmailer\Mailer;

$params = array_merge(
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'language' => 'ru_RU',
    'bootstrap' => ['queue'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => ['class' => 'yii\caching\FileCache'],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'locale' => 'ru_RU',
            'timeZone' => 'Europe/Samara',
            'defaultTimeZone'=> 'Europe/Samara',
        ],
        'queue' => [
            'class' => Queue::class,
            'db' => 'db',
            'tableName' => '{{%queue}}',
            'channel' => 'default',
            'mutex' => MysqlMutex::class,
        ],
        'mailer' => [
            'class' => Mailer::class,
            'enableSwiftMailerLogging' => true,
            'useFileTransport' => false,
            'htmlLayout' => '@common/mail/layouts/html',
            'textLayout' => '@common/mail/layouts/text',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'port' => '587',
                'encryption' => 'tls',
                "username" => $params['senderEmail'],
                "password" => $params['senderPassword'],
            ],
        ],
    ],
];
