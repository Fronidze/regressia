<?php

namespace api\resources\fake;

use common\models\Schedule;
use yii\helpers\ArrayHelper;

class CoursesFakeResource extends Schedule
{
    public function fields()
    {
        return [
            'id' => function (Schedule $record) {
                return ArrayHelper::getValue($record, 'id');
            },
            'technology_id' => function (Schedule $record) {
                return ArrayHelper::getValue($record, 'technology_id');
            },
            'technology_name' => function (Schedule $record) {
                return ArrayHelper::getValue($record, 'technology.title');
            },
            'teacher_id' => function (Schedule $record) {
                return ArrayHelper::getValue($record, 'teacher_id');
            },
            'teacher_name' => function (Schedule $record) {
                return ArrayHelper::getValue($record, 'teacher.name');
            },
            'status' => function (Schedule $record) {
                return ArrayHelper::getValue($record, 'status');
            },
            'code' => function (Schedule $record) {
                return ArrayHelper::getValue($record, 'code');
            },
            'publish' => function (Schedule $record) {
                return $record->publish === Schedule::PUBLISH_OPEN;
            },
            'sorting' => function (Schedule $record) {
                return ArrayHelper::getValue($record, 'sorting');
            },
        ];
    }

}