<?php

namespace api\helpers;

use yii\helpers\ArrayHelper;
use yii\web\ErrorHandler;

class ApiErrorHandler extends ErrorHandler
{
    protected function convertExceptionToArray($exception)
    {
        $data = parent::convertExceptionToArray($exception);
        return [
            'status' => 'error',
            'message' => ArrayHelper::getValue($data, 'message'),
        ];
    }
}