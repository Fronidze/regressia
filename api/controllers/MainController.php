<?php

namespace api\controllers;

use api\resources\fake\CoursesFakeResource;
use yii\rest\Controller;

class MainController extends Controller
{
    public function actionIndex()
    {
        return CoursesFakeResource::find()
            ->orderBy(['sorting' => SORT_ASC])
            ->all();
    }
}