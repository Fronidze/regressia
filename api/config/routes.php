<?php

return [
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'main',
        'pluralize' => false,
    ],
];