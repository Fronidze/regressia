<?php

namespace services\technology;

use backend\forms\technology\Create;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\Schedule;
use common\models\Technology;
use services\BaseService;
use yii\helpers\ArrayHelper;

class TechnologyService extends BaseService
{
    public function create(Create $form)
    {
        $technology = new Technology();
        $technology->setAttributes($form->getAttributes());
        $technology->create_at = (new \DateTime())->format('Y-m-d H:i:s');
        $technology->update_at = (new \DateTime())->format('Y-m-d H:i:s');
        $technology->update_by = \Yii::$app->user->getId();

        if ($technology->validate() === false) {
            throw new ModelValidateException($technology);
        }

        if ($technology->save(false) === false) {
            throw new ModelSaveException($technology);
        }
    }

    public function update(Create $form)
    {
        $technology = Technology::findOne($form->id);
        $technology->setAttributes($form->getAttributes());
        $dirty = $technology->getDirtyAttributes();
        if ($new_program = ArrayHelper::getValue($dirty, 'program')) {
            $this->updateProgram($technology->id, $new_program);
        }

        $technology->update_at = (new \DateTime())->format('Y-m-d H:i:s');
        $technology->update_by = \Yii::$app->user->getId();

        if ($technology->validate() === false) {
            throw new ModelValidateException($technology);
        }

        if ($technology->save(false) === false) {
            throw new ModelSaveException($technology);
        }
    }

    protected function updateProgram(int $id, string $program)
    {
        \Yii::$app->db->createCommand()
            ->update(Schedule::tableName(), ['program' => $program], [
                'and',
                ['=', 'technology_id', $id],
                ['in', 'status', [Schedule::STATUS_DRAFT, Schedule::STATUS_ACTIVE]]
            ])
            ->execute();
    }
}