<?php

namespace services\technology;

use common\models\links\TeacherAbility;
use services\BaseService;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class AbilityService extends BaseService
{

    public function insert(int $teacher_id, array $abilities)
    {
        $identifiers = array_values($abilities);
        $this->oldRemove($teacher_id, $identifiers);
        $this->attach($teacher_id, $identifiers);
    }

    protected function oldRemove(int $teacher, array $identifiers)
    {
        \Yii::$app->db->createCommand()
            ->delete(TeacherAbility::tableName(), [
                'and',
                ['=', 'teacher_id', $teacher],
                ['not in', 'ability_id', $identifiers]
            ])
            ->execute();
    }

    protected function attach(int $teacher_id, array $identifiers)
    {
        $query = (new Query())
            ->select(['ability' => 'link.ability_id'])
            ->from(['link' => TeacherAbility::tableName()])
            ->where(['=', 'teacher_id', $teacher_id]);

        $different_abilities = array_diff($identifiers, ArrayHelper::getColumn($query->all(), 'ability', []));
        if (empty($different_abilities) === false) {

            $data = [];
            foreach ($different_abilities as $ability) {
                $data[] = [$teacher_id, $ability];
            }

            \Yii::$app->db->createCommand()
                ->batchInsert(TeacherAbility::tableName(), ['teacher_id', 'ability_id'], $data)
                ->execute();

        }

    }

}