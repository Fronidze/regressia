<?php

namespace services\profile;

use backend\forms\schoolchild\CreateSchoolchild;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\Schoolchild;
use services\BaseService;

class SchoolchildService extends BaseService
{

    public function find(int $id)
    {
        $schoolchild = Schoolchild::find()
            ->where(['=', 'id', $id])
            ->one();

        $form = new CreateSchoolchild();
        if ($schoolchild !== null) {
            $form->setAttributes($schoolchild->getAttributes());
            $form->id = $schoolchild->id;
        }

        return $form;
    }

    public function insert(CreateSchoolchild $form)
    {
        if ($form->id === null) {
            $this->create($form);
        } else {
            $this->update($form);
        }
    }

    protected function update(CreateSchoolchild $form)
    {
        $schoolchild = Schoolchild::find()
            ->where(['=', 'id', $form->id])
            ->one();

        $attributes = $form->getAttributes();
        if (empty($schoolchild->social) === false && empty($form->social) === true) {
            unset($attributes['social']);
        }
        $schoolchild->setAttributes($attributes);
        if ($schoolchild->validate() === false) {
            throw new ModelValidateException($schoolchild);
        }

        if ($schoolchild->save(false) === false) {
            throw new ModelSaveException($schoolchild);
        }

    }

    protected function create(CreateSchoolchild $form)
    {
        $schoolchild = Schoolchild::find()->where(['=', 'email', $form->email])->one();
        $attributes = $form->getAttributes();
        if ($schoolchild === null) {
            $schoolchild = new Schoolchild();
            $schoolchild->create_at = (new \DateTime())->format('Y-m-d H:i:s');
            $schoolchild->update_at = (new \DateTime())->format('Y-m-d H:i:s');
        } else {
            if (empty($schoolchild->social) === false && empty($form->social) === true) {
                unset($attributes['social']);
            }
        }

        $schoolchild->setAttributes($attributes);
        if ($schoolchild->validate() === false) {
            throw new ModelValidateException($schoolchild);
        }

        if ($schoolchild->save(false) === false) {
            throw new ModelSaveException($schoolchild);
        }
    }
}