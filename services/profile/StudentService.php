<?php

namespace services\profile;

use backend\forms\student\CreateStudent;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\Students;
use services\BaseService;

class StudentService extends BaseService
{

    public function find(int $id)
    {
        $student = Students::find()
            ->where(['=', 'id', $id])
            ->one();

        $form = new CreateStudent();
        if ($student !== null) {
            $form->setAttributes($student->getAttributes());
            $form->id = $student->id;
        }

        return $form;
    }

    public function insert(CreateStudent $form)
    {
        if ($form->id === null) {
            $this->create($form);
        } else {
            $this->update($form);
        }
    }

    protected function update(CreateStudent $form)
    {
        $student = Students::find()
            ->where(['=', 'id', $form->id])
            ->one();

        $attributes = $form->getAttributes();
        if (empty($student->social) === false && empty($form->social) === true) {
            unset($attributes['social']);
        }

        $attributes['abilities'] = $student->abilities . ' ' . $form->abilities;
        $student->setAttributes($attributes);

        if ($student->validate() === false) {
            throw new ModelValidateException($student);
        }

        if ($student->save(false) === false) {
            throw new ModelSaveException($student);
        }

    }

    protected function create(CreateStudent $form)
    {
        $student = Students::find()->where(['=', 'email', $form->email])->one();
        $attributes = $form->getAttributes();
        if ($student === null) {
            $student = new Students();
        } else {
            if (empty($student->social) === false && empty($form->social) === true) {
                unset($attributes['social']);
            }
            $attributes['abilities'] = $student->abilities . ' ' . $form->abilities;
        }

        $student->setAttributes($attributes);
        if ($student->validate() === false) {
            throw new ModelValidateException($student);
        }

        if ($student->save(false) === false) {
            throw new ModelSaveException($student);
        }
    }
}