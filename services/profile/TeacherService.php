<?php

namespace services\profile;

use backend\forms\teacher\Create;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\abstractions\PublishActiveRecord;
use common\models\Files;
use common\models\Teacher;
use entities\image\TeacherFile;
use services\BaseService;
use services\image\FileService;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class TeacherService extends BaseService
{
    public function create(Create $form)
    {
        $teacher = new Teacher();
        $teacher->setAttributes($form->getAttributes());
        $teacher->is_publish = PublishActiveRecord::PUBLISH_NO;
        $teacher->sorting = 1000;
        $teacher->create_at = (new \DateTime())->format('Y-m-d H:i:s');
        $teacher->update_at = (new \DateTime())->format('Y-m-d H:i:s');
        $teacher->update_by = \Yii::$app->user->getId();

        if ($teacher->validate() === false) {
            throw new ModelValidateException($teacher);
        }

        if ($teacher->save(false) === false) {
            throw new ModelSaveException($teacher);
        }

        if ($uploaded = UploadedFile::getInstance($form, 'file')) {
            $record = (new FileService(new TeacherFile()))
                ->setUploadedFile($uploaded)
                ->save();

            $teacher->image_id = $record->id;
            $teacher->update(false, ['image_id']);
        }
    }

    public function update(Create $form)
    {
        $teacher = Teacher::findOne($form->id);
        $attributes = $form->getAttributes();
        unset($attributes['create_at']);
        unset($attributes['image_id']);

        $teacher->setAttributes($attributes);
        $teacher->update_at = (new \DateTime())->format('Y-m-d H:i:s');
        $teacher->update_by = \Yii::$app->user->getId();
        
        if ($teacher->validate() === false) {
            throw new ModelValidateException($teacher);
        }

        if ($teacher->save(false) === false) {
            throw new ModelSaveException($teacher);
        }

        if ($uploaded = UploadedFile::getInstance($form, 'file')) {

            if ($old_file = ArrayHelper::getValue($teacher, 'image_id')) {
                $files = Files::findOne($old_file);
                $files->delete();
            }

            $record = (new FileService(new TeacherFile()))
                ->setUploadedFile($uploaded)
                ->save();

            $teacher->image_id = $record->id;
            $teacher->update(false, ['image_id']);
        }

    }

    /**
     * @param int $id
     * @return Create|null
     */
    public function find(int $id)
    {
        $model = Teacher::find()
            ->where(['id' => $id])
            ->one();

        return new Create([
            'id' => $model->id,
            'is_publish' => $model->is_publish,
            'name' => $model->name,
            'position' => $model->position,
            'image_id' => $model->image_id,
            'curator' => $model->curator,
            'sorting' => $model->sorting,
        ]);
    }

    public function changePublish(int $id, string $status)
    {
        \Yii::$app->db->createCommand()
            ->update(Teacher::tableName(), ['is_publish' => $status], ['=', 'id', $id])
            ->execute();
    }

    public function changeSorting(int $id, string $sort)
    {
        \Yii::$app->db->createCommand()
            ->update(Teacher::tableName(), ['sorting' => $sort], ['=', 'id', $id])
            ->execute();
    }
}