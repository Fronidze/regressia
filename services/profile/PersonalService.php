<?php

namespace services\profile;

use backend\forms\profile\PersonalForm;
use backend\models\User;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\Files;
use entities\image\PersonalFile;
use services\BaseService;
use services\image\FileService;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class PersonalService extends BaseService
{

    public function find(int $id)
    {
        $record = User::findOne($id);

        $form = new PersonalForm();
        $form->id = $record->id;
        $form->position = $record->position;
        $form->social = $record->social;
        $form->image_id = $record->image_id;

        return $form;
    }

    public function update(PersonalForm $form)
    {
        $user = User::findOne($form->id);
        $attributes = $form->getAttributes();

        $user->setAttributes($attributes);
        if ($user->validate() === false) {
            throw new ModelValidateException($user);
        }

        if ($user->save(false) === false) {
            throw new ModelSaveException($user);
        }

        $uploaded = UploadedFile::getInstance($form, 'file');
        if ($uploaded instanceof UploadedFile) {

            if ($old_file = ArrayHelper::getValue($user, 'image_id')) {
                $files = Files::findOne($old_file);
                $files->delete();
            }

            $file_record = (new FileService(new PersonalFile()))
                ->setUploadedFile($uploaded)
                ->save();

            $user->image_id = $file_record->id;
            $user->update(false, ['image_id']);
        }

    }

}