<?php

namespace services\profile;

use backend\forms\profile\AdminForm;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use services\BaseService;

class AdminService extends BaseService
{
    public function create(AdminForm $admin)
    {
        if ($admin->validate() === false) {
            throw new ModelValidateException($admin);
        }

        if ($admin->save(false) === false) {
            throw new ModelSaveException($admin);
        }
    }
}