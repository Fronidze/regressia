<?php

namespace services\image;

use common\models\Files;
use common\models\Gallery;
use services\BaseService;

class GalleryService extends BaseService
{
    public function remove(int $id)
    {
        $gallery = Gallery::find()
            ->where(['=', 'id', $id])
            ->one();

        if ($gallery !== null) {
            $file = Files::findOne($gallery->image_id);
            if ($file !== null) {
                $file->delete();
            }
            $gallery->delete();
        }
    }

    public function changePublish(int $id, string $status)
    {
        \Yii::$app->db->createCommand()
            ->update(Gallery::tableName(), ['publish' => $status], ['=', 'id', $id])
            ->execute();
    }

    public function changeSorting(int $id, string $sort)
    {
        \Yii::$app->db->createCommand()
            ->update(Gallery::tableName(), ['sorting' => $sort], ['=', 'id', $id])
            ->execute();
    }
}