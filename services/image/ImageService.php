<?php

namespace services\image;

use Imagine\Imagick\Imagine;
use services\BaseService;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class ImageService extends BaseService
{
    const PATH_PREFIX = 'upload';

    public function save(ActiveRecord $model, string $field, $filename = null, $filenameThumbnail = null, $instance = null)
    {
        if ($instance === null) {
            $instance = UploadedFile::getInstance($model, $field);
        }

        if ($instance !== null) {
            $filename = $this->generateFileName($instance, $filename);
            $path = \Yii::getAlias("@upload/$filename");
            $this->saveFile($instance, $path);
            
            $pathThumbnail = \Yii::getAlias("@upload/$filenameThumbnail" . '.jpg');

            (new Imagine())
                ->open($path)
                ->save($pathThumbnail, ['jpeg_quality' => 20]);

            $id = ArrayHelper::getValue($model, 'id');
            if ($id !== null) {
                \Yii::$app->db->createCommand()
                    ->update($model::tableName(), [$field => $filename, 'path_thumbnail' => $filenameThumbnail . '.jpg'], ['=', 'id', $id])
                    ->execute();
            }
        }
    }

    public function path($filename)
    {
        return \Yii::getAlias('/' . static::PATH_PREFIX . '/' . $filename);
    }

    protected function saveFile(UploadedFile $instance, $file)
    {
        if (file_exists($file)) {
            unlink($file);
        }
        
        $instance->saveAs($file);
    }

    protected function generateFileName(UploadedFile $instance, $filename)
    {
        return \Yii::t('app', '{filename}.{extension}', [
            'filename' => $filename ?? $instance->getBaseName(),
            'extension' => $instance->getExtension(),
        ]);
    }

}