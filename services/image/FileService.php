<?php

namespace services\image;

use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\Files;
use entities\image\FileInterface;
use entities\storage\StorageInterface;
use yii\base\ErrorException;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;

class FileService
{
    /** @var FileInterface */
    protected $file;

    /** @var StorageInterface */
    protected $storage;

    /** @var UploadedFile */
    protected $uploaded;

    /** @var ActiveRecord */
    protected $record;

    public function __construct(FileInterface $file)
    {
        $this->file = $file;
    }

    protected function getFile(): FileInterface
    {
        return $this->file;
    }

    protected function getStorage(): StorageInterface
    {
        if ($this->storage === null) {
            $this->storage = $this->getFile()->makeStorage();
        }

        if ($this->storage === null) {
            throw new ErrorException('Not defined file storage');
        }

        return $this->storage;
    }

    public function makeUploadedFile(Model $model, string $attribute)
    {
        $this->uploaded = UploadedFile::getInstance($model, $attribute);
        return $this;
    }

    public function setUploadedFile(UploadedFile $uploaded)
    {
        $this->uploaded = $uploaded;
        return $this;
    }

    public function save()
    {
        if ($this->uploaded === null) {
            throw new ErrorException('UploadedFile not defined');
        }

        $transaction = \Yii::$app->db->beginTransaction();
        $save_file = false;
        try {

            $this->saveModel();
            $this->uploaded->saveAs($this->getFullPathWithFilename());

            $transaction->commit();
        } catch (\Exception $exception) {

            if ($save_file === true) {
                $this->removeFile();
            }
            $transaction->rollBack();

            throw new BadRequestHttpException($exception->getMessage());
        }

        return $this->record;

    }

    protected function getFullPathWithFilename(): string
    {
        $storage = $this->getStorage();
        return \Yii::t('app', '{folder}/{filename}.{extension}', [
            'folder' => $storage->getFullPath(),
            'filename' => $this->uploaded->getBaseName(),
            'extension' => $this->uploaded->getExtension(),
        ]);

    }

    protected function removeFile()
    {
        unlink($this->getFullPathWithFilename());
    }

    protected function saveModel()
    {
        $record = new Files([
            'name' => ArrayHelper::getValue($this->uploaded, 'name'),
            'type' => ArrayHelper::getValue($this->uploaded, 'type'),
            'size' => ArrayHelper::getValue($this->uploaded, 'size'),
            'relative_path' => $this->getStorage()->getPath(),
            'full_path' => $this->getFullPathWithFilename(),
        ]);

        if ($record->validate() === false) {
            throw new ModelValidateException($record);
        }

        if ($record->save(false) === false) {
            throw new ModelSaveException($record);
        }

        $this->record = $record;
    }

}