<?php

namespace services\group;

use common\models\links\GroupSchedule;
use services\BaseService;
use yii\db\Query;

class GroupLinkScheduleService extends BaseService
{
    public function attach(int $id, array $links)
    {
        $this->remove($id, $links);
        $this->insert($id, $links);
    }

    protected function remove(int $group, array $schedules)
    {
        \Yii::$app->db->createCommand()
            ->delete(GroupSchedule::tableName(), ['and', ['=', 'group_id', $group], ['not in', 'schedule_id', $schedules]])
            ->execute();
    }

    protected function insert(int $id, array $links)
    {
        $exists = (new Query())
            ->select(['schedule_id'])
            ->from(GroupSchedule::tableName())
            ->where(['and', ['=', 'group_id', $id], ['in', 'schedule_id', $links]])
            ->column();

        $insert = array_diff($links, $exists);
        foreach ($insert as $schedule) {
            $columns = ['group_id' => $id, 'schedule_id' => $schedule];
            \Yii::$app->db->createCommand()
                ->insert(GroupSchedule::tableName(), $columns)
                ->execute();
        }
    }

}