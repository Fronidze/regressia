<?php

namespace services\group;

use backend\forms\group\CreateGroup;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\Group;
use services\BaseService;
use yii\helpers\ArrayHelper;

class GroupService extends BaseService
{

    public function insert(CreateGroup $from)
    {
        if ($from->id !== null) {
            $this->update($from);
        } else {
            $this->create($from);
        }
    }

    protected function create(CreateGroup $form)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $group = new Group();
            $group->setAttributes($form->getAttributes());
            $group->sorting = 1;
            $group->publish = Group::PUBLISH_NOT;
            if ($group->validate() === false) {
                throw new ModelValidateException($group);
            }

            if ($group->save(false) === false) {
                throw new ModelSaveException($group);
            }

            GroupLinkScheduleService::instance()->attach($group->id, $form->schedules);

        } catch (\Exception $exception) {
            $transaction->rollBack();
        }

        $transaction->commit();
    }

    public function find(int $id)
    {
        $group = Group::find()
            ->with(['schedules'])
            ->where(['=', 'id', $id])
            ->one();

        $form = new CreateGroup();
        $form->id = $group->id;
        $form->setAttributes($group->getAttributes());
        $form->date_start = (new \DateTime($group->date_start))->format('d.m.Y');
        $form->schedules = ArrayHelper::getColumn($group->schedules, 'schedule_id');

        return $form;
    }

    protected function update(CreateGroup $form)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $group = Group::find()->where(['=', 'id', $form->id])->one();
            if ($group === null) {
                $group = new Group();
            }

            $group->setAttributes($form->getAttributes());
            if ($group->validate() === false) {
                throw new ModelValidateException($group);
            }

            if ($group->save(false) === false) {
                throw new ModelSaveException($group);
            }

            GroupLinkScheduleService::instance()->attach($group->id, $form->schedules);

        } catch (\Exception $exception) {
            $transaction->rollBack();
        }

        $transaction->commit();
    }

    public function changeSorting(int $id, string $sort)
    {
        \Yii::$app->db->createCommand()
            ->update(Group::tableName(), ['sorting' => $sort], ['=', 'id', $id])
            ->execute();
    }

    public function publish(int $id, bool $checked)
    {
        $publish = $checked ? Group::PUBLISH_YES : Group::PUBLISH_NOT;
        \Yii::$app->db->createCommand()
            ->update(Group::tableName(), ['publish' => $publish], ['=', 'id', $id])
            ->execute();
    }

}