<?php

namespace services\exam;

use backend\forms\recordschoolchild\CreateRecordSchoolchild;
use backend\forms\schoolchild\CreateSchoolchild;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\RecordSchoolchild;
use common\models\Schoolchild;
use frontend\forms\CourseRegisterExamPreparation;
use frontend\jobs\MailJobSchoolchild;
use services\BaseService;
use services\profile\SchoolchildService;
use yii\queue\Queue;

class RecordSchoolchildService extends BaseService
{

    public function create(CreateRecordSchoolchild $form)
    {
        $record = new RecordSchoolchild();
        $record->setAttributes($form->getAttributes());

        $record->date_create = (new \DateTime($record->date_create, new \DateTimeZone('Europe/Samara')))->format('Y-m-d H:i:s');

        if ($record->validate() === false) {
            throw new ModelValidateException($record);
        }

        if ($record->save(false) === false) {
            throw new ModelSaveException($record);
        }
    }

    public function createFromFrontend(CourseRegisterExamPreparation $form)
    {
        $schoolchild_form = new CreateSchoolchild([
            'email'  => $form->email,
            'name'   => $form->name,
            'social' => $form->social,
        ]);

        /** @var Queue $queue */
        $queue = \Yii::$app->get('queue');
        $queue->push(new MailJobSchoolchild([
            'exam_preparation_id' => $form->exam_preparation_id,
            'schoolchild_name'    => $schoolchild_form->name,
            'schoolchild_email'   => $schoolchild_form->email
        ]));

        SchoolchildService::instance()->insert($schoolchild_form);
        $schoolchild = Schoolchild::find()->where(['=', 'email', $form->email])->one();

        $record_form = new CreateRecordSchoolchild([
            'exam_preparation_id' => $form->exam_preparation_id,
            'schoolchild_id'      => $schoolchild->id,
        ]);
        $this->create($record_form);
    }

}