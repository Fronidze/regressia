<?php

namespace services\exam;

use backend\forms\template_exam\CreateTemplate;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\TemplateExamPreparation;
use services\BaseService;

class TemplateExamPreparationService extends BaseService
{

    public function insert(CreateTemplate $form)
    {
        $form->id === null ? $this->create($form) : $this->update($form);
    }

    protected function create(CreateTemplate $form)
    {
        $template = new TemplateExamPreparation([
            'title'             => $form->title,
            'left_description'  => $form->left_description,
            'right_description' => $form->right_description,
            'program'           => $form->program,
            'about_course'      => $form->about_course,
            'create_at'         => (new \DateTime())->format('Y-m-d H:i:s'),
            'update_at'         => (new \DateTime())->format('Y-m-d H:i:s'),
        ]);


        if ($template->validate() === false) {
            throw new ModelValidateException($template);
        }

        if ($template->save(false) === false) {
            throw new ModelSaveException($template);
        }

        return $template;
    }

    protected function update(CreateTemplate $form)
    {
        $template = TemplateExamPreparation::findOne($form->id);
        $template->title = $form->title;
        $template->left_description = $form->left_description;
        $template->right_description = $form->right_description;
        $template->about_course = $form->about_course;
        $template->program = $form->program;
        $template->update_at = (new \DateTime())->format('Y-m-d H:i:s');

        if ($template->validate() === false) {
            throw new ModelValidateException($template);
        }

        if ($template->save(false) === false) {
            throw new ModelSaveException($template);
        }

        return $template;
    }

    public function find(int $id)
    {
        return TemplateExamPreparation::find()
            ->where(['id' => $id])
            ->one();
    }

    public function findForForm(int $id)
    {
        $template = $this->find($id);
        $form = new CreateTemplate();

        $form->id = $template->id;
        $form->title = $template->title;
        $form->left_description = $template->left_description;
        $form->right_description = $template->right_description;
        $form->program = $template->program;
        $form->about_course = $template->about_course;
        $form->create_at = $template->create_at;
        $form->update_at = $template->update_at;

        return $form;
    }

}
