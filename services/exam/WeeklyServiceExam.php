<?php

namespace services\exam;

use common\models\WeeklyExamPreparation;
use services\BaseService;

class WeeklyServiceExam extends BaseService
{
    public function attach(int $exam_preparation, array $weekly)
    {
        $this->removeOldWeekly($exam_preparation);
        $this->attachNewWeekly($exam_preparation, $weekly);
    }

    protected function removeOldWeekly(int $exam_preparation)
    {
        \Yii::$app->db->createCommand()
            ->delete(WeeklyExamPreparation::tableName(), ['=', 'exam_preparation_id', $exam_preparation])
            ->execute();
    }

    protected function attachNewWeekly(int $exam_preparation, array $weekly)
    {
        $weekly['exam_preparation_id'] = $exam_preparation;
        \Yii::$app->db->createCommand()
            ->insert(WeeklyExamPreparation::tableName(), $weekly)
            ->execute();
    }
}