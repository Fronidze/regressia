<?php

namespace services\exam;

use backend\forms\exam\Create;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\ExamPreparation;
use common\models\WeeklyExamPreparation;
use services\BaseService;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ExamPreparationService extends BaseService
{

    public function insert(Create $form)
    {
        $exam_preparation = $form->id === null ? $this->create($form) : $this->update($form);
        $weekly = ArrayHelper::getValue($form, 'weekly');
        WeeklyServiceExam::instance()->attach($exam_preparation->id, $weekly);
    }

    protected function create(Create $form)
    {

        $exam_preparation = new ExamPreparation([
            'title'            => $form->title,
            'sub_title'        => $form->sub_title,
            'publish'          => ExamPreparation::PUBLISH_CLOSE,
            'empty_date'       => $form->empty_date,
            'code'             => $form->code,
            'register'         => $form->register ? ExamPreparation::REGISTER_OPEN : ExamPreparation::REGISTER_CLOSE,
            'status'           => $form->status,
            'sub_status'       => $form->sub_status,
            'lecturer'         => $form->lecturer,
            'responsible_id'   => $form->responsible_id,
            'template_exam_id' => $form->template_exam_id,
            'date_start'       => empty($form->date_start) === true ? null : (new \DateTime($form->date_start))->format('d.m.Y'),
            'date_completion'  => empty($form->date_completion) === true ? null : (new \DateTime($form->date_completion))->format('d.m.Y'),
            'create_at'        => (new \DateTime())->format('Y-m-d H:i:s'),
            'update_at'        => (new \DateTime())->format('Y-m-d H:i:s'),
        ]);


        if ($exam_preparation->validate() === false) {
            throw new ModelValidateException($exam_preparation);
        }

        if ($exam_preparation->save(false) === false) {
            throw new ModelSaveException($exam_preparation);
        }

        return $exam_preparation;
    }

    protected function update(Create $form)
    {
        $exam_preparation = ExamPreparation::findOne($form->id);
        $exam_preparation->title = $form->title;
        $exam_preparation->sub_title = $form->sub_title;
        $exam_preparation->publish = $form->publish;
        $exam_preparation->empty_date = $form->empty_date;
        $exam_preparation->code = $form->code;
        $exam_preparation->register = $form->register ? ExamPreparation::REGISTER_OPEN : ExamPreparation::REGISTER_CLOSE;
        $exam_preparation->status = $form->status;
        $exam_preparation->sub_status = $form->sub_status;
        $exam_preparation->lecturer = $form->lecturer;
        $exam_preparation->responsible_id = $form->responsible_id;
        $exam_preparation->template_exam_id = $form->template_exam_id;
        $exam_preparation->date_start = empty($form->date_start) === true ? null : (new \DateTime($form->date_start))->format('Y-m-d');
        $exam_preparation->date_completion = empty($form->date_completion) === true ? null : (new \DateTime($form->date_completion))->format('Y-m-d');
        $exam_preparation->update_at = (new \DateTime())->format('Y-m-d H:i:s');

        if ($exam_preparation->validate() === false) {
            throw new ModelValidateException($exam_preparation);
        }

        if ($exam_preparation->save(false) === false) {
            throw new ModelSaveException($exam_preparation);
        }

        return $exam_preparation;
    }

    public function find(int $id)
    {
        return ExamPreparation::find()
            ->with(['weeklyExam', 'responsible'])
            ->where(['id' => $id])
            ->one();
    }

    public function findForForm(int $id)
    {
        $exam_preparation = $this->find($id);
        $form = new Create();

        $form->id = $exam_preparation->id;
        $form->title = $exam_preparation->title;
        $form->sub_title = $exam_preparation->sub_title;
        $form->date_start = empty($exam_preparation->date_start) === true ? null : (new \DateTime($exam_preparation->date_start))->format('d.m.Y');
        $form->date_completion = empty($exam_preparation->date_completion) === true ? null : (new \DateTime($exam_preparation->date_completion))->format('d.m.Y');
        $form->publish = $exam_preparation->publish;
        $form->empty_date = $exam_preparation->empty_date;
        $form->register = $exam_preparation->register;
        $form->status = $exam_preparation->status;
        $form->sub_status = $exam_preparation->sub_status;
        $form->code = $exam_preparation->code;
        $form->lecturer = $exam_preparation->lecturer;
        $form->responsible_id = $exam_preparation->responsible_id;
        $form->template_exam_id = $exam_preparation->template_exam_id;
        $form->create_at = $exam_preparation->create_at;
        $form->update_at = $exam_preparation->update_at;

        $weekly = [];
        /** @var WeeklyExamPreparation $records */
        $records = ArrayHelper::getValue($exam_preparation, 'weeklyExam');
        if (empty($records) === false) {
            foreach ($records as $field => $value) {
                if ($field === 'id' || $field === 'exam_preparation_id') {
                    continue;
                }
                $weekly[$field] = $value;
            }
        }

        $form->weekly = $weekly;
        return $form;
    }

    public function publish(int $id, bool $checked)
    {
        $publish = $checked ? ExamPreparation::PUBLISH_OPEN : ExamPreparation::PUBLISH_CLOSE;
        \Yii::$app->db->createCommand()
            ->update(ExamPreparation::tableName(), [
                'publish'   => $publish,
                'update_at' => (new \DateTime())->format('Y-m-d H:i:s'),
            ], ['=', 'id', $id])
            ->execute();

        \Yii::$app->db->createCommand()
            ->update(ExamPreparation::tableName(), [
                'publish'   => ExamPreparation::PUBLISH_CLOSE,
                'update_at' => (new \DateTime())->format('Y-m-d H:i:s'),
            ], ['!=', 'id', $id])
            ->execute();
    }

    public function lessonsDate(int $id)
    {
        $exam_preparation = $this->find($id);
        if ($exam_preparation === null) {
            return [];
        }

        $selected_week_days = $exam_preparation->weekly->selectedWeekDay();
        $begin = (new \DateTime($exam_preparation->date_start));
        $end = (new \DateTime($exam_preparation->date_last_lesson));
        $result = [];

        if (empty($exam_preparation->date_last_lesson) === true) {
            $end = clone $begin;
            $end->add(new \DateInterval('P60D'));
        }

        for ($date = $begin; $date <= $end; $date->add(new \DateInterval('P1D'))) {
            $week_number = $date->format('N');
            if (array_key_exists($week_number, $selected_week_days)) {
                $result[] = $date->format('Y-m-d') . ' ' . $selected_week_days[$week_number];
            }
        }

        return $result;
    }

    public function statusLabelForDates(array $exam_preparations)
    {
        $query = (new Query())
            ->select(['exam-preparation.date_start', 'exam-preparation.sub_status', 'exam-preparation.empty_date'])
            ->from(['exam-preparation' => ExamPreparation::tableName()])
            ->where(['in', 'exam-preparation.id', $exam_preparations])
            ->orderBy(['exam-preparation.date_start' => SORT_ASC])
            ->one();

        $date_start = ArrayHelper::getValue($query, 'date_start');

        if (ArrayHelper::getValue($query, 'empty_date') === ExamPreparation::NOT_KNOW_DATE) {
            return ArrayHelper::getValue($query, 'sub_status');
        }
        $query = (new Query())
            ->select(['exam-preparation.date_completion'])
            ->from(['exam-preparation' => ExamPreparation::tableName()])
            ->where(['in', 'exam-preparation.id', $exam_preparations])
            ->orderBy(['exam-preparation.date_start' => SORT_DESC]);
        $date_completion = ArrayHelper::getValue($query->one(), 'date_completion');

        $query = (new Query())
            ->select(['exam-preparation.date_completion'])
            ->from(['exam-preparation' => ExamPreparation::tableName()])
            ->where(['or', ['is', 'exam-preparation.date_completion', null], ['=', 'exam-preparation.date_completion', '']])
            ->andWhere(['in', 'id', $exam_preparations]);

        $is_has_empty = $query->exists();

        $now = new \DateTime();
        $start = new \DateTime($date_start);
        $end = $is_has_empty === true ? null : new \DateTime($date_completion);

        $rework = new \DateTime($date_completion);
        $rework = is_null($end) ? null : $rework->modify('+2 month');

        if ($now < $start) {
            return 'Старт занятий ' . \Yii::$app->formatter->asDate($start->format('Y-m-d'), 'long');
        }

        if ($now > $start && $is_has_empty === true) {
            return 'Сейчас идет';
        }

        if ($now > $start && $now < $end) {
            return 'Сейчас идет';
        }

        if ($now > $end) {
            if ($now > $rework && !is_null($rework)) {
                return 'Курс в проработке';
            }
            return 'Курс только что завершился';
        }

        return 'Старт занятий ' . \Yii::$app->formatter->asDate($start->format('Y-m-d'), 'long');
    }
}
