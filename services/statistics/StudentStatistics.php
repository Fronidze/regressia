<?php

namespace services\statistics;

use common\models\Record;
use common\models\Schedule;
use common\models\Students;
use services\BaseService;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class StudentStatistics extends BaseService
{
    public function countStudentByRecordPage()
    {
        $request = \Yii::$app->request->get('SearchRecord');
        $query = (new Query())
            ->select('record.student_id')
            ->distinct('record.student_id')
            ->from(['record' => Record::tableName()]);

        if (empty($request)) {
            return $query->count();
        }

        $query->innerJoin(['schedule' => Schedule::tableName()], ['schedule.id' => new Expression('record.schedule_id')]);
        $query->innerJoin(['student' => Students::tableName()], ['student.id' => new Expression('record.student_id')]);

        $query->andFilterWhere(['=', 'schedule.technology_id', ArrayHelper::getValue($request, 'technology')]);
        $query->andFilterWhere(['=', 'record.schedule_id', ArrayHelper::getValue($request, 'schedule_id')]);
        $query->andFilterWhere(['=', 'record.schedule_id', ArrayHelper::getValue($request, 'schedule_id')]);
        $query->andFilterWhere(['or', ['like', 'student.email', ArrayHelper::getValue($request, 'name')], ['like', 'student.name', ArrayHelper::getValue($request, 'name')]]);

        $completed = empty(ArrayHelper::getValue($request, 'completed')) === true ? null : 1;
        $listened = empty(ArrayHelper::getValue($request, 'listened')) === true ? null : 1;

        if ($completed !== null && $listened !== null) {
            $query->andFilterWhere(['or', ['=', 'record.listened', $listened], ['=', 'record.completed', $completed]]);
        } else {
            $query->andFilterWhere(['=', 'record.listened', $listened]);
            $query->andFilterWhere(['=', 'record.completed', $completed]);
        }

        return $query->count();

    }
}