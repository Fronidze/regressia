<?php

namespace services\statistics;

use common\models\Record;
use common\models\Schedule;
use common\models\StatisticsCourse;
use common\models\Students;
use services\BaseService;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class RecordStatistics extends BaseService
{
    public function countRecordByRecordPage()
    {
        $request = \Yii::$app->request->get('SearchRecord');
        $query = (new Query())
            ->from(['record' => Record::tableName()]);

        if (empty($request)) {
            return $query->count();
        }

        $query->innerJoin(['schedule' => Schedule::tableName()], ['schedule.id' => new Expression('record.schedule_id')]);
        $query->innerJoin(['student' => Students::tableName()], ['student.id' => new Expression('record.student_id')]);

        $query->andFilterWhere(['=', 'schedule.technology_id', ArrayHelper::getValue($request, 'technology')]);
        $query->andFilterWhere(['=', 'record.schedule_id', ArrayHelper::getValue($request, 'schedule_id')]);
        $query->andFilterWhere(['=', 'record.schedule_id', ArrayHelper::getValue($request, 'schedule_id')]);
        $query->andFilterWhere(['or', ['like', 'student.email', ArrayHelper::getValue($request, 'name')], ['like', 'student.name', ArrayHelper::getValue($request, 'name')]]);

        $completed = empty(ArrayHelper::getValue($request, 'completed')) === true ? null : 1;
        $listened = empty(ArrayHelper::getValue($request, 'listened')) === true ? null : 1;

        if ($completed !== null && $listened !== null) {
            $query->andFilterWhere(['or', ['=', 'record.listened', $listened], ['=', 'record.completed', $completed]]);
        } else {
            $query->andFilterWhere(['=', 'record.listened', $listened]);
            $query->andFilterWhere(['=', 'record.completed', $completed]);
        }

        return $query->count();
    }

    public function calculate(int $schedule_id)
    {
        $query = StatisticsCourse::find()
            ->where(['=', 'schedule_id', $schedule_id]);

        if ($query->exists() === false) {
            $record = new StatisticsCourse();
            $record->schedule_id = $schedule_id;
            $record->save();
        } else {
            $record = $query->one();
        }

        $this->recount($record);
    }

    private function recount(StatisticsCourse $record)
    {
        $columns = [
            'records' => $this->getRecords($record->schedule_id),
            'total_completed' => $this->getTotalCompleted($record->schedule_id),
            'listened' => $this->getListened($record->schedule_id),
            'completed' => $this->getCompleted($record->schedule_id),
            'accepted' => $this->getAccepted($record->schedule_id),
        ];

        $condition = ['and', ['=', 'id', $record->id], ['=', 'schedule_id', $record->schedule_id]];
        \Yii::$app->db->createCommand()
            ->update(StatisticsCourse::tableName(), $columns, $condition)
            ->execute();
    }

    private function getRecords(int $schedule_id)
    {
        return (new Query())
            ->from(Record::tableName())
            ->where(['=', 'schedule_id', $schedule_id])
            ->count();
    }

    private function getTotalCompleted(int $schedule_id)
    {
        return (new Query())
            ->from(Record::tableName())
            ->where(['and',
                ['=', 'schedule_id', $schedule_id],
                ['or', ['=', 'listened', 1], ['=', 'completed', 1]]
            ])->count();
    }

    private function getListened(int $schedule_id)
    {
        return (new Query())
            ->from(Record::tableName())
            ->where([
                'and',
                ['=', 'schedule_id', $schedule_id],
                ['=', 'listened', 1]
            ])
            ->count();
    }

    private function getCompleted(int $schedule_id)
    {
        return (new Query())
            ->from(Record::tableName())
            ->where([
                'and',
                ['=', 'schedule_id', $schedule_id],
                ['=', 'completed', 1]
            ])
            ->count();
    }

    private function getAccepted(int $schedule_id)
    {
        return (new Query())
            ->from(Record::tableName())
            ->where([
                'and',
                ['=', 'schedule_id', $schedule_id],
                ['=', 'accepted', 1]
            ])
            ->count();
    }

    public function calculateRegressionStatistic()
    {
        $schedules = Schedule::find()->all();
        /** @var Schedule $schedule */
        foreach ($schedules as $schedule) {
            $this->calculateSchedule($schedule);
        }
    }

    public function calculateSchedule(Schedule $schedule)
    {
        $stats = (new Query())
            ->select([
                'avg_rating' => new Expression('avg(rating)'),
                'quantity_students' => new Expression('count(distinct student_id)'),
            ])
            ->from(Record::tableName())
            ->where(['=', 'schedule_id', $schedule->id])
            ->one();

        $columns = [
            'schedule_id' => $schedule->id,
            'avg_rating' => round(ArrayHelper::getValue($stats, 'avg_rating'), 2),
            'quantity_students' => ArrayHelper::getValue($stats, 'quantity_students'),
            'quantity_vacancies' => $schedule->quantity_vacancies,
            'quantity_organizations' => $schedule->quantity_organizations,
            'quantity_lesson' => $schedule->quantity_lesson,
            'avg_quantity_course' => round($this->getCourseQuantityAvg($schedule), 2),
            'avg_quantity_rating' => round($this->getCourseRatingAvg($schedule), 2),
        ];

        $query = (new Query())
            ->select('id')
            ->from('statistics')
            ->where(['=', 'schedule_id', $schedule->id]);

        if ($query->exists() === false) {
            \Yii::$app->db->createCommand()
                ->insert('statistics', $columns)
                ->execute();
        } else {
            \Yii::$app->db->createCommand()
                ->update('statistics', $columns, ['=', 'schedule_id', $schedule->id])
                ->execute();
        }
    }

    private function getCourseQuantityAvg(Schedule $schedule)
    {
        $query = (new Query())
            ->select(['student_id'])
            ->from(Record::tableName())
            ->where(['=', 'schedule_id', $schedule->id]);
        $students = ArrayHelper::getColumn($query->all(), 'student_id');

        $sub_query = (new Query())
            ->select(['students' => 'r.student_id', 'quantity' => new Expression('count(r.id)')])
            ->from(['r' => Record::tableName()])
            ->innerJoin(['s' => Schedule::tableName()], ['s.id' => new Expression('r.schedule_id')])
            ->where(['and', ['in', 'r.student_id', $students], ['<', 's.date_start', $schedule->date_start]])
            ->groupBy(['r.student_id']);

        $query = (new Query())
            ->select(['avg' => new Expression('avg(q.quantity)')])
            ->from(['q' => $sub_query]);

        return ArrayHelper::getValue($query->one(), 'avg', 0) ?? 0;
    }

    private function getCourseRatingAvg(Schedule $schedule)
    {
        $query = (new Query())
            ->select(['student_id'])
            ->from(Record::tableName())
            ->where(['=', 'schedule_id', $schedule->id]);
        $students = ArrayHelper::getColumn($query->all(), 'student_id');

        $sub_query = (new Query())
            ->select(['students' => 'r.student_id', 'rating' => new Expression('avg(r.rating)')])
            ->from(['r' => Record::tableName()])
            ->innerJoin(['s' => Schedule::tableName()], ['s.id' => new Expression('r.schedule_id')])
            ->where(['and', ['in', 'r.student_id', $students], ['<', 's.date_start', $schedule->date_start]])
            ->groupBy(['r.student_id']);

        $query = (new Query())
            ->select(['avg' => new Expression('avg(q.rating)')])
            ->from(['q' => $sub_query]);

        return ArrayHelper::getValue($query->one(), 'avg', 0) ?? 0;
    }
}