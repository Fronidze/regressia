<?php

namespace services\statistics;

use backend\forms\statistics\RegressionForm;
use entities\regression\RegressionResponse;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\httpclient\Request;

class RegressionService
{
    /** @var string  */
    const REQUEST_URL = 'http://regression.ru';
    /**  @var Client */
    private $client;

    public function __construct(string $base_url = null)
    {
        if ($base_url === null) {
            $base_url = static::REQUEST_URL;
        }

        $this->client = new Client([
            'baseUrl' => $base_url
        ]);
    }

    public function postRegressionFit(RegressionForm $form)
    {
        $response = new RegressionResponse();
        try {
            /** @var Request $request */
            $request = $this->client
                ->createRequest()
                ->setFormat(Client::FORMAT_JSON)
                ->setMethod('post')
                ->setUrl('/regression/fit')
                ->setData($form->getDataForRequest());

            $response = new RegressionResponse($request->send());
        } catch (InvalidConfigException $e) {
        } catch (Exception $e) {
        }

        return $response;
    }

    public function getRegressionModelParams()
    {
        $response = new RegressionResponse();
        try {
            /** @var Request $request */
            $request = $this->client
                ->createRequest()
                ->setMethod('get')
                ->setUrl('/regression/model/params');

            $response = new RegressionResponse($request->send());
        } catch (InvalidConfigException $e) {
        } catch (Exception $e) {
        }

        return $response;
    }

    public function postregRessionPredict(array $data)
    {
        $response = new RegressionResponse();
        try {
            /** @var Request $request */
            $request = $this->client
                ->createRequest()
                ->setFormat(Client::FORMAT_JSON)
                ->setMethod('post')
                ->setUrl('/regression/predict')
                ->setData($data);

            $response = new RegressionResponse($request->send());
        } catch (InvalidConfigException $e) {
        } catch (Exception $e) {
        }

        return $response;
    }

}