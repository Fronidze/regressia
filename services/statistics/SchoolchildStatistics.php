<?php

namespace services\statistics;

use common\models\ExamPreparation;
use common\models\RecordSchoolchild;
use common\models\Schoolchild;
use services\BaseService;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class SchoolchildStatistics extends BaseService
{
    public function countSchoolchildByRecordPage()
    {
        $request = \Yii::$app->request->get('SearchRecordSchoolchild');
        $query = (new Query())
            ->select('record_schoolchild.schoolchild_id')
            ->distinct('record_schoolchild.schoolchild_id')
            ->from(['record_schoolchild' => RecordSchoolchild::tableName()]);

        if (empty($request)) {
            return $query->count();
        }

        $query->innerJoin(['exam_preparation' => ExamPreparation::tableName()], ['exam_preparation.id' => new Expression('record_schoolchild.exam_preparation_id')]);
        $query->innerJoin(['schoolchild' => Schoolchild::tableName()], ['schoolchild.id' => new Expression('record_schoolchild.schoolchild_id')]);

        $query->andFilterWhere(['=', 'record_schoolchild.exam_preparation_id', ArrayHelper::getValue($request, 'exam_preparation_id')]);
        $query->andFilterWhere(['or', ['like', 'schoolchild.email', ArrayHelper::getValue($request, 'name')], ['like', 'schoolchild.name', ArrayHelper::getValue($request, 'name')]]);

        return $query->count();
    }
}