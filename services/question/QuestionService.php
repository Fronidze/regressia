<?php

namespace services\question;

use backend\forms\question\Create;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\abstractions\PublishActiveRecord;
use common\models\Question;
use services\BaseService;

class QuestionService extends BaseService
{
    public function create(Create $form)
    {
        $question = new Question();
        $question->setAttributes($form->getAttributes());
        $question->is_publish = PublishActiveRecord::PUBLISH_NO;
        $question->sorting = 1000;
        $question->create_at = (new \DateTime())->format('Y-m-d H:i:s');
        $question->update_at = (new \DateTime())->format('Y-m-d H:i:s');

        if ($question->validate() === false) {
            throw new ModelValidateException($question);
        }

        if ($question->save(false) === false) {
            throw new ModelSaveException($question);
        }

    }

    public function update(Create $form)
    {
        $question = Question::findOne($form->id);
        $attributes = $form->getAttributes();
        unset($attributes['create_at']);

        $question->setAttributes($attributes);
        $question->update_at = (new \DateTime())->format('Y-m-d H:i:s');

        if ($question->validate() === false) {
            throw new ModelValidateException($question);
        }

        if ($question->save(false) === false) {
            throw new ModelSaveException($question);
        }
    }

    public function delete(int $id)
    {
        \Yii::$app->db->createCommand()
            ->delete(Question::tableName(), ['=', 'id', $id])
            ->execute();
    }

    /**
     * @param int $id
     * @return Create|null
     */
    public function find(int $id)
    {
        $model = Question::find()
            ->where(['id' => $id])
            ->one();

        return new Create([
            'id' => $model->id,
            'question' => $model->question,
            'answer' => $model->answer,
            'is_publish' => $model->is_publish,
            'sorting' => $model->sorting,
        ]);
    }

    public function changePublish(int $id, string $status)
    {
        \Yii::$app->db->createCommand()
            ->update(Question::tableName(), ['is_publish' => $status], ['=', 'id', $id])
            ->execute();
    }

    public function changeSorting(int $id, string $sort)
    {
        \Yii::$app->db->createCommand()
            ->update(Question::tableName(), ['sorting' => $sort], ['=', 'id', $id])
            ->execute();
    }
}