<?php

namespace services\import;

use backend\forms\import\RecordImport;
use backend\forms\record\CreateRecord;
use backend\forms\student\CreateStudent;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\Students;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use services\BaseService;
use services\profile\StudentService;
use services\schedule\RecordService;
use yii\base\Model;

class ImportRecordService extends BaseService
{
    public function parseExcelSheet(RecordImport $import, Worksheet $sheet)
    {
        for ($row = 2; $row <= $sheet->getHighestRow(); $row++) {
            try {
                $date = Date::excelToDateTimeObject($sheet->getCell("A$row")->getValue())->format('Y-m-d H:i:s');
                $form = new CreateStudent([
                    'name' => trim($sheet->getCell("B$row")->getValue()),
                    'email' => trim($sheet->getCell("C$row")->getValue()),
                    'social' => trim($sheet->getCell("D$row")->getValue()),
                    'abilities' => trim($sheet->getCell("E$row")->getValue()),
                ]);

                if (empty($form->email) === true) {
                    continue;
                }

                StudentService::instance()->insert($form);

                $student = Students::find()->where(['=', 'email', $form->email])->one();
                $record = new CreateRecord([
                    'schedule_id' => $import->schedule_id,
                    'student_id' => $student->id,
                    'date_create' => $date
                ]);
                RecordService::instance()->create($record);
            } catch (ModelValidateException $exception) {
                $this->loggerErrors($exception->model, $import);
            } catch (ModelSaveException $exception) {
                $this->loggerErrors($exception->model, $import);
            } catch (\Exception $exception) {

            }
        }
    }

    private function loggerErrors(Model $form, RecordImport $import)
    {
        $file = \Yii::getAlias('@runtime/logs/'.$form->formName().'.log');
        $render = "Дата: " . (new \DateTime())->format('Y-m-d H:i:s');
        $render .= "\n";
        $render .= "Атрибуты: \n";
        foreach ($form->getAttributes() as $code => $value) {
            $render .= \Yii::t('app', " {code} => {value}\n", [
                'code' => $code,
                'value' => $value,
            ]);
        }

        if ($form->hasErrors() === true) {
            $render .= "Ошибки валидации: \n";
            foreach ($form->getErrors() as $field => $errors) {
                $render .= \Yii::t('app', " {field} => {errors}\n", [
                    'field' => $field,
                    'errors' => implode(', ', $errors),
                ]);
            }
        }

        $import->appendError($render);
        $render .= "\n===============================\n";
        file_put_contents($file, $render, FILE_APPEND);
    }
}