<?php

namespace services\import;

use backend\forms\import\RecordSchoolchildImport;
use backend\forms\recordschoolchild\CreateRecordSchoolchild;
use backend\forms\schoolchild\CreateSchoolchild;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\Schoolchild;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use services\BaseService;
use services\exam\RecordSchoolchildService;
use services\profile\SchoolchildService;
use yii\base\Model;

class ImportRecordSchoolchildService extends BaseService
{
    public function parseExcelSheet(RecordSchoolchildImport $import, Worksheet $sheet)
    {
        for ($row = 2; $row <= $sheet->getHighestRow(); $row++) {
            try {
                $date = Date::excelToDateTimeObject($sheet->getCell("A$row")->getValue())->format('Y-m-d H:i:s');

                $form = new CreateSchoolchild([
                    'name'   => trim($sheet->getCell("B$row")->getValue()),
                    'email'  => trim($sheet->getCell("C$row")->getValue()),
                    'social' => trim($sheet->getCell("D$row")->getValue()),
                ]);

                if (empty($form->email) === true) {
                    continue;
                }

                SchoolchildService::instance()->insert($form);

                $schoolchild = Schoolchild::find()->where(['=', 'email', $form->email])->one();
                $record = new CreateRecordSchoolchild([
                    'exam_preparation_id' => $import->exam_preparation_id,
                    'schoolchild_id'      => $schoolchild->id,
                    'create_at'           => $date
                ]);
                RecordSchoolchildService::instance()->create($record);

            } catch (ModelValidateException $exception) {
                var_dump('qqq');
                die();
                $this->loggerErrors($exception->model, $import);
            } catch (ModelSaveException $exception) {
                var_dump('www');
                die();

                $this->loggerErrors($exception->model, $import);
            } catch (\Exception $exception) {
                echo "<pre>";
                var_dump($exception);
                die();
            }
        }
    }

    private function loggerErrors(Model $form, RecordSchoolchildImport $import)
    {
        $file = \Yii::getAlias('@runtime/logs/' . $form->formName() . '.log');
        $render = "Дата: " . (new \DateTime())->format('Y-m-d H:i:s');
        $render .= "\n";
        $render .= "Атрибуты: \n";
        foreach ($form->getAttributes() as $code => $value) {
            $render .= \Yii::t('app', " {code} => {value}\n", [
                'code'  => $code,
                'value' => $value,
            ]);
        }

        if ($form->hasErrors() === true) {
            $render .= "Ошибки валидации: \n";
            foreach ($form->getErrors() as $field => $errors) {
                $render .= \Yii::t('app', " {field} => {errors}\n", [
                    'field'  => $field,
                    'errors' => implode(', ', $errors),
                ]);
            }
        }

        $import->appendError($render);
        $render .= "\n===============================\n";
        file_put_contents($file, $render, FILE_APPEND);
    }
}