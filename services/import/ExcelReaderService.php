<?php

namespace services\import;

use backend\forms\import\RecordImport;
use backend\forms\import\RecordSchoolchildImport;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use services\BaseService;

class ExcelReaderService extends BaseService
{
    /** @var Spreadsheet */
    protected $spreadsheet;

    /** @var integer */
    protected $schedule_id;
    protected $exam_preparation_id;

    /** @var RecordImport */
    protected $import;

    /** @var RecordSchoolchildImport */
    protected $importSchoolchild;

    public function load(string $path)
    {
        $this->spreadsheet = IOFactory::load($path);
        return $this;
    }

    public function setModelImport(RecordImport $import)
    {
        $this->schedule_id = $import->schedule_id;
        $this->import = $import;
        return $this;
    }

    public function setModelSchoolchildImport(RecordSchoolchildImport $import)
    {
        $this->exam_preparation_id = $import->exam_preparation_id;
        $this->importSchoolchild = $import;
        return $this;
    }


    public function parse()
    {
        $sheet = $this->spreadsheet->getActiveSheet();
        ImportRecordService::instance()->parseExcelSheet($this->import, $sheet);
    }

    public function parseSchoolchild()
    {
        $sheet = $this->spreadsheet->getActiveSheet();
        ImportRecordSchoolchildService::instance()->parseExcelSheet($this->importSchoolchild, $sheet);
    }


}