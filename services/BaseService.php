<?php

namespace services;

abstract class BaseService
{
    public function __construct()
    {

    }

    public static function instance()
    {
        return new static();
    }
}