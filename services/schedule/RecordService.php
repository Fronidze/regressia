<?php

namespace services\schedule;

use backend\forms\record\CreateRecord;
use backend\forms\student\CreateStudent;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\Record;
use common\models\Students;
use frontend\forms\CourseRegister;
use frontend\jobs\MailJob;
use services\BaseService;
use services\profile\StudentService;
use services\statistics\RecordStatistics;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\queue\Queue;

class RecordService extends BaseService
{

    public function create(CreateRecord $form)
    {
        $record = new Record();
        $record->setAttributes($form->getAttributes());

        $record->date_create = (new \DateTime($record->date_create,
            new \DateTimeZone('Europe/Samara')))->format('Y-m-d H:i:s');

        if ($record->validate() === false) {
            throw new ModelValidateException($record);
        }

        if ($record->save(false) === false) {
            throw new ModelSaveException($record);
        }
    }

    public function createFromFrontend(CourseRegister $form)
    {
        $student_form = new CreateStudent([
            'email' => $form->email,
            'name' => $form->name,
            'social' => $form->social,
            'abilities' => $form->ability
        ]);

        /** @var Queue $queue */
        $queue = \Yii::$app->get('queue');
        $queue->push(new MailJob([
            'schedule_id' => $form->schedule_id,
            'student_name' => $student_form->name,
            'student_email' => $student_form->email
        ]));

        StudentService::instance()->insert($student_form);
        $student_id = Students::find()->where(['=', 'email', $form->email])->one();

        $record_form = new CreateRecord([
            'schedule_id' => $form->schedule_id,
            'student_id' => $student_id->id,
        ]);
        $this->create($record_form);
    }

    public function listened(int $id, int $result)
    {
        \Yii::$app->db->createCommand()
            ->update(Record::tableName(), ['listened' => $result], ['=', 'id', $id])
            ->execute();

        $this->recountStatistics($id);
    }

    public function completed(int $id, int $result)
    {
        \Yii::$app->db->createCommand()
            ->update(Record::tableName(), ['completed' => $result], ['=', 'id', $id])
            ->execute();

        $this->recountStatistics($id);
    }

    public function accepted(int $id, int $result)
    {
        \Yii::$app->db->createCommand()
            ->update(Record::tableName(), ['accepted' => $result], ['=', 'id', $id])
            ->execute();

        $this->recountStatistics($id);
    }

    protected function recountStatistics(int $record_id)
    {
        $query = (new Query())
            ->select(['schedule_id'])
            ->from(Record::tableName())
            ->where(['=', 'id', $record_id]);

        if ($schedule_id = ArrayHelper::getValue($query->one(), 'schedule_id')) {
            RecordStatistics::instance()->calculate($schedule_id);
        }
    }

}