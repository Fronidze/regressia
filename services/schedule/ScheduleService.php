<?php

namespace services\schedule;


use backend\forms\schedule\Create;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\Schedule;
use common\models\Technology;
use common\models\WeeklySchedule;
use services\BaseService;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ScheduleService extends BaseService
{

    public function insert(Create $form)
    {
        $schedule = $form->id === null ? $this->create($form) : $this->update($form);
        $weekly = ArrayHelper::getValue($form, 'weekly');
        WeeklyService::instance()->attach($schedule->id, $weekly);
    }

    protected function create(Create $form)
    {
        $schedule = new Schedule([
            'code' => $form->code,
            'publish' => Schedule::PUBLISH_CLOSE,
            'technology_id' => $form->technology_id,
            'teacher_id' => $form->teacher_id,
            'date_start' => (new \DateTime($form->date_start))->format('Y-m-d H:i:s'),
            'date_completion' => empty($form->date_completion) === true ? null : (new \DateTime($form->date_completion))->format('Y-m-d H:i:s'),
            'date_last_lesson' => empty($form->date_last_lesson) === true ? null : (new \DateTime($form->date_last_lesson))->format('Y-m-d H:i:s'),
            'quantity_lesson' => (int)$form->quantity_lesson,
            'quantity_vacancies' => (int)$form->quantity_vacancies,
            'quantity_organizations' => (int)$form->quantity_organizations,
            'program' => $this->getProgram((int)$form->technology_id),
            'responsible_id' => $form->responsible_id,
            'date_create' => (new \DateTime())->format('Y-m-d H:i:s'),
            'date_update' => (new \DateTime())->format('Y-m-d H:i:s'),
            'update_by' => \Yii::$app->user->getId(),
            'sorting' => 1,
        ]);

        if ($schedule->validate() === false) {
            throw new ModelValidateException($schedule);
        }

        if ($schedule->save(false) === false) {
            throw new ModelSaveException($schedule);
        }

        return $schedule;
    }

    protected function update(Create $form)
    {
        $schedule = Schedule::findOne($form->id);
        $schedule->technology_id = $form->technology_id;
        $schedule->teacher_id = $form->teacher_id;
        $schedule->date_start = (new \DateTime($form->date_start))->format('Y-m-d H:i:s');
        $schedule->date_completion = empty($form->date_completion) === true ? null : (new \DateTime($form->date_completion))->format('Y-m-d H:i:s');
        $schedule->date_last_lesson = empty($form->date_last_lesson) === true ? null : (new \DateTime($form->date_last_lesson))->format('Y-m-d H:i:s');
        $schedule->quantity_lesson = (int)$form->quantity_lesson;
        $schedule->quantity_vacancies = (int)$form->quantity_vacancies;
        $schedule->quantity_organizations = (int)$form->quantity_organizations;
        $schedule->program = $form->program;
        $schedule->date_update = (new \DateTime())->format('Y-m-d H:i:s');
        $schedule->update_by = \Yii::$app->user->getId();
        $schedule->code = $form->code;
        $schedule->publish = $form->publish;
        $schedule->sorting = $form->sorting;
        $schedule->responsible_id = $form->responsible_id;

        if ($schedule->validate() === false) {
            throw new ModelValidateException($schedule);
        }

        if ($schedule->save(false) === false) {
            throw new ModelSaveException($schedule);
        }

        return $schedule;
    }

    protected function getProgram(int $technology_id)
    {
        $query = (new Query())
            ->from(Technology::tableName())
            ->where(['=', 'id', $technology_id]);

        return ArrayHelper::getValue($query->one(), 'program');
    }

    public function find(int $id)
    {
        return Schedule::find()
            ->with(['technology', 'teacher', 'editor', 'weekly', 'responsible'])
            ->where(['id' => $id])
            ->one();
    }

    public function findForForm(int $id)
    {
        $schedule = $this->find($id);
        $form = new Create();

        $form->id = $schedule->id;
        $form->technology_id = $schedule->technology_id;
        $form->teacher_id = $schedule->teacher_id;
        $form->date_start = (new \DateTime($schedule->date_start))->format('d.m.Y');
        $form->date_completion = $schedule->date_completion === null ? null : (new \DateTime($schedule->date_completion))->format('d.m.Y');
        $form->date_last_lesson = $schedule->date_last_lesson === null ? null : (new \DateTime($schedule->date_last_lesson))->format('d.m.Y');
        $form->quantity_lesson = $schedule->quantity_lesson;
        $form->date_create = $schedule->date_create;
        $form->date_update = $schedule->date_update;
        $form->update_by = $schedule->update_by;
        $form->date_create = $schedule->date_create;
        $form->status = $schedule->status;
        $form->program = $schedule->program;
        $form->publish = $schedule->publish;
        $form->code = $schedule->code;
        $form->sorting = $schedule->sorting;
        $form->responsible_id = $schedule->responsible_id;

        $weekly = [];
        /** @var WeeklySchedule $records */
        $records = ArrayHelper::getValue($schedule, 'weekly');
        if (empty($records) === false) {
            foreach ($records as $field => $value) {
                if ($field === 'id' || $field === 'schedule_id') {
                    continue;
                }
                $weekly[$field] = $value;
            }
        }

        $form->weekly = $weekly;
        return $form;
    }

    public function publish(int $id, bool $checked)
    {
        $publish = $checked ? Schedule::PUBLISH_OPEN : Schedule::PUBLISH_CLOSE;
        \Yii::$app->db->createCommand()
            ->update(Schedule::tableName(), [
                'publish' => $publish,
                'update_by' => \Yii::$app->user->getId(),
                'date_update' => (new \DateTime())->format('Y-m-d H:i:s'),
            ], ['=', 'id', $id])
            ->execute();
    }

    public function changeStatus(int $id, string $status)
    {
        \Yii::$app->db->createCommand()
            ->update(Schedule::tableName(),
                [
                    'status' => $status,
                    'update_by' => \Yii::$app->user->getId(),
                    'date_update' => (new \DateTime())->format('Y-m-d H:i:s')
                ],
                ['=', 'id', $id]
            )
            ->execute();

        if ($status == Schedule::STATUS_ACTIVE) {
            $this->moveToArchive($id);
        }
    }

    public function moveToArchive($id)
    {
        $schedule = (new Query())->select(['technology_id'])->from(['schedule' => Schedule::tableName()])->where(['=', 'id', $id])->one();
        \Yii::$app->db->createCommand()->update(Schedule::tableName(),
            [
                'status' => Schedule::STATUS_ARCHIVE,
                'update_by' => \Yii::$app->user->getId(),
                'date_update' => (new \DateTime())->format('Y-m-d H:i:s'),
            ],
            [
                'and',
                ['!=', 'id', $id],
                ['=', 'technology_id', $schedule['technology_id']],
                ['=', 'status', 'active']
            ])->execute();
    }

    public function changeSorting(int $id, string $sort)
    {
        \Yii::$app->db->createCommand()
            ->update(Schedule::tableName(), ['sorting' => $sort], ['=', 'id', $id])
            ->execute();
    }

    public function lessonsDate(int $id)
    {
        $schedule = $this->find($id);
        if ($schedule === null) {
            return [];
        }

        $selected_week_days = $schedule->weekly->selectedWeekDay();
        $begin = (new \DateTime($schedule->date_start));
        $end = (new \DateTime($schedule->date_last_lesson));
        $result = [];

        if (empty($schedule->date_last_lesson) === true) {
            $end = clone $begin;
            $end->add(new \DateInterval('P60D'));
        }

        for ($date = $begin; $date <= $end; $date->add(new \DateInterval('P1D'))) {
            $week_number = $date->format('N');
            if (array_key_exists($week_number, $selected_week_days)) {
                $result[] = $date->format('Y-m-d') . ' ' . $selected_week_days[$week_number];
            }
        }

        return $result;
    }

    public function statusLabelForDates(array $schedules)
    {
        $query = (new Query())
            ->select(['schedule.date_start'])
            ->from(['schedule' => Schedule::tableName()])
            ->where(['in', 'schedule.id', $schedules])
            ->orderBy(['schedule.date_start' => SORT_ASC]);
        $date_start = ArrayHelper::getValue($query->one(), 'date_start');

        $query = (new Query())
            ->select(['schedule.date_completion'])
            ->from(['schedule' => Schedule::tableName()])
            ->where(['in', 'schedule.id', $schedules])
            ->orderBy(['schedule.date_start' => SORT_DESC]);
        $date_completion = ArrayHelper::getValue($query->one(), 'date_completion');

        $query = (new Query())
            ->select(['schedule.date_completion'])
            ->from(['schedule' => Schedule::tableName()])
            ->where(['or', ['is', 'schedule.date_completion', null], ['=', 'schedule.date_completion', '']])
            ->andWhere(['in', 'id', $schedules]);

        $is_has_empty = $query->exists();

        $now = new \DateTime();
        $start = new \DateTime($date_start);
        $end = $is_has_empty === true ? null : new \DateTime($date_completion);

        $rework = new \DateTime($date_completion);
        $rework = is_null($end) ? null : $rework->modify('+2 month');

        if ($now < $start) {
            return 'Старт занятий ' . \Yii::$app->formatter->asDate($start->format('Y-m-d'), 'long');
        }

        if ($now > $start && $is_has_empty === true) {
            return 'Сейчас идет';
        }

        if ($now > $start && $now < $end) {
            return 'Сейчас идет';
        }

        if ($now > $end) {
            if ($now > $rework && !is_null($rework)) {
                return 'Курс в проработке';
            }
//            return 'Курс завершился ' . \Yii::$app->formatter->asDate($end->format('Y-m-d'), 'long');
            return 'Курс только что завершился';
        }

        return 'Старт занятий ' . \Yii::$app->formatter->asDate($start->format('Y-m-d'), 'long');
    }

}