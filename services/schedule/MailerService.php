<?php

namespace services\schedule;

use common\models\ExamPreparation;
use common\models\Schedule;
use services\BaseService;
use Yii;
use yii\base\ErrorException;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class MailerService extends BaseService
{
    protected $login;
    protected $password;

    public function __construct()
    {
        $this->login = \Yii::$app->params['senderEmail'];
        $this->password = \Yii::$app->params['senderPassword'];

        if (empty($this->login) || empty($this->password)) {
            throw new ErrorException('Не установлены параметры работы с почтой');
        }

        parent::__construct();
    }

    protected function makeFromSender(): array
    {
        return [$this->login => \Yii::$app->params['senderName']];
    }

    /**
     * Отправка письма: "Уведомление об регистрации на курс"
     *
     * @param Schedule $schedule
     * @param string $name
     * @param string $email
     * @throws InvalidConfigException
     */
    public function courseRegistrationNotice(Schedule $schedule, string $name, string $email)
    {
        $now = new \DateTime();
        $start = (new \DateTime($schedule->date_start));

        $schedule_date_start = \Yii::t('app', '{date} в {time}', [
            'date' => \Yii::$app->formatter->asDate($start->format('Y-m-d'), 'long'),
            'time' => ArrayHelper::getValue($schedule->getWeeklySchedule(), '0.time'),
        ]);

        if ($now > $start) {
            $schedule_date_start = $schedule->getWeeklyScheduleForPage();
        }

        Yii::$app->mailer->compose('@common/mail/course-registration-notice-html', [
            'name'                => $name,
            'schedule_title'      => "«" . $schedule->getTechnologySubTitle() . "»",
            'schedule_date_start' => $schedule_date_start,
            'academy_logo'        => \Yii::getAlias('@frontend/web/images/logo_academy_rgb.png')
        ])
            ->setFrom($this->makeFromSender())
            ->setTo($email)
            ->setSubject("«" . $schedule->getTechnologySubTitle() . "» — старт " . \Yii::$app->formatter->asDate($schedule->date_start, "long"))
            ->send();

    }

    /**
     * Отправка письма: "Уведомление об регистрации на курс ЕГЭ"
     *
     * @param ExamPreparation $exam_preparation
     * @param string $name
     * @param string $email
     * @throws InvalidConfigException
     */
    public function courseRegistrationNoticeExamPreparation(ExamPreparation $exam_preparation, string $name, string $email)
    {

        $now = new \DateTime();
        $start = (new \DateTime($exam_preparation->date_start));

        $schedule_date_start = \Yii::t('app', '{date} в {time}', [
            'date' => \Yii::$app->formatter->asDate($start->format('Y-m-d'), 'long'),
            'time' => ArrayHelper::getValue($exam_preparation->getWeeklyExamPreparation(), '0.time'),
        ]);

        if ($now > $start) {
            $schedule_date_start = $exam_preparation->getWeeklyExamPreparationForPage();
        }

        Yii::$app->mailer->compose('@common/mail/course-registration-notice-html', [
            'name'                => $name,
            'schedule_title'      => "«" . $exam_preparation->sub_title . "»",
            'schedule_date_start' => $schedule_date_start,
            'academy_logo'        => \Yii::getAlias('@frontend/web/images/logo_academy_rgb.png')
        ])
            ->setFrom($this->makeFromSender())
            ->setTo($email)
            ->setSubject("«" . $exam_preparation->sub_title . "» — старт " . \Yii::$app->formatter->asDate($exam_preparation->date_start, "long"))
            ->send();

    }
}