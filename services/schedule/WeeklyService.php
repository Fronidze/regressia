<?php

namespace services\schedule;

use common\models\WeeklySchedule;
use services\BaseService;

class WeeklyService extends BaseService
{
    public function attach(int $schedule, array $weekly)
    {
        $this->removeOldWeekly($schedule);
        $this->attachNewWeekly($schedule, $weekly);
    }

    protected function removeOldWeekly(int $schedule)
    {
        \Yii::$app->db->createCommand()
            ->delete(WeeklySchedule::tableName(), ['=', 'schedule_id',  $schedule])
            ->execute();
    }

    protected function attachNewWeekly(int $schedule, array $weekly)
    {
        $weekly['schedule_id'] = $schedule;
        \Yii::$app->db->createCommand()
            ->insert(WeeklySchedule::tableName(), $weekly)
            ->execute();
    }
}