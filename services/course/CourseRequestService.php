<?php

namespace services\course;

use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\CourseRequest;
use frontend\forms\CourseSignUp;
use services\BaseService;

class CourseRequestService extends BaseService
{
    public function create(CourseSignUp $form)
    {

        if ($this->update($form) === false) {
            $request = new CourseRequest();
            $request->setAttributes($form->getAttributes());

            if ($request->validate() === false) {
                throw new ModelValidateException($request);
            }

            if ($request->save(false) === false) {
                throw new ModelSaveException($request);
            }
        }
    }

    protected function update(CourseSignUp $form)
    {
        $request = CourseRequest::find()
            ->where(['and', ['=', 'email', $form->email], ['=', 'code_technology_or_group', $form->code_technology_or_group]])
            ->one();

        if ($request !== null) {
            $request->date_create = (new \DateTime())->format('Y-m-d H:i:s');
            $request->update(false, ['date_create']);
            return true;
        }

        return false;
    }
}