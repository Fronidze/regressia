<?php

namespace services\document;

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use services\BaseService;

class DocumentService extends BaseService
{

    protected $filename = 'Сгенерированный документ';

    public function setFilename(string $name)
    {
        $this->filename = $name;
        return $this;
    }

    public function generate(array $records)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        foreach ($records as $key => $record) {
            $this->setRowValues($sheet, $key, $record);
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $this->filename . '.xls"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');

    }

    protected function setRowValues(Worksheet $sheet, int $key, array $records)
    {
        $row = $key + 1;
        $counter = 1;
        foreach ($records as $code => $record) {

            $cell = \Yii::t('app', '{column}{index}', [
                'column' => Coordinate::stringFromColumnIndex($counter),
                'index' => $row,
            ]);

            $sheet->getColumnDimension(Coordinate::stringFromColumnIndex($counter))->setWidth(50);
            $sheet->setCellValue($cell, $record);
            $counter++;
        }
    }

}