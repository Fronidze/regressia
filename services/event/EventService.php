<?php

namespace services\event;

use backend\forms\event\CreateEvent;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\Event;
use services\BaseService;
use yii\helpers\ArrayHelper;

class EventService extends BaseService
{

    /**
     * @param CreateEvent $form
     * @return CreateEvent
     * @throws \yii\db\Exception
     */
    public function create(CreateEvent $form)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $event = new CreateEvent();
            $event->setAttributes($form->getAttributes());

            $event->date_event = (new \DateTime($event->date_event . $form->date_event_clock))->format('Y-m-d H:i:s');
            if (!empty($event->date_reminder)) {
                $event->date_reminder = (new \DateTime($event->date_reminder . $form->date_reminder_clock))->format('Y-m-d H:i:s');
            }

            if ($event->validate() === false) {
                throw new ModelValidateException($event);
            }

            if ($event->save(false) === false) {
                throw new ModelSaveException($event);
            }

            if (is_array($form->users)) {
                EventLinkUserService::instance()->attach($event->id, $form->users);
            }

        } catch (\Exception $exception) {
            $transaction->rollBack();
        }

        $transaction->commit();

        return $event;
    }

    public function remove(int $id)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $event = Event::find()->where(['=', 'id', $id])->one();
            if ($event->user_id != \Yii::$app->user->id) {
                return ['message' => 'permission denied'];
            }
            $event->delete();
            EventLinkUserService::instance()->attach($id, []);
        } catch (\Exception $exception) {
            $transaction->rollBack();
        }

        $transaction->commit();
        return ['message' => 'ok'];
    }

    public function find(int $id)
    {
        $event = Event::find()
            ->with(['users'])
            ->where(['=', 'id', $id])
            ->one();

        $form = new CreateEvent();
        $form->id = $event->id;
        $form->setAttributes($event->getAttributes());
        $form->date_event = (new \DateTime($event->date_event))->format('d.m.Y');
        $form->date_event_clock = (new \DateTime($event->date_event))->format('H:i');
        $form->date_reminder = (new \DateTime($event->date_reminder))->format('d.m.Y');
        $form->date_reminder_clock = (new \DateTime($event->date_reminder))->format('H:i');
        $form->users = ArrayHelper::getColumn($event->users, 'user_id');

        return $form;
    }

    public function insert(CreateEvent $from)
    {
        if ($from->id !== null) {
            return $this->update($from);
        } else {
            $this->create($from);
        }
    }

    protected function update(CreateEvent $form)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $event = Event::find()->where(['=', 'id', $form->id])->one();
            if ($event === null) {
                $event = new Event();
            }

            $event->setAttributes($form->getAttributes());
            if ($event->validate() === false) {
                throw new ModelValidateException($event);
            }

            $event->date_event = (new \DateTime($form->date_event . $form->date_event_clock))->format('Y-m-d H:i:s');
            if (!empty($form->date_reminder)) {
                $event->date_reminder = (new \DateTime($form->date_reminder . $form->date_reminder_clock))->format('Y-m-d H:i:s');
            }

            if ($event->save(false) === false) {
                throw new ModelSaveException($event);
            }

            if (is_array($form->users)) {
                EventLinkUserService::instance()->attach($event->id, $form->users);
            }

        } catch (\Exception $exception) {
            $transaction->rollBack();
        }

        $transaction->commit();

        return $event;
    }
}