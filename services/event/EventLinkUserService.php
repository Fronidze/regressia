<?php

namespace services\event;

use common\models\links\ParticipantsEvent;
use services\BaseService;
use yii\db\Query;

class EventLinkUserService extends BaseService
{
    public function attach(int $id, array $links)
    {
        $this->remove($id, $links);
        $this->insert($id, $links);
    }

    protected function remove(int $event_id, array $users)
    {
        \Yii::$app->db->createCommand()
            ->delete(ParticipantsEvent::tableName(), ['and', ['=', 'event_id', $event_id], ['not in', 'user_id', $users]])
            ->execute();
    }

    protected function insert(int $id, array $links)
    {
        $exists = (new Query())
            ->select(['user_id'])
            ->from(ParticipantsEvent::tableName())
            ->where(['and', ['=', 'event_id', $id], ['in', 'user_id', $links]])
            ->column();

        $insert = array_diff($links, $exists);
        foreach ($insert as $user) {
            $columns = ['event_id' => $id, 'user_id' => $user];
            \Yii::$app->db->createCommand()
                ->insert(ParticipantsEvent::tableName(), $columns)
                ->execute();
        }
    }

}