<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'baseUrl' => '/api',
            'cookieValidationKey' => getenv('COOKIE_SECRET_KEY'),
        ],
    ],
];
