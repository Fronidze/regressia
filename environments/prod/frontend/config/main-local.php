<?php
return [
    'components' => [
        'request' => [
            'cookieValidationKey' => getenv('COOKIE_SECRET_KEY'),
        ],
    ],
];
