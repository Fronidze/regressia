<?php

use yii\db\Migration;

/**
 * Class m190726_134118_add_teacher_sorting
 */
class m190726_134118_add_teacher_sorting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%teacher}}', 'sorting', $this->integer()->defaultValue(1000));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
         $this->dropColumn('{{%teacher}}', 'sorting');
    }
}
