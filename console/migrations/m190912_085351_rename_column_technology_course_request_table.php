<?php

use yii\db\Migration;

/**
 * Class m190912_085351_rename_column_technology_course_request_table
 */
class m190912_085351_rename_column_technology_course_request_table extends Migration
{
    private $table = '{{%course_request}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn($this->table, 'technology', 'code_technology_or_group');
        $this->alterColumn($this->table, 'code_technology_or_group', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn($this->table, 'code_technology_or_group', 'technology');
        $this->alterColumn($this->table, 'code_technology_or_group', $this->integer()->defaultValue(null));
    }
}
