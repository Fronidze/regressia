<?php

use yii\db\Migration;

/**
 * Class m190804_063317_add_field_in_schedule
 */
class m190804_063317_add_field_in_schedule extends Migration
{
    private $schedule = '{{%schedule}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->schedule, 'date_last_lesson', $this->dateTime()->defaultValue(null)->after('date_completion'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->schedule, 'date_last_lesson');
    }
}
