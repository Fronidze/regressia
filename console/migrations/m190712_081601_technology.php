<?php

use yii\db\Migration;

/**
 * Class m190712_081601_technology
 */
class m190712_081601_technology extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%technology}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'left_description' => $this->text()->defaultValue(null),
            'right_description' => $this->text()->defaultValue(null),
            'program' => $this->text()->defaultValue(null),
            'create_at' => $this->dateTime()->defaultValue(null),
            'update_at' => $this->dateTime()->defaultValue(null),
            'create_by' => $this->integer()->defaultValue(null),
            'update_by' => $this->integer()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%technology}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190712_081601_technology cannot be reverted.\n";

        return false;
    }
    */
}
