<?php

use yii\db\Migration;

/**
 * Class m190726_180945_modify_add_sorting_schedule
 */
class m190726_180945_modify_add_sorting_schedule extends Migration
{
    private $schedule = '{{%schedule}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->schedule, 'sorting', $this->integer()->defaultValue(1000));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->schedule, 'sorting');
    }
}
