<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%exam-preparation}}`.
 */
class m191121_065717_create_exam_preparation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%exam_preparation}}', [
            'id'               => $this->primaryKey(),
            'sub_title'        => $this->string()->defaultValue(null),
            'date_start'       => $this->dateTime()->defaultValue(null),
            'date_completion'  => $this->dateTime()->defaultValue(null),
            'publish'          => $this->string()->defaultValue('no'),
            'status'           => $this->string()->notNull(),
            'code'             => $this->string()->defaultValue(null),
            'teacher_id'       => $this->integer()->notNull(),
            'responsible_id'   => $this->integer()->defaultValue(null),
            'template_exam_id' => $this->integer()->notNull(),
            'create_at'        => $this->dateTime()->defaultValue(null),
            'update_at'        => $this->dateTime()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%exam_preparation}}');
    }
}
