<?php

use yii\db\Migration;

/**
 * Handles adding table_mobile to table `{{%group}}`.
 */
class m190911_114759_add_table_mobile_column_to_group_table extends Migration
{
    private $table = '{{%group}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'table_mobile', $this->text()->defaultValue(null)->after('table'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_mobile');
    }
}
