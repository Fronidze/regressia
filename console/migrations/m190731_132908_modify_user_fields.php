<?php

use yii\db\Migration;

/**
 * Class m190731_132908_modify_user_fields
 */
class m190731_132908_modify_user_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'position', $this->string()->defaultValue(null));
        $this->addColumn('{{%user}}', 'social', $this->string()->defaultValue(null));
        $this->addColumn('{{%user}}', 'image', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'position');
        $this->dropColumn('{{%user}}', 'social');
        $this->dropColumn('{{%user}}', 'image');
    }

}
