<?php

use yii\db\Migration;

/**
 * Class m191125_135034_modify_exam_preparation
 */
class m191125_135034_modify_exam_preparation extends Migration
{
    protected $table = '{{%exam_preparation}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn($this->table, 'teacher_id');
        $this->addColumn($this->table, 'lecturer', $this->string()->defaultValue(null)->after('code'));
        $this->dropColumn($this->table, 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'lecturer');
        $this->addColumn($this->table, 'teacher_id', $this->integer()->notNull()->after('code'));
        $this->addColumn($this->table, 'status', $this->string()->notNull());
    }
}
