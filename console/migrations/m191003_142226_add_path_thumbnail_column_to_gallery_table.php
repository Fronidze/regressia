<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%gallery}}`.
 */
class m191003_142226_add_path_thumbnail_column_to_gallery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%gallery}}', 'path_thumbnail', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%gallery}}', 'path_thumbnail');
    }
}
