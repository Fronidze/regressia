<?php

use common\models\Schedule;
use yii\db\Migration;

/**
 * Class m200425_093448_add_schedule_fields_quantity
 */
class m200425_093448_add_schedule_fields_quantity extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Schedule::tableName(), 'quantity_vacancies', $this->integer()->after('quantity_lesson'));
        $this->addColumn(Schedule::tableName(), 'quantity_organizations', $this->integer()->after('quantity_lesson'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Schedule::tableName(), 'quantity_vacancies');
        $this->dropColumn(Schedule::tableName(), 'quantity_organizations');
    }
}
