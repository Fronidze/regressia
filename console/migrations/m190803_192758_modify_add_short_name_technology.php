<?php

use yii\db\Migration;

/**
 * Class m190803_192758_modify_add_short_name_technology
 */
class m190803_192758_modify_add_short_name_technology extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%technology}}', 'short_title', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%technology}}', 'short_title');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190803_192758_modify_add_short_name_technology cannot be reverted.\n";

        return false;
    }
    */
}
