<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%weekly_exam_preparation}}`.
 */
class m191121_082028_create_weekly_exam_preparation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%weekly_exam_preparation}}', [
            'id'                  => $this->primaryKey(),
            'exam_preparation_id' => $this->integer()->notNull(),
            'monday'              => $this->string()->defaultValue(null),
            'tuesday'             => $this->string()->defaultValue(null),
            'wednesday'           => $this->string()->defaultValue(null),
            'thursday'            => $this->string()->defaultValue(null),
            'friday'              => $this->string()->defaultValue(null),
            'saturday'            => $this->string()->defaultValue(null),
            'sunday'              => $this->string()->defaultValue(null),
            'create_at'           => $this->dateTime()->defaultValue(null),
            'update_at'           => $this->dateTime()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%weekly_exam_preparation}}');
    }
}
