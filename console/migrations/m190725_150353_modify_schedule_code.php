<?php

use yii\db\Migration;

/**
 * Class m190725_150353_modify_schedule_code
 */
class m190725_150353_modify_schedule_code extends Migration
{
    private $schedule = '{{%schedule}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->schedule, 'code', $this->string()->notNull());
        $this->addColumn($this->schedule, 'publish', $this->string()->defaultValue('no'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->schedule, 'code');
        $this->dropColumn($this->schedule, 'publish');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190725_150353_modify_schedule_code cannot be reverted.\n";

        return false;
    }
    */
}
