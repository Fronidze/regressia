<?php

use yii\db\Migration;

/**
 * Class m191025_135735_modify_gallery
 */
class m191025_135735_modify_gallery extends Migration
{
    protected $table = '{{%gallery}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable($this->table);
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'image_id' => $this->integer()->notNull(),
            'publish' => $this->boolean()->defaultValue(0),
            'sorting' => $this->integer()->defaultValue(1000),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'title' => $this->string()->defaultValue(null),
            'path' => $this->string()->defaultValue(null),
            'filename' => $this->string()->defaultValue(null),
            'filesize' => $this->string()->defaultValue(null),
            'publish' => $this->string()->defaultValue('no'),
            'sorting' => $this->integer()->defaultValue(1000),
        ]);
    }
}
