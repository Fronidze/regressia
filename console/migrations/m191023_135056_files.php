<?php

use yii\db\Migration;

/**
 * Class m191023_135056_files
 */
class m191023_135056_files extends Migration
{

    protected $table = '{{%files}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'type' => $this->string()->defaultValue(null),
            'size' => $this->integer()->defaultValue(null),
            'relative_path' => $this->string()->notNull(),
            'full_path' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
