<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m190728_083609_course_request
 */
class m190728_083609_course_request extends Migration
{
    public $table = '{{%course_request}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'name' => $this->string()->defaultValue(null),
            'university' => $this->string()->defaultValue(null),
            'technology' => $this->integer()->defaultValue(null),
            'date_create' => $this->dateTime()->defaultValue(new Expression('NOW()')),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190728_083609_course_request cannot be reverted.\n";

        return false;
    }
    */
}
