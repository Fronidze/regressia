<?php

use yii\db\Migration;

/**
 * Class m191204_131649_add_sub_status_column_exam_preparation_table
 */
class m191204_131649_add_sub_status_column_exam_preparation_table extends Migration
{
    protected $table = '{{%exam_preparation}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'sub_status', $this->string()->defaultValue("")->after('status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'sub_status');
    }
}
