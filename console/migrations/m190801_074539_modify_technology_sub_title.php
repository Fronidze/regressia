<?php

use yii\db\Migration;

/**
 * Class m190801_074539_modify_technology_sub_title
 */
class m190801_074539_modify_technology_sub_title extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%technology}}', 'sub_title', $this->string()->defaultValue(null)->after('title'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%technology}}', 'sub_title');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190801_074539_modify_technology_sub_title cannot be reverted.\n";

        return false;
    }
    */
}
