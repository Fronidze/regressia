<?php

use yii\db\Migration;

/**
 * Class m190717_131311_modifire_apply_fields
 */
class m190717_131311_modifire_apply_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%teacher}}', 'is_publish', $this->string()->defaultValue('yes'));
        $this->addColumn('{{%technology}}', 'is_publish', $this->string()->defaultValue('yes'));
        $this->dropColumn('{{%technology}}', 'create_by');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%teacher}}', 'is_publish');
        $this->dropColumn('{{%technology}}', 'is_publish');
        $this->addColumn('{{%technology}}', 'create_by', $this->integer()->defaultValue(null));
    }
}
