<?php

use yii\db\Migration;

/**
 * Class m190720_083436_remove_ispublis_from_technology
 */
class m190720_083436_remove_ispublis_from_technology extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%technology}}', 'is_publish');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%technology}}', 'is_publish', $this->string()->defaultValue('yes'));
    }
}
