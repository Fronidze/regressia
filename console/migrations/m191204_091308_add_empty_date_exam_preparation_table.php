<?php

use yii\db\Migration;

/**
 * Class m191204_091308_add_empty_date_exam_preparation_table
 */
class m191204_091308_add_empty_date_exam_preparation_table extends Migration
{
    protected $table = '{{%exam_preparation}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'empty_date', $this->string()->defaultValue("no")->after('publish'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'empty_date');
    }
}
