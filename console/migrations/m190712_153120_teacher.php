<?php

use yii\db\Migration;

/**
 * Class m190712_153120_teacher
 */
class m190712_153120_teacher extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%teacher}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'position' => $this->string()->notNull(),
            'image' => $this->string()->defaultValue(null),
            'create_at' => $this->dateTime()->defaultValue(null),
            'update_at' => $this->dateTime()->defaultValue(null),
            'update_by' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%teacher}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190712_153120_teacher cannot be reverted.\n";

        return false;
    }
    */
}
