<?php

use yii\db\Migration;

/**
 * Class m190808_131433_statistics_course
 */
class m190808_131433_statistics_course extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%statistics_course}}', [
            'id' => $this->primaryKey(),
            'schedule_id' => $this->integer()->defaultValue(null),
            'records' => $this->integer()->defaultValue(null),
            'total_completed' => $this->integer()->defaultValue(null),
            'listened' => $this->integer()->defaultValue(null),
            'completed' => $this->integer()->defaultValue(null),
            'accepted' => $this->integer()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%statistics_course}}');
    }
}
