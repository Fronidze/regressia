<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%questions}}`.
 */
class m191113_060900_create_questions_table extends Migration
{
    protected $table = '{{%questions}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'question' => $this->text()->notNull(),
            'answer' => $this->text()->notNull(),
            'is_publish' => $this->boolean()->defaultValue(0),
            'sorting' => $this->integer()->defaultValue(1000),
            'create_at' => $this->dateTime()->defaultValue(null),
            'update_at' => $this->dateTime()->defaultValue(null)
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%questions}}');
    }
}
