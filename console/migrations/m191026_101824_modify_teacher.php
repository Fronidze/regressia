<?php

use yii\db\Migration;

/**
 * Class m191026_101824_modify_teacher
 */
class m191026_101824_modify_teacher extends Migration
{

    protected $table = '{{%teacher}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // Правки поля изображений
        $this->dropColumn($this->table, 'image');
        $this->addColumn($this->table, 'image_id', $this->integer()->defaultValue(null)->after('position'));

        // Правки поля статуса публикации
        $this->dropColumn($this->table, 'is_publish');
        $this->addColumn($this->table, 'is_publish', $this->boolean()->defaultValue(0)->after('update_by'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'image_id');
        $this->addColumn($this->table, 'image', $this->string()->defaultValue(null)->after('position'));

        $this->dropColumn($this->table, 'is_publish');
        $this->addColumn($this->table, 'is_publish', $this->string()->defaultValue('no')->after('update_by'));
    }
}
