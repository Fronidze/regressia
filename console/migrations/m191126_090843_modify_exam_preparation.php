<?php

use yii\db\Migration;

/**
 * Class m191126_090843_modify_exam_preparation
 */
class m191126_090843_modify_exam_preparation extends Migration
{
    protected $table = '{{%exam_preparation}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'title', $this->string()->defaultValue(null)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'title');
    }
}
