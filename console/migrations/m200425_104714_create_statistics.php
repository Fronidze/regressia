<?php

use yii\db\Migration;

/**
 * Class m200425_104714_create_statistics
 */
class m200425_104714_create_statistics extends Migration
{

    protected $table = '{{%statistics}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'schedule_id' => $this->integer(),
            'avg_rating' => $this->float(),
            'quantity_students' => $this->integer(),
            'quantity_vacancies' => $this->integer(),
            'quantity_organizations' => $this->integer(),
            'quantity_lesson' => $this->integer(),
            'avg_quantity_course' => $this->float(),
            'avg_quantity_rating' => $this->float(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
