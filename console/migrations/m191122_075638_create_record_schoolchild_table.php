<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%record_schoolchild}}`.
 */
class m191122_075638_create_record_schoolchild_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%record_schoolchild}}', [
            'id'                  => $this->primaryKey(),
            'schoolchild_id'      => $this->integer()->notNull(),
            'exam_preparation_id' => $this->integer()->notNull(),
            'date_create'         => $this->dateTime()->defaultValue(new Expression('NOW()')),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%record_schoolchild}}');
    }
}
