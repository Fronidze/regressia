<?php

use yii\db\Migration;

/**
 * Class m190716_110501_schedule
 */
class m190716_110501_schedule extends Migration
{

    private $schedule = '{{%schedule}}';
    private $weekly_schedule = '{{%weekly_schedule}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->schedule, [
            'id' => $this->primaryKey(),
            'technology_id' => $this->integer()->defaultValue(null),
            'teacher_id' => $this->integer()->defaultValue(null),
            'program' => $this->text()->defaultValue(null),
            'date_start' => $this->dateTime()->defaultValue(null),
            'date_completion' => $this->dateTime()->defaultValue(null),
            'quantity_lesson' => $this->integer()->defaultValue(null),
            'quantity_order' => $this->integer()->defaultValue(null),
            'quantity_finish' => $this->integer()->defaultValue(null),
            'quantity_execute' => $this->integer()->defaultValue(null),
            'quantity_intern' => $this->integer()->defaultValue(null),
            'date_create' => $this->dateTime()->defaultValue(null),
            'date_update' => $this->dateTime()->defaultValue(null),
            'update_by' => $this->integer()->notNull(),
        ]);

        $this->createTable($this->weekly_schedule, [
            'id' => $this->primaryKey(),
            'schedule_id' => $this->integer()->notNull(),
            'monday' => $this->string()->defaultValue(null),
            'tuesday' => $this->string()->defaultValue(null),
            'wednesday' => $this->string()->defaultValue(null),
            'thursday' => $this->string()->defaultValue(null),
            'friday' => $this->string()->defaultValue(null),
            'saturday' => $this->string()->defaultValue(null),
            'sunday' => $this->string()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->schedule);
        $this->dropTable($this->weekly_schedule);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190716_110501_schedule cannot be reverted.\n";

        return false;
    }
    */
}
