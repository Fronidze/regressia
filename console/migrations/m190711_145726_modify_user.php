<?php

use yii\db\Migration;

/**
 * Class m190711_145726_modify_user
 */
class m190711_145726_modify_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%user}}', '[[auth_key]]');
        $this->dropColumn('{{%user}}', '[[password_reset_token]]');
        $this->dropColumn('{{%user}}', '[[verification_token]]');

        $this->alterColumn('{{%user}}', '[[created_at]]', $this->dateTime()->defaultValue(null));
        $this->alterColumn('{{%user}}', '[[updated_at]]', $this->dateTime()->defaultValue(null));
        $this->alterColumn('{{%user}}', '[[status]]', $this->string()->defaultValue(null));
        $this->alterColumn('{{%user}}', '[[username]]', $this->string()->defaultValue(null));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190711_145726_modify_user was reverted.\n";
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190711_145726_modify_user cannot be reverted.\n";

        return false;
    }
    */
}
