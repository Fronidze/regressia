<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%template_exam_preparation}}`.
 */
class m191121_120356_create_template_exam_preparation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%template_exam_preparation}}', [
            'id'                => $this->primaryKey(),
            'title'             => $this->string()->notNull(),
            'left_description'  => $this->text()->defaultValue(null),
            'right_description' => $this->text()->defaultValue(null),
            'about_course'      => $this->text()->defaultValue(null),
            'program'           => $this->text()->defaultValue(null),
            'create_at'         => $this->dateTime()->defaultValue(null),
            'update_at'         => $this->dateTime()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%template_exam_preparation}}');
    }
}
