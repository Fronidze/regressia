<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m190729_130234_record
 */
class m190729_130234_record extends Migration
{
    public $table = '{{%record}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'student_id' => $this->integer()->notNull(),
            'schedule_id' => $this->integer()->notNull(),
            'date_create' => $this->dateTime()->defaultValue(new Expression('NOW()')),
            'listened' => $this->tinyInteger()->defaultValue(0),
            'completed' => $this->tinyInteger()->defaultValue(0),
            'accepted' => $this->tinyInteger()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190729_130234_record cannot be reverted.\n";

        return false;
    }
    */
}
