<?php

use yii\db\Migration;
use yii\db\Expression;

/**
 * Handles the creation of table `{{%event}}`.
 */
class m190916_090315_create_event_table extends Migration
{
    public $table = '{{%event}}';
    public $link_table = '{{%participants_event}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->text(),
            'user_id' => $this->integer()->notNull(),
            'place' => $this->string()->defaultValue(null),
            'date_event' => $this->dateTime()->defaultValue(null),
            'date_reminder' => $this->dateTime()->defaultValue(null),
            'date_create' => $this->dateTime()->defaultValue(new Expression('NOW()')),
            'date_update' => $this->dateTime()->defaultValue(null),
        ]);

        $this->createTable($this->link_table, [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
        $this->dropTable($this->link_table);
    }
}
