<?php

use yii\db\Migration;

/**
 * Class m190726_140515_modify_teacher_remove_links
 */
class m190726_140515_modify_teacher_remove_links extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%teacher_ability}}');
        $this->addColumn('{{%teacher}}', 'curator', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%teacher_ability}}', [
            'id' => $this->primaryKey(),
            'teacher_id' => $this->integer()->notNull(),
            'ability_id' => $this->integer()->notNull(),
        ]);
        $this->dropColumn('{{%teacher}}', 'curator');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190726_140515_modify_teacher_remove_links cannot be reverted.\n";

        return false;
    }
    */
}
