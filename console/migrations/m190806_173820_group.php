<?php

use yii\db\Migration;

/**
 * Class m190806_173820_group
 */
class m190806_173820_group extends Migration
{
    public $table = '{{%group}}';
    public $link_table = '{{%group_schedule}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'code' => $this->string()->defaultValue(null),
            'title' => $this->string()->defaultValue(null),
            'full_title' => $this->string()->defaultValue(null),
            'date_start' => $this->dateTime()->defaultValue(null),
            'left_side' => $this->text()->defaultValue(null),
            'right_side' => $this->text()->defaultValue(null),
            'table' => $this->text()->defaultValue(null),
            'responsible' => $this->integer()->defaultValue(null),
            'sorting' => $this->integer()->defaultValue(10000),
            'publish' => $this->smallInteger()->defaultValue(0),
        ]);

        $this->createTable($this->link_table, [
            'id' => $this->primaryKey(),
            'schedule_id' => $this->integer()->notNull(),
            'group_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
        $this->dropTable($this->link_table);
    }
}
