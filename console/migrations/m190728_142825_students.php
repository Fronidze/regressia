<?php

use yii\db\Migration;

/**
 * Class m190728_142825_students
 */
class m190728_142825_students extends Migration
{
    public $table = '{{%students}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'name' => $this->string()->defaultValue(null),
            'social' => $this->string()->defaultValue(null),
            'abilities' => $this->text()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190728_142825_students cannot be reverted.\n";

        return false;
    }
    */
}
