<?php

use yii\db\Migration;

/**
 * Class m190717_154455_modifire_schedule
 */
class m190717_154455_modifire_schedule extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%schedule}}', 'quantity_order');
        $this->dropColumn('{{%schedule}}', 'quantity_finish');
        $this->dropColumn('{{%schedule}}', 'quantity_execute');
        $this->dropColumn('{{%schedule}}', 'quantity_intern');
        $this->addColumn('{{%schedule}}', 'status', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%schedule}}', 'quantity_order', $this->integer()->defaultValue(null));
        $this->addColumn('{{%schedule}}', 'quantity_finish', $this->integer()->defaultValue(null));
        $this->addColumn('{{%schedule}}', 'quantity_execute', $this->integer()->defaultValue(null));
        $this->addColumn('{{%schedule}}', 'quantity_intern',$this->integer()->defaultValue(null));
        $this->dropColumn('{{%schedule}}', 'status');
    }
}
