<?php

use yii\db\Migration;

/**
 * Class m190728_094243_gallery
 */
class m190728_094243_gallery extends Migration
{

    public $table = '{{%gallery}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'title' => $this->string()->defaultValue(null),
            'path' => $this->string()->defaultValue(null),
            'filename' => $this->string()->defaultValue(null),
            'filesize' => $this->string()->defaultValue(null),
            'publish' => $this->string()->defaultValue('no'),
            'sorting' => $this->integer()->defaultValue(1000),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }

}
