<?php

use yii\db\Migration;

/**
 * Class m191126_132554_modify_exam_preparation
 */
class m191126_132554_modify_exam_preparation extends Migration
{
    protected $table = '{{%exam_preparation}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'register', $this->string()->defaultValue("no")->after('publish'));
        $this->addColumn($this->table, 'status', $this->string()->defaultValue("")->after('publish'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'register');
        $this->dropColumn($this->table, 'status');
    }
}
