<?php

use common\models\Record;
use yii\db\Migration;

/**
 * Class m200425_102437_add_field_rating_for_record
 */
class m200425_102437_add_field_rating_for_record extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Record::tableName(), 'rating', $this->integer()->after('date_create'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Record::tableName(), 'rating');
    }
}
