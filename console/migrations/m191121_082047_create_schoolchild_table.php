<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%schoolchild}}`.
 */
class m191121_082047_create_schoolchild_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%schoolchild}}', [
            'id'        => $this->primaryKey(),
            'email'     => $this->string()->notNull(),
            'name'      => $this->string()->defaultValue(null),
            'social'    => $this->string()->defaultValue(null),
            'create_at' => $this->dateTime()->defaultValue(null),
            'update_at' => $this->dateTime()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%schoolchild}}');
    }
}
