<?php

use yii\db\Migration;

/**
 * Class m190801_082555_modify_schedule_responsible
 */
class m190801_082555_modify_schedule_responsible extends Migration
{
    private $schedule = '{{%schedule}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->schedule, 'responsible_id', $this->integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->schedule, 'responsible_id');
    }
}
