<?php

use yii\db\Migration;

/**
 * Class m191024_131548_user_drop_column_image
 */
class m191024_131548_user_drop_column_image extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%user}}', 'image');
        $this->addColumn('{{%user}}', 'image_id', $this->integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%user}}', 'image', $this->string()->defaultValue(null));
        $this->dropColumn('{{%user}}', 'image_id');
    }


}
