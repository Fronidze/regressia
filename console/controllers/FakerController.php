<?php

namespace console\controllers;

use common\models\Record;
use common\models\Students;
use Faker\Factory;
use Faker\Generator;
use yii\console\Controller;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\web\Application;

class FakerController extends Controller
{
    /** @var Generator */
    protected $faker;

    public function init()
    {
        $this->faker = Factory::create('ru_RU');
        parent::init();
    }

    public function actionMakeFakeUsers(int $count = null)
    {
        $count = $count ?? 100;
        $users = [];
        for ($index = 0; $index < $count; $index++) {
            $users[$index] = [
                'email' => $this->faker->email,
                'name' => $this->faker->name('male'),
                'social' => $this->faker->url,
                'abilities' => $this->faker->text(100),
            ];
        }

        try {
            \Yii::$app->db->createCommand()
                ->batchInsert(Students::tableName(), ['email', 'name', 'social', 'abilities'], $users)
                ->execute();
            $this->stdout("Добавление новых студентов прошло успешно\n", Console::FG_GREEN);
        } catch (\Throwable $exception) {
            $this->stdout($exception->getMessage() . "\n", Console::FG_RED);
        }
    }

    public function actionAttachUsers(int $schedule_id)
    {
        $config = $this->getScheduleConfig($schedule_id);
        $map_rating = $config['rates'];

        $rows = [];
        $student_used = [];

        foreach ($map_rating as $rating => $count_students) {
            $query_students = (new Query())
                ->select('id')
                ->from(Students::tableName())
                ->where(['not in', 'id', $student_used])
                ->orderBy(new Expression('rand()'))
                ->limit($count_students);
            
            $students_id = ArrayHelper::getColumn($query_students->all(), 'id');
            $student_used = ArrayHelper::merge($student_used, $students_id);

            foreach ($students_id as $student_id) {
                $rows[] = [
                    'student_id' => $student_id,
                    'schedule_id' => $schedule_id,
                    'date_create' => (new \DateTime())->format('Y-m-d H:i:s'),
                    'rating' => $rating,
                    'listened' => 1,
                    'completed' => $rating > 0,
                    'accepted' => 0,
                ];
            }
        }

        try {
            \Yii::$app->db->createCommand()
                ->batchInsert(Record::tableName(), [
                    'student_id',
                    'schedule_id',
                    'date_create',
                    'rating',
                    'listened',
                    'completed',
                    'accepted',
                ], $rows)->execute();
            $this->stdout("Прикрепление прошло успешно\n", Console::FG_GREEN);
        } catch (\Throwable $exception) {
            $this->stdout($exception->getMessage() . "\n", Console::FG_RED);
        }

    }

    protected function getScheduleConfig(int $schedule_id)
    {
        $config = [
            1 => [
                'count' => 33,
                'rates' => ['0' => 1, '1' => 0, '2' => 0, '3' => 12, '4' => 13, '5' => 7],
            ],
            2 => [
                'count' => 53,
                'rates' => ['0' => 2, '1' => 0, '2' => 0, '3' => 11, '4' => 24, '5' => 16],
            ],
            3 => [
                'count' => 61,
                'rates' => ['0' => 6, '1' => 0, '2' => 0, '3' => 18, '4' => 24, '5' => 12],
            ],
            4 => [
                'count' => 74,
                'rates' => ['0' => 4, '1' => 0, '2' => 0, '3' => 22, '4' => 33, '5' => 15],
            ],
            5 => [
                'count' => 71,
                'rates' => ['0' => 7, '1' => 0, '2' => 0, '3' => 22, '4' => 28, '5' => 14],
            ],
            6 => [
                'count' => 31,
                'rates' => ['0' => 2, '1' => 0, '2' => 0, '3' => 9, '4' => 14, '5' => 6],
            ],
            7 => [
                'count' => 49,
                'rates' => ['0' => 5, '1' => 0, '2' => 0, '3' => 10, '4' => 17, '5' => 17],
            ],
            8 => [
                'count' => 94,
                'rates' => ['0' => 0, '1' => 0, '2' => 0, '3' => 9, '4' => 38, '5' => 47],
            ],
            9 => [
                'count' => 30,
                'rates' => ['0' => 2, '1' => 0, '2' => 0, '3' => 3, '4' => 13, '5' => 12],
            ],
            10 => [
                'count' => 110,
                'rates' => ['0' => 6, '1' => 0, '2' => 0, '3' => 11, '4' => 32, '5' => 61],
            ],
            11 => [
                'count' => 158,
                'rates' => ['0' => 0, '1' => 0, '2' => 0, '3' => 24, '4' => 55, '5' => 79],
            ],
            12 => [
                'count' => 77,
                'rates' => ['0' => 3, '1' => 0, '2' => 0, '3' => 8, '4' => 39, '5' => 27],
            ],
        ];

        return $config[$schedule_id];
    }

}
