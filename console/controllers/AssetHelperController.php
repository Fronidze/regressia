<?php

namespace console\controllers;

use yii\console\controllers\AssetController as BaseAssetController;
use yii\console\Exception;
use yii\web\AssetBundle;

class AssetHelperController extends BaseAssetController
{
    /**
     * @inheritDoc
     */
    protected function buildTarget($target, $type, $bundles)
    {
        $inputFiles = [];
        foreach ($target->depends as $name) {
            if (isset($bundles[$name])) {
                if (!$this->isBundleExternal($bundles[$name])) {
                    foreach ($bundles[$name]->$type as $file) {
                        if (is_array($file)) {
                            $inputFiles[] = $bundles[$name]->basePath . '/' . $file[0];
                        } else {
                            $inputFiles[] = $bundles[$name]->basePath . '/' . $file;
                        }
                    }
                }
            } else {
                throw new Exception("Unknown bundle: '{$name}'");
            }
        }

        if (empty($inputFiles)) {
            $target->$type = [];
        } else {
            $tempFile = $target->basePath . '/' . strtr($target->$type, ['{hash}' => 'temp']);
            $targetFile = strtr($target->$type, ['{hash}' => md5($tempFile)]);
            $target->$type = [$targetFile];
        }
    }

    /**
     * @param AssetBundle $bundle
     * @return bool whether asset bundle external or not.
     */
    private function isBundleExternal($bundle)
    {
        return empty($bundle->sourcePath) && empty($bundle->basePath);
    }
}
