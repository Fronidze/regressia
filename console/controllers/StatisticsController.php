<?php

namespace console\controllers;

use common\models\Schedule;
use services\statistics\RecordStatistics;
use yii\console\Controller;
use yii\db\Query;

class StatisticsController extends Controller
{

    /**
     * Перерасчет статистики курсов
     * @param null $id
     */
    public function actionRecount($id = null)
    {
        if ($id === null) {
            $query = (new Query())
                ->select(['id'])
                ->from(Schedule::tableName());

            $records = $query->column();
        } else {
            $records = [$id];
        }

        $this->recount($records);
    }

    private function recount(array $records)
    {
        foreach ($records as $record) {
            RecordStatistics::instance()->calculate($record);
        }
    }

}