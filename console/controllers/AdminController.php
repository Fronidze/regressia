<?php

namespace console\controllers;

use backend\forms\profile\AdminForm;
use services\profile\AdminService;
use yii\console\Controller;

class AdminController extends Controller
{

    public function actionCreate(string $email, string $password, string $username)
    {
        $admin = new AdminForm();
        $admin->email = $email;
        $admin->setPassword($password);
        $admin->username = $username;

        try {
            AdminService::instance()->create($admin);
        } catch (\Throwable $e) {
            echo 'User already exists';
        }
    }

}
