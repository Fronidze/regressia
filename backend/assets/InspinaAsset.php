<?php

namespace backend\assets;

use yii\web\AssetBundle;


class InspinaAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $css = [
        'css/bootstrap.min.css',
        'css/plugins/clockpicker/clockpicker.css',
        'font-awesome/css/font-awesome.css',
        'css/plugins/toastr/toastr.min.css',
        'js/plugins/gritter/jquery.gritter.css',
        'css/animate.css',
        'css/plugins/select2/select2.min.css',
        'css/plugins/cropper/cropper.min.css',
        'css/plugins/datapicker/datepicker3.css',
        'css/plugins/switchery/switchery.css',
        'css/plugins/dropzone/dropzone.css',
        'libs/fullcalendar/core/main.css',
        'libs/fullcalendar/daygrid/main.css',
        'css/plugins/iCheck/custom.css',
        'css/style.css',
    ];

    public $js = [
        'https://cdn.ckeditor.com/ckeditor5/12.3.0/classic/ckeditor.js',
//        'js/popper.min.js',
//        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js',
        'js/bootstrap.js',
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/slimscroll/jquery.slimscroll.min.js',
        'js/plugins/flot/jquery.flot.js',
        'js/plugins/flot/jquery.flot.tooltip.min.js',
        'js/plugins/flot/jquery.flot.spline.js',
        'js/plugins/flot/jquery.flot.resize.js',
        'js/plugins/flot/jquery.flot.pie.js',
        'js/plugins/peity/jquery.peity.min.js',
        'js/demo/peity-demo.js',
        'js/plugins/select2/select2.full.min.js',
        'js/plugins/cropper/cropper.min.js',
        'js/plugins/switchery/switchery.js',
        'js/plugins/chartJs/Chart.min.js',
        'js/inspinia.js',
        'js/plugins/pace/pace.min.js',
        'js/plugins/jquery-ui/jquery-ui.min.js',
        'js/plugins/datapicker/bootstrap-datepicker.js',
        'js/plugins/clockpicker/clockpicker.js',
        'js/plugins/gritter/jquery.gritter.min.js',
        'js/plugins/sparkline/jquery.sparkline.min.js',
        'js/demo/sparkline-demo.js',
        'js/plugins/chartJs/Chart.min.js',
        'js/plugins/toastr/toastr.min.js',
        'js/html5sortable.js',
        'js/plugins/dropzone/dropzone.js',
        'libs/fullcalendar/core/main.js',
        'libs/fullcalendar/daygrid/main.js',
        'libs/fullcalendar/interaction/main.js',
        'js/plugins/iCheck/icheck.min.js',
    ];

    public $depends = [
        'backend\assets\AppAsset',
        'backend\assets\JsAsset',
    ];
}
