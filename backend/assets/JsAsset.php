<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class JsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

    ];
    public $js = [
        'js/request.js',
        'js/ajax-modal.js',
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU&coordorder=latlong',
    ];
    public $depends = [
    ];
}
