<?php

namespace backend\assets;

use yii\web\AssetBundle;


class LoginInspinaAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $css = [
        'css/bootstrap.min.css',
        'font-awesome/css/font-awesome.css',
        'css/animate.css',
        'css/style.css',
    ];

    public $js = [
        'js/jquery-3.1.1.min.js',
        'js/popper.min.js',
        'js/bootstrap.js',
    ];

    public $depends = [];
}
