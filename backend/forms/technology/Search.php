<?php

namespace backend\forms\technology;

use common\models\Technology;
use yii\data\ActiveDataProvider;

class Search extends Technology
{
    public function search()
    {
        $request = \Yii::$app->request;
        $provider = new ActiveDataProvider([
            'query' => static::find()
                ->with(['editor'])
                ->orderBy(['create_at' => SORT_ASC]),
            'sort' => false,
        ]);

        if ($this->load($request->get()) === false) {
            return $provider;
        }

        return $provider;
    }
}