<?php

namespace backend\forms\group;

use common\models\Group;
use yii\data\ActiveDataProvider;

class SearchGroup extends Group
{
    public function search()
    {
        $request = \Yii::$app->request;
        $query = static::find()
            ->with(['person', 'schedules'])
            ->orderBy(['sorting' => SORT_ASC]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => false,
        ]);

        if ($this->load($request->post()) === false) {
            return $provider;
        }

        return $provider;
    }
}