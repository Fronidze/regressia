<?php

namespace backend\forms\group;

use backend\models\User;
use common\models\Group;
use common\models\Schedule;
use common\models\Technology;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class CreateGroup extends Group
{

    public $schedules = [];

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['schedules', 'required'];
        return $rules;
    }

    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $attributeLabels['schedules'] = 'Список курсов';
        return $attributeLabels;
    }

    public function listPersonal()
    {
        $query = (new Query())
            ->select(['id', 'username'])
            ->from(User::tableName());

        return ArrayHelper::map($query->all(), 'id', 'username');
    }

    public function listSchedules()
    {
        $query = (new Query())
            ->select(['s.id', 't.title', 's.date_start'])
            ->from(['s' => Schedule::tableName()])
            ->innerJoin(['t' => Technology::tableName()], ['t.id' => new Expression('s.technology_id')])
            ->orderBy(['s.sorting' => SORT_ASC]);

        $result = [];
        $records = $query->all();
        foreach ($records as $record) {
            $result[$record['id']] = \Yii::t('app', '{name} ( {date} )', [
                'name' => $record['title'],
                'date' => \Yii::$app->formatter->asDate($record['date_start'], 'long'),
            ]);
        }

        return $result;
    }
}