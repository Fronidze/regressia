<?php

namespace backend\forms\schedule;

use backend\models\User;
use common\models\Schedule;
use common\models\Teacher;
use common\models\Technology;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Create extends Schedule
{

    public $weekly;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['weekly', 'safe'];
        $rules[] = [['date_completion', 'date_last_lesson'], 'validateDate'];
        $rules[] = ['technology_id', 'required'];
        return $rules;
    }

    public function validateDate()
    {
        if (strtotime($this->date_completion) < strtotime($this->date_start) && !empty($this->date_completion)) {
            $this->addError('date_completion', 'Дата завершения курса не может быть раньше даты начала курса');
        }
        if (strtotime($this->date_last_lesson) < strtotime($this->date_start) && !empty($this->date_last_lesson)) {
            $this->addError('date_last_lesson', 'Дата последней лекции не может быть раньше даты начала курса');
        }
        if (strtotime($this->date_last_lesson) > strtotime($this->date_completion) && !empty($this->date_last_lesson) && !empty($this->date_completion)) {
            $this->addError('date_last_lesson', 'Дата последней лекции не может быть позже даты завершения курса');
        }
    }

    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $attributeLabels['weekly'] = 'Расписание';
        return $attributeLabels;
    }

    public function listTechnology()
    {
        $records = Technology::find()
            ->all();

        return ArrayHelper::map($records, 'id', 'title');
    }

    public function listTeacher()
    {

        $query = (new Query())
            ->select(['id' => 't.id', 'name' => 't.name'])
            ->from(['t' => Teacher::tableName()]);

        return ArrayHelper::map($query->all(), 'id', 'name');
    }

    public function listPersonal()
    {
        $query = (new Query())
            ->select(['id', 'username'])
            ->from(User::tableName());

        return ArrayHelper::map($query->all(), 'id', 'username');
    }
}