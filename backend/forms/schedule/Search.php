<?php

namespace backend\forms\schedule;

use common\models\Schedule;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class Search extends Schedule
{
    public function search()
    {
        $request = \Yii::$app->request;
        $query = static::find()
            ->with(['technology', 'teacher', 'editor'])
            ->orderBy([
                new Expression("case when status = 'draft' then 1 when status = 'archive' then 3 when status = 'active' then 2 end ASC"),
                'sorting' => SORT_ASC
            ]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => false,
        ]);

        if ($this->load($request->post()) === false) {
            return $provider;
        }

        $query->andFilterWhere(['=', 'status', $this->status]);

        return $provider;
    }
}