<?php

namespace backend\forms\teacher;

use common\models\Teacher;
use yii\data\ActiveDataProvider;

class Search extends Teacher
{
    public function search()
    {
        $request = \Yii::$app->request;
        $provider = new ActiveDataProvider([
            'query' => static::find()->orderBy(['sorting' => SORT_ASC]),
            'sort' => false,
        ]);

        if ($this->load($request->get()) === false) {
            return $provider;
        }

        return $provider;
    }
}