<?php

namespace backend\forms\teacher;

use common\models\abstractions\PublishActiveRecord;
use common\models\Teacher;

class Create extends Teacher
{

    public $file;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['is_publish', 'default', 'value' => PublishActiveRecord::PUBLISH_YES];
        $rules[] = ['file', 'file'];
        return $rules;
    }

    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $attributeLabels['file'] = 'Изображение';
        return $attributeLabels;
    }

}