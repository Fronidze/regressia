<?php

namespace backend\forms\record;

use common\models\CourseRequest;
use common\models\Group;
use common\models\Technology;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class SearchCourseRequest extends CourseRequest
{

    public $date_begin;
    public $date_end;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['date_begin', 'date_end'], 'safe'];
        return $rules;
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['date_begin'] = 'с даты';
        $labels['date_end'] = 'по дату';
        return $labels;
    }

    public function search()
    {
        $request = \Yii::$app->request;
        $query = static::find()->with(['technologyRecord', 'groupRecord'])->orderBy(['date_create' => SORT_DESC]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => ['pageSize' => 20]
        ]);

        if ($this->load($request->post()) === false) {
            return $provider;
        }

        $query->andFilterWhere(['>', 'date_create', $this->date_begin ? (new \DateTime($this->date_begin))->format('Y-m-d') : '']);
        $query->andFilterWhere(['<', 'date_create', $this->date_end ? (new \DateTime($this->date_end))->format('Y-m-d') : '']);
        $query->andFilterWhere([' = ', 'code_technology_or_group', $this->code_technology_or_group]);
        $query->andFilterWhere(['or', ['like', 'email', $this->name], ['like', 'name', $this->name]]);

        return $provider;
    }

    public function listTechnology()
    {
        $query = (new Query())
            ->select(['short_title', 'title'])
            ->from(Technology::tableName())
            ->orderBy(['title' => SORT_ASC]);

        return ArrayHelper::map($query->all(), 'short_title', 'title');
    }

    public function listGroup()
    {
        $query = (new Query())
            ->select(['code', 'title'])
            ->from(Group::tableName())
            ->orderBy(['title' => SORT_ASC]);

        return ArrayHelper::map($query->all(), 'code', 'title');
    }
}