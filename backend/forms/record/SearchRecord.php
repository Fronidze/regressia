<?php

namespace backend\forms\record;

use common\models\Record;
use common\models\Schedule;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;

class SearchRecord extends Record
{

    public $technology;
    public $name;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['technology', 'name'], 'safe'];
        return $rules;
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['technology'] = 'Направление';
        $labels['name'] = 'Поиск по ФИО или E-mail';
        return $labels;
    }

    public function search()
    {
        $request = \Yii::$app->request;
        $query = static::find()
            ->alias('record')
            ->innerJoinWith([
                'schedule' => function (ActiveQuery $relation) {
                    $relation->alias('schedule')->innerJoinWith([
                        'technology' => function (ActiveQuery $relation) {
                            $relation->alias('technology');
                        }
                    ]);
                },
                'student' => function (ActiveQuery $relation) {
                    $relation->alias('student');
                },
            ]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['date_create' => SORT_DESC]],
            'pagination' => ['pageSize' => 15],
        ]);

        if ($this->load($request->get()) === false) {
            $query->groupBy(['student_id', 'record.id']);
            return $provider;
        }

        $this->listened = empty($this->listened) === true ? null : 1;
        $this->completed = empty($this->completed) === true ? null : 1;

        if (
            empty($this->technology) === true &&
            empty($this->name) === true &&
            empty($this->listened) === true &&
            empty($this->completed) === true
        ) {
            $query->groupBy(['student_id', 'record.id']);
            return $provider;
        }

        if ($this->listened == 1 && $this->completed == 1) {
            $query->andFilterWhere(['or', ['=', 'record.listened', $this->listened], ['=', 'record.completed', $this->completed]]);
        } else {
            $query->andFilterWhere(['=', 'record.listened', $this->listened]);
            $query->andFilterWhere(['=', 'record.completed', $this->completed]);
        }


        $query->andFilterWhere(['=', 'schedule.technology_id', $this->technology]);
        $query->andFilterWhere(['=', 'record.schedule_id', $this->schedule_id]);
        $query->andFilterWhere([
            'or',
            ['like', 'student.name', $this->name],
            ['like', 'student.email', $this->name],
            ['like', 'student.abilities', $this->name],
        ]);

        return $provider;
    }

    public function listSchedule()
    {
        $query = (new Query())
            ->select(['s.id', 's.date_start'])
            ->from(['s' => Schedule::tableName()])
            ->where(['=', 's.technology_id', $this->technology])
            ->orderBy(['s.sorting' => SORT_ASC]);

        $records = $query->all();
        $result = [];
        foreach ($records as $record) {
            $result[$record['id']] = \Yii::$app->formatter->asDate($record['date_start'], 'long');
        }

        return $result;
    }
}