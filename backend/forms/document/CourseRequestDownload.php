<?php

namespace backend\forms\document;

use common\models\CourseRequest;
use yii\base\Model;
use yii\db\Query;

class CourseRequestDownload extends Model
{
    public $date_begin;
    public $date_end;
    public $technology;
    public $name;

    public function rules()
    {
        return [
            [['date_begin', 'date_end', 'technology', 'name'], 'safe'],
        ];
    }

    public function search()
    {
        $this->load(\Yii::$app->request->get());
        $query = (new Query())
            ->select(['email'])
            ->distinct(['email'])
            ->from(CourseRequest::tableName());

        if (empty($this->technology) === false) {
            $query->andWhere(['=', 'technology', $this->technology]);
        }

        if (empty($this->name) === false) {
            $query->andWhere(['or', ['like', 'name', $this->name], ['like', 'email', $this->name]]);
        }

        if (empty($this->date_begin) === false) {
            $query->andWhere(['>', 'date_create', $this->date_begin]);
        }

        if (empty($this->date_end) === false) {
            $query->andWhere(['<', 'date_create', $this->date_end]);
        }

        return $query->all();
    }

    public function getFileName()
    {
        return 'Предварительные записи на курсы';
    }
}