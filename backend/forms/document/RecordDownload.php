<?php

namespace backend\forms\document;

use common\models\Record;
use common\models\Schedule;
use common\models\Students;
use common\models\Technology;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;

class RecordDownload extends Model
{
    public $schedule_id;
    public $technology;
    public $name;

    public function rules()
    {
        return [
            [['schedule_id', 'technology', 'name'], 'safe'],
        ];
    }

    public function search()
    {
        $this->load(\Yii::$app->request->get());
        $query = (new Query())
            ->select(['student.email'])
            ->distinct(['student.email'])
            ->from(['r' => Record::tableName()])
            ->innerJoin(['student' => Students::tableName()], ['r.student_id' => new Expression('student.id')])
            ->innerJoin(['s' => Schedule::tableName()], ['r.schedule_id' => new Expression('s.id')])
            ->innerJoin(['t' => Technology::tableName()], ['s.technology_id' => new Expression('t.id')]);

        if (empty($this->technology) === false) {
            $query->andWhere(['=', 't.id', $this->technology]);
        }

        if (empty($this->technology) === false && empty($this->schedule_id) === false) {
            $query->andWhere(['=', 's.id', $this->schedule_id]);
        }

        if (empty($this->name) === false) {
            $query->andWhere([
                'or',
                ['like', 'student.name', $this->name],
                ['like', 'student.email', $this->name],
                ['like', 'student.abilities', $this->name]
            ]);
        }

        return $query->all();
    }

    public function getFileName()
    {
        $name = 'Список записавшихся студентов';
        return $name;
    }
}