<?php

namespace backend\forms\document;

use common\models\ExamPreparation;
use common\models\RecordSchoolchild;
use common\models\Schoolchild;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;

class RecordSchoolchildDownload extends Model
{
    public $exam_preparation_id;
    public $name;

    public function rules()
    {
        return [
            [['exam_preparation_id', 'name'], 'safe'],
        ];
    }

    public function search()
    {
        $this->load(\Yii::$app->request->get());
        $query = (new Query())
            ->select(['schoolchild.email'])
            ->distinct(['schoolchild.email'])
            ->from(['rs' => RecordSchoolchild::tableName()])
            ->innerJoin(['schoolchild' => Schoolchild::tableName()], ['rs.schoolchild_id' => new Expression('schoolchild.id')])
            ->innerJoin(['ep' => ExamPreparation::tableName()], ['rs.exam_preparation_id' => new Expression('ep.id')]);


        if (empty($this->schedule_id) === false) {
            $query->andWhere(['=', 'ep.id', $this->exam_preparation_id]);
        }

        if (empty($this->name) === false) {
            $query->andWhere([
                'or',
                ['like', 'schoolchild.name', $this->name],
                ['like', 'schoolchild.email', $this->name],
                ['like', 'schoolchild.abilities', $this->name]
            ]);
        }

        return $query->all();
    }

    public function getFileName()
    {
        $name = 'Список записавшихся школьников';
        return $name;
    }
}