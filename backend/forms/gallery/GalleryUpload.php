<?php

namespace backend\forms\gallery;

use yii\base\Model;

class GalleryUpload extends Model
{
    public $file;

    public function rules()
    {
        return [[['file'], 'file']];
    }

}