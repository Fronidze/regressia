<?php

namespace backend\forms\question;

use common\models\abstractions\PublishActiveRecord;
use common\models\Question;

class Create extends Question
{

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['is_publish', 'default', 'value' => PublishActiveRecord::PUBLISH_YES];
        return $rules;
    }

    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        return $attributeLabels;
    }

}