<?php

namespace backend\forms\question;

use common\models\Question;
use yii\data\ActiveDataProvider;

class Search extends Question
{
    public function search()
    {
        $request = \Yii::$app->request;
        $provider = new ActiveDataProvider([
            'query' => static::find()->orderBy(['sorting' => SORT_ASC]),
            'sort' => false,
        ]);

        if ($this->load($request->get()) === false) {
            return $provider;
        }

        return $provider;
    }
}