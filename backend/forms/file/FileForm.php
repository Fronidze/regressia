<?php

namespace backend\forms\file;

use yii\base\Model;

class FileForm extends Model
{
    /** @var  */
    public $file;

    public function rules()
    {
        return [['file', 'required']];
    }

}