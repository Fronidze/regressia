<?php

namespace backend\forms\recordschoolchild;

use common\models\RecordSchoolchild;
use yii\data\ActiveDataProvider;

class SearchRecordSchoolchild extends RecordSchoolchild
{

    public $name;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['name'], 'safe'];
        return $rules;
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = 'Поиск по ФИО или E-mail';
        return $labels;
    }

    public function search()
    {
        $request = \Yii::$app->request;

        $query = static::find()
            ->innerJoinWith('schoolchild')
            ->innerJoinWith('examPreparation');

        $provider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => ['defaultOrder' => ['date_create' => SORT_DESC]],
            'pagination' => ['pageSize' => 15],
        ]);

        if ($this->load($request->get()) === true) {
            $query->andFilterWhere(['=', 'record_schoolchild.exam_preparation_id', $this->exam_preparation_id]);
            $query->andFilterWhere([
                'or',
                ['like', 'schoolchild.name', $this->name],
                ['like', 'schoolchild.email', $this->name],
            ]);
        }

        return $provider;
    }


    public function listSchedule()
    {
        return 'list search record schoolchild';
    }
}