<?php

namespace backend\forms\student;

use common\models\Students;
use common\models\Technology;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class SearchStudent extends Students
{
    public $technology;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['technology'], 'safe'];
        return $rules;
    }

    public function search()
    {
        $request = \Yii::$app->request;
        $provider = new ActiveDataProvider([
            'query' => static::find()->with(['records', 'listened', 'completed']),
            'sort' => false,
        ]);

        if ($this->load($request->post()) === false) {
            return $provider;
        }

        return $provider;
    }

    public function listTechnology()
    {
        $query = (new Query())
            ->select(['id', 'title'])
            ->from(Technology::tableName())
            ->orderBy(['title' => SORT_ASC]);

        return ArrayHelper::map($query->all(), 'id', 'title');
    }
}