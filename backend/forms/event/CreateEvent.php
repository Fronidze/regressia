<?php

namespace backend\forms\event;

use backend\models\User;
use common\models\Event;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class CreateEvent extends Event
{

    public $users = [];

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['users', 'safe'];
        return $rules;
    }

    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $attributeLabels['users'] = 'Участники';
        return $attributeLabels;
    }

    public function listPersonal()
    {
        $query = (new Query())
            ->select(['id', 'username'])
            ->from(User::tableName());

        return ArrayHelper::map($query->all(), 'id', 'username');
    }

}