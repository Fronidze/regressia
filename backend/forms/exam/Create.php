<?php

namespace backend\forms\exam;

use backend\models\User;
use common\models\ExamPreparation;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Create extends ExamPreparation
{

    public $weekly;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['weekly', 'safe'];
        $rules[] = [['date_completion'], 'validateDate'];
        return $rules;
    }

    public function validateDate()
    {
        if (strtotime($this->date_completion) < strtotime($this->date_start) && !empty($this->date_completion)) {
            $this->addError('date_completion', 'Дата завершения курса не может быть раньше даты начала курса');
        }
    }

    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $attributeLabels['weekly'] = 'Расписание';
        return $attributeLabels;
    }

    public function listPersonal()
    {
        $query = (new Query())
            ->select(['id', 'username'])
            ->from(User::tableName());

        return ArrayHelper::map($query->all(), 'id', 'username');
    }
}