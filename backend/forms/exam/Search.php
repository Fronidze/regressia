<?php

namespace backend\forms\exam;

use common\models\ExamPreparation;
use yii\data\ActiveDataProvider;

class Search extends ExamPreparation
{
    public function search()
    {
        $request = \Yii::$app->request;
        $query = static::find();

        $provider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => false,
            'pagination' => false,
        ]);

        if ($this->load($request->post()) === false) {
            return $provider;
        }

        return $provider;
    }
}