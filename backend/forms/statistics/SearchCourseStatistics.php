<?php

namespace backend\forms\statistics;

use common\models\StatisticsCourse;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\db\Query;

class SearchCourseStatistics extends StatisticsCourse
{
    public function search()
    {
        $request = \Yii::$app->request;
        $query = (new Query())
            ->select([
                'year' => 'YEAR(schedule.date_start)',
                'records' => 'sum(statistics_course.records)',
                'total_completed' => 'sum(statistics_course.total_completed)',
                'listened' => 'sum(statistics_course.listened)',
                'completed' => 'sum(statistics_course.completed)',
                'accepted' => 'sum(statistics_course.accepted)',
            ])
            ->from('statistics_course')
            ->innerJoin('schedule', ['schedule.id' => new Expression('statistics_course.schedule_id')])
            ->groupBy('year');


        $provider = new SqlDataProvider([
            'sql' => $query->createCommand()->getRawSql(),
            'totalCount' => $query->count(),
        ]);

        $provider->setSort([
            'attributes' => [
                'year',
                'records',
            ],
        ]);

        if ($this->load($request->post()) === false) {
            return $provider;
        }

        return $provider;
    }
}