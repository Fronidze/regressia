<?php

namespace backend\forms\statistics;

use PhpOffice\PhpSpreadsheet\IOFactory;
use yii\base\Model;
use yii\db\Query;

class RegressionForm extends Model
{
    /** @var array */
    public $columns = ["y", "x1", "x2", "x3", "x4", "x5", "x6"];
    /** @var array */
    public $rows = [];
    /** @var float */
    public $relevance_min_value = 0.8;
    /** @var array */
    public $ignore_columns = [];

    public function rules()
    {
        return [
            [['relevance_min_value'], 'filter', 'filter' => function ($value) {
                return (float)$value;
            }],
            [['columns', 'rows', 'relevance_min_value', 'ignore_columns'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'relevance_min_value' => 'Проверка факторов на наличие коллинеарности',
        ];
    }

    public function getDataForRequest()
    {
        return [
            'columns' => $this->columns,
            'rows' => $this->prepareRows(),
            'relevance_min_value' => $this->relevance_min_value,
            'ignore_columns' => $this->prepareIgnoreColumns(),
        ];
    }

    public function getColumnLabels()
    {
        return [
            "y" => 'Средняя оценка за ИЗ студентов',
            "x1" => 'Кол-во студентов, записавшиеся на обучение',
            "x2" => 'Кол-во вакансий на данную технологию в регионе',
            "x3" => 'Кол-во организаций, работающих с этой технологией в регионе ',
            "x4" => 'Кол-во лекций на курс обучения ',
            "x5" => 'Среднее кол-во курсов пройденных до этого студентами',
            "x6" => 'Средняя оценка за ИЗ по курсам пройденных до этого студентами',
        ];
    }

    /**
     * Подготовка массива с данными к отправке
     *
     * @return array
     */
    protected function prepareRows(): array
    {
        $reverse_columns = array_flip($this->columns);
        $result = [];
        foreach ($this->rows as $index => $row) {
            $result_row = [];
            foreach ($row as $column_name => $value) {
                $key = $reverse_columns[$column_name];
                $result_row[$key] = (float)$value;
            }
            array_push($result, $result_row);
        }

        return $result;
    }

    protected function prepareIgnoreColumns()
    {
        $result = [];
        foreach ($this->ignore_columns as $code => $value) {
            if ((bool)$value === true) {
                array_push($result, $code);
            }
        }
        
        return $result;
    }

    public function initDefaultValues()
    {
        $file = \Yii::getAlias('@webroot/time.xlsx');
        if (file_exists($file) === true) {
            $reader = IOFactory::createReader('Xlsx');
            $reader->getReadDataOnly();

            $resource = $reader->load($file);
            $sheet = $resource->getActiveSheet();

            $begin_row = 2;
            $end_row = 13;

            $begin_cell = 2;
            $end_cell = 8;

            for ($row = $begin_row; $row <= $end_row; $row++) {
                $row_id = $sheet->getCellByColumnAndRow(1, $row)->getValue();
                for ($cell = $begin_cell; $cell <= $end_cell; $cell++) {
                    $cell_id = $sheet->getCellByColumnAndRow($cell, 1)->getValue();
                    $this->rows[$row_id][$cell_id] = $sheet->getCellByColumnAndRow($cell, $row)->getValue();
                }
            }
        }
    }

    public function initDatabaseValues()
    {
        $records = (new Query())
            ->from('statistics')
            ->all();

        foreach ($records as $index => $record) {
            $this->rows[$record['schedule_id']] = [
                'y' => $record['avg_rating'],
                'x1' => $record['quantity_students'],
                'x2' => $record['quantity_vacancies'],
                'x3' => $record['quantity_organizations'],
                'x4' => $record['quantity_lesson'],
                'x5' => $record['avg_quantity_course'],
                'x6' => $record['avg_quantity_rating'],
            ];
        }
    }
}