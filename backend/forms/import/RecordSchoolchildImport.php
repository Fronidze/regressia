<?php

namespace backend\forms\import;

use common\models\ExamPreparation;
use common\models\Schedule;
use common\models\Technology;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;

class RecordSchoolchildImport extends Model
{
    public $exam_preparation_id;
    public $excel;

    protected $add_schoolchilds = 0;
    protected $add_records = 0;
    protected $schoolchild_errors = [];

    public function rules()
    {
        return [
            [['exam_preparation_id', 'excel'], 'safe'],
        ];
    }

    public function appendStudentCounter()
    {
        $this->add_schoolchilds++;
    }

    public function appendRecordCounter()
    {
        $this->add_records++;
    }

    public function appendError(string $error)
    {
        $this->schoolchild_errors[] = $error;
    }

    public function getImportErrors()
    {
        return $this->schoolchild_errors;
    }

    public function countRecords()
    {
        return $this->add_records;
    }

    public function countStudents()
    {
        return $this->add_schoolchilds;
    }

    public function listExamPreparation()
    {
        $query = (new Query())
            ->select(['ep.id', 'ep.sub_title', 'ep.date_start'])
            ->from(['ep' => ExamPreparation::tableName()]);

        $result = [];
        $records = $query->all();
        foreach ($records as $record) {
            $result[$record['id']] = \Yii::t('app', '{name} ( {date} )', [
                'name' => $record['sub_title'],
                'date' => \Yii::$app->formatter->asDate($record['date_start'], 'long'),
            ]);
        }

        return $result;
    }
}