<?php

namespace backend\forms\import;

use common\models\Schedule;
use common\models\Technology;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;

class RecordImport extends Model
{
    public $schedule_id;
    public $excel;

    protected $add_students = 0;
    protected $add_records = 0;
    protected $student_errors = [];

    public function rules()
    {
        return [
            [['schedule_id', 'excel'], 'safe'],
        ];
    }

    public function appendStudentCounter()
    {
        $this->add_students++;
    }

    public function appendRecordCounter()
    {
        $this->add_records++;
    }

    public function appendError(string $error)
    {
        $this->student_errors[] = $error;
    }

    public function getImportErrors()
    {
        return $this->student_errors;
    }

    public function countRecords()
    {
        return $this->add_records;
    }

    public function countStudents()
    {
        return $this->add_students;
    }

    public function listSchedule()
    {
        $query = (new Query())
            ->select(['s.id', 't.title', 's.date_start'])
            ->from(['s' => Schedule::tableName()])
            ->innerJoin(['t' => Technology::tableName()], ['t.id' => new Expression('s.technology_id')]);

        $result = [];
        $records = $query->all();
        foreach ($records as $record) {
            $result[$record['id']] = \Yii::t('app', '{name} ( {date} )', [
                'name' => $record['title'],
                'date' => \Yii::$app->formatter->asDate($record['date_start'], 'long'),
            ]);
        }

        return $result;
    }
}