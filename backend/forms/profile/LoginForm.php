<?php

namespace backend\forms\profile;

use backend\models\User;
use yii\base\Model;

class LoginForm extends Model
{
    public $email;
    public $password;

    /** @var User|null */
    private $user = null;

    public function rules(): array
    {
        return [
            [['email', 'password'], 'string', 'max' => 255],
            [['email', 'password'], 'required'],
            ['password', function ($attribute, $params) {
                if ($this->hasErrors() === false) {
                    $user = $this->getUser();
                    if ($user === null || $user->validatePassword($this->password) === false) {
                        $this->addError($attribute, 'Проверьте правильноть логина или пароля');
                    }
                }
            }],
        ];
    }

    public function getUser()
    {
        if ($this->user === null) {
            $this->user = User::findByEmail($this->email);
        }
        return $this->user;
    }

    public function login(): bool
    {
        if ($this->validate() === false) {
            return false;
        }

        return \Yii::$app->user->login($this->getUser());
    }
}