<?php

namespace backend\forms\profile;

use backend\models\User;
use yii\data\ActiveDataProvider;

class SearchPersonal extends User
{
    public function search()
    {
        $request = \Yii::$app->request;
        $query = static::find();

        $provider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        if ($this->load($request->post()) === false) {
            return $provider;
        }

        return $provider;
    }
}