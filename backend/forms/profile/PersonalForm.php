<?php

namespace backend\forms\profile;

use backend\models\User;

class PersonalForm extends User
{
    public $file;

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['file', 'file'];
        return $rules;
    }

    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $attributeLabels['file'] = 'Изображение';
        return $attributeLabels;
    }
}