<?php

namespace backend\forms\student;

use common\models\ExamPreparation;
use common\models\Schoolchild;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class SearchSchoolchild extends Schoolchild
{

    public function rules()
    {
        $rules = parent::rules();
        return $rules;
    }

    public function search()
    {
        $request = \Yii::$app->request;
        $provider = new ActiveDataProvider([
            'query' => static::find()->with(['record_schoolchild']),
            'sort'  => false,
        ]);

        if ($this->load($request->post()) === false) {
            return $provider;
        }

        return $provider;
    }

    public function listTechnology()
    {
        $query = (new Query())
            ->select(['id', 'sub_title'])
            ->from(ExamPreparation::tableName())
            ->orderBy(['sub_title' => SORT_ASC]);

        return ArrayHelper::map($query->all(), 'id', 'sub_title');
    }
}