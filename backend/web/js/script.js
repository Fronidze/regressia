let switchers = [];

let load = () => {

    document.addEventListener('click', clickEvents);
    loadPlugins();
    initClickChangeStatus();
    $('.custom-file-input').on('change', function () {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });


    let zone = document.getElementById('gallery_zone');
    if (zone !== undefined && zone !== null) {
        let dropzone = new Dropzone('#gallery_zone', {
            url: zone.dataset['request']
        });
    }

    initChart();
    if ($("#calendar").length) {
        sendRequest('get', '/dashboard/schedule/calendar-events').then(xhr => {
            initCalendar(JSON.parse(xhr.responseText));
        });
    }

    $('.change_submit').on('change', (event) => {
        let form = event.target.closest('form');
        if (form !== null || form !== undefined) {
            form.submit();
        }
    });

    $(document).on('pjax:end', function () {
        loadPlugins()
    });

    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-green',
        cursor: true,
    });

    switchFormExamPreparation();
    loadPreviusTab();
    //toastr.success('Without any options','Simple notification!');
};

window.onload = load;

let loadPlugins = () => {
    selectWidget();
    initTextArea();
    initDataPicker();
    initClockPiker();
    initSortable();
    initSwitcher();
    initDelete();
};

let switchFormExamPreparation = () => {
    function loadSwitchExamPreparation() {
        if ($("#empty_date :selected").val() === "yes") {
            $('#block_without_date').hide();
            $('#block_from_date').show();
        } else {
            $('#block_without_date').show();
            $('#block_from_date').hide();
        }
    }

    loadSwitchExamPreparation();
    $('#empty_date').change(function (event) {
        loadSwitchExamPreparation();
    })
};

let loadPreviusTab = () => {
    $('#exam-preparation-tabs a').on('click', function (event) {
        $(this).tab('show');
    });
    var lastTab = localStorage.getItem('lastTab');

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        localStorage.setItem('lastTab', $(this).attr('href'));
    });
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    } else {
        lastTab = $('#exam-preparation-tabs a:first').attr('href');
        $('[href="' + lastTab + '"]').tab('show');
    }
};

let clickEvents = (event) => {
    let target = event.target;
    let data = target.dataset;
    let classList = target.classList;

    if (data['showModal'] !== undefined) {
        event.preventDefault();

        let modal = data['showModal'];
        let elementModal = document.getElementById(modal);
        let href = target.getAttribute('href');
        let ajax = new AjaxModal(elementModal);
        ajax.afterShow = () => {
            if (!$(document).find('#template-tab').length) {
                initTextArea();
            }
            $('#btn_update_course').on('click', function (ev) {
                let target = ev.target;
                let form = target.closest('form');
                let formdata = new FormData(form);
                sendRequest('post', form.getAttribute('action'), formdata)
                    .then(xhr => {
                        let result = JSON.parse(xhr.responseText);
                        if (result.msg === false) {
                            Object.keys(result.errors).forEach(error => {
                                var element = document.getElementById("create-" + error);
                                element.parentNode.parentNode.classList.add("has-error");
                                let examPreparation = $("#create-responsible_id");
                                if (error === "responsible_id") {
                                    checkSelectCoordinator(examPreparation, true);
                                } else {
                                    checkSelectCoordinator(examPreparation, false);
                                }
                            });
                        } else {
                            document.location.reload(true);
                        }
                    });
            });

            selectWidget();
            initDataPicker();
            initClockPiker();
            switchFormExamPreparation();
            initSwitcher();
        };
        ajax.setAjaxContent(href);
    }

    if (target.parentNode.parentNode.id === 'form_creat_exam_preparation') {
        let coordinatorExam = $("#create-responsible_id");
        if ($("#create-responsible_id :selected").val() === "") {
            checkSelectCoordinator(coordinatorExam, true);
        } else {
            checkSelectCoordinator(coordinatorExam, false);
        }
    }

    if (classList.contains('check_compare') === true) {
        event.preventDefault();
        let form = target.closest('form');
        let formdata = new FormData(form);
        sendRequest('post', form.dataset['compare'], formdata)
            .then(xhr => {
                let result = JSON.parse(xhr.responseText);
                let need_open = result['compare'];
                console.log(need_open);
                console.log(typeof need_open);
                if (need_open) {
                    let ajax = new AjaxModal(document.getElementById('myModal4'));
                    ajax.setContent(result['content']);
                    ajax.show();
                    ajax.setIcon(null);
                    ajax.setSubTitle('Ниже представлены данные записи что есть, и записи что вы пытаетесь добавить вы можете применить эти новые данные или же отказаться от изменения');
                    ajax.setTitle('Данный Email уже используеться в системе');
                } else {
                    form.submit();
                }
            });
    }

    if (classList.contains('regression_prediction_send') === true) {
        let form = target.closest('form');
        let formdata = new FormData(form);
        sendRequest('post', form.action, formdata)
            .then(xhr => {
                let prediction = xhr.responseText;
                let placeholder = document.getElementById('placeholder_result_prediction');
                placeholder.innerHTML = prediction;

                if (+prediction < 5) {
                    let hint_prediction = document.getElementById('hint_prediction');
                    hint_prediction.style.display = 'block';
                }

            });
    }
};

let checkSelectCoordinator = (coordinatorExam, hasEroor = false) => {

    if (hasEroor) {
        coordinatorExam.each(function () {
            $(this).siblings(".select2-container").css('border', '1px solid #ed5565');
        });
    } else {
        coordinatorExam.each(function () {
            $(this).siblings(".select2-container").css('border', '0px solid #ed5565');
        });
    }
};

let initTextArea = () => {
    let editors = document.querySelectorAll('.summernote');
    editors.forEach(editor => {
        ClassicEditor.create(editor).catch(error => {
            console.error(error);
        });
    });
};

let initDataPicker = () => {
    let datepiker = $('input.datepiker');
    $.fn.datepicker.dates['ru'] = {
        days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
        daysShort: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
        today: "Сегодня",
        clear: "Очистить",
        format: "dd.mm.yyyy",
        titleFormat: "MM yyyy",
        weekStart: 1
    };

    datepiker.datepicker({
        language: 'ru',
        autoclose: true,
    });

};

let selectWidget = () => {
    let select = $('select.select2');
    let select2_config = {width: '100%'};
    select.select2(select2_config);
};

let initClockPiker = () => {
    let clock_input = $('.clockpicker');
    clock_input.clockpicker({
        autoclose: true,
        placement: 'top',
        align: 'right',
    });
};

let initSwitcher = () => {
    let elements = document.querySelectorAll('input.js-switch');
    elements.forEach(element => {

        let switcher = null;
        switchers.filter(elem => {
            if (elem.element === element) {
                switcher = elem.widget;
            }
        });

        if (switcher === null) {
            if ($('#form_update_course').length) {
                switcher = new Switchery(element, {color: '#1ab394', size: 'small'});
            } else {
                switcher = new Switchery(element, {color: '#1ab394'});
            }
        }
        switchers.push({
            element: element,
            widget: switcher
        });

        if (!$('#form_update_course').length) {
            element.onchange = (event) => {
                let form = new FormData();
                form.append('checked', element.checked);
                sendRequest('post', element.dataset['request'], form)
                    .then(xhr => {
                        $.pjax.reload({container: '#list_exam_preparation'});
                    });
            };
        }
    });
};

let initDelete = () => {
    let elements = document.querySelectorAll('button.js-delete');
    elements.forEach(element => {
        element.onclick = (event) => {
            let form = new FormData();
            sendRequest('get', element.dataset['request'], form)
                .then(xhr => {
                    let result = JSON.parse(xhr.responseText);
                    let ajax = new AjaxModal(document.getElementById('myModal5'));
                    ajax.setContent(result['content']);
                    ajax.setSubTitle(result['question']);
                    ajax.setTitle('Подтвердите удаление вопроса:');
                    ajax.show();
                });
        }
    });
};

let initSortable = () => {
    let table = document.querySelector('table.sortable tbody');
    if (table === undefined || table === null) {
        return null;
    }
    sortable(table, {
        forcePlaceholderSize: true,
        handle: '.sortable_handle',
    });
    table.addEventListener('sortupdate', (event) => {
        let items = event.detail.origin.items;
        let formData = new FormData();
        let table = event.target.closest('table');

        let sort = 100;
        items.forEach(item => {
            formData.append('elements[' + item.dataset['key'] + ']', sort.toString());
            sort += 100;
        });
        sendRequest('post', table.dataset['request'], formData);
    });
};

let initClickChangeStatus = () => {
    $('.schedule_status_change').on('click', (event) => {
        event.preventDefault();
        let target = event.target;
        let block = target.closest('div.btn-group');
        sendRequest('get', target.href)
            .then(xhr => {
                if (xhr.status === 200) {
                    let buttons = block.querySelectorAll('.btn');
                    buttons.forEach(button => {
                        if (button.classList.contains('btn-primary')) {
                            button.classList.remove('btn-primary');
                            button.classList.add('btn-white');
                        }
                    });
                    target.classList.add('btn-primary');
                    target.classList.remove('btn-white');

                    let dataset = target.dataset;
                    let checkbox = target.closest('tr').querySelector('.js-switch');

                    let widget = null;
                    switchers.forEach(switcher => {
                        if (switcher.element === checkbox) {
                            widget = switcher.widget;
                        }
                    });

                    if (dataset['status'] === 'active') {
                        widget.enable();
                    } else {
                        if (checkbox.checked === true) {
                            checkbox.click();
                        }
                        widget.disable();
                    }

                }
            });
    });
};

let initChart = () => {
    let element = document.getElementById('chart');
    if (element !== null) {
        sendRequest('get', element.dataset['request'])
            .then(xhr => {
                let result = JSON.parse(xhr.responseText);
                let ctx = element.getContext('2d');
                let lineData = {
                    labels: result['labels'],
                    datasets: result['datasets'],
                };
                let lineOptions = {
                    responsive: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    legend: {display: false, position: 'bottom'}
                };
                let chart = new Chart(ctx, {type: 'line', data: lineData, options: lineOptions});
            });
    }

    let bar_chart = document.getElementById('chart-bar');
    if (bar_chart !== null) {
        sendRequest('get', bar_chart.dataset['request'])
            .then(xhr => {
                let result = JSON.parse(xhr.responseText);
                let ctx = bar_chart.getContext('2d');
                let lineData = {
                    labels: result['labels'],
                    datasets: result['datasets'],
                };
                let lineOptions = {
                    responsive: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    legend: {display: true, position: 'right'},
                };
                let chart = new Chart(ctx, {type: 'bar', data: lineData, options: lineOptions});
            });
    }
};

let initCalendar = (events) => {

    let calendarEl = document.getElementById('calendar');

    if (calendarEl !== null) {
        let form;
        sendRequest('get', '/dashboard/event/form')
            .then(xhr => {
                let result = JSON.parse(xhr.responseText);
                form = result['content'];
            });

        let popoverElement = '';
        $(function () {
            $('body').on('click', function (e) {
                if (!$(popoverElement).is(e.target) && $(popoverElement).has(e.target).length === 0
                    && $('.popover').has(e.target).length === 0 && $(popoverElement).length !== 0
                    && ($(e.target).closest('a').attr('aria-describedby') === undefined)
                    && !$(e.target).hasClass('day')
                    && !$(e.target).hasClass('select2-selection__choice__remove')) {

                    let form = $(popoverElement).find('form');
                    let formData = new FormData(form[0]);

                    if (form.attr('id') !== 'remove_event') {
                        sendRequest(form.attr('method'), form.attr('action'), formData)
                            .then(xhr => {
                                let result = JSON.parse(xhr.responseText);
                                if (form.attr('id') === 'create_event') {
                                    calendar.addEvent(result);
                                } else if (form.attr('id') === 'update_event') {
                                    var update_event = calendar.getEventById(result.id);
                                    update_event.setProp('title', result.title);
                                    update_event.setStart(result.start);
                                    update_event.setEnd(result.end);
                                }
                            });
                    }

                    popoverElement.popover('hide');
                    $('.popover').remove();
                    popoverElement = '';
                }
            });
        });

        $("#calendar").on("contextmenu", function (event) {
            event.preventDefault();
        });

        let calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['dayGrid', 'interaction'],
            locale: 'ru',
            eventLimit: 4,
            eventLimitText: 'Показать всё',
            firstDay: 1,
            events: events,
            eventTimeFormat: {
                hour: '2-digit',
                minute: '2-digit',
                hour12: false
            },
            eventClick: (info) => {
                let event = $(info.el);
                if (event.hasClass('type_event')) {
                    popoverElement = $('#' + event.attr('aria-describedby'));
                    let id_event = info.event.extendedProps.id;

                    sendRequest('get', '/dashboard/event/update?id=' + id_event)
                        .then(xhr => {
                            let result = JSON.parse(xhr.responseText);
                            event.attr('data-content', result['content']);
                            setTimeout(function () {
                                event.popover('show');
                                initYandexInput();
                            }, 250);
                        });
                    if ($('.popover').length !== 0) {
                        $('.popover').remove();
                    }
                }
            },
            eventRender: function (info) {
                let event = $(info.el);
                if (event.hasClass('type_event')) {
                    event.popover({
                        title: '',
                        content: '',
                        toggle: 'popover',
                        html: true,
                        animation: false,
                    }).on('shown.bs.popover', function () {
                        popoverElement = $('#' + event.attr('aria-describedby'));
                        $(popoverElement).find('#createevent-date_event').attr('value', $(event).attr('data-date'));
                        loadPlugins();
                    }).contextmenu(function () {
                        event.attr('data-content',
                            `<form id="remove_event" action="" method="post"><button class="btn btn-light" type="button">Удалить</button></form>`);
                        event.popover('show');
                        $(popoverElement).find('.arrow').remove();
                        $('#remove_event button').on('click', function () {
                            let form = $(popoverElement).find('form');
                            let formData = new FormData(form[0]);
                            sendRequest('post', '/dashboard/event/delete?id=' + info.event.extendedProps.id, formData)
                                .then(xhr => {
                                    let result = JSON.parse(xhr.responseText);
                                    if (result['message'] === 'ok') {
                                        event.remove();
                                    } else {
                                        alert(result['message']);
                                    }
                                });
                            popoverElement.popover('hide');
                            $('.popover').remove();
                            popoverElement = '';
                        });
                    });
                }
            },
            dateClick: function (info) {
                // return false;
            },
            dayRender: function (dayRenderInfo) {
                let day = $(dayRenderInfo.el);
                day.popover({
                    html: true,
                    animation: false,
                    title: '<h4>Новое мероприятие</h4>',
                    content: function () {
                        return form;
                    },
                    toggle: 'popover',
                    placement: 'right',
                    trigger: 'manual',
                }).on('dblclick', function () {
                    day.popover('show');
                    initYandexInput();
                }).on('shown.bs.popover', function () {
                    popoverElement = $('#' + day.attr('aria-describedby'));
                    $(popoverElement).find('#createevent-date_event').attr('value', $(day).attr('data-date'));
                    loadPlugins();
                });
            },
        });
        calendar.render();
    }
};

let initYandexInput = () => {
    ymaps.ready(init);

    function init() {
        var suggestView1 = new ymaps.SuggestView('createevent-place', {boundedBy: [[54.204650, 48.205661], [54.513039, 48.830509]]});
    }
};

