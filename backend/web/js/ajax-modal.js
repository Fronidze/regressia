class AjaxModal {
    constructor(element) {
        this.element = element;
        this.beforeShow = () => {};
        this.afterShow = () => {};
    }

    setTitle(title) {
        this.element.querySelector('.modal-title').innerHTML = title;
        return this;
    }

    setSubTitle(text) {
        this.element.querySelector('.modal-header small').innerHTML = text;
        return this;
    }

    setIcon(icon_class) {
        let icon = this.element.querySelector('.modal-icon');
        let classList = icon.classList;
        classList.remove('fa-clock-o');
        if (icon_class !== '' || icon_class !== null) {
            classList.add(icon_class);
        }
        return this;
    }

    setContent(content) {
        this.element.querySelector('.modal-body').innerHTML = content;
        return this;
    }

    setAjaxContent(url) {
        let xhr = new XMLHttpRequest();
        xhr.open('get', url);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send();
        xhr.onload = () => {
            try {
                let result = JSON.parse(xhr.responseText);
                this.setTitle(result['title']);
                this.setSubTitle(result['sub_title']);
                this.setIcon(result['icon']);
                this.setContent(result['content']);
                this.beforeShow();
                this.show();
                this.afterShow();
            } catch (e) {
                console.warn(e.message);
            }
        };
    }

    show() {
        $(this.element).modal({focus: false});
        $(this.element).modal('show');
    }
}
