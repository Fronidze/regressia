let sendRequest = (method, url, data = null) => {
    return new Promise((success, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        if (data === null) {
            xhr.send();
        } else {
            if (data instanceof FormData) {
                let param = document.querySelector('meta[name="csrf-param"]').getAttribute('content');
                let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                data.append(param, token);
            }
            xhr.send(data);
        }

        xhr.onload = () => {
            let status = xhr.status;
            if (status === 200 || status === 302 || status === 301) {
                success(xhr);
            }
            reject(xhr);
        };
    });
};