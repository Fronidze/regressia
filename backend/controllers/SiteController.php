<?php
namespace backend\controllers;

use backend\forms\statistics\SearchCourseStatistics;
use common\models\Schedule;
use services\statistics\RecordStatistics;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'recount'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => ['class' => 'yii\web\ErrorAction'],
        ];
    }

    public function actionIndex()
    {
        $filter = new SearchCourseStatistics();
        $provider = $filter->search();

        return $this->render('index', [
            'records' => $provider,
        ]);
    }

    public function actionRecount()
    {
        $query = (new Query())
            ->select(['id'])
            ->from(Schedule::tableName());

        $records = $query->column();
        foreach ($records as $record) {
            RecordStatistics::instance()->calculate($record);
        }

        return $this->redirect('index');
    }
}
