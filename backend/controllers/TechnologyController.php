<?php
namespace backend\controllers;

use backend\forms\technology\Create;
use backend\forms\technology\Search;
use common\models\Technology;
use services\technology\TechnologyService;
use yii\base\Module;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Site controller
 */
class TechnologyController extends Controller
{

    /** @var TechnologyService  */
    private $technology;

    public function __construct(string $id, Module $module, TechnologyService $technology, array $config = [])
    {
        $this->technology = $technology;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionList()
    {
        $search = new Search();
        $provider = $search->search();
        return $this->render('list', [
            'provider' => $provider,
        ]);
    }

    public function actionCreate()
    {
        $technology = new Create();
        if ($technology->load(\Yii::$app->request->post()) && $technology->validate()) {
            $this->technology->create($technology);
            return $this->redirect(['list']);
        }

        return $this->render('create', [
            'create' => $technology,
        ]);
    }

    public function actionUpdate(int $id)
    {
        $technology = Create::findOne($id);
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            if ($technology->load($request->post()) && $technology->validate()) {
                $this->technology->update($technology);
            }
            return $this->redirect(['technology/list']);
        }

        $content = [
            'title' => \Yii::t('app', '{title}', ['title' => $technology->title]),
            'icon' => null,
            'sub_title' => null,
            'content' => $this->renderAjax('update', ['technology' => $technology]),
        ];

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $content;
    }

    public function actionPreview(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $technology = Technology::findOne($id);
        $result = [
            'title' => $technology->title,
            'sub_title' => null,
            'icon' => 'fa-paste',
            'content' => $this->renderAjax('preview', ['technology' => $technology])
        ];
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }
}
