<?php

namespace backend\controllers;

use backend\forms\statistics\RegressionForm;
use common\models\Schedule;
use common\models\Technology;
use entities\regression\RegressionResponse;
use services\statistics\RecordStatistics;
use services\statistics\RegressionService;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class StatisticController extends Controller
{
    public function actionIndex()
    {

        (new RecordStatistics())
            ->calculateRegressionStatistic();

        $request = \Yii::$app->request;
        $request_form = new RegressionForm();
        $service_response = new RegressionResponse();

        $request_form->initDatabaseValues();

        if ($request_form->load($request->post()) && $request_form->validate()) {
            $service = new RegressionService();

            $service_response = $service->postRegressionFit($request_form);
            $service_response->modifyContent();

            $params_response = $service->getRegressionModelParams();

        }

        return $this->render('index', [
            'request_form' => $request_form,
            'technology' => $this->getTechnology(),
            'service_response' => $service_response,
            'params_response' => $params_response,
        ]);
    }

    public function actionPrediction()
    {
        $request = \Yii::$app->request;
        $response = \Yii::$app->response;

        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $request_data = [];
        foreach ($request->post('prediction') as $code => $value) {
            $request_data[$code] = (float)$value;
        }

        $service = new RegressionService();
        $result = $service->postregRessionPredict($request_data);

        $prediction = ArrayHelper::getValue($result->content, 'prediction', 0);
        if ($prediction > 5) {
            $prediction = 5;
        }

        if ($prediction < 0) {
            $prediction = 0;
        }

        $response->data = round($prediction, 3);
        $response->send();
    }

    protected function getTechnology(): array
    {
        $query = (new Query())
            ->select(['s.id', 't.title'])
            ->from(['s' => Schedule::tableName()])
            ->leftJoin(['t' => Technology::tableName()], ['t.id' => new Expression('s.technology_id')]);

        return ArrayHelper::map($query->all(), 'id', 'title');
    }
}