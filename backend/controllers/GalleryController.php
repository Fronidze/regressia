<?php

namespace backend\controllers;

use backend\forms\gallery\GalleryUpload;
use common\exception\ModelSaveException;
use common\exception\ModelValidateException;
use common\models\abstractions\PublishActiveRecord;
use common\models\Gallery;
use entities\image\GalleryFile;
use services\image\FileService;
use services\image\GalleryService;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class GalleryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $gallery = new GalleryUpload();

        return $this->render('index', [
            'gallery' => $gallery,
            'images' => Gallery::find()->orderBy(['sorting' => SORT_ASC])->all(),
        ]);
    }

    public function actionRemove(int $id)
    {
        (new GalleryService())
            ->remove($id);

        return $this->redirect(['gallery/index']);
    }

    public function actionUploadFile()
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }
        
        if ($uploaded = UploadedFile::getInstanceByName('file')) {
            $record = (new FileService(new GalleryFile()))
                ->setUploadedFile($uploaded)
                ->save();

            $gallery = new Gallery(['image_id' => $record->id]);
            if ($gallery->validate() === false) {
                throw new ModelValidateException($gallery);
            }

            if ($gallery->save(false) === false) {
                throw new ModelSaveException($gallery);
            }
        }

    }


    public function actionPublish(int $id)
    {
        $request = \Yii::$app->request;
        $service = new GalleryService();
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $status = $request->post('checked') === 'true' ? PublishActiveRecord::PUBLISH_YES : PublishActiveRecord::PUBLISH_NO;
        $service->changePublish($id, $status);
        return $this->redirect(['gallery/index']);
    }

    public function actionSorting()
    {
        $request = \Yii::$app->request;
        $service = new GalleryService();
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $elements = $request->post('elements');
        foreach ($elements as $id => $sort) {
            $service->changeSorting($id, $sort);
        }
    }
}