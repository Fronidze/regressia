<?php

namespace backend\controllers;

use common\models\ExamPreparation;
use common\models\Schoolchild;
use services\profile\SchoolchildService;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class SchoolchildController extends Controller
{
    /** @var SchoolchildService */
    private $schoolchild;

    public function __construct(string $id, Module $module, SchoolchildService $service, array $config = [])
    {
        $this->schoolchild = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionPreview(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $schoolchild = Schoolchild::find()
            ->with(['recordSchoolchild.examPreparation'])
            ->where(['=', 'id', $id])
            ->one();

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'icon'      => null,
            'title'     => $schoolchild->name,
            'sub_title' => $schoolchild->email,
            'content'   => $this->renderAjax('preview', ['schoolchild' => $schoolchild]),
        ];
    }
}