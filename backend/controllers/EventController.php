<?php

namespace backend\controllers;

use backend\forms\event\CreateEvent;
use common\models\Event;
use services\event\EventService;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;


/**
 * Site controller
 */
class EventController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionForm()
    {
        $event = new CreateEvent();
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => $this->renderAjax('form', ['event' => $event])
        ];
    }

    public function actionCreate()
    {
        $request = \Yii::$app->request;
        $event = new CreateEvent();
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if ($event->load($request->post()) && $event->validate()) {
            $new_event = EventService::instance()->create($event);
            return $this->eventForCalendar($new_event);
        }
        return ['message' => 'the data is not valid or failed to load data into the model'];
    }

    public function actionUpdate(int $id)
    {
        $request = \Yii::$app->request;
        $event = EventService::instance()->find($id);
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if ($event->load($request->post()) && $event->validate()) {
            $update_event = EventService::instance()->insert($event);
            return $this->eventForCalendar($update_event);
        }
        return [
            'content' => $this->renderAjax('form_update', [
                'event' => $event
            ])
        ];
    }

    public function actionDelete(int $id)
    {
        $request = \Yii::$app->request;
        $result = [];
        if ($request->post()) {
            $result = EventService::instance()->remove($id);
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $result;
    }

    private function eventForCalendar(Event $event)
    {
        $result = [];
        $now = (new \DateTime());

        $start = (new \DateTime($event->date_event));
        $class = $now > $start ? 't_past' : 't_active';
        $result = [
            'id' => $event->id,
            'title' => $event->title,
            'start' => $event->date_event,
            'end' => $event->date_event,
            'classNames' => $class,
        ];
        return $result;
    }
}
