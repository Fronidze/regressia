<?php

namespace backend\controllers;

use common\models\Record;
use common\models\Schedule;
use common\models\Technology;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;

class StatisticsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionCountRecords()
    {

        $query = (new Query())
            ->select(['records' => new Expression('COUNT(DISTINCT record.student_id)'), 'period' => new Expression('YEAR(schedule.date_start)')])
            ->from(['record' => Record::tableName()])
            ->where(['>', 'YEAR(schedule.date_start)', 2017])
            ->innerJoin(['schedule' => Schedule::tableName()], ['schedule.id' => new Expression('record.schedule_id')])
            ->orderBy(['schedule.date_start' => SORT_ASC])
            ->groupBy(['schedule.date_start']);
        $records = (new Query())->select(['sum(records) as records', 'period'])->from([$query])->groupBy('period')->all();
        $datasets = [
            [
                'label' => 'Количество студентов',
                'backgroundColor' => 'rgba(26,179,148,0.5)',
                'borderColor' => 'rgba(26,179,148,0.7)',
                'pointBackgroundColor' => 'rgba(26,179,148,1)',
                'pointBorderColor' => '#fff',
                'data' => ArrayHelper::getColumn($records, 'records'),
            ]
        ];

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'labels' => ArrayHelper::getColumn($records, 'period'),
            'datasets' => $datasets,
        ];
    }

    public function actionCountPerYear(int $year)
    {
        $query = (new Query())
            ->select([
                'records' => new Expression('count(record.id)'),
                'title' => 'technology.short_title'
            ])
            ->from(['record' => Record::tableName()])
            ->innerJoin(['schedule' => Schedule::tableName()], ['schedule.id' => new Expression('record.schedule_id')])
            ->innerJoin(['technology' => Technology::tableName()], ['technology.id' => new Expression('schedule.technology_id')])
            ->where(['=', new Expression('YEAR(schedule.date_start)'), $year])
            ->groupBy(['schedule.technology_id']);

        $query_2018 = (new Query())
            ->select([
                'records' => new Expression('count(record.id)'),
                'title' => 'technology.short_title'
            ])
            ->from(['record' => Record::tableName()])
            ->innerJoin(['schedule' => Schedule::tableName()], ['schedule.id' => new Expression('record.schedule_id')])
            ->innerJoin(['technology' => Technology::tableName()], ['technology.id' => new Expression('schedule.technology_id')])
            ->where(['=', new Expression('YEAR(schedule.date_start)'), 2018])
            ->groupBy(['record.schedule_id']);

        $query_2017 = (new Query())
            ->select([
                'records' => new Expression('count(record.id)'),
                'title' => 'technology.short_title'
            ])
            ->from(['record' => Record::tableName()])
            ->innerJoin(['schedule' => Schedule::tableName()], ['schedule.id' => new Expression('record.schedule_id')])
            ->innerJoin(['technology' => Technology::tableName()], ['technology.id' => new Expression('schedule.technology_id')])
            ->where(['=', new Expression('YEAR(schedule.date_start)'), 2017])
            ->groupBy(['record.schedule_id']);

        $records = (new Query())->select(['sum(records) as records', 'title'])->from([$query])->groupBy('title')->all();
        $labels = ArrayHelper::getColumn((new Query())->select(['short_title'])->from(['technology' => Technology::tableName()])->all(), 'short_title');

        $query_2017 = (new Query())->select(['sum(records) as records', 'title'])->from([$query_2017])->groupBy('title')->all();
        $query_2018 = (new Query())->select(['sum(records) as records', 'title'])->from([$query_2018])->groupBy('title')->all();

        $records_2019 = ArrayHelper::map($records, 'title', 'records');
        $records_2017 = ArrayHelper::map($query_2017, 'title', 'records');
        $records_2018 = ArrayHelper::map($query_2018, 'title', 'records');

        $data_2019 = [];
        $data_2018 = [];
        $data_2017 = [];
        foreach ($labels as $label) {
            $data_2019[] = ArrayHelper::getValue($records_2019, $label, 0);
            $data_2018[] = ArrayHelper::getValue($records_2018, $label, 0);
            $data_2017[] = ArrayHelper::getValue($records_2017, $label, 0);
        }

        $datasets = [
            [
                'label' => $year . ' год',
                'backgroundColor' => 'rgba(26,179,148,0.5)',
                'borderColor' => 'rgba(26,179,148,0.7)',
                'pointBackgroundColor' => 'rgba(26,179,148,1)',
                'pointBorderColor' => '#fff',
                'data' => $data_2019,
            ],
            [
                'label' => '2018 год',
                'backgroundColor' => 'rgba(69,133,165,0.5)',
                'borderColor' => 'rgba(69,133,165,0.7)',
                'pointBackgroundColor' => 'rgba(69,133,165,1)',
                'pointBorderColor' => '#fff',
                'data' => $data_2018,
            ],
            [
                'label' => '2017 год',
                'backgroundColor' => 'rgba(189,188,182,0.5)',
                'borderColor' => 'rgba(189,188,182,0.7)',
                'pointBackgroundColor' => 'rgba(189,188,182,1)',
                'pointBorderColor' => '#fff',
                'data' => $data_2017,
            ]
        ];

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'labels' => $labels,
            'datasets' => $datasets,
        ];
    }
}
