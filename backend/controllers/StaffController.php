<?php

namespace backend\controllers;

use backend\forms\profile\SearchPersonal;
use services\profile\PersonalService;
use yii\base\Module;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;

class StaffController extends Controller
{
    /** @var PersonalService */
    private $personal;

    public function __construct(string $id, Module $module, PersonalService $service, array $config = [])
    {
        $this->personal = $service;
        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionList()
    {
        $filter = new SearchPersonal();
        $provider = $filter->search();

        return $this->render('list', [
            'provider' => $provider,
        ]);
    }

    public function actionUpdate(int $id)
    {
        $personal = $this->personal->find($id);
        if ($personal->load(\Yii::$app->request->post()) && $personal->validate()) {
            $this->personal->update($personal);
        }

        return $this->redirect(['staff/list']);
    }

    public function actionPreview(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $personal = $this->personal->find($id);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'icon' => null,
            'title' => null,
            'sub_title' => null,
            'content' => $this->renderAjax('preview', ['personal' => $personal]),
        ];
    }
}