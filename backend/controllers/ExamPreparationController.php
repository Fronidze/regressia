<?php

namespace backend\controllers;

use backend\forms\exam\Create;
use backend\forms\exam\Search;
use backend\forms\recordschoolchild\CreateRecordSchoolchild;
use backend\forms\recordschoolchild\SearchRecordSchoolchild;
use backend\forms\schoolchild\CreateSchoolchild;
use backend\forms\template_exam\CreateTemplate;
use common\models\TemplateExamPreparation;
use services\exam\ExamPreparationService;
use services\exam\RecordSchoolchildService;
use services\exam\TemplateExamPreparationService;
use services\profile\SchoolchildService;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Exam Preparation controller
 */
class ExamPreparationController extends Controller
{

    /** @var ExamPreparationService */
    private $exam_preparation;

    /** @var TemplateExamPreparationService */
    private $template_exam;

    public function __construct(string $id, Module $module, ExamPreparationService $exam_preparation, array $config = [])
    {
        $this->exam_preparation = $exam_preparation;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionMain()
    {
        $query_template = TemplateExamPreparation::find()->one();

        if ($query_template == null) {
            $template = [
                'model' => new CreateTemplate(),
                'view'  => 'template/_create'
            ];
        } else {
            $template = [
                'model' => TemplateExamPreparationService::instance()->findForForm($query_template->id),
                'view'  => 'template/_update'
            ];
        }

        $search = new Search();
        $provider = $search->search();

        $request = \Yii::$app->request;
        $schoolchild_form = new CreateSchoolchild();

        if ($schoolchild_form->load($request->post()) && $schoolchild_form->validate()) {
            (new SchoolchildService())->insert($schoolchild_form);
            \Yii::$app->session->setFlash('success', 'Новый школьник был успешно создан');
            return $this->redirect(['exam-preparation/main']);
        }

        $record_form = new CreateRecordSchoolchild();
        if ($record_form->load($request->post()) && $record_form->validate()) {
            (new RecordSchoolchildService())->create($record_form);
            \Yii::$app->session->setFlash('success', 'Новая запись на курс была успешно создана');
            return $this->redirect(['exam-preparation/main']);
        }

        $record_filter = new SearchRecordSchoolchild();
        $record_provider = $record_filter->search();

        return $this->render('main', [
            'template'         => $template,
            'provider'         => $provider,
            'search'           => $search,
            'record_form'      => $record_form,
            'schoolchild_form' => $schoolchild_form,
            'record_filter'    => $record_filter,
            'record_provider'  => $record_provider,
        ]);
    }

    public function actionCreate()
    {
        $exam_preparation = new Create();
        if ($exam_preparation->load(\Yii::$app->request->post()) && $exam_preparation->validate()) {
            $this->exam_preparation->insert($exam_preparation);
            return $this->redirect(['exam-preparation/main']);
        }

        $template = TemplateExamPreparation::find()->one();
        return $this->render('create', [
            'exam_preparation' => $exam_preparation,
            'template_id'      => $template->id
        ]);
    }

    public function actionUpdate(int $id)
    {
        $request = \Yii::$app->request;
        $exam_preparation = $this->exam_preparation->findForForm($id);

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if ($request->post()) {
            if ($exam_preparation->load($request->post()) && $exam_preparation->validate()) {
                $this->exam_preparation->insert($exam_preparation);
                return [
                    'msg' => true
                ];
            } else {
                return [
                    'msg'    => false,
                    'errors' => $exam_preparation->errors
                ];
            }
        } else {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'icon'      => null,
                'sub_title' => null,
                'title'     => $exam_preparation->title,
                'content'   => $this->renderAjax('update', [
                    'exam_preparation' => $exam_preparation,
                ])
            ];
        }
    }

    public function actionPublish(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $checked = $request->post('checked') === 'true';
        $this->exam_preparation->publish($id, $checked);
    }

}
