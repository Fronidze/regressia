<?php

namespace backend\controllers;

use backend\forms\student\CreateStudent;
use backend\forms\student\SearchStudent;
use common\models\Students;
use services\profile\StudentService;
use yii\base\Module;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Site controller
 */
class StudentsController extends Controller
{
    /** @var StudentService */
    private $student;

    public function __construct(string $id, Module $module, StudentService $service, array $config = [])
    {
        $this->student = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionList()
    {
        throw new BadRequestHttpException();
        /*$search = new SearchStudent();
        $provider = $search->search();
        return $this->render('list', ['provider' => $provider]);*/
    }

    public function actionCreate()
    {
        throw new BadRequestHttpException();
        /*$request = \Yii::$app->request;
        $student = new CreateStudent();
        
        if ($student->load($request->post()) && $student->validate()) {
            $this->student->insert($student);
            return $this->redirect(['students/list']);
        }

        return $this->render('create', [
            'student' => $student
        ]);*/
    }

    public function actionPreview(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $student = Students::find()
            ->with(['records.schedule.technology'])
            ->where(['=', 'id', $id])
            ->one();

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'icon' => null,
            'title' => $student->name,
            'sub_title' => $student->email,
            'content' => $this->renderAjax('preview', ['student' => $student]),
        ];
    }
}