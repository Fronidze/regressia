<?php

namespace backend\controllers;

use backend\forms\document\CourseRequestDownload;
use backend\forms\record\SearchCourseRequest;
use common\models\CourseRequest;
use services\document\DocumentService;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class CourseRequestController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionList()
    {
        $filter = new SearchCourseRequest();
        $provider = $filter->search();


        return $this->render('list', ['provider' => $provider, 'filter' => $filter]);
    }


    public function actionDownload()
    {
        $download = new CourseRequestDownload();
        DocumentService::instance()
            ->setFilename($download->getFileName())
            ->generate($download->search());
    }
}