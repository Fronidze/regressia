<?php
namespace backend\controllers;

use backend\forms\profile\LoginForm;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class AuthController extends Controller
{

    public $layout = '//login';

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionLogin()
    {
        $login = new LoginForm();
        if ($login->load(\Yii::$app->request->post()) && $login->login()) {
            return $this->goHome();
        }
        return $this->render('login', ['login' => $login]);
    }

    public function actionLogout()
    {
        \Yii::$app->user->logout(true);
        return $this->goHome();
    }

}
