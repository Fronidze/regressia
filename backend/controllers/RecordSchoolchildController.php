<?php

namespace backend\controllers;

use backend\forms\document\RecordSchoolchildDownload;
use backend\forms\schoolchild\CreateSchoolchild;
use common\models\Schoolchild;
use services\document\DocumentService;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class RecordSchoolchildController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionCompare()
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $schoolchild = new CreateSchoolchild();
        $schoolchild->load(\Yii::$app->request->post());

        $record = Schoolchild::find()
            ->where(['=', 'email', $schoolchild->email])
            ->one();

        \Yii::$app->response->format = Response::FORMAT_JSON;
        if ($record !== null) {
            return [
                'compare' => true,
                'content' => $this->renderAjax('compare', ['schoolchild' => $schoolchild, 'record' => $record])
            ];
        }
        return [
            'compare' => false,
            'content' => null
        ];

    }

    public function actionDownload()
    {
        $download = new RecordSchoolchildDownload();
        DocumentService::instance()
            ->setFilename($download->getFileName())
            ->generate($download->search());
    }

}