<?php

namespace backend\controllers;

use backend\forms\question\Create;
use backend\forms\question\Search;
use common\models\Question;
use services\question\QuestionService;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class QuestionController extends Controller
{

    /** @var QuestionService */
    private $question;

    public function __construct(string $id, Module $module, QuestionService $question, array $config = [])
    {
        $this->question = $question;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionList()
    {
        $search = new Search();
        $provider = $search->search();

        return $this->render('list', [
            'provider' => $provider
        ]);
    }

    public function actionUpdate(int $id)
    {
        $question = $this->question->find($id);
        if ($question->load(\Yii::$app->request->post()) && $question->validate()) {
            $this->question->update($question);
            return $this->redirect(['question/list']);
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title' => '',
            'sub_title' => '',
            'icon' => null,
            'content' => $this->renderAjax('update', [
                'question' => $question,
            ])
        ];

    }

    public function actionCreate()
    {
        $question = new Create();
        if ($question->load(\Yii::$app->request->post()) && $question->validate()) {
            $this->question->create($question);
            return $this->redirect(['question/list']);
        }

        return $this->render('create', [
            'question' => $question,
        ]);
    }

    public function actionChangePublish(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $status = $request->post('checked') === 'true' ? Question::PUBLISH_YES : Question::PUBLISH_NO;
        $this->question->changePublish($id, $status);
        return $this->redirect(['question/list']);
    }

    public function actionDelete(int $id)
    {
        $question = Question::findOne($id);

        if (\Yii::$app->request->post()) {
            $this->question->delete($id);
            return $this->redirect(['question/list']);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title' => '',
            'sub_title' => '',
            'icon' => null,
            'question' => $question->question,
            'content' => $this->renderAjax('delete', [
                'question' => $question
            ])
        ];
    }

    public function actionSorting()
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $elements = $request->post('elements');
        foreach ($elements as $id => $sort) {
            $this->question->changeSorting($id, $sort);
        }
    }
}
