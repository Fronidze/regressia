<?php

namespace backend\controllers;

use backend\forms\document\RecordDownload;
use backend\forms\record\CreateRecord;
use backend\forms\record\SearchRecord;
use backend\forms\student\CreateStudent;
use common\models\Record;
use common\models\Students;
use services\document\DocumentService;
use services\profile\StudentService;
use services\schedule\RecordService;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Site controller
 */
class RecordController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionCompare()
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $student = new CreateStudent();
        $student->load(\Yii::$app->request->post());

        $record = Students::find()
            ->where(['=', 'email', $student->email])
            ->one();

        \Yii::$app->response->format = Response::FORMAT_JSON;
        if ($record !== null) {
            return [
                'compare' => true,
                'content' => $this->renderAjax('compare', ['student' => $student, 'record' => $record])
            ];
        }
        return [
            'compare' => false,
            'content' => null
        ];

    }

    public function actionList()
    {
        $request = \Yii::$app->request;
        $student_form = new CreateStudent();

        if ($student_form->load($request->post()) && $student_form->validate()) {
            (new StudentService())->insert($student_form);
            \Yii::$app->session->setFlash('success', 'Новый студент был успешно создан');
            return $this->redirect(['record/list']);
        }

        $record_form = new CreateRecord();
        if ($record_form->load($request->post()) && $record_form->validate()) {
            (new RecordService())->create($record_form);
            \Yii::$app->session->setFlash('success', 'Новая запись на курс была успешно создана');
            return $this->redirect(['record/list']);
        }

        $record_filter = new SearchRecord();
        $record_provider = $record_filter->search();

        return $this->render('list', [
            'record_form' => $record_form,
            'student_form' => $student_form,
            'record_filter' => $record_filter,
            'record_provider' => $record_provider,
        ]);
    }

    public function actionChangeListened(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $result = $request->post('checked') === 'true' ? Record::RESULT_POSITIVE : Record::RESULT_NEGATIVE;
        (new RecordService())->listened($id, $result);
    }

    public function actionChangeCompleted(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $result = $request->post('checked') === 'true' ? Record::RESULT_POSITIVE : Record::RESULT_NEGATIVE;
        (new RecordService())->completed($id, $result);
    }

    public function actionChangeAccepted(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $result = $request->post('checked') === 'true' ? Record::RESULT_POSITIVE : Record::RESULT_NEGATIVE;
        (new RecordService())->accepted($id, $result);
    }

    public function actionDownload()
    {
        $download = new RecordDownload();
        DocumentService::instance()
            ->setFilename($download->getFileName())
            ->generate($download->search());
    }

}