<?php

namespace backend\controllers;

use backend\forms\teacher\Create;
use backend\forms\teacher\Search;
use common\models\Teacher;
use services\profile\TeacherService;
use yii\base\Module;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Site controller
 */
class TeacherController extends Controller
{

    /** @var TeacherService  */
    private $teacher;

    public function __construct(string $id, Module $module, TeacherService $teacher, array $config = [])
    {
        $this->teacher = $teacher;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionList()
    {
        $search = new Search();
        $provider = $search->search();

        return $this->render('list', [
            'provider' => $provider
        ]);
    }

    public function actionUpdate(int $id)
    {
        $teacher = $this->teacher->find($id);
        if ($teacher->load(\Yii::$app->request->post()) && $teacher->validate()) {
            $this->teacher->update($teacher);
            return $this->redirect(['teacher/list']);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title' => $teacher->name,
            'icon' => null,
            'sub_title' => $teacher->position,
            'content' => $this->renderAjax('update', [
                'teacher' => $teacher,
            ])
        ];

    }

    public function actionCreate()
    {
        $teacher = new Create();
        if ($teacher->load(\Yii::$app->request->post()) && $teacher->validate()) {
            $this->teacher->create($teacher);
            return $this->redirect(['teacher/list']);
        }

        return $this->render('create', [
            'teacher' => $teacher,
        ]);
    }

    public function actionChangePublish(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $status = $request->post('checked') === 'true' ? Teacher::PUBLISH_YES : Teacher::PUBLISH_NO;
        $this->teacher->changePublish($id, $status);
        return $this->redirect(['teacher/list']);
    }

    public function actionSorting()
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $elements = $request->post('elements');
        foreach ($elements as $id => $sort) {
            $this->teacher->changeSorting($id, $sort);
        }
    }
}
