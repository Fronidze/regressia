<?php

namespace backend\controllers;

use backend\forms\group\CreateGroup;
use backend\forms\group\SearchGroup;
use services\group\GroupService;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Site controller
 */
class GroupController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionList()
    {
        $filter = new SearchGroup();
        $provider = $filter->search();

        return $this->render('list', [
            'provider' => $provider,
        ]);
    }

    public function actionCreate()
    {
        $request = \Yii::$app->request;
        $group = new CreateGroup();

        if ($group->load($request->post()) && $group->validate()) {
            GroupService::instance()->insert($group);
            \Yii::$app->session->setFlash('success', 'Новая группа была успешно создана.');
            return $this->redirect(['group/list']);
        }

        return $this->render('create', [
            'group' => $group,
        ]);
    }

    public function actionUpdate(int $id)
    {

        $request = \Yii::$app->request;
        $group = GroupService::instance()->find($id);
        if ($request->getIsAjax() === false) {
            if ($group->load($request->post()) && $group->validate()) {
                GroupService::instance()->insert($group);
                \Yii::$app->session->setFlash('success', 'Группа была успешно обновлена.');
                return $this->redirect(['group/list']);
            }
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'icon' => null,
            'sub_title' => null,
            'title' => $group->title,
            'content' => $this->renderAjax('update', [
                'group' => $group
            ])
        ];

        /*$request = \Yii::$app->request;
        $group = GroupService::instance()->find($id);

        if ($group->load($request->post()) && $group->validate()) {
            GroupService::instance()->insert($group);
            \Yii::$app->session->setFlash('success', 'Группа была успешно обновлена.');
            return $this->redirect(['group/list']);
        }

        return $this->render('create', [
            'group' => $group,
        ]);*/
    }

    public function actionPublish(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $checked = $request->post('checked') === 'true';
        GroupService::instance()->publish($id, $checked);
    }

    public function actionSorting()
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $elements = $request->post('elements');
        foreach ($elements as $id => $sort) {
            GroupService::instance()->changeSorting($id, $sort);
        }
    }
}