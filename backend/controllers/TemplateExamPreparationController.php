<?php

namespace backend\controllers;

use backend\forms\template_exam\CreateTemplate;
use services\exam\TemplateExamPreparationService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Template Exam Preparation controller
 */
class TemplateExamPreparationController extends Controller
{

    /** @var TemplateExamPreparationService */
    private $template_exam;

    public function __construct(string $id, Module $module, TemplateExamPreparationService $template_exam, array $config = [])
    {
        $this->template_exam = $template_exam;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $template_exam = new CreateTemplate();

        if ($template_exam->load(Yii::$app->request->post()) && $template_exam->validate()) {
            $this->template_exam->insert($template_exam);
        }
        return $this->redirect(['exam-preparation/main']);
    }

    public function actionUpdate(int $id)
    {
        $request = Yii::$app->request;
        $template_exam = $this->template_exam->findForForm($id);

        if ($template_exam->load($request->post()) && $template_exam->validate()) {
            $this->template_exam->insert($template_exam);
        }
        return $this->redirect(['exam-preparation/main']);
    }
}
