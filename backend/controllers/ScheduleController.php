<?php

namespace backend\controllers;

use backend\forms\schedule\Create;
use backend\forms\schedule\Search;
use common\models\Event;
use common\models\links\ParticipantsEvent;
use common\models\Schedule;
use services\schedule\ScheduleService;
use yii\base\Module;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Schedule controller
 */
class ScheduleController extends Controller
{

    /** @var ScheduleService */
    private $schedule;

    public function __construct(string $id, Module $module, ScheduleService $schedule, array $config = [])
    {
        $this->schedule = $schedule;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionList()
    {
        $search = new Search();
        $provider = $search->search();

        return $this->render('list', [
            'provider' => $provider,
            'search' => $search,
        ]);
    }

    public function actionCreate()
    {
        $schedule = new Create();
        if ($schedule->load(\Yii::$app->request->post()) && $schedule->validate()) {
            $this->schedule->insert($schedule);
            return $this->redirect(['schedule/list']);
        }

        return $this->render('create', [
            'schedule' => $schedule
        ]);
    }

    public function actionUpdate(int $id)
    {
        $request = \Yii::$app->request;
        $schedule = $this->schedule->findForForm($id);
        if ($request->getIsAjax() === false) {
            if ($schedule->load($request->post()) && $schedule->validate()) {
                $this->schedule->insert($schedule);
                return $this->redirect(['schedule/list']);
            }
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'icon' => null,
            'sub_title' => null,
            'title' => $schedule->getTechnologyTitle(),
            'content' => $this->renderAjax('update', [
                'schedule' => $schedule
            ])
        ];
    }

    public function actionPublish(int $id)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $checked = $request->post('checked') === 'true';
        $this->schedule->publish($id, $checked);
    }

    public function actionChangeStatus(int $id, string $status)
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $this->schedule->changeStatus($id, $status);
    }

    public function actionSorting()
    {
        $request = \Yii::$app->request;
        if ($request->getIsAjax() === false) {
            throw new BadRequestHttpException();
        }

        $elements = $request->post('elements');
        foreach ($elements as $id => $sort) {
            $this->schedule->changeSorting($id, $sort);
        }
    }

    public function actionCalendarEvents()
    {
        $schedules = Schedule::find()
            ->with(['weekly', 'technology'])
            ->where(['in', 'status', [Schedule::STATUS_ACTIVE, Schedule::STATUS_ARCHIVE]])
            ->all();

        $result = [];
        $now = (new \DateTime());
        foreach ($schedules as $schedule) {
            $title = ArrayHelper::getValue($schedule, 'technology.short_title');
            $id = $schedule->id;
            $events = ScheduleService::instance()->lessonsDate($schedule->id);
            foreach ($events as $event) {
                $start = (new \DateTime($event));
                $class = $now > $start ? 't_past' : 't_active';
                $result[] = [
                    'id' => $id,
                    'title' => $title,
                    'start' => $event,
                    'end' => $event,
                    'classNames' => $class,
                ];
            }
        }

        foreach ($schedules as $schedule) {
            $title = ArrayHelper::getValue($schedule, 'technology.short_title');
            $id = $schedule->id;

            $close_course_date = $schedule->date_completion;
            if ($close_course_date === null) {
                continue;
            }

            $close_date = new \DateTime($close_course_date);
            $class = ['close_course'];
            if ($close_date < $now) {
                $class[] = ['t_past'];
            }
            $result[] = [
                'id' => $id,
                'title' => $title,
                'start' => $close_date->format('Y-m-d'),
                'end' => $close_date->format('Y-m-d'),
                'classNames' => $class,
            ];
        }

        $user_id = \Yii::$app->user->id;
        $events = (new Query())
            ->select(['event.*'])->distinct()
            ->from(['event' => Event::tableName()])
            ->leftJoin(['participants' => ParticipantsEvent::tableName()], ['participants.event_id' => new Expression('event.id')])
            ->where(['=', 'participants.user_id', $user_id])
            ->orWhere(['=', 'event.user_id', $user_id])
            ->all();

        foreach ($events as $event) {
            $date_event = new \DateTime($event['date_event']);
            $class = $now > $date_event ? 't_past' : 't_active';
            $result[] = [
                'id' => $event['id'],
                'title' => $event['title'],
                'start' => $event['date_event'],
                'end' => $event['date_event'],
                'classNames' => $class . ' type_event',
                'extendedProps' => [
                    'id' => $event['id']
                ],
            ];
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;

    }
}
