<?php

namespace backend\controllers;

use backend\forms\import\RecordImport;
use backend\forms\import\RecordSchoolchildImport;
use services\import\ExcelReaderService;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class ImportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']]
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $import = new RecordImport();
        $importSchoolchild = new RecordSchoolchildImport();
        $request = \Yii::$app->request;
        if ($import->load($request->post())) {
            $uploaded = UploadedFile::getInstance($import, 'excel');

            if ($uploaded !== null) {
                ExcelReaderService::instance()
                    ->load($uploaded->tempName)
                    ->setModelImport($import)
                    ->parse();
            }
        }

        if ($importSchoolchild->load($request->post())) {
            $uploaded = UploadedFile::getInstance($importSchoolchild, 'excel');
            if ($uploaded !== null) {
                ExcelReaderService::instance()
                    ->load($uploaded->tempName)
                    ->setModelSchoolchildImport($importSchoolchild)
                    ->parseSchoolchild();
            }
        }

        return $this->render('index', [
            'import'            => $import,
            'importSchoolchild' => $importSchoolchild
        ]);
    }

}