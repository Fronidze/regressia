<?php

/**
 * @var $this View
 * @var $login LoginForm
 */

use backend\forms\profile\LoginForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\web\View;

?>

<style>
    .loginscreen.middle-box {
        max-width: 800px;
    }

    .loginscreen.middle-box form {
        max-width: 350px;
        margin: 50px auto;
    }

    div.login-wrapper {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100%;
    }

    div.login-wrapper img {
        padding: 50px 0;
    }

    .help-block {
        color: white;
    }

</style>

<div class="middle-box loginscreen animated fadeInDown login-wrapper">
    <div>
<!--        <img width="800" src="https://mediasoft.team/assets/images/academy-new/logo_academy_rgb.png" alt="">-->
        <?php
        $form = ActiveForm::begin(['options' => ['class' => ['m-t'], 'role' => 'form']]);
        echo $form->field($login, 'email')->textInput(['placeholder' => 'Email'])->label(false);
        echo $form->field($login, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false);
        echo Html::submitButton('Войти', ['class' => ['btn', 'btn-primary', 'block', 'full-width', 'm-b']]);
        ActiveForm::end();
        ?>
    </div>
</div>
