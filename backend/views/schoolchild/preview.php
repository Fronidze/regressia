<?php

use common\models\RecordSchoolchild;
use common\models\Schoolchild;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var $this View
 * @var $schoolchild Schoolchild
 */

/** @var RecordSchoolchild[] $records */
$records = ArrayHelper::getValue($schoolchild, 'recordSchoolchild');

if ($records !== null) { ?>
    <div class="ibox">
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped table-hover issue-tracker">
                    <tr>
                        <th>Название курса</th>
                    </tr>
                    <?php foreach ($records as $record) { ?>
                        <tr>
                            <td>
                                <?php
                                echo \Yii::t('app', '{title} ({date})', [
                                    'title' => ArrayHelper::getValue($record, 'examPreparation')->sub_title,
                                    'date'  => \Yii::$app->formatter->asDate($record->date_create, 'long'),
                                ]);
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
<?php } ?>