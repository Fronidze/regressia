<?php

use backend\forms\import\RecordImport;
use backend\forms\import\RecordSchoolchildImport;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $import RecordImport
 * @var $importSchoolchild RecordSchoolchildImport
 */

$this->title = 'Академия разработки Mediasoft: Импорт';
$this->params['links'][] = ['label' => 'Студенты', 'url' => ['record/list']];
$this->params['links'][] = ['label' => 'Импорт', 'url' => null];

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content record_form">
                <h2>Импорт списков студентов</h2>
                <?php
                $form = ActiveForm::begin();
                echo $form->field($import, 'schedule_id')->dropDownList($import->listSchedule(), [
                    'class' => ['select2'],
                    'prompt' => ' - Выберите курс - ',
                ]);
                echo $form->field($import, 'excel')->fileInput();
                echo Html::submitInput('Импорт', ['class' => ['btn', 'btn-primary']]);
                ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content record_form">
                <h2>Импорт списков школьников</h2>
                <?php
                $form = ActiveForm::begin();
                echo $form->field($importSchoolchild, 'exam_preparation_id')->dropDownList($importSchoolchild->listExamPreparation(), [
                    'class'  => ['select2'],
                    'prompt' => ' - Выберите курс - ',
                ]);
                echo $form->field($importSchoolchild, 'excel')->fileInput();
                echo Html::submitInput('Импорт', ['class' => ['btn', 'btn-primary']]);
                ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php
        if (!empty($import->getImportErrors())) {
            foreach ($import->getImportErrors() as $error) { ?>
                <div class="ibox">
                    <div class="ibox-content">
                        <?= str_replace("\n", "<br>", $error); ?>
                    </div>
                </div>
            <?php }
        }
        if (!empty($importSchoolchild->getImportErrors())) {
            foreach ($importSchoolchild->getImportErrors() as $error) { ?>
                <div class="ibox">
                    <div class="ibox-content">
                        <?= str_replace("\n", "<br>", $error); ?>
                    </div>
                </div>
            <?php }
        } ?>
    </div>
</div>
