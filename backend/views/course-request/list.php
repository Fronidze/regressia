<?php

use backend\forms\record\SearchCourseRequest;
use yii\data\ActiveDataProvider;
use yii\web\View;

/**
 * @var $this View
 * @var $provider ActiveDataProvider
 * @var $filter SearchCourseRequest
 */

$this->title = 'Академия разработки Mediasoft: Предварительные записи';
$this->params['links'][] = ['label' => 'Предварительные записи', 'url' => null];

?>

<div class="row">

    <div class="col-lg-8">
        <div class="ibox">
            <div class="ibox-content record_form">
                <h2>Фильтр по записям</h2>
                <?php echo $this->render('blocks/_filter', ['filter' => $filter]); ?>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="widget navy-bg p-lg text-center"
             style="height: 267px; margin: 0; display: flex; align-items: center; justify-content: center;">
            <div class="m-b-md">
                <i class="fa fa-bell fa-4x"></i>
                <h1 class="m-xs"><?= $provider->totalCount ?></h1>
                <h3 class="no-margins">запросов</h3>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Список записей</h2>
                <?php echo $this->render('blocks/_items', ['provider' => $provider]); ?>
            </div>
        </div>
    </div>
</div>

