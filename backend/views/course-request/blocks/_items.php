<?php

use backend\forms\document\CourseRequestDownload;
use common\models\CourseRequest;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $provider ActiveDataProvider
 */

$icon_import = Html::tag('i', null, ['class' => ['fa', 'fa-upload']]);
$url = [
    'course-request/download',
    (new CourseRequestDownload())->formName() => ArrayHelper::getValue(\Yii::$app->request->post(),
        'SearchCourseRequest', []),
];
echo Html::a($icon_import . ' Импорт', $url, [
    'class' => ['btn', 'btn-primary', 'pull-right'],
    'target' => '_blank',
    'style' => 'position: absolute; top: 17px; right: 35px;'
]);

echo GridView::widget([
    'options' => ['class' => ['table-responsive']],
    'layout' => "{items}\n{pager}",
    'tableOptions' => ['class' => ['table', 'table-hover', 'issue-tracker']],
    'dataProvider' => $provider,
    'showHeader' => false,
    'columns' => [
        [
            'attribute' => false,
            'contentOptions' => ['style' => 'text-align: center;'],
            'format' => 'raw',
            'value' => function (CourseRequest $record) {
                return \Yii::t('app', '{date} <small>{time}</small>', [
                    'date' => \Yii::$app->formatter->asDate($record->date_create, 'long'),
                    'time' => (new DateTime($record->date_create))->format('H:i:s')
                ]);
            }
        ],
        [
            'attribute' => false,
            'value' => function (CourseRequest $record) {
                return $record->name;
            }
        ],
        [
            'attribute' => false,
            'format' => 'raw',
            'value' => function (CourseRequest $record) {
                if ($record->email !== null) {
                    return Html::mailto($record->email, $record->email,
                        ['class' => ['text-navy']]);
                }
                return $record->email;
            }
        ],
        [
            'attribute' => false,
            'contentOptions' => ['style' => 'text-align: center'],
            'value' => function (CourseRequest $record) {
                return $record->university;
            }
        ],
        [
            'attribute' => false,
            'contentOptions' => ['style' => 'text-align: center'],
            'value' => function (CourseRequest $record) {
                $group_title = ArrayHelper::getValue($record, 'groupRecord.title');
                $technology_title = ArrayHelper::getValue($record, 'technologyRecord.title');
                return $group_title ? $group_title : $technology_title;
            }
        ]
    ],
]);