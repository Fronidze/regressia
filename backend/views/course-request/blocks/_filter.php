<?php

use backend\forms\record\SearchCourseRequest;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $filter SearchCourseRequest
 */


$form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($filter, 'date_begin')->textInput([
                'class' => ['form-control', 'datepiker'],
                'autocomplete' => 'off',
            ]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($filter, 'date_end')->textInput([
                'class' => ['form-control', 'datepiker'],
                'autocomplete' => 'off',
            ]) ?>
        </div>
    </div>

<?php
echo $form->field($filter, 'code_technology_or_group')->dropDownList(
    ArrayHelper::merge($filter->listGroup(), $filter->listTechnology()),
    [
        'class' => ['select2', 'form-control'],
        'autocomplete' => 'off',
        'prompt' => '-Выберите направление-',
    ]);
echo $form->field($filter,
    'name')->label('Поиск по имени или email')->textInput(['autocomplete' => 'off']);

?>
    <div class="row">
        <div class="col-lg-12">
            <?php echo Html::submitInput('Применить фильтр', ['class' => ['btn', 'btn-primary', 'pull-right']]); ?>
        </div>
    </div>
<?php
ActiveForm::end();
?>