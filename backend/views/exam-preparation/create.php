<?php

use backend\forms\exam\Create;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View
 * @var $exam_preparation Create
 * @var $template_id int
 */

$this->title = \Yii::t('app', 'Академия разработки Mediasoft: {action} курса ЕГЭ', [
    'action' => 'Добавление нового'
]);

$this->params['links'][] = ['label' => 'ЕГЭ', 'url' => Url::to(['exam-preparation/main'])];
$this->params['links'][] = ['label' => 'Добавление нового курса ЕГЭ', 'url' => null];

$field_config = [
    'options'      => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template'     => "{label}<div class=\"col-sm-10\">{input}</div>",
];

$field_config_datepiker = [
    'options'      => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template'     => "{label}<div class=\"col-sm-10\">
    <div class=\"input-group date\">
        <span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>
        {input}
    </div>
</div>",
];

?>
<div class="ibox ">
    <div class="ibox-title"></div>
    <div class="ibox-content">
        <?php
        $form = ActiveForm::begin(['options' => ['id' => 'form_creat_exam_preparation']]);

        echo $form->field($exam_preparation, 'template_exam_id', $field_config)->hiddenInput(['value' => $template_id])->label(false);

        echo $form->field($exam_preparation, 'title', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'sub_title', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'lecturer', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'responsible_id', $field_config)->dropDownList(
            $exam_preparation->listPersonal(),
            ['class' => ['form-control select2'], 'prompt' => ' -Выберите ответственного- ']
        );
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'code', $field_config)
            ->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'empty_date', $field_config)->dropDownList(
            [
                'yes' => 'Да',
                'no' => 'Нет',
            ],
            ['class' => ['col-sm-10 select2 select2-hidden-accessible'], 'id' => 'empty_date']
        );
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo Html::beginTag('div', ['style' => 'display:none;', 'id' => 'block_without_date']);

        echo $form->field($exam_preparation, 'sub_status', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'status', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'register', $field_config)->checkbox([
            'class' => 'form-check-input',
        ], false);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => '', 'id' => 'block_from_date']);
        echo $form->field($exam_preparation, 'date_start', $field_config_datepiker)->textInput([
            'class'        => ['datepiker', 'form-control'],
            'autocomplete' => 'off'
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'date_completion', $field_config_datepiker)->textInput([
            'class'        => ['datepiker', 'form-control'],
            'autocomplete' => 'off',
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);
        echo $this->render('weekly', [
            'form'             => $form,
            'exam_preparation' => $exam_preparation,
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => ['actions', 'clearfix']]);
        echo Html::a('Назад', Url::to(['exam-preparation/main']), ['class' => ['btn', 'btn-default', 'pull-right'], 'style' => 'margin: 0 5px']);
        echo Html::submitButton('Добавить', ['class' => ['btn', 'btn-primary', 'pull-right']]);
        echo Html::endTag('div');

        ActiveForm::end();
        ?>
    </div>
</div>

