<?php

use backend\forms\template_exam\CreateTemplate;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $template_exam CreateTemplate */


$field_config = [
    'options'      => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template'     => "{label}<div class=\"col-sm-10\">{input}</div>",
];

$form = ActiveForm::begin([
    'action'  => ['template-exam-preparation/create'],
    'options' => [
        'class' => 'form-horizontal',
    ],
    'method'  => 'post'
]);

echo $form->field($template_exam, 'title', $field_config)->textInput(['autocomplete' => 'off']);
echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

echo $form->field($template_exam, 'left_description', $field_config)->textarea(['class' => 'summernote']);
echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

echo $form->field($template_exam, 'right_description', $field_config)->textarea(['class' => 'summernote']);
echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

echo $form->field($template_exam, 'about_course', $field_config)->textarea(['class' => 'summernote']);
echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

echo $form->field($template_exam, 'program', $field_config)->textarea(['class' => 'summernote']);
echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

echo Html::beginTag('div', ['class' => ['actions', 'clearfix']]);
echo Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary', 'pull-right']]);
echo Html::endTag('div');

ActiveForm::end();
