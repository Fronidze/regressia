<?php

use backend\forms\exam\Create;
use common\models\ExamPreparation;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $exam_preparation Create */
/* @var $template_id int */

$this->title = \Yii::t('app', '{action} курса', ['action' => 'Обновление']);

$field_config = [
    'options'      => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template'     => "{label}<div class=\"col-sm-10\">{input}</div>",
];

$field_config_datepiker = [
    'options'      => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template'     => "{label}<div class=\"col-sm-10\">
    <div class=\"input-group date\">
        <span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>
        {input}
    </div>
</div>",
];

?>
<div class="ibox ">
    <div class="ibox-content">
        <?php
        $form = ActiveForm::begin(['options' => ['id' => 'form_update_course']]);

        echo $form->field($exam_preparation, 'template_exam_id', $field_config)->hiddenInput(['value' => $exam_preparation->template_exam_id])->label(false);

        echo $form->field($exam_preparation, 'title', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'sub_title', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'lecturer', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'responsible_id', $field_config)->dropDownList(
            $exam_preparation->listPersonal(),
            ['class' => ['select2 form-control'], 'prompt' => ' -Выберите ответственного- ']
        );
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'code', $field_config)
            ->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'empty_date', $field_config)->dropDownList(
            [
                'yes' => 'Да',
                'no'  => 'Нет',
            ],
            ['class' => ['col-sm-10 select2 select2-hidden-accessible form-control'], 'id' => 'empty_date']
        );
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo Html::beginTag('div', ['id' => 'block_without_date']);

        echo $form->field($exam_preparation, 'sub_status', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'status', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'register', $field_config)->checkbox(
            [
                'class'    => 'js-switch form-check-input',
                'checked ' => $exam_preparation->register == ExamPreparation::REGISTER_OPEN ? true : false
            ], false);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => '', 'id' => 'block_from_date']);
        echo $form->field($exam_preparation, 'date_start', $field_config_datepiker)->textInput([
            'class'        => ['datepiker', 'form-control'],
            'autocomplete' => 'off'
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($exam_preparation, 'date_completion', $field_config_datepiker)->textInput([
            'class'        => ['datepiker', 'form-control'],
            'autocomplete' => 'off',
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);
        echo $this->render('weekly', [
            'form'             => $form,
            'exam_preparation' => $exam_preparation,
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => ['actions', 'clearfix']]);
        echo Html::button('Закрыть', ['class' => ['btn', 'btn-white', 'pull-right'], 'data' => ['dismiss' => 'modal'], 'style' => 'margin: 0 5px;']);
        echo Html::button('Сохранить изменения', ['class' => ['btn', 'btn-primary', 'pull-right'], 'id' => 'btn_update_course']);
        echo Html::endTag('div');

        ActiveForm::end();
        ?>
    </div>
</div>
