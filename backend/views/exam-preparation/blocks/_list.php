<?php
/* @var $search Search
 * @var $schoolchild_form CreateSchoolchild
 * @var $record_form CreateRecordSchoolchild
 * @var $schoolchild_filter SearchSchoolchild
 * @var $record_filter SearchRecordSchoolchild
 * @var $schoolchild_provider ActiveDataProvider
 * @var $record_provider ActiveDataProvider
 */

use backend\forms\recordschoolchild\SearchRecordSchoolchild;
use backend\forms\recordschoolchild\CreateRecordSchoolchild;
use backend\forms\schoolchild\CreateSchoolchild;
use backend\forms\student\SearchSchoolchild;
use common\models\ExamPreparation;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <?php
                $icon = Html::tag('i', null, ['class' => ['fa', 'fa-plus']]);
                echo Html::a("$icon Добавить новый курс по ЕГЭ", ['exam-preparation/create'], ['class' => ['btn', 'btn-primary']]);
                ?>
            </div>
            <div class="ibox-content">
                <?php Pjax::begin(['id' => 'list_exam_preparation'
                ]); ?>
                <?php
                echo GridView::widget([
                    'options'      => ['class' => ['table-responsive']],
                    'layout'       => '{items}',
                    'tableOptions' => [
                        'class' => ['table', 'table-hover', 'issue-tracker'],
                    ],
                    'dataProvider' => $provider,
                    'showHeader'   => true,
                    'columns'      => [
                        [
                            'attribute'      => 'title',
                            'format'         => 'raw',
                            'headerOptions'  => [
                                'style' => 'vertical-align: middle;'
                            ],
                            'contentOptions' => function (ExamPreparation $record) {
                                return ['class' => ['issue-info']];
                            },
                            'value'          => function (ExamPreparation $record) {
                                $render = Html::a($record->title,
                                    ['exam-preparation/update', 'id' => $record->id], [
                                        'data'  => ['show-modal' => 'myModal4', 'pjax' => '0'],
                                        'class' => ['text-navy']
                                    ]);
                                $render .= Html::tag('small', $record->getActiveStatusLabel(), []);
                                return $render;
                            }
                        ],
                        [
                            'attribute'     => 'responsible_id',
                            'format'        => 'raw',
                            'headerOptions' => [
                                'style' => 'vertical-align: middle;'
                            ],
                            'value'         => function (ExamPreparation $record) {
                                return ArrayHelper::getValue($record, 'responsible.username');
                            }
                        ],
                        [
                            'attribute'      => 'publish',
                            'format'         => 'raw',
                            'header'         => "Публикация на <br> главной странице",
                            'headerOptions'  => [
                                'style' => 'text-align: center; vertical-align: middle;'
                            ],
                            'contentOptions' => function (ExamPreparation $record) {
                                return ['style' => 'text-align: center; width: 200px;'];
                            },
                            'value'          => function (ExamPreparation $record) {
                                $config = [
                                    'class' => ['js-switch'],
                                    'data'  => ['request' => Url::to(['exam-preparation/publish', 'id' => $record->id])],
                                ];

                                return Html::checkbox('publish', $record->isPublish(), $config);
                            }
                        ],
                    ],
                ]);
                ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>