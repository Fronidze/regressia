<?php /* @var $search Search
 * @var $schoolchild_form CreateSchoolchild
 * @var $record_form CreateRecordSchoolchild
 * @var $student_filter SearchRecordSchoolchild
 * @var $record_filter SearchRecordSchoolchild
 * @var $student_provider ActiveDataProvider
 * @var $record_provider ActiveDataProvider
 */

use backend\forms\document\RecordSchoolchildDownload;
use backend\forms\recordschoolchild\CreateRecordSchoolchild;
use backend\forms\recordschoolchild\SearchRecordSchoolchild;
use backend\forms\schoolchild\CreateSchoolchild;
use services\statistics\RecordSchoolchildStatistics;
use services\statistics\SchoolchildStatistics;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<div class="row">
    <div class="col-sm-8">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Запись школьника на курс.</h2>
                <p>
                    Записываем школьника на определенный курс занятий. Если же у нас нету этого школьника мы можем создать
                    его в форме создания нового школьника
                </p>
                <div class="record_form">
                    <?= $this->render('blocks/_record', ['record' => $record_form]); ?>
                </div>
            </div>
        </div>
        <div class="ibox">
            <div class="ibox-content record_form">
                <h2>Фильтр по школьникам</h2>
                <?= $this->render('blocks/_filter', ['filter' => $record_filter]); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-4 student">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Новый школьник</h2>
                <p>Форма создания нового школьника</p>
                <div class="student_form">
                    <?= $this->render('blocks/_schoolchild', ['schoolchild_form' => $schoolchild_form]); ?>
                </div>
            </div>
        </div>
        <div class="widget navy-bg p-m text-center">
            <div class="m-b-md" style="margin-bottom: 0">
                <h1 class="m-xs">
                    <?php
                    $count_records = RecordSchoolchildStatistics::instance()->countRecordByRecordPage();
                    echo $count_records;
                    ?>
                    <small style="color: white;">
                        <?php
                        function plural_form($number, $after)
                        {
                            $cases = array(2, 0, 1, 1, 1, 2);
                            echo $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
                        }

                        plural_form($count_records, ['запись', 'записи', 'записей']);
                        ?>
                    </small>
                </h1>
            </div>
        </div>
        <div class="widget navy-bg p-m text-center">
            <div class="m-b-md" style="margin-bottom: 0">
                <h1 class="m-xs">
                    <?php
                    $count_students = SchoolchildStatistics::instance()->countSchoolchildByRecordPage();
                    echo $count_students;
                    ?>
                    <small style="color: white;">
                        <?php
                        plural_form($count_students, ['школьник', 'школьника', 'школьников']);
                        ?>
                    </small>
                </h1>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Список школьников</h2>
                <?php
                $icon = Html::tag('i', null, ['class' => ['fa', 'fa-download']]);
                $icon_import = Html::tag('i', null, ['class' => ['fa', 'fa-upload']]);
                $url = [
                    'record-schoolchild/download',
                    (new RecordSchoolchildDownload())->formName() => ArrayHelper::getValue(\Yii::$app->request->get(), 'SearchRecordSchoolchild', []),
                ];
                echo Html::a($icon_import . ' Импорт', $url, [
                    'class'  => ['btn', 'btn-primary', 'pull-right'],
                    'target' => '_blank',
                    'style'  => 'position: absolute; top: 24px; right: 35px;'
                ]);

                echo Html::a($icon . ' Экспорт', ['import/index'], [
                    'class' => ['btn', 'btn-primary', 'pull-right'],
                    'style' => 'position: absolute; top: 24px; right: 125px;'
                ]);

                ?>
                <div class="record_form">
                    <?= $this->render('blocks/_list_schoolchild', [
                        'provider' => $record_provider,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>