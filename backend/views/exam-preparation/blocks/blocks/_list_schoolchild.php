<?php

use backend\forms\recordschoolchild\SearchRecordSchoolchild;
use common\models\RecordSchoolchild;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $provider ActiveDataProvider
 * @var $filter SearchRecordSchoolchild
 */

echo GridView::widget([
    'dataProvider' => $provider,
    'options'      => ['class' => ['table-responsive']],
    'layout'       => "{items}\n{pager}",
    'tableOptions' => ['class' => ['table', 'table-striped', 'table-hover']],
    'columns'      => [
        [
            'header'         => "Название курса",
            'headerOptions'  => [
                'style' => 'vertical-align: middle;'
            ],
            'attribute'      => false,
            'format'         => 'raw',
            'contentOptions' => ['style' => 'width: 15%'],
            'value'          => function (RecordSchoolchild $record) {

                $date_begin = \Yii::$app->formatter->asDate(ArrayHelper::getValue($record, 'examPreparation')->date_start, 'long');
                return \Yii::t('app', '{title} <br> <small>({date})</small>', [
                    'title' => ArrayHelper::getValue($record, 'examPreparation')->sub_title,
                    'date'  => $date_begin,
                ]);
            }
        ],
        [
            'header'         => "Дата регистрации",
            'headerOptions'  => [
                'style' => 'vertical-align: middle;'
            ],
            'attribute'      => false,
            'format'         => 'raw',
            'contentOptions' => ['style' => 'width: 15%'],
            'value'          => function (RecordSchoolchild $record) {
                return \Yii::$app->formatter->asDate($record->date_create, 'medium')
                    . ' ' . \Yii::$app->formatter->asTime($record->date_create, 'short');
            }
        ],
        [
            'header'         => "ФИО",
            'headerOptions'  => [
                'style' => 'vertical-align: middle;'
            ],
            'attribute'      => false,
            'format'         => 'raw',
            'contentOptions' => ['style' => 'width: 15%'],
            'value'          => function (RecordSchoolchild $record) {
                $name = ArrayHelper::getValue($record, 'schoolchild')->name;
                if (empty($name) === true) {
                    $name = 'Не указано';
                }
                $render = Html::a($name, ['schoolchild/preview', 'id' => $record->schoolchild_id], [
                    'class' => ['text-navy'],
                    'data'  => ['show-modal' => 'myModal4']
                ]);
                return $render;
            }
        ],
        [
            'header'         => "Почта",
            'headerOptions'  => [
                'style' => 'vertical-align: middle;'
            ],
            'attribute'      => false,
            'format'         => 'raw',
            'contentOptions' => ['style' => 'width: 15%'],
            'value'          => function (RecordSchoolchild $record) {
                return ArrayHelper::getValue($record, 'schoolchild')->email;
            }
        ],
        [
            'header'         => "Ссылка на соц. сеть",
            'headerOptions'  => [
                'style' => 'vertical-align: middle;'
            ],
            'attribute'      => false,
            'format'         => 'raw',
            'contentOptions' => ['style' => 'width: 15%'],
            'value'          => function (RecordSchoolchild $record) {
                return ArrayHelper::getValue($record, 'schoolchild')->social;
            }
        ],
    ],
]);