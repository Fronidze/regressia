<?php

use backend\forms\recordschoolchild\SearchRecordSchoolchild;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $filter SearchRecordSchoolchild
 */

$form = ActiveForm::begin(['enableClientValidation' => false, 'method' => 'get', 'action' => ['exam-preparation/main']]);
?>

    <div class="row">
        <div class="col-lg-6">
            <?php
            echo $form->field($filter, 'exam_preparation_id')->dropDownList($filter->listExamPreparation(), [
                'class'  => ['select2', 'form-group', 'change_submit'],
                'prompt' => '- Выберите курс -',
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($filter, 'name')->textInput([
                'placeholder'  => $filter->getAttributeLabel('name'),
                'autocomplete' => 'off',
            ])->label(false); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= Html::submitInput('Применить фильтр', ['class' => ['btn', 'btn-primary', 'pull-right']]); ?>
        </div>
    </div>
<?php

ActiveForm::end();