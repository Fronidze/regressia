<?php

use backend\forms\schoolchild\CreateSchoolchild;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $schoolchild_form CreateSchoolchild
 */

$form = ActiveForm::begin(['options' => [
    'data' => ['compare' => Url::to(['record-schoolchild/compare'])]
]]);
echo $form->field($schoolchild_form, 'name')->textInput(['autocomplete' => 'off',]);
echo $form->field($schoolchild_form, 'email')->textInput(['autocomplete' => 'off',]);
echo $form->field($schoolchild_form, 'social')->textInput(['autocomplete' => 'off',]);

echo Html::beginTag('div', ['class' => ['form-group', 'text-right']]);
echo Html::submitInput('Создать школьника', ['class' => ['btn', 'btn-primary', 'check_compare']]);
echo Html::endTag('div');

ActiveForm::end();