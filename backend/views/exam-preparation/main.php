<?php

/* @var $this View */

use backend\forms\record\SearchRecord;
use backend\forms\recordschoolchild\CreateRecordSchoolchild;
use backend\forms\schedule\Search;
use backend\forms\student\CreateStudent;
use backend\forms\student\SearchSchoolchild;
use common\models\TemplateExamPreparation;
use yii\data\ActiveDataProvider;
use yii\web\View;


/* @var $search Search
 * @var $schoolchild_form CreateStudent
 * @var $record_form CreateRecordSchoolchild
 * @var $student_filter SearchSchoolchild
 * @var $record_filter SearchRecord
 * @var $student_provider ActiveDataProvider
 * @var $record_provider ActiveDataProvider
 * @var $template TemplateExamPreparation
 */

/* @var $provider ActiveDataProvider */

$this->title = 'Академия разработки Mediasoft: ЕГЭ';
$this->params['links'][] = ['label' => 'ЕГЭ', 'url' => null];

?>

<ul class="nav nav-tabs" id="exam-preparation-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="template-tab" data-toggle="tab" href="#template" role="tab" aria-controls="template" aria-selected="true">Шаблон
            курсов</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="list-courses-tab" data-toggle="tab" href="#list-courses" role="tab" aria-controls="list-courses" aria-selected="false">Список
            курсов</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="list-schoolchild-tab" data-toggle="tab" href="#list-schoolchild" role="tab" aria-controls="list-schoolchild"
           aria-selected="false">Список школьников</a>
    </li>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="template" role="tabpanel" aria-labelledby="template-tab">
        <!--Шаблон курсорв ЕГЭ-->
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-content">
                        <?= $this->render($template['view'], ['template_exam' => $template['model']]); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- конец Шаблон курсорв ЕГЭ-->
    </div>
    <div class="tab-pane" id="list-courses" role="tabpanel" aria-labelledby="list-courses-tab">
        <!-- список ЕГЭ-->
        <?= $this->render('blocks/_list', [
            'provider' => $provider,
            'search'   => $search,
        ]); ?>
        <!-- список ЕГЭ-->
    </div>
    <div class="tab-pane" id="list-schoolchild" role="tabpanel" aria-labelledby="list-schoolchild-tab">
        <!-- список школьников-->
        <?= $this->render('blocks/_schoolchilds', [
            'record_form'      => $record_form,
            'schoolchild_form' => $schoolchild_form,
            'record_filter'    => $record_filter,
            'record_provider'  => $record_provider,
        ]); ?>
        <!-- список школьников-->
    </div>
</div>
