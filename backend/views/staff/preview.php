<?php

use backend\forms\profile\PersonalForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $personal PersonalForm
 */


?>
<div class="ibox ">
    <div class="ibox-content">
        <?php

        $form = ActiveForm::begin(['action' => Url::to(['staff/update', 'id' => $personal->id])]);

        echo $form->field($personal, 'position')->textInput();
        echo $form->field($personal, 'social')->textInput();
        echo $form->field($personal, 'file')->fileInput();

        echo Html::beginTag('div', ['class' => ['actions', 'clearfix'] ]);
        echo Html::button('Закрыть', ['class' => ['btn', 'btn-white', 'pull-right'], 'data' => ['dismiss' => 'modal'], 'style' => 'margin: 0 5px;']);
        echo Html::submitButton('Сохранить изменения', ['class' => ['btn', 'btn-primary', 'pull-right']]);
        echo Html::endTag('div');

        ActiveForm::end();
        ?>
    </div>
</div>
