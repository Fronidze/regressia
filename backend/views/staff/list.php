<?php

use backend\models\User;
use common\models\Files;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $provider ActiveDataProvider
 */

$this->title = 'Академия разработки Mediasoft: Персонал';
$this->params['links'][] = ['label' => 'Персонал', 'url' => null];

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Список пользователей</h2>
                <?php
                echo GridView::widget([
                    'options' => ['class' => ['table-responsive']],
                    'layout' => '{items}',
                    'tableOptions' => ['class' => ['table', 'table-hover', 'issue-tracker']],
                    'dataProvider' => $provider,
                    'showHeader' => true,
                    'columns' => [
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'contentOptions' => ['style' => 'width: 10%; text-align: center;'],
                            'value' => function (User $record) {
                                $path = null;
                                /** @var Files $file */
                                if ($file = ArrayHelper::getValue($record, 'imageFile')) {
                                    $path = $file->makeResize(50, 50);
                                }
                                
                                return Html::img($path, ['style' => 'height: 50px;']);
                            }
                        ],
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'contentOptions' => ['class' => ['issue-info'], 'style' => 'width: 30%;'],
                            'value' => function (User $record) {
                                $render = Html::a(
                                    $record->username,
                                    ['staff/preview', 'id' => $record->id],
                                    ['class' => ['text-navy'], 'data' => ['show-modal' => 'myModal4']]
                                );
                                $render .= Html::tag('small', $record->position);
                                return $render;
                            }
                        ],
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'contentOptions' => ['style' => 'width: 30%;'],
                            'value' => function (User $record) {
                                return $record->email;
                            }
                        ],
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'contentOptions' => ['style' => 'width: 30%;'],
                            'value' => function (User $record) {
                                return Html::a($record->social, $record->social, [
                                    'class' => ['text-navy'],
                                    'target' => '_blank',
                                ]);
                            }
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
