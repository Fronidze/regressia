<?php

use common\models\Schedule;
use common\models\StatisticsCourse;
use common\models\Technology;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\grid\GridView;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var $this View
 * @var $year string
 */

$query = (new Query())
    ->select([
        'title' => 'technology.title',
        'date_start' => 'schedule.date_start',
        'records' => new Expression('SUM(statistics_course.records)'),
        'total_completed' => new Expression('SUM(statistics_course.total_completed)'),
        'listened' => new Expression('SUM(statistics_course.listened)'),
        'completed' => new Expression('SUM(statistics_course.completed)'),
        'accepted' => new Expression('SUM(statistics_course.accepted)'),
        'total_completed_percent' => new Expression('((SUM(statistics_course.total_completed) / SUM(statistics_course.records))*100)'),
        'completed_percent' => new Expression('((SUM(statistics_course.completed) / SUM(statistics_course.records))*100)'),
    ])
    ->from(StatisticsCourse::tableName())
    ->innerJoin(Schedule::tableName(), ['schedule.id' => new Expression('statistics_course.schedule_id')])
    ->innerJoin(Technology::tableName(), ['technology.id' => new Expression('schedule.technology_id')])
    ->where(['=', new Expression('YEAR(schedule.date_start)'), $year])
    ->groupBy('statistics_course.schedule_id');

$provider = new SqlDataProvider([
    'sql' => $query->having(['!=', 'records', '0'])->createCommand()->getRawSql(),
    'totalCount' => $query->count(),
    'sort' => [
        'defaultOrder' => ['title' => SORT_DESC],
        'attributes' => [
            'title' => [
                'asc' => ['date_start' => SORT_ASC],
                'desc' => ['date_start' => SORT_DESC],
                'default' => SORT_DESC,
            ],
            'records',
            'total_completed',
            'listened',
            'completed',
            'accepted',
            'total_completed_percent',
            'completed_percent',
        ],
    ],
]);

if ($year >= 2019) {
    Pjax::begin(['id' => "statistics_{$year}", 'enablePushState' => false]);
    echo GridView::widget([
        'options' => ['style' => 'margin-bottom: 50px;'],
        'tableOptions' => ['class' => ['table', 'table-hover']],
        'layout' => "{items}",
        'dataProvider' => $provider,
        'columns' => [
            [
                'attribute' => 'title',
                'label' => \Yii::t('app', 'Курсы за {year}', ['year' => $year]),
                'headerOptions' => ['style' => 'width: auto;'],
                'contentOptions' => ['style' => 'padding-left: 20px;'],
                'value' => function (array $record) {
                    return \Yii::t('app', '{title} ({year})', [
                        'title' => $record['title'],
                        'year' => \Yii::$app->formatter->asDate($record['date_start'], 'long'),
                    ]);
                },
            ],
            [
                'attribute' => 'records',
                'label' => 'Заявки',
                'headerOptions' => ['style' => 'text-align: center; width: 15%;'],
                'contentOptions' => ['style' => 'text-align: center;'],
                'value' => function (array $record) {
                    return $record['records'];
                },
            ],
            [
                'attribute' => 'total_completed',
                'label' => 'Прослушали',
                'headerOptions' => ['style' => 'text-align: center; '],
                'contentOptions' => ['style' => 'text-align: center;'],
                'value' => function (array $record) {
                    return $record['total_completed'];
                },
            ],
            // Процентр прослушавших от общего числа заявок на курс
            [
                'attribute' => 'total_completed_percent',
                'label' => '%',
                'headerOptions' => ['style' => 'text-align: left; width: 10%;'],
                'contentOptions' => ['style' => 'text-align: left;'],
                'value' => function (array $record) {
                    return (int)(($record['total_completed'] / $record['records']) * 100);
                },
            ],
            [
                'attribute' => 'completed',
                'label' => 'Задание',
                'headerOptions' => ['style' => 'text-align: center;'],
                'contentOptions' => ['style' => 'text-align: center;'],
                'value' => function (array $record) {
                    return $record['completed'];
                },
            ],
            // Процентр выполнивших задание от общего числа заявок на курс
            [
                'attribute' => 'completed_percent',
                'label' => '%',
                'headerOptions' => ['style' => 'text-align: left; width: 10%;'],
                'contentOptions' => ['style' => 'text-align: left;'],
                'value' => function (array $record) {
                    return (int)(($record['completed'] / $record['records']) * 100);
                },
            ],
            [
                'attribute' => 'accepted',
                'label' => 'Вышли на стажировку',
                'headerOptions' => ['style' => 'text-align: center; width: 10%;'],
                'contentOptions' => ['style' => 'text-align: center;'],
                'value' => function (array $record) {
                    return $record['accepted'];
                },
            ],
        ],
    ]);
    Pjax::end();

} else {
    Pjax::begin(['id' => "statistics_{$year}", 'enablePushState' => false]);

    echo GridView::widget([
        'options' => ['style' => 'margin-bottom: 50px;'],
        'tableOptions' => ['class' => ['table', 'table-hover']],
        'layout' => "{items}",
        'dataProvider' => $provider,
        'columns' => [
            [
                'attribute' => 'title',
                'label' => \Yii::t('app', 'Курсы за {year}', ['year' => $year]),
                'contentOptions' => ['style' => 'padding-left: 20px;'],
                'value' => function (array $record) {
                    return \Yii::t('app', '{title} ({year})', [
                        'title' => $record['title'],
                        'year' => \Yii::$app->formatter->asDate($record['date_start'], 'long'),
                    ]);
                },
            ],
            [
                'attribute' => 'records',
                'label' => 'Заявки',
                'contentOptions' => ['style' => 'text-align: center;'],
                'headerOptions' => ['style' => 'text-align: center;'],
                'value' => function (array $record) {
                    return $record['records'];
                },
            ],
        ],
    ]);
    Pjax::end();
}
