<?php

use common\models\Technology;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $technology Technology[]
 * @var $records ActiveDataProvider
 */

$this->title = "Панель упаравления";
$that = $this;
?>

<div class="row">
    <div class="col-lg-6">
        <div class="ibox ">
            <div class="ibox-content">
                <h2>Количество студентов</h2>
                <canvas id="chart" data-request="<?php echo Url::to(['statistics/count-records']) ?>"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="ibox ">
            <div class="ibox-content">
                <h2>Записи по технологиям</h2>
                <canvas id="chart-bar"
                        data-request="<?php echo Url::to(['statistics/count-per-year', 'year' => 2019]) ?>"></canvas>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <?php
    foreach ([2019] as $year) { ?>
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h2>Статистика за <?= $year ?> год</h2>
                    <div class="ibox-tools">
                        <a class="collapse-link" href=""><i class="fa fa-chevron-up"></i></a>
                    </div>
                </div>
                <div class="ibox-content statistic-years">
                    <?php echo $this->render('blocks/year', ['year' => $year]); ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<div class="row">
    <?php
    foreach ([2018, 2017] as $year) { ?>
        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h2>Статистика за <?= $year ?> год</h2>
                    <div class="ibox-tools">
                        <a class="collapse-link" href=""><i class="fa fa-chevron-up"></i></a>
                    </div>
                </div>
                <div class="ibox-content statistic-years">
                    <?php echo $this->render('blocks/year', ['year' => $year]); ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
</div>
