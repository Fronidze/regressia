<?php

use backend\forms\technology\Create;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $technology Create
 */

$field_config = [
    'options' => ['class' => ['form-group'], 'style' => 'margin-bottom: 40px;'],
    'labelOptions' => ['class' => []],
    'template' => "{label}{input}",
];

?>

<div class="ibox ">

    <div class="ibox-content">
        <?php
        $form = ActiveForm::begin();

        echo $form->field($technology, 'id')->hiddenInput()->label(false);

        echo $form->field($technology, 'title', $field_config)->textInput(['autocomplete' => 'off']);
        echo $form->field($technology, 'sub_title', $field_config)->textInput(['autocomplete' => 'off']);
        echo $form->field($technology, 'short_title', $field_config)->textInput(['autocomplete' => 'off']);

        echo $form->field($technology, 'left_description', $field_config)->textarea(['class' => ['summernote', 'form-control']]);
        echo $form->field($technology, 'right_description', $field_config)->textarea(['class' => ['summernote', 'form-control']]);
        echo $form->field($technology, 'program', $field_config)->textarea(['class' => ['summernote', 'form-control']]);

        echo Html::beginTag('div', ['class' => ['actions', 'clearfix'] ]);
        echo Html::button('Закрыть', ['class' => ['btn', 'btn-white', 'pull-right'], 'data' => ['dismiss' => 'modal'], 'style' => 'margin: 0 5px;']);
        echo Html::submitInput('Сохранить изменения', ['class' => ['btn', 'btn-primary', 'pull-right']]);
        echo Html::endTag('div');





        ActiveForm::end();
        ?>
    </div>
</div>
