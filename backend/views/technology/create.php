<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $create \backend\forms\technology\Create */

$this->title = \Yii::t('app', 'Академия разработки Mediasoft: {action} направление', [
    'action' => $create->id === null ? 'Добавить' : 'Обновить'
]);

$this->params['links'][] = ['label' => 'Направления', 'url' => Url::to(['technology/list'])];
$this->params['links'][] = ['label' => 'Добавить направление', 'url' => null];

$field_config = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template' => "{label}<div class=\"col-sm-10\">{input}</div>",
];

?>
<div class="ibox ">

    <div class="ibox-title">
    </div>

    <div class="ibox-content">
        <?php
        $form = ActiveForm::begin();

        echo $form->field($create, 'title', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($create, 'sub_title', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($create, 'short_title', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($create, 'left_description', $field_config)->textarea(['class' => 'summernote']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($create, 'right_description', $field_config)->textarea(['class' => 'summernote']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($create, 'program', $field_config)->textarea(['class' => 'summernote']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo Html::beginTag('div', ['class' => ['actions', 'clearfix'] ]);
        echo Html::a('Назад', Url::to(['technology/list']), [
            'class' => ['btn', 'btn-default', 'pull-right'],
            'style' => 'margin-right: 5px'
        ]);

        echo Html::submitButton('Добавить', [
            'class' => ['btn', 'btn-primary', 'pull-right'],
            'style' => 'margin-right: 5px',
        ]);
        echo Html::endTag('div');

        ActiveForm::end();
        ?>
    </div>
</div>
