<?php

use common\models\Technology;
use yii\web\View;

/* @var $this View */
/* @var $technology Technology */


?>

<div class="well">
    <h3><?= $technology->getAttributeLabel('left_description');?></h3>
    <?= $technology->left_description; ?>
</div>

<div class="well">
    <h3><?= $technology->getAttributeLabel('right_description');?></h3>
    <?= $technology->right_description; ?>
</div>

<div class="well">
    <h3><?= $technology->getAttributeLabel('program');?></h3>
    <?= $technology->program; ?>
</div>
