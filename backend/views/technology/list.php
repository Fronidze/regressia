<?php

/* @var $this \yii\web\View */

use backend\forms\technology\Search;
use common\models\Technology;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $search Search */
/* @var $provider ActiveDataProvider */

$this->title = 'Академия разработки Mediasoft: Направления';
$this->params['links'][] = ['label' => 'Направления', 'url' => null];
?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <?php
                $icon = Html::tag('i', null, ['class' => ['fa', 'fa-plus']]);
                echo Html::a("$icon Добавить направление", ['technology/create'], ['class' => ['btn', 'btn-primary']]);
                ?>
            </div>
            <?php
            echo GridView::widget([
                'options' => ['class' => ['ibox-content']],
                'layout' => '{items}',
                'tableOptions' => ['class' => ['table']],
                'dataProvider' => $provider,
                'showHeader' => true,
                'columns' => [
                    [
                        'class' => SerialColumn::class,
                        'contentOptions' => ['style' => 'vertical-align: middle'],
                    ],
                    [
                        'attribute' => 'title',
                        'format' => 'raw',
                        'contentOptions' => [
                            'style' => 'vertical-align: middle',
                        ],
                        'value' => function (Technology $record) {
                            return Html::a($record->title, ['technology/update', 'id' => $record->id], [
                                'class' => ['text-navy'],
                                'data' => ['show-modal' => 'myModal4'],
                            ]);
                        }
                    ],
                    [
                        'attribute' => 'sub_title',
                        'value' => function (Technology $record) {
                            return $record->sub_title;
                        }
                    ],
                    [
                        'attribute' => 'short_title',
                        'value' => function (Technology $record) {
                            return $record->short_title;
                        }
                    ],

                ],
            ]);
            ?>
        </div>
    </div>
</div>
