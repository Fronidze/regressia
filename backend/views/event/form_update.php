<?php

use backend\forms\event\CreateEvent;
use yii\web\View;
use yii\widgets\ActiveForm;


/**
 * @var $this View
 * @var $event CreateEvent
 */
?>

<?php

$field_config = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-12', 'col-form-label']],
    'template' => "{label}<div class=\"col-sm-12\">{input}</div>",
];

$field_config_datepiker = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-12', 'col-form-label']],
    'template' => "{label}<div class=\"col-sm-12\">
    <div class=\"input-group date\">
    <span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>
    {input}
</div>
</div>",
];
$field_config_datepiker_clock = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-12', 'col-form-label']],
    'template' => "{label}<div class=\"col-sm-12\">
    <div class=\"input-group date\">
        <span class=\"input-group-addon\"><i class=\"fa fa-clock-o\"></i></span>
        {input}
    </div>
</div>",
];
?>

<div class="form_event">
    <div class="ibox-content calendar_popover">
        <?php
        $form = ActiveForm::begin(['id' => 'update_event']);

        echo $form->field($event, 'user_id')->hiddenInput(['value' => $event->user_id])->label(false);
        echo $form->field($event, 'date_event')->hiddenInput()->label(false);

        echo $form->field($event, 'title')->label(false)
            ->textInput([
                'placeholder' => 'Название мероприятия',
                'disabled' => $event->user_id === Yii::$app->user->id ? false : true,
            ]);
        echo $form->field($event, 'description')->label(false)->textInput([
            'placeholder' => 'Описание',
            'disabled' => $event->user_id === Yii::$app->user->id ? false : true,

        ]);

        echo $form->field($event, "date_event_clock", $field_config_datepiker_clock)->textInput([
            'autocomplete' => 'off',
            'class' => ['clockpicker', 'form-control'],
            'disabled' => $event->user_id === Yii::$app->user->id ? false : true,

        ])->label('Время начала');

        echo $form->field($event, 'date_reminder', $field_config_datepiker)->textInput([
            'class' => ['datepiker', 'form-control'],
            'autocomplete' => 'off',
            'disabled' => $event->user_id === Yii::$app->user->id ? false : true,

        ]);
        echo $form->field($event, "date_reminder_clock", $field_config_datepiker_clock)->textInput([
            'autocomplete' => 'off',
            'class' => ['clockpicker', 'form-control'],
            'disabled' => $event->user_id === Yii::$app->user->id ? false : true,
        ])->label(false);

        echo $form->field($event, 'users', $field_config)->dropDownList($event->listPersonal(), [
            'class' => ['select2'],
            'multiple' => 'true',
            'disabled' => $event->user_id === Yii::$app->user->id ? false : true,
        ]);

        echo $form->field($event, 'place')->label(false)->textInput([
            'placeholder' => 'Место проведения',
            'disabled' => $event->user_id === Yii::$app->user->id ? false : true,
        ]);

        ActiveForm::end();
        ?>
    </div>
</div>
