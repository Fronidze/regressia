<?php
/**
 * @var $this View
 */

use backend\models\User;
use common\models\Files;
use yii\bootstrap\Nav;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var User $user */
$user = \Yii::$app->user->getIdentity();
?>

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse" style="width: 220px; position: fixed;">
        <div class="nav-header">
            <div class="dropdown profile-element">
                <?php
                $path = null;
                /** @var Files $file */
                if ($file = ArrayHelper::getValue($user, 'imageFile')) {
                    $path = $file->makeResize(50, 50);
                }
                echo Html::img($path, ['class' => ['rounded-circle'], 'width' => 48, 'height' => 48]);
                ?>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                    <span class="block m-t-xs font-bold"><?= $user->username ?></span>
                    <span class="text-muted text-xs block"><?= $user->position ?> <b class="caret"></b></span>
                </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs" x-placement="bottom-start"
                    style="position: absolute; top: 91px; left: 0px; will-change: top, left;">
                    <li><a class="dropdown-item" href="<?= Url::to(['site/recount']) ?>">Обновить статистику</a></li>
                    <li><a class="dropdown-item" href="<?= Url::to(['auth/logout']) ?>">Выход</a></li>
                </ul>
            </div>
            <div class="logo-element"> M</div>
        </div>

        <?php
        echo Nav::widget([
            'options' => [
                'id' => 'side-menu',
                'class' => ['nav', 'metismenu'],
            ],
            'items' => [
                [
                    'label' => Html::tag('i', null, ['class' => ['fa', 'fa-database']]) . Html::tag('span',
                            'Главная страница', ['class' => ['nav-label']]),
                    'url' => ['site/index'],
                    'encode' => false,
                ],
                [
                    'label' => Html::tag('i', null, ['class' => ['fa', 'fa-database']]) . Html::tag('span',
                            'Стастистика', ['class' => ['nav-label']]),
                    'url' => ['statistic/index'],
                    'encode' => false,
                ],
                [
                    'label' => Html::tag('i', null, ['class' => ['fa', 'fa-database']]) . Html::tag('span',
                            'Направления курсов', ['class' => ['nav-label']]),
                    'url' => ['technology/list'],
                    'encode' => false,
                ],
                [
                    'label'  => Html::tag('i', null, ['class' => ['fa', 'fa-database']]) . Html::tag('span',
                            'ЕГЭ', ['class' => ['nav-label']]),
                    'url'    => ['exam-preparation/main'],
                    'encode' => false,
                ],
                [
                    'label'  => Html::tag('i', null, ['class' => ['fa', 'fa-user']]) . Html::tag('span', 'Преподаватели',
                            ['class' => ['nav-label']]),
                    'url' => ['teacher/list'],
                    'encode' => false
                ],
                [
                    'label' => Html::tag('i', null, ['class' => ['fa', 'fa-calendar']]) . Html::tag('span',
                            'Страницы курсов', ['class' => ['nav-label']]),
                    'url' => ['schedule/list'],
                    'encode' => false
                ],
                [
                    'label' => Html::tag('i', null, ['class' => ['fa', 'fa-folder-open']]) . Html::tag('span', 'Группы курсов', ['class' => ['nav-label']]),
                    'url' => ['group/list'],
                    'encode' => false
                ],
                [
                    'label' => Html::tag('i', null, ['class' => ['fa', 'fa fa-users']]) . Html::tag('span', 'Студенты',
                            ['class' => ['nav-label']]),
                    'url' => ['record/list'],
                    'encode' => false
                ],
                [
                    'label' => Html::tag('i', null, ['class' => ['fa', 'fa fa-bullhorn']]) . Html::tag('span',
                            'Предварительная запись', ['class' => ['nav-label']]),
                    'url' => ['course-request/list'],
                    'encode' => false
                ],
                [
                    'label' => Html::tag('i', null, ['class' => ['fa', 'fa-pied-piper']]) . Html::tag('span', 'Галерея',
                            ['class' => ['nav-label']]),
                    'url' => ['gallery/index'],
                    'encode' => false
                ],
                [
                    'label' => Html::tag('i', null, ['class' => ['fa', 'fa-drupal']]) . Html::tag('span', 'Персонал',
                            ['class' => ['nav-label']]),
                    'url' => ['staff/list'],
                    'encode' => false
                ],
                [
                    'label' => Html::tag('i', null, ['class' => ['fa', 'fa-question']]) . Html::tag('span', 'FAQ',
                            ['class' => ['nav-label']]),
                    'url' => ['question/list'],
                    'encode' => false
                ],
            ],
        ]);
        ?>

    </div>
</nav>
