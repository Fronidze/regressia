<?php

/**
 * @var $this View
 * @var $content string
 */

use backend\assets\InspinaAsset;
use yii\helpers\Html;
use yii\web\View;

InspinaAsset::register($this)
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="md-skin">
<?php $this->beginBody() ?>

<div id="wrapper">
    <?= $this->render('menu') ?>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?= $this->render('header_section') ?>
        <div class="wrapper wrapper-content"><?= $content ?></div>
    </div>
</div>

<?= $this->render('modals'); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
