<?php

/**
 * Created by PhpStorm.
 * User: fronidze
 * Date: 11/07/2019
 * Time: 15:34
 */

use common\widgets\Alert;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */

?>
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i></a>
        </div>
    </nav>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->title ?></h2>
        <?php echo Breadcrumbs::widget([
            'links' => isset($this->params['links']) ? $this->params['links'] : [],
            'tag' => 'ol',
            'options' => ['class' => 'breadcrumb'],
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php
        echo Alert::widget([
            'options' => ['class' => 'show header_alert'],
        ]);
        ?>
    </div>
</div>