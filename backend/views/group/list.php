<?php

/**
 * @var $this \yii\web\View
 * @var $provider ActiveDataProvider
 */

use common\models\Group;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Академия разработки Mediasoft: Группа курсов';
$this->params['links'][] = ['label' => 'Группы курсов', 'url' => null];

?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox">

            <div class="ibox-title">
                <?php
                $icon = Html::tag('i', null, ['class' => ['fa', 'fa-plus']]);
                echo Html::a("$icon Новая группа", ['group/create'], ['class' => ['btn', 'btn-primary']]);
                ?>
            </div>

            <div class="ibox-content">
                <?php
                echo GridView::widget([
                    'options' => ['class' => ['table-responsive']],
                    'layout' => '{items}',
                    'tableOptions' => [
                        'class' => ['table', 'table-hover', 'issue-tracker', 'sortable'],
                        'data' => ['request' => Url::to(['group/sorting'])]
                    ],
                    'dataProvider' => $provider,
                    'showHeader' => true,
                    'columns' => [
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'contentOptions' => [
                                'style' => 'width: 30px; text-align: center;'
                            ],
                            'value' => function (Group $record) {
                                return Html::tag(
                                    'span',
                                    Html::tag('i', null,
                                        ['class' => ['fa', 'fa-ellipsis-v'], 'style' => 'margin: 0 2px']) .
                                    Html::tag('i', null,
                                        ['class' => ['fa', 'fa-ellipsis-v'], 'style' => 'margin: 0'])
                                    , [
                                    'class' => ['sortable_handle'],
                                    'style' => 'cursor: move;'
                                ]);
                            }
                        ],
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'headerOptions' => [],
                            'contentOptions' => function (Group $record) {
                                return [
                                    'style' => 'text-align: center; width: 30px;',
                                ];
                            },
                            'value' => function (Group $record) {
                                $icon = Html::tag('i', null, ['class' => ['fa', 'fa-eye']]);
                                $link = 'http://simapple.ru/group/' . $record->code . '/';
                                return Html::a($icon, $link, ['target' => '_blank', 'class' => ['text-navy']]);
                            }
                        ],
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'contentOptions' => function (Group $record) {
                                return ['class' => ['issue-info'], 'style' => 'width: auto;'];
                            },
                            'value' => function (Group $record) {
                                $render = Html::a($record->title,
                                    ['group/update', 'id' => $record->id], [
                                        'data' => ['show-modal' => 'myModal4'],
                                        'class' => ['text-navy']
                                    ]);
                                $render .= Html::tag('small', $record->getDateStart());
                                return $render;
                            }
                        ],
                        [
                            'attribute' => 'responsible',
                            'format' => 'raw',
                            'headerOptions' => [
                                'style' => 'text-align: center; vertical-align: middle;'
                            ],
                            'contentOptions' => function (Group $record) {
                                return ['style' => 'text-align: center;'];
                            },
                            'value' => function (Group $record) {
                                return ArrayHelper::getValue($record, 'person.username');
                            }
                        ],
                        [
                            'attribute' => 'publish',
                            'format' => 'raw',
                            'header' => "Публикация на <br> главной странице",
                            'headerOptions' => [
                                'style' => 'text-align: center; vertical-align: middle;'
                            ],
                            'contentOptions' => function (Group $record) {
                                return ['style' => 'text-align: center; width: 200px;'];
                            },
                            'value' => function (Group $record) {
                                $config = [
                                    'class' => ['js-switch'],
                                    'data' => ['request' => Url::to(['group/publish', 'id' => $record->id])],
                                ];
                                return Html::checkbox('publish', $record->isPublish(), $config);
                            }
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>