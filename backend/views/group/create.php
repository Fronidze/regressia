<?php

use backend\forms\group\CreateGroup;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $this \yii\web\View
 * @var $group CreateGroup
 */


$this->title = \Yii::t('app', 'Академия разработки Mediasoft: Добавление новой группы');

$this->params['links'][] = ['label' => 'Группы курсов', 'url' => Url::to(['group/list'])];
$this->params['links'][] = ['label' => 'Добавление новой группы', 'url' => null];

$field_config = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template' => "{label}<div class=\"col-sm-10\">{input}</div>",
];

$field_config_datepiker = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template' => "{label}<div class=\"col-sm-10\">
    <div class=\"input-group date\">
        <span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>
        {input}
    </div>
</div>",
];

?>

<div class="ibox ">
    <div class="ibox-title"></div>
    <div class="ibox-content record_form">
        <?php
        $form = ActiveForm::begin();

        echo $form->field($group, 'code', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($group, 'title', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($group, 'full_title', $field_config)->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($group, 'date_start', $field_config_datepiker)->textInput([
            'class' => ['datepiker', 'form-control'],
            'autocomplete' => 'off'
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($group, 'schedules', $field_config)->dropDownList($group->listSchedules() ,[
            'class' => ['select2'],
            'multiple' => 'true',
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($group, 'responsible', $field_config)->dropDownList($group->listPersonal() ,[
            'class' => ['select2'],
            'prompt' => ' -Выберите ответственного- '
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($group, 'left_side', $field_config)->textarea(['class' => 'summernote']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($group, 'right_side', $field_config)->textarea(['class' => 'summernote']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($group, 'table', $field_config)->textarea(['class' => 'summernote']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($group, 'table_mobile', $field_config)->textarea(['class' => 'summernote']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo Html::beginTag('div', ['class' => ['actions', 'clearfix'] ]);
        echo Html::a('Назад', Url::to(['schedule/list']), ['class' => ['btn', 'btn-default', 'pull-right'], 'style' => 'margin: 0 5px']);
        echo Html::submitButton('Добавить', ['class' => ['btn', 'btn-primary', 'pull-right']]);
        echo Html::endTag('div');

        ActiveForm::end();
        ?>
    </div>
</div>
