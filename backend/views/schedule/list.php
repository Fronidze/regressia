<?php

/* @var $this \yii\web\View */

use backend\forms\schedule\Search;

/* @var $search Search */

/* @var $provider ActiveDataProvider */

use common\models\Schedule;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Академия разработки Mediasoft: Список курсов';
$this->params['links'][] = ['label' => 'Список курсов', 'url' => null];
?>

<div class="row">
    <div class="col-lg-6">
        <div class="ibox">
            <div class="ibox-content record_form">
                <?php
                $form = ActiveForm::begin();
                echo $form->field($search, 'status')->dropDownList($search->statusLabels(), [
                    'class' => ['select2'],
                    'prompt' => ' -Выберите статус- ',
                ]);

                echo Html::submitButton('Применить фильтр', ['class' => ['btn', 'btn-primary']]);
                ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <?php
                $icon = Html::tag('i', null, ['class' => ['fa', 'fa-plus']]);
                echo Html::a("$icon Добавить новый курс", ['schedule/create'], ['class' => ['btn', 'btn-primary']]);
                ?>
            </div>
            <div class="ibox-content">
                <?php
                echo GridView::widget([
                    'options' => ['class' => ['table-responsive']],
                    'layout' => '{items}',
                    'tableOptions' => [
                        'class' => ['table', 'table-hover', 'issue-tracker', 'sortable'],
                        'data' => ['request' => Url::to(['schedule/sorting'])]
                    ],
                    'dataProvider' => $provider,
                    'showHeader' => true,
                    'columns' => [
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'contentOptions' => function (Schedule $record) {
                                return ['style' => 'width: 30px; padding: 0; line-height: 1; text-align: center;'];
                            },
                            'value' => function (Schedule $record) use ($search) {
                                return Html::tag(
                                    'span',
                                    Html::tag('i', null,
                                        ['class' => ['fa', 'fa-ellipsis-v'], 'style' => 'margin: 0 2px']) .
                                    Html::tag('i', null,
                                        ['class' => ['fa', 'fa-ellipsis-v'], 'style' => 'margin: 0'])
                                    , [
                                    'class' => [(empty($search->status) === true) ? 'sortable_handle' : ''],
                                    'style' => 'cursor: move;'
                                ]);
                            }
                        ],
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'headerOptions' => [],
                            'contentOptions' => function (Schedule $record) {
                                return ['style' => 'width: 14%'];
                            },
                            'value' => function (Schedule $record) {
                                $render = Html::beginTag('div', ['class' => ['btn-group']]);
                                $render .= Html::a(
                                    $record->getStatusLabel(Schedule::STATUS_DRAFT),
                                    ['schedule/change-status', 'id' => $record->id, 'status' => Schedule::STATUS_DRAFT],
                                    [
                                        'class' => [
                                            'btn',
                                            $record->isDraft() ? 'btn-primary' : 'btn-white',
                                            'schedule_status_change'
                                        ],
                                        'data' => ['status' => Schedule::STATUS_DRAFT],
                                    ]
                                );
                                $render .= Html::a(
                                    $record->getStatusLabel(Schedule::STATUS_ACTIVE),
                                    [
                                        'schedule/change-status',
                                        'id' => $record->id,
                                        'status' => Schedule::STATUS_ACTIVE
                                    ],
                                    [
                                        'class' => [
                                            'btn',
                                            $record->isActive() ? 'btn-primary' : 'btn-white',
                                            'schedule_status_change'
                                        ],
                                        'data' => ['status' => Schedule::STATUS_ACTIVE],
                                    ]
                                );
                                $render .= Html::a(
                                    $record->getStatusLabel(Schedule::STATUS_ARCHIVE),
                                    [
                                        'schedule/change-status',
                                        'id' => $record->id,
                                        'status' => Schedule::STATUS_ARCHIVE
                                    ],
                                    [
                                        'class' => [
                                            'btn',
                                            $record->isArchive() ? 'btn-primary' : 'btn-white',
                                            'schedule_status_change'
                                        ],
                                        'data' => ['status' => Schedule::STATUS_ARCHIVE],
                                    ]
                                );
                                $render .= Html::endTag('div');
                                return $render;
                            }
                        ],
                        /*[
                            'attribute' => false,
                            'format' => 'raw',
                            'headerOptions' => [],
                            'contentOptions' => function (Schedule $record) {
                                return [
                                    'style' => 'text-align: center',
                                ];
                            },
                            'value' => function (Schedule $record) {
                                $icon = Html::tag('i', null, ['class' => ['fa', 'fa-eye']]);
                                $link = 'http://simapple.ru/education/' . $record->code . '/';
                                return Html::a($icon, $link, ['target' => '_blank', 'class' => ['text-navy']]);
                            }
                        ],*/
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'headerOptions' => [],
                            'contentOptions' => function (Schedule $record) {
                                return ['class' => ['issue-info']];
                            },
                            'value' => function (Schedule $record) {
                                $render = Html::a($record->getTechnologyTitle(),
                                    ['schedule/update', 'id' => $record->id], [
                                        'data' => ['show-modal' => 'myModal4'],
                                        'class' => ['text-navy']
                                    ]);
                                $render .= Html::tag('small', $record->getScheduleDateRange(), []);
                                return $render;
                            }
                        ],
                        [
                            'attribute' => 'teacher_id',
                            'format' => 'raw',
                            'headerOptions' => [
                                'style' => 'vertical-align: middle;'
                            ],
                            'contentOptions' => function (Schedule $record) {
                                return ['style' => ''];
                            },
                            'value' => function (Schedule $record) {
                                return ArrayHelper::getValue($record, 'teacher.name');
                            }
                        ],
                        [
                            'attribute' => 'responsible_id',
                            'format' => 'raw',
                            'headerOptions' => [
                                'style' => 'vertical-align: middle;'
                            ],
                            'value' => function (Schedule $record) {
                                return ArrayHelper::getValue($record, 'responsible.username');
                            }
                        ],
                        [
                            'attribute' => 'publish',
                            'format' => 'raw',
                            'header' => "Публикация на <br> главной странице",
                            'headerOptions' => [
                                'style' => 'text-align: center; vertical-align: middle;'
                            ],
                            'contentOptions' => function (Schedule $record) {
                                return ['style' => 'text-align: center; width: 200px;'];
                            },
                            'value' => function (Schedule $record) {
                                $config = [
                                    'class' => ['js-switch'],
                                    'data' => ['request' => Url::to(['schedule/publish', 'id' => $record->id])],
                                ];

                                if ($record->isActive() === false) {
                                    $config['disabled'] = true;
                                }

                                return Html::checkbox('publish', $record->isPublish(), $config);
                            }
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>