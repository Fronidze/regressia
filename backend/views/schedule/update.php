<?php

use backend\forms\schedule\Create;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $schedule Create */

$this->title = \Yii::t('app', '{action} курса', ['action' => 'Обновление']);

$field_config = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template' => "{label}<div class=\"col-sm-10\">{input}</div>",
];

$field_config_datepiker = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template' => "{label}<div class=\"col-sm-10\">
    <div class=\"input-group date\">
        <span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>
        {input}
    </div>
</div>",
];

?>
<div class="ibox ">
    <div class="ibox-content">
        <?php
        $form = ActiveForm::begin();

        echo $form->field($schedule, 'technology_id', $field_config)->dropDownList(
            $schedule->listTechnology(),
            ['class' => ['select2'], 'prompt' => ' -Укажите технологию- ']
        );
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($schedule, 'teacher_id', $field_config)->dropDownList(
            $schedule->listTeacher(),
            ['class' => ['select2'], 'prompt' => ' -Выберите преподователя- ']
        );
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($schedule, 'responsible_id', $field_config)->dropDownList(
            $schedule->listPersonal(),
            ['class' => ['select2'], 'prompt' => ' -Выберите ответственного- ']
        );
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($schedule, 'date_start', $field_config_datepiker)->textInput([
            'class' => ['datepiker', 'form-control'],
            'autocomplete' => 'off'
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($schedule, 'date_last_lesson', $field_config_datepiker)->textInput([
            'class' => [ 'datepiker', 'form-control' ],
            'autocomplete' => 'off',
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($schedule, 'date_completion', $field_config_datepiker)->textInput([
            'class' => [ 'datepiker', 'form-control' ],
            'autocomplete' => 'off',
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);


        echo $form->field($schedule, 'code', $field_config)
            ->textInput(['autocomplete' => 'off']);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $this->render('weekly', [
            'form' => $form,
            'schedule' => $schedule,
        ]);
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo Html::beginTag('div', ['class' => ['actions', 'clearfix'] ]);
        echo Html::button('Закрыть', ['class' => ['btn', 'btn-white', 'pull-right'], 'data' => ['dismiss' => 'modal'], 'style' => 'margin: 0 5px;']);
        echo Html::submitButton('Сохранить изменения', ['class' => ['btn', 'btn-primary', 'pull-right']]);
        echo Html::endTag('div');

        ActiveForm::end();
        ?>
    </div>
</div>
