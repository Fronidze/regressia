<?php

use backend\forms\schedule\Create;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $form ActiveForm
 * @var $schedule Create
 */

$months = [
    'monday' => 'Понедельник',
    'tuesday' => 'Вторник',
    'wednesday' => 'Среда',
    'thursday' => 'Четверг',
    'friday' => 'Пятница',
    'saturday' => 'Суббота',
    'sunday' => 'Воскресенье',
];

$field_config_datepiker = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template' => "{label}<div class=\"col-sm-10\">
    <div class=\"input-group date\">
        <span class=\"input-group-addon\"><i class=\"fa fa-clock-o\"></i></span>
        {input}
    </div>
</div>",
];


?>


<div class="form-group row">
    <label class="col-sm-2 col-form-label"
           for="create-quantity_lesson"><?= $schedule->getAttributeLabel('weekly') ?></label>
    <div class="col-sm-10">
        <div class="weekly">
            <?php foreach ($months as $code => $name) : ?>
                <div class="form-group" id="data_3">
                    <label class="font-normal"><?= $name ?></label>
                    <div class="input-group">
                        <?= $form
                            ->field($schedule, "weekly[$code]", $field_config_datepiker)
                            ->textInput([
                                    'autocomplete' => 'off',
                                'class' => ['clockpicker', 'form-control']
                                ])->label(false); ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>



