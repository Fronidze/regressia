<?php

use common\models\Schedule;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $schedule Schedule
 */

?>

<div class="ibox" id="preview_schedule">
    <div class="ibox-content">

        <div class="form-group row">
            <label class="col-sm-4 col-form-label text-right"><?= $schedule->getAttributeLabel('teacher_id'); ?></label>
            <div class="col-sm-8"><?= $schedule->getTeacherName(); ?></div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label text-right">Период проведения</label>
            <div class="col-sm-8"><?= $schedule->getScheduleDateRange();?></div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label text-right">Время занятий</label>
            <div class="col-sm-8">
                <div class="weekly">
                    <?php
                    $weekly = $schedule->getWeeklySchedule();
                    foreach ($weekly as $week) {
                        echo \Yii::t('app', "{week} в {time}<br>", [
                            'week' => ArrayHelper::getValue($week, 'label_week'),
                            'time' => ArrayHelper::getValue($week, 'time'),
                        ]);
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label text-right"><?= $schedule->getAttributeLabel('program'); ?></label>
            <div class="col-sm-8"><div class="program_wrapper"><?= $schedule->program; ?></div></div>
        </div>

        <div class="actions clearfix">
            <button type="button" class="btn btn-white pull-right" style="margin: 0 5px" data-dismiss="modal">Закрыть</button>
            <?php echo Html::a('Активировать',
                ['schedule/activate', 'id' => $schedule->id],
                ['class' => ['btn', 'btn-primary', 'pull-right']]
            ); ?>
        </div>
    </div>
</div>