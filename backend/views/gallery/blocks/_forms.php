<?php


/**
 * @var $this View
 * @var $gallery GalleryUpload
 */

use backend\forms\gallery\GalleryUpload;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$config = [
    'template' => "<div class=\"custom-file\">{input}\n{label}</div>",
    'labelOptions' => ['class' => 'custom-file-label'],
    'inputOptions' => ['class' => 'custom-file-input']
];

$form = ActiveForm::begin();

echo $form->field($gallery, 'image', $config)->fileInput();

echo Html::beginTag('div', ['style' => 'display: flex; justify-content: flex-end;']);
echo Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]);
echo Html::endTag('div');
ActiveForm::end();