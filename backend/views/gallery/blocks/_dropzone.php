<?php


/**
 * @var $this View
 * @var $gallery GalleryUpload
 */

use backend\forms\gallery\GalleryUpload;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;


$form = ActiveForm::begin(['action' => ['gallery/upload-file'], 'id' => 'dropzoneForm', 'options' => ['class' => ['dropzone']]]);
echo Html::beginTag('div', ['class' => ['fallback']]);
echo $form->field($gallery, 'file');
echo Html::endTag('div');
ActiveForm::end();

echo Html::beginTag('div', [ 'class' => ['row'], 'style' => 'margin-top: 15px' ]);
echo Html::beginTag('div', [ 'class' => ['col-lg-12'] ]);
echo Html::a('Сохранить', ['gallery/index'], ['class' => ['btn', 'btn-primary', 'pull-right']]);
echo Html::endTag('div');
echo Html::endTag('div');
