<?php


/**
 * @var $this View
 * @var $images Gallery[]
 */


use common\models\Files;
use common\models\Gallery;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

?>

<div class="table-responsive">
    <table class="table table-hover issue-tracker sortable" data-request="<?= Url::to(['gallery/sorting'])?>">
        <?php foreach ($images as $image): ?>
            <tr data-key="<?= $image->id?>">
                <td style="text-align: center; width: 100px">
                <span class="sortable_handle" style="cursor: move;">
                    <i class="fa fa-ellipsis-v"></i>
                    <i class="fa fa-ellipsis-v"></i>
                </span>
                </td>

                <td style="width: 200px; text-align: center">
                    <?php
                    $path = null;
                    /** @var Files $file */
                    if ($file = ArrayHelper::getValue($image, 'imageFile')) {
                        $path = $file->makeResize(200, 133);
                    }
                    echo Html::img($path, ['width' => '200']);
                    ?>
                </td>

                <td><?= Html::a(Html::tag('i', null, ['class' => ['fa', 'fa-times']]), ['gallery/remove', 'id' => $image->id], ['class' => ['text-danger']]) ?></td>

                <td style="text-align: center">
                    <?php
                    echo Html::checkbox('publish', $image->isPublish(), [
                        'class' => ['js-switch'],
                        'data' => ['request' => Url::to(['gallery/publish', 'id' => $image->id])],
                    ]);
                    ?>
                </td>

            </tr>
        <?php endforeach; ?>
    </table>
</div>