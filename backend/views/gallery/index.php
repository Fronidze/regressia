<?php

use backend\forms\gallery\GalleryUpload;
use common\models\Gallery;
use yii\web\View;

/**
 * @var $this View
 * @var $gallery GalleryUpload
 * @var $images Gallery[]
 */


$this->title = 'Академия разработки Mediasoft: Галлерея';
$this->params['links'][] = ['label' => 'Галлерея', 'url' => null];
?>

<div class="row">

    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">Добавление новых фотографий</div>
            <div class="ibox-content">
                <?= $this->render('blocks/_dropzone', ['gallery' => $gallery]); ?>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">Изображения</div>
            <div class="ibox-content">
                <?= $this->render('blocks/_items', ['images' => $images]) ?>
            </div>
        </div>
    </div>
</div>
