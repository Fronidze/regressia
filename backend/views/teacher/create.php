<?php

use backend\forms\teacher\Create;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $teacher Create */

$this->title = \Yii::t('app', 'Академия разработки Mediasoft: {action} Преподавателя', [
    'action' => $teacher->id === null ? 'Добавить' : 'Обновить'
]);

$this->params['links'][] = ['label' => 'Преподаватели', 'url' => Url::to(['teacher/list'])];
$this->params['links'][] = ['label' => 'Добавить преподавателя', 'url' => null];

$field_config = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template' => "{label}<div class=\"col-sm-10\">{input}</div>",
];

?>
<div class="ibox ">
    <div class="ibox-title"></div>
    <div class="ibox-content">
        <?php
        $form = ActiveForm::begin();

        echo $form->field($teacher, 'name', $field_config)->textInput();
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($teacher, 'position', $field_config)->textInput();
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($teacher, 'curator', $field_config)->textInput();
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($teacher, 'file', $field_config)->fileInput();
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo Html::beginTag('div', ['class' => ['actions', 'clearfix'] ]);
        echo Html::a('Назад',
            Url::to(['teacher/list']),
            [
                'class' => ['btn', 'btn-default', 'pull-right'],
                'style' => 'margin: 0 5px'
            ]);
        echo Html::submitButton('Добавить', ['class' => ['btn', 'btn-primary', 'pull-right']]);
        echo Html::endTag('div');


        ActiveForm::end();
        ?>
    </div>
</div>

