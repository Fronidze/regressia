<?php

use common\models\links\TeacherAbility;
use common\models\Teacher;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $teacher Teacher */


$links = ArrayHelper::getValue($teacher, 'linkAbility');
if ($links === null) {
    return false;
}
echo Html::beginTag('div',
    ['class' => ['label_wrapper'], 'style' => 'display: flex; flex-direction: row; flex-wrap: wrap;']);
/** @var TeacherAbility $link */
foreach ($links as $link) {
    echo Html::tag(
        'span',
        ArrayHelper::getValue($link, 'ability.title'),
        ['class' => ['badge', 'badge-primary'], 'style' => 'margin: 5px']
    );
}
echo Html::endTag('div');

