<?php

use backend\forms\teacher\Create;
use common\models\Files;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $teacher Create */

$this->title = \Yii::t('app', 'Академия разработки Mediasoft: {action} преподователя', [
    'action' => $teacher->id === null ? 'Добавить' : 'Обновить'
]);

$field_config = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template' => "{label}<div class=\"col-sm-10\">{input}</div>",
];

?>

<div class="ibox ">

    <div class="ibox-content">
        <?php
        $form = ActiveForm::begin();

        echo $form->field($teacher, 'name', $field_config)->textInput();
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($teacher, 'position', $field_config)->textInput();
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($teacher, 'curator', $field_config)->textInput();
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo Html::beginTag('div', ['class' => ['row', 'form-group']]);
        echo Html::beginTag('div', ['class' => ['col-sm-2']]);
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => ['col-sm-10']]);
        $path = null;
        /** @var Files $file */
        if ($file = ArrayHelper::getValue($teacher, 'imageFile')) {
            $path = $file->makeResize(200, 200);
        }
        echo Html::img($path, ['class' => ['teacher_img'], 'style' => 'width: 200px;']);
        echo Html::endTag('div');
        echo Html::endTag('div');

        echo $form->field($teacher, 'file', $field_config)->fileInput();
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo Html::beginTag('div', ['class' => ['actions', 'clearfix'] ]);
        echo Html::button('Закрыть', ['class' => ['btn', 'btn-white', 'pull-right'], 'data' => ['dismiss' => 'modal'], 'style' => 'margin: 0 5px;']);
        echo Html::submitButton('Сохранить изменения', ['class' => ['btn', 'btn-primary', 'pull-right']]);
        echo Html::endTag('div');

        ActiveForm::end();
        ?>
    </div>
</div>
