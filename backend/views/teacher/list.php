<?php

/* @var $this \yii\web\View */

use backend\forms\technology\Search;
use common\models\Teacher;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $search Search */
/* @var $provider ActiveDataProvider */

$this->title = 'Академия разработки Mediasoft: Преподаватели';
$this->params['links'][] = ['label' => 'Преподаватели', 'url' => null];
?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <?php
                $icon = Html::tag('i', null, ['class' => ['fa', 'fa-plus']]);
                echo Html::a("$icon Добавить преподователя", ['teacher/create'], ['class' => ['btn', 'btn-primary']]);
                ?>
            </div>
            <div class="ibox-content">
                <?php
                echo GridView::widget([
                    'options' => ['class' => ['table-responsive']],
                    'layout' => '{items}',
                    'tableOptions' => [
                        'class' => ['table', 'table-hover', 'issue-tracker', 'sortable'],
                        'data' => [
                            'request' => Url::to(['teacher/sorting'])
                        ]
                    ],
                    'dataProvider' => $provider,
                    'showHeader' => true,
                    'columns' => [
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'contentOptions' => function (Teacher $record) {
                                return ['style' => 'width: 30px; padding: 0; line-height: 1; text-align: center;'];
                            },
                            'value' => function (Teacher $record) {
                                return Html::tag(
                                    'span',
                                    Html::tag('i', null,
                                        ['class' => ['fa', 'fa-ellipsis-v'], 'style' => 'margin: 0 2px']) .
                                    Html::tag('i', null,
                                        ['class' => ['fa', 'fa-ellipsis-v'], 'style' => 'margin: 0'])
                                    , [
                                    'class' => ['sortable_handle'],
                                    'style' => 'cursor: move;'
                                ]);
                            }
                        ],
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'headerOptions' => [],
                            'contentOptions' => function (Teacher $record) {
                                return [
                                    'class' => ['issue-info'],
                                    'style' => 'vertical-align: middle;'
                                ];
                            },
                            'value' => function (Teacher $record) {
                                $render = Html::a($record->name, ['teacher/update', 'id' => $record->id], [
                                    'class' => ['text-navy'],
                                    'data' => ['show-modal' => 'myModal4'],
                                ]);
                                return $render . Html::tag('small', $record->position, []);
                            }
                        ],
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'headerOptions' => [],
                            'contentOptions' => function (Teacher $record) {
                                return [
                                    'style' => 'vertical-align: middle;'
                                ];
                            },
                            'value' => function (Teacher $record) {
                                return $record->curator;
                            }
                        ],
                        [
                            'attribute' => 'is_publish',
                            'format' => 'raw',
                            'header' => "Публикация на <br> главной странице",
                            'contentOptions' => function (Teacher $record) {
                                return [
                                    'style' => 'vertical-align: middle; text-align: center;',
                                ];
                            },
                            'headerOptions' => [
                                'style' => 'vertical-align: middle; text-align: center;'
                            ],
                            'value' => function (Teacher $record) {
                                return Html::checkbox('publish', $record->isPublish(), [
                                    'class' => ['js-switch'],
                                    'data' => ['request' => Url::to(['teacher/change-publish', 'id' => $record->id])]
                                ]);
                            }
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
