<?php

use common\models\Record;
use common\models\Students;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $student Students
 */

/** @var Record[] $records */
$records = ArrayHelper::getValue($student, 'records');
if ($records !== null) { ?>
    <div class="ibox">
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped table-hover issue-tracker">
                    <tr>
                        <th></th>
                        <th style="text-align: center">Прослушал</th>
                        <th style="text-align: center">Выполнил</th>
                        <th style="text-align: center">Принят</th>
                    </tr>
                    <?php foreach ($records as $record) { ?>
                        <tr>
                            <td>
                                <?php
                                echo \Yii::t('app', '{title} ({date})', [
                                    'title' => ArrayHelper::getValue($record, 'schedule.technology.title'),
                                    'date' => \Yii::$app->formatter->asDate($record->date_create, 'long'),
                                ]);
                                ?>
                            </td>
                            <td style="text-align: center">
                                <?php
                                echo Html::checkbox('', $record->listened === Record::RESULT_POSITIVE, [
                                    'class' => ['js-switch'],
                                    'data' => ['request' => Url::to(['record/change-listened', 'id' => $record->id])],
                                ]);
                                ?>
                            </td>
                            <td style="text-align: center">
                                <?php
                                echo Html::checkbox('completed', $record->completed === Record::RESULT_POSITIVE, [
                                    'class' => ['js-switch'],
                                    'data' => ['request' => Url::to(['record/change-completed', 'id' => $record->id])],
                                ]);
                                ?>
                            </td>
                            <td style="text-align: center">
                                <?php
                                echo Html::checkbox('accepted', $record->accepted === Record::RESULT_POSITIVE, [
                                    'class' => ['js-switch'],
                                    'data' => ['request' => Url::to(['record/change-accepted', 'id' => $record->id])],
                                ]);
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
<?php } ?>