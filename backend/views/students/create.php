<?php

use backend\forms\student\CreateStudent;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $student CreateStudent
 */

$this->title = 'Академия разработки Mediasoft: Новый студент';
$this->params['links'][] = ['label' => 'Студенты', 'url' => Url::to(['students/list'])];
$this->params['links'][] = ['label' => 'Новый студент', 'url' => null];

?>

<div class="ibox ">
    <div class="ibox-title"></div>
    <div class="ibox-content">
        <?php
        $form = ActiveForm::begin();

        echo $form->field($student, 'email');
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo $form->field($student, 'name');
        echo Html::tag('div', null, ['class' => ['hr-line-dashed']]);

        echo Html::beginTag('div', ['class' => ['actions', 'clearfix']]);
        echo Html::a('Назад', Url::to(['students/list']), ['class' => ['btn', 'btn-default', 'pull-right'], 'style' => 'margin: 0 5px']);
        echo Html::submitButton('Добавить', ['class' => ['btn', 'btn-primary', 'pull-right']]);
        echo Html::endTag('div');


        ActiveForm::end();
        ?>
    </div>
</div>
