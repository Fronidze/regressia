<?php


/**
 * @var $this \yii\web\View
 * @var $provider ActiveDataProvider
 */

use common\models\Students;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Академия разработки Mediasoft: Студенты';
$this->params['links'][] = ['label' => 'Студенты', 'url' => null];

?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <?php
                $icon = Html::tag('i', null, ['class' => ['fa', 'fa-plus']]);
                echo Html::a("$icon Добавить студента", ['students/create'], ['class' => ['btn', 'btn-primary']]);
                ?>
            </div>
            <div class="ibox-content">
                <?php
                echo GridView::widget([
                    'options' => ['class' => ['table-responsive']],
                    'layout' => '{items}',
                    'tableOptions' => ['class' => ['table', 'table-hover', 'issue-tracker']],
                    'dataProvider' => $provider,
                    'showHeader' => true,
                    'columns' => [
                        [
                            'attribute' => false,
                            'value' => function (Students $record) {
                                return $record->email;
                            }
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
