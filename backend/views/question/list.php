<?php

/* @var $this View */

use backend\forms\technology\Search;
use common\models\Question;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $search Search */
/* @var $provider ActiveDataProvider */

$this->title = 'Академия разработки Mediasoft: FAQ: все, что нужно знать о наших курсах';
$this->params['links'][] = ['label' => 'FAQ: все, что нужно знать о наших курсах', 'url' => null];
?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <?php
                $icon = Html::tag('i', null, ['class' => ['fa', 'fa-plus']]);
                echo Html::a("$icon Добавить вопрос", ['question/create'], ['class' => ['btn', 'btn-primary']]);
                ?>
            </div>
            <div class="ibox-content">
                <?php
                echo GridView::widget([
                    'options' => ['class' => ['table-responsive']],
                    'layout' => '{items}',
                    'tableOptions' => [
                        'class' => ['table', 'table-hover', 'issue-tracker', 'sortable'],
                        'data' => [
                            'request' => Url::to(['question/sorting'])
                        ]
                    ],
                    'dataProvider' => $provider,
                    'showHeader' => true,
                    'columns' => [
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'contentOptions' => function (Question $record) {
                                return ['style' => 'width: 30px; padding: 0; line-height: 1; text-align: center;'];
                            },
                            'value' => function (Question $record) {
                                return Html::tag(
                                    'span',
                                    Html::tag('i', null,
                                        ['class' => ['fa', 'fa-ellipsis-v'], 'style' => 'margin: 0 2px']) .
                                    Html::tag('i', null,
                                        ['class' => ['fa', 'fa-ellipsis-v'], 'style' => 'margin: 0'])
                                    , [
                                    'class' => ['sortable_handle'],
                                    'style' => 'cursor: move;'
                                ]);
                            }
                        ],
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'header' => "Вопрос",
                            'headerOptions' => [],
                            'contentOptions' => function (Question $record) {
                                return [
                                    'style' => 'vertical-align: middle;'
                                ];
                            },
                            'value' => function (Question $record) {
                                $render = Html::a($record->question, ['question/update', 'id' => $record->id], [
                                    'class' => ['text-navy'],
                                    'data' => ['show-modal' => 'myModal4'],
                                ]);
                                return $render;
                            }
                        ],
                        [
                            'attribute' => false,
                            'format' => 'raw',
                            'header' => "Ответ",
                            'headerOptions' => [],
                            'contentOptions' => function (Question $record) {
                                return [
                                    'class' => 'table-questions-column-answer',
                                    'style' => 'vertical-align: middle;'
                                ];
                            },
                            'value' => function (Question $record) {
                                return $record->answer;
                            }
                        ],
                        [
                            'attribute' => 'is_publish',
                            'format' => 'raw',
                            'header' => "Публикация на <br> главной странице",
                            'contentOptions' => function (Question $record) {
                                return [
                                    'style' => 'vertical-align: middle; text-align: center;',
                                ];
                            },
                            'headerOptions' => [
                                'style' => 'vertical-align: middle; text-align: center;'
                            ],
                            'value' => function (Question $record) {
                                return Html::checkbox('publish', $record->isPublish(), [
                                    'class' => ['js-switch'],
                                    'data' => ['request' => Url::to(['question/change-publish', 'id' => $record->id])]
                                ]);
                            }
                        ],
                        [
                            'attribute' => '',
                            'format' => 'raw',
                            'header' => "",
                            'contentOptions' => function (Question $record) {
                                return [
                                    'style' => 'vertical-align: middle; text-align: center;',
                                ];
                            },
                            'headerOptions' => [
                                'style' => 'vertical-align: middle; text-align: center;'
                            ],
                            'value' => function (Question $record) {
                                return Html::Button('', [
                                    'class' => ['btn glyphicon glyphicon-trash js-delete'],
                                    'data' => ['request' => Url::to(['question/delete', 'id' => $record->id])]
                                ]);
                            }
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
