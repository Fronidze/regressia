<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */


$field_config = [
    'options' => ['class' => ['form-group', 'row']],
    'labelOptions' => ['class' => ['col-sm-2', 'col-form-label']],
    'template' => "<div class=\"col-sm-10\">{input}</div>",
];

$form = ActiveForm::begin();
echo Html::beginTag('div', ['class' => ['actions', 'clearfix']]);
echo Html::button('Отмена', ['class' => ['btn', 'btn-white', 'pull-right', 'col-5'], 'data' => ['dismiss' => 'modal'], 'style' => '']);
echo Html::submitButton('Удалить', ['class' => ['btn', 'btn-danger', 'pull-left', 'col-5']]);
echo Html::endTag('div');
ActiveForm::end();
