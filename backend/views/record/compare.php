<?php

use backend\forms\student\CreateStudent;
use common\models\Students;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;


/**
 * @var $this View
 * @var $student CreateStudent
 * @var $record Students
 */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content compare_modal">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th>Старое значение</th>
                        <th>Новое значение</th>
                    </tr>
                    <?php foreach (['email', 'name', 'social', 'abilities'] as $field): ?>
                        <tr>
                            <td><?= $record->getAttributeLabel($field); ?></td>
                            <td><?= $record->getAttribute($field); ?></td>
                            <td><?= $student->getAttribute($field); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>

                <?php
                $form = ActiveForm::begin(['action' => Url::to(['record/list'])]);
                echo $form->field($student, 'email')->hiddenInput()->label(false);
                echo $form->field($student, 'name')->hiddenInput()->label(false);
                echo $form->field($student, 'social')->hiddenInput()->label(false);
                echo $form->field($student, 'abilities')->hiddenInput()->label(false);
                ?>
                <div class="row">
                    <div class="col-lg-12" style="margin: 20px 0;">
                        <?php
                        echo Html::submitInput('Применить изменения',
                            ['class' => ['btn', 'btn-primary', 'pull-right']]);
                        echo Html::button('Отменить изменения', [
                            'class' => ['btn', 'btn-white', 'pull-right'],
                            'data' => ['dismiss' => 'modal'],
                            'style' => 'margin: 0 10px;'
                        ]);
                        ?>
                    </div>
                </div>
                <?php
                ActiveForm::end();
                ?>

            </div>
        </div>
    </div>
</div>