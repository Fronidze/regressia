<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $student \backend\forms\student\CreateStudent
 */

$form = ActiveForm::begin(['options' => [
    'data' => ['compare' => Url::to(['record/compare'])]
]]);
echo $form->field($student, 'name')->textInput(['autocomplete' => 'off',]);
echo $form->field($student, 'email')->textInput(['autocomplete' => 'off',]);
echo $form->field($student, 'social')->textInput(['autocomplete' => 'off',]);
echo $form->field($student, 'abilities')->textarea(['rows' => 4]);

echo Html::beginTag('div', ['class' => ['form-group', 'text-right']]);
echo Html::submitInput('Создать студента', ['class' => ['btn', 'btn-primary', 'check_compare']]);
echo Html::endTag('div');

ActiveForm::end();