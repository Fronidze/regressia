<?php

use backend\forms\record\SearchRecord;
use common\models\Record;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $provider ActiveDataProvider
 * @var $filter SearchRecord
 */

echo GridView::widget([
    'dataProvider' => $provider,
    'options' => ['class' => ['table-responsive']],
    'layout' => "{items}\n{pager}",
    'tableOptions' => ['class' => ['table', 'table-striped', 'table-hover']],
    'columns' => [
        [
            'attribute' => false,
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 15%'],
            'value' => function (Record $record) {
                $date_begin = \Yii::$app->formatter->asDate(ArrayHelper::getValue($record, 'schedule.date_start'), 'long');
                return \Yii::t('app', '{title} <br> <small>({date})</small>', [
                    'title' => ArrayHelper::getValue($record, 'schedule.technology.title'),
                    'date' => $date_begin,
                ]);
            }
        ],
        [
            'attribute' => false,
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 15%'],
            'value'          => function (Record $record) {
                $date_create_record = new DateTime($record->date_create);
                if ($date_create_record->format('H:i:s') === "00:00:00"
                    && $date_create_record->format('Y-m-d') < (new DateTime('2019-12-01'))->format('Y-m-d')) {
                    return \Yii::$app->formatter->asDate($record->date_create, 'long');
                }
                return \Yii::$app->formatter->asDate($record->date_create, 'long') . " "
                    . \Yii::$app->formatter->asTime($record->date_create, 'short');
            }
        ],
        [
            'attribute' => false,
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 15%'],
            'value' => function (Record $record) {
                $name = ArrayHelper::getValue($record, 'student.name');
                if (empty($name) === true) {
                    $name = 'Не указано';
                }

                $render = Html::a($name, ['students/preview', 'id' => ArrayHelper::getValue($record, 'student.id')], [
                    'class' => ['text-navy'],
                    'data' => ['show-modal' => 'myModal4']
                ]);
                return $render;
            }
        ],
        [
            'attribute' => false,
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 15%'],
            'value' => function (Record $record) {
                return ArrayHelper::getValue($record, 'student.email');
            }
        ],
        [
            'attribute' => false,
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 15%'],
            'value' => function (Record $record) {
                return ArrayHelper::getValue($record, 'student.social');
            }
        ],
        [
            'attribute' => false,
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 25%'],
            'value' => function (Record $record) {
                return ArrayHelper::getValue($record, 'student.abilities');
            }
        ],
    ],
]);