<?php

use backend\forms\record\SearchRecord;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $filter SearchRecord
 */

$form = ActiveForm::begin(['enableClientValidation' => false, 'method' => 'get', 'action' => ['record/list']]);

?>
    <div class="row">
        <div class="col-lg-6">
            <?php
            echo $form->field($filter, 'technology')->dropDownList($filter->listTechnology(), [
                'class' => ['select2', 'form-group', 'change_submit'],
                'prompt' => '- Выберите направление -',
            ]);
            ?>
        </div>
        <div class="col-lg-6">
            <?php
            if (empty($filter->technology) === false) {
                echo $form->field($filter, 'schedule_id')->dropDownList($filter->listSchedule(), [
                    'class' => ['select2'],
                    'prompt' => '- Выберите курс -',
                ]);
            }
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($filter, 'name')->textInput([
                'placeholder' => $filter->getAttributeLabel('name'),
                'autocomplete' => 'off',
            ])->label(false); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php
            echo $form->field($filter, 'listened')->checkbox(['class' => ['iCheck']]);
            echo $form->field($filter, 'completed')->checkbox(['class' => ['iCheck']]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= Html::submitInput('Применить фильтр', ['class' => ['btn', 'btn-primary', 'pull-right']]); ?>
        </div>
    </div>
<?php

ActiveForm::end();