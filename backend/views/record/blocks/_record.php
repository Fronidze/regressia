<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $record \backend\forms\record\CreateRecord
 */


$form = ActiveForm::begin();

echo $form->field($record, 'student_id')->dropDownList($record->listStudents(), [
    'class' => ['select2'],
    'prompt' => 'Выберите студента',
]);

echo $form->field($record, 'schedule_id')->dropDownList($record->listSchedule(), [
    'class' => ['select2'],
    'prompt' => 'Выберите курс'
]);

$config = [
    'template' => "
{label}\n
 <div class=\"input-group\">
 <span class=\"input-group-addon\">
 <i class=\"fa fa-calendar\"></i>
 </span> 
 {input}\n{hint}\n{error} 
 </div>",
];
echo $form->field($record, 'date_create', $config)->textInput([
    'class' => ['datepiker', 'form-control'],
    'autocomplete' => 'off',
]);

echo Html::beginTag('div', ['class' => ['form-group', 'text-right']]);
echo Html::submitInput('Записать на курс', ['class' => ['btn', 'btn-primary']]);
echo Html::endTag('div');

ActiveForm::end();