<?php


/**
 * @var $this View
 * @var $student_form CreateStudent
 * @var $record_form CreateRecord
 * @var $student_filter SearchStudent
 * @var $record_filter SearchRecord
 * @var $student_provider ActiveDataProvider
 * @var $record_provider ActiveDataProvider
 */

use backend\forms\document\RecordDownload;
use backend\forms\record\CreateRecord;
use backend\forms\record\SearchRecord;
use backend\forms\student\CreateStudent;
use backend\forms\student\SearchStudent;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Академия разработки Mediasoft: Студенты';
$this->params['links'][] = ['label' => 'Студенты', 'url' => null];

?>

<div class="row">

    <div class="col-sm-8">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Запись студента на курс.</h2>
                <p>
                    Записываем студента на определенный курс занятий. Если же у нас нету этого студента мы можем создать
                    его в форме создания нового студента
                </p>
                <div class="record_form">
                    <?= $this->render('blocks/_record', ['record' => $record_form]); ?>
                </div>
            </div>
        </div>

        <div class="ibox">
            <div class="ibox-content record_form">
                <h2>Фильтр по студентам</h2>
                <?php
                echo $this->render('blocks/_filter', ['filter' => $record_filter]);
                ?>
            </div>
        </div>
    </div>

    <div class="col-sm-4 student">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Новый студент</h2>
                <p>Форма создания нового студента</p>
                <div class="student_form">
                    <?= $this->render('blocks/_student', ['student' => $student_form]); ?>
                </div>
            </div>
        </div>

        <div class="widget navy-bg p-m text-center">
            <div class="m-b-md" style="margin-bottom: 0">
                <h1 class="m-xs">
                    <?php
                    $count_records = \services\statistics\RecordStatistics::instance()->countRecordByRecordPage();
                    echo $count_records;
                    ?>
                    <small style="color: white;">
                        <?php
                        function plural_form($number, $after)
                        {
                            $cases = array(2, 0, 1, 1, 1, 2);
                            echo $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
                        }

                        plural_form($count_records, ['запись', 'запись', 'записей']);
                        ?>
                    </small>
                </h1>
            </div>
        </div>

        <div class="widget navy-bg p-m text-center">
            <div class="m-b-md" style="margin-bottom: 0">
                <h1 class="m-xs">
                    <?php
                    $count_students = \services\statistics\StudentStatistics::instance()->countStudentByRecordPage();
                    echo $count_students;
                    ?>
                    <small style="color: white;">
                        <?php
                        plural_form($count_students, ['студент', 'студента', 'студента']);
                        ?>
                    </small>
                </h1>
            </div>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-sm-12">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Список студентов</h2>
                <?php
                $icon = Html::tag('i', null, ['class' => ['fa', 'fa-download']]);
                $icon_import = Html::tag('i', null, ['class' => ['fa', 'fa-upload']]);
                $url = [
                    'record/download',
                    (new RecordDownload())->formName() => ArrayHelper::getValue(\Yii::$app->request->get(), 'SearchRecord', []),
                ];
                echo Html::a($icon_import . ' Импорт', $url, [
                    'class' => ['btn', 'btn-primary', 'pull-right'],
                    'target' => '_blank',
                    'style' => 'position: absolute; top: 24px; right: 35px;'
                ]);

                echo Html::a($icon . ' Экспорт', ['import/index'], [
                    'class' => ['btn', 'btn-primary', 'pull-right'],
                    'style' => 'position: absolute; top: 24px; right: 125px;'
                ]);

                ?>
                <div class="record_form">
                    <?= $this->render('blocks/_list_student', [
                        'provider' => $record_provider,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>