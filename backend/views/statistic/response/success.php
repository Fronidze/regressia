<?php

use backend\forms\statistics\RegressionForm;
use entities\regression\RegressionResponse;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var $this View
 * @var $response RegressionResponse
 * @var $request_form RegressionForm
 */

?>

<p><h3>Корреляционная матрица</h3></p>
<style type="text/css">
    #table_correlation {
        margin: 20px 0;
        width: 550px;
    }

    #table_correlation th,
    #table_correlation td {
        text-align: center;
        padding: 5px;
    }

    #table_correlation th,
    #table_correlation td {
        border: 1px solid black;
    }

    #table_correlation tr td:first-child,
    #table_correlation th {
        text-align: center;
        font-weight: 600;
    }

    .regression-response p {
        margin: 0;
    }

    .predict_y span {
        margin: 0 5px;
    }

</style>
<table id="table_correlation">
    <?php
    $correlation = ArrayHelper::getValue($response->content, 'correlation.table', []);
    $correlation_keys = array_keys($correlation);
    array_unshift($correlation_keys, array_pop($correlation_keys));
    ?>

    <tr>
        <th></th>
        <?php foreach ($correlation_keys as $column): ?>
            <th><?= $column; ?></th>
        <?php endforeach; ?>
    </tr>

    <?php foreach ($correlation_keys as $row): ?>
        <tr>
            <td><?= $row; ?></td>
            <?php foreach ($correlation_keys as $column): ?>
                <td><?= ArrayHelper::getValue($correlation, "{$row}.{$column}"); ?></td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>

</table>

<p>Уровень значимости: <?= ArrayHelper::getValue($response->content, 'correlation.relevance_min_value') ?></p>

<?php
$correlated_params = ArrayHelper::getValue($response->content, 'correlation.correlated_params', []);
if (empty($correlated_params) === false) : ?>
    <p>Из матрицы следует, что между собой коррелируют параметры:</p>
    <?php foreach ($correlated_params as $correlated_param): ?>
        <p>
            <?= ArrayHelper::getValue($correlated_param, '0') ?>
            -->
            <?= ArrayHelper::getValue($correlated_param, '1') ?>:
            r <sub><?= ArrayHelper::getValue($correlated_param, '0') ?></sub>
            <sub><?= ArrayHelper::getValue($correlated_param, '1') ?></sub> =
            <?= ArrayHelper::getValue($correlated_param, '2') ?>
        </p>
    <?php endforeach; ?>
    <p>
        Из этого следует о необходимости исключения одного из факторов из дальнейшего анализа. <br>
        Для этого необходимо вернуться к исходным данным и исключить выбранный вами факторы, нажав над ними галочку.
    </p>
<?php else: ?>
    <p>Из матрицы следует, что коллинеарность между факторами отсутствует и нету основания исключать какой-либо фактор
        из рассмотрения</p>
<?php endif; ?>

<br>
<p><h3>Результаты регрессионого анализа</h3></p>
<style type="text/css">
    #table_estimate {
        margin: 20px 0;
        width: 550px;
    }

    #table_estimate th,
    #table_estimate td {
        padding: 5px;
        text-align: center;
    }

    #table_estimate th,
    #table_estimate td {
        border: 1px solid black;
    }

    #table_estimate tr td:first-child,
    #table_estimate th {
        text-align: center;
        font-weight: 600;
    }

</style>
<table id="table_estimate">

    <?php
    $result = ArrayHelper::getValue($response->content, 'result-table', []);
    $result_table = [];
    $labels = [
        'estimates' => 'коэффициенты',
        'std-error' => 'стандартная ошибка',
        'p-values' => 'p-значение',
        't-values' => 't-статистика',
    ];

    foreach ($labels as $code => $label) {
        $result_table[$code] = ArrayHelper::getValue($result, $code);
    }

    $cells = array_keys($result_table);
    $rows = array_keys($result_table['estimates']);
    array_unshift($rows, array_pop($rows));
    ?>

    <tr>
        <th></th>
        <?php foreach ($cells as $cell): ?>
            <th>
                <?= $labels[$cell]; ?>
            </th>
        <?php endforeach; ?>
    </tr>

    <?php foreach ($rows as $row): ?>
        <tr>
            <td><?= $row === 'y-zero' ? 'y-пересечение' : $row; ?></td>
            <?php foreach ($cells as $cell): ?>
                <td><?= ArrayHelper::getValue($result_table, "{$cell}.{$row}"); ?></td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>

</table>

<br>
<p>Коэффициент детерминации R<sup>2</sup>: <?= ArrayHelper::getValue($response->content, 'r_squared'); ?></p>
<p>Модифицированный коэффициент детерминации R<sup>2</sup>: <?= ArrayHelper::getValue($response->content,
        'adj_r_squared'); ?></p>

<br>
<?php
$significance_p = ArrayHelper::getValue($response->content, 'p-significance', []);
?>

<p>Проверим значимость коэффициентов в регрессионой модели. Применим t-критерий Стьюдента.</p>
<p>Статистически значимыми для модели являются параметры <?= implode(', ', $significance_p) ?>, т.к их p < 0.05</p>

<br>
<p>F<sub>факт</sub>: <?= ArrayHelper::getValue($response->content, 'f-stat-values.f-stat') ?></p>
<p>Уровень значимости: <?= ArrayHelper::getValue($response->content, 'f-stat-values.f-significance') ?></p>
<p>F<sub>крит</sub>: <?= ArrayHelper::getValue($response->content, 'f-stat-values.f-crit') ?></p>
<p>
    Для проверки значимости построенного уравнения регрессии должно выполняться следующее условие
    F<sub>факт</sub> > F<sub>крит</sub> <br>
    <?php
    $is_zero_hypothesis = (bool)ArrayHelper::getValue($response->content, 'f-stat-values.is_zero_hypothesis');
    ?>
    Следовательно, делаем вывод о <b><?= $is_zero_hypothesis ? 'не значимости' : 'значимости' ?></b> регрессионой
    модели
</p>

<br>
<p class="predict_y">
    Прогнозируемые значения <b>y</b> от текущей выборки параметров <b>x</b>:
    <?php
    $predicted_y = ArrayHelper::getValue($response->content, 'predicted_y', []);
    echo \Yii::t('app', '[ <b>{implode}</b> ]', [
        'implode' => implode('</b>, <b>', $predicted_y)
    ]);
    ?>
</p>
<p>Исходя из прогнозируемых значений <b>y</b> среднее абсолютное отклонение
    = <?= ArrayHelper::getValue($response->content, 'deviation'); ?></p>

<?php
$significances = ArrayHelper::getValue($response->content, 'p-significance', []);
$labels = $request_form->getColumnLabels();

$significance_title = [];
foreach ($significances as $significance) {
    array_push($significance_title, $labels[$significance]);
}

$result = ArrayHelper::getValue($response->content, 'result-table.estimates');
array_pop($result);
$max_key = array_search(max($result), $result);

?>

<br>
<p><h3>Заключение</h3></p>
<p>На успешность реализации прохождения обучения в большой степени будут влиять следующие факторы: </p>
<ul>
    <?php foreach ($significance_title as $title): ?>
    <li><?= $title?></li>
    <?php endforeach; ?>
</ul>

<p>Коэфициенты полученной регрессионной модели положительные, что говорит о прямой зависимости успешности реализации прохождения обучения от отобранных факторов.</p>
<p>Таким образом увеличив долю <?= $labels[$max_key]; ?> можно тем самым увеличить успешность реализации прохождения обучения на данной образовательной площадке.</p>
<p>Значение отклонения получилось близким к 0, что свидетельствует о высокой точности построенной модели.</p>