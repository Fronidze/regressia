<?php

use backend\forms\statistics\RegressionForm;
use entities\regression\RegressionResponse;
use yii\web\View;

/**
 * @var $this View
 * @var $response RegressionResponse
 * @var $request_form RegressionForm
 */

?>

<div class="ibox">

    <div class="ibox-title"><h2><?= $response->message?></h2></div>
    <div class="ibox-content regression-response">
        <?php
        if ($response->isSuccess() === true) {
            echo $this->render('response/success', [
                'response' => $response,
                'request_form' => $request_form,
            ]);
        }
        ?>
    </div>

</div>