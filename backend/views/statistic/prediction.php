<?php

use backend\forms\statistics\RegressionForm;
use entities\regression\RegressionResponse;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $response RegressionResponse
 * @var $service_response RegressionResponse
 * @var $request_form RegressionForm
 */

$params = ArrayHelper::getValue($response->content, 'params', []);
$labels = $request_form->getColumnLabels();

$result = ArrayHelper::getValue($service_response->content, 'result-table.estimates');
array_pop($result);
$max_key = array_search(max($result), $result);

?>
<style type="text/css">
    #placeholder_result_prediction {
        font-weight: 800;
    }
</style>

<div class="ibox">

    <div class="ibox-title"><h2>Моделирование</h2></div>
    <div class="ibox-content regression-response">
        <form action="<?= Url::to(['statistic/prediction'])?>" id="ajax_prediction" onsubmit="return false;">

            <?php foreach ($params as $param): ?>
            <div class="form-group">
                <label for=""><?= $labels[$param]?></label>
                <input class="form-control" type="text" name="prediction[<?= $param?>]" value="" title="">
            </div>
            <?php endforeach; ?>

            <input type="submit" value="Отправить" class="btn btn-primary regression_prediction_send">
        </form>
        <p style="margin-top: 20px">На основании построенной модели регресси оценка за выполненое итоговое задание студентов будет равна:
            <span id="placeholder_result_prediction">не рассчитано</span>
        </p>
        <p style="margin-top: 20px; display: none" id="hint_prediction">
            Если вы хотите увеличить успеваемость прохождения обучения, то следует увеличить следующий фактор: <?= $labels[$max_key]; ?>
        </p>
    </div>

</div>


