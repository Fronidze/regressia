<?php

use backend\forms\statistics\RegressionForm;
use entities\regression\RegressionResponse;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var $this View
 * @var $request_form RegressionForm
 * @var $technology array
 * @var $service_response RegressionResponse
 * @var $params_response RegressionResponse
 */

?>

<div class="row">
    <div class="col-md-12">
        <div class="ibox">

            <div class="ibox-title"><h2>Исходные данные</h2></div>
            <div class="ibox-content">

                <?php $form = ActiveForm::begin(); ?>

                <style type="text/css">

                    #form_regression .help-block,
                    #form_regression .form-group {
                        margin: 0;
                    }

                    #form_regression {
                        width: 100%;
                        table-layout: fixed;
                        margin: 20px 0;
                    }

                    #form_regression th,
                    #form_regression td {
                        width: 100%;
                        padding: 5px;
                    }

                    #form_regression th {
                        border-left: 1px solid #ccc;
                        text-align: center;
                    }

                    #form_regression th:nth-child(1),
                    #form_regression th:nth-child(2) {
                        border: none;
                    }

                    #form_regression tr td:first-child {
                        text-align: right;
                    }

                    .header_form_regression .form-group {
                        display: inline-block;
                    }

                </style>


                <table id="form_regression">
                    <?php

                    echo Html::beginTag('tr');
                    echo Html::tag('th', null);
                    foreach ($request_form->columns as $index => $column_label) {
                        if ($column_label === 'y') {
                            echo Html::tag('th', null);
                            continue;
                        }
                        echo Html::tag('th',
                            $form->field($request_form, "ignore_columns[{$column_label}]")
                                ->checkbox(['label' => 'Исключение фактора из РА'])
                                ->label(false)
                        );

                    }
                    echo Html::endTag('tr');

                    echo Html::beginTag('tr');
                    $column_labels = $request_form->getColumnLabels();
                    echo Html::tag('th', null);
                    foreach ($request_form->columns as $index => $column_label) {
                        echo Html::tag('th',
                            ArrayHelper::getValue($column_labels, $column_label) .
                            ", {$column_label} "
                        );
                    }
                    echo Html::endTag('tr');


                    foreach ($technology as $technology_id => $technology_label) {
                        echo Html::beginTag('tr');
                        echo Html::tag('td', $technology_label);
                        foreach ($request_form->columns as $index => $column_label) {
                            echo Html::beginTag('td');
                            echo $form->field($request_form, "rows[{$technology_id}][{$column_label}]")
                                ->textInput()
                                ->label(false);
                            echo Html::endTag('td');
                        }
                        echo Html::endTag('tr');
                    }

                    ?>
                </table>
                <?php

                echo $form->field($request_form, 'relevance_min_value');

                echo Html::submitButton('Отправить', ['class' => ['btn', 'btn-primary']]);
                ActiveForm::end();
                ?>

            </div>

        </div>
    </div>
</div>


<?php if ($service_response->content !== null): ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo $this->render('response', [
                'response' => $service_response,
                'request_form' => $request_form,
            ]);
            ?>
        </div>
    </div>
<?php endif; ?>

<?php if ($params_response->content !== null): ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo $this->render('prediction', [
                'response' => $params_response,
                'service_response' => $service_response,
                'request_form' => $request_form,
            ]);
            ?>
        </div>
    </div>
<?php endif; ?>


