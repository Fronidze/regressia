<?php
namespace backend\models;

use common\models\Files;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $position
 * @property string $social
 * @property int $image_id
 *
 * @property Files $imageFile
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLE = 'disable';

    /**
     * @return ActiveQuery
     */
    public function getImageFile()
    {
        return $this->hasOne(Files::class, ['id' => 'image_id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'datetime' => [
                'class' => TimestampBehavior::class,
                'value' => function () {
                    return (new \DateTime())
                        ->format('Y-m-d H:i:s');
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE]],
            [['position', 'social'], 'string'],
            [['image_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Индетификатор',
            'username' => 'Фамилия Имя',
            'password_hash' => 'Хэш',
            'email' => 'E-Mail',
            'status' => 'Статус',
            'created_at' => 'Дата созания',
            'updated_at' => 'Дата обновления',
            'password' => 'Пароль',
            'position' => 'Должность',
            'image_id' => 'Изображение',
            'social' => 'Ссылка на социальную сеть',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::find()
            ->where(['and', ['=', 'id', $id], ['=', 'status', static::STATUS_ACTIVE]])
            ->one();
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Поиск пользователя по Электронной почте
     *
     * @param string $email
     * @return User|null
     */
    public static function findByEmail(string $email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        throw new NotSupportedException('Реализация аутентификации через токен не реализована');
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        throw new NotSupportedException('Реализация аутентификации через токен не реализована');
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function isActive()
    {
        return $this->status === static::STATUS_ACTIVE;
    }
}
